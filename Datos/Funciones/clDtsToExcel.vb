﻿Imports System.Web.UI.WebControls
Imports System.Web

Public Class clDtsToExcel

    Public Shared Sub Convert(ByVal ds As DataSet, ByVal response As HttpResponse)
        'first let's clean up the response.object
        response.Clear()
        response.Buffer = True
        response.Charset = ""
        'set the response mime type for excel
        response.ContentType = "application/vnd.ms-excel"
        response.AddHeader("Content-Disposition", "attachment;filename=Arch.xls")


        'create a string writer
        Dim stringWrite As New System.IO.StringWriter
        'create an htmltextwriter which uses the stringwriter
        Dim htmlWrite As New System.Web.UI.HtmlTextWriter(stringWrite)
        'instantiate a datagrid
        Dim dg As New DataGrid
        'set the datagrid datasource to the dataset passed in
        dg.DataSource = ds.Tables(0)
        'bind the datagrid
        dg.DataBind()
        'tell the datagrid to render itself to our htmltextwriter
        dg.RenderControl(htmlWrite)
        'all that's left is to output the html
        response.Write(stringWrite.ToString)
        response.Flush()
        response.End()
    End Sub

    Public Shared Sub Convert(ByVal ds As DataSet, ByVal TableIndex As Integer, ByVal response As HttpResponse)
        'lets make sure a table actually exists at the passed in value
        'if it is not call the base method
        If TableIndex > ds.Tables.Count - 1 Then
            Convert(ds, response)
        End If
        'we've got a good table so
        'let's clean up the response.object
        response.Clear()
        response.Buffer = True

        response.Charset = ""
        'set the response mime type for excel
        response.ContentType = "application/vnd.ms-excel"
        response.AddHeader("Content-Disposition", "attachment;filename=Arch.xls")
        'create a string writer
        Dim stringWrite As New System.IO.StringWriter
        'create an htmltextwriter which uses the stringwriter
        Dim htmlWrite As New System.Web.UI.HtmlTextWriter(stringWrite)
        'instantiate a datagrid
        Dim dg As New DataGrid
        'set the datagrid datasource to the dataset passed in
        dg.DataSource = ds.Tables(TableIndex)
        'bind the datagrid
        dg.DataBind()
        'tell the datagrid to render itself to our htmltextwriter
        dg.RenderControl(htmlWrite)
        'all that's left is to output the html
        response.Write(stringWrite.ToString)
        response.Flush()

        response.End()
    End Sub

    Public Shared Sub Convert(ByVal ds As DataSet, ByVal TableName As String, ByVal response As HttpResponse)
        'let's make sure the table name exists
        'if it does not then call the default method
        If ds.Tables(TableName) Is Nothing Then
            Convert(ds, response)
        End If
        'we've got a good table so
        'let's clean up the response.object
        response.Clear()
        response.Buffer = True

        response.Charset = ""
        'set the response mime type for excel
        response.ContentType = "application/vnd.ms-excel"
        response.AddHeader("Content-Disposition", "attachment;filename=Arch.xls")
        'create a string writer
        Dim stringWrite As New System.IO.StringWriter
        'create an htmltextwriter which uses the stringwriter
        Dim htmlWrite As New System.Web.UI.HtmlTextWriter(stringWrite)
        'instantiate a datagrid
        Dim dg As New DataGrid
        'set the datagrid datasource to the dataset passed in
        dg.DataSource = ds.Tables(TableName)
        'bind the datagrid
        dg.DataBind()
        'tell the datagrid to render itself to our htmltextwriter
        dg.RenderControl(htmlWrite)
        'all that's left is to output the html
        response.Write(stringWrite.ToString)
        response.Flush()

        response.End()
    End Sub

    Public Shared Sub Convert(ByVal ds As DataSet, ByVal response As HttpResponse, ByVal parGvDatos As GridView)
        Dim gvDatos As New GridView
        For Each gvCol As DataControlField In parGvDatos.Columns
            gvDatos.Columns.Add(gvCol)
        Next
        gvDatos.AutoGenerateColumns = False

        response.Clear()
        response.Buffer = True
        response.Charset = ""
        response.ContentType = "application/vnd.ms-excel"
        response.AddHeader("Content-Disposition", "attachment;filename=Arch.xls")
        response.Charset = "UTF-8"
        response.ContentEncoding = System.Text.Encoding.Default
        Dim stringWrite As New System.IO.StringWriter
        Dim htmlWrite As New System.Web.UI.HtmlTextWriter(stringWrite)
        'Dim dg As New GridView
        gvDatos.DataSourceID = ""
        gvDatos.DataSource = ds.Tables(0)
        gvDatos.HeaderStyle.Font.Bold = True
        gvDatos.DataBind()
        gvDatos.RenderControl(htmlWrite)
        'all that's left is to output the html
        'response.Write(HttpUtility.HtmlDecode(stringWrite.ToString))
        response.Write(stringWrite.ToString)
        response.Flush()
        response.End()
    End Sub


    Public Shared Sub ConvertTitulos(ByVal ds As DataSet, ByVal response As HttpResponse, ByVal parGvDatos As GridView, Optional ByVal parIdPagina As Integer = 0, _
                              Optional ByVal parTituloRepor As String = "", Optional ByVal parFchInicio As String = "", Optional ByVal parFchFin As String = "", _
                              Optional ByVal parMensajero As String = "", Optional ByVal parFchGeneraRepor As String = "", Optional ByVal NomArch As String = "Arch", _
                              Optional ByVal objImagen As String = "", Optional ByVal objTituloRepor As String = "", Optional ByVal parPatinador As String = "", _
                              Optional ByVal parCabeceraRepor As String = "")

        Dim gvDatos As New GridView
        For Each gvCol As DataControlField In parGvDatos.Columns
            gvDatos.Columns.Add(gvCol)
        Next
        gvDatos.AutoGenerateColumns = False

        response.Clear()
        response.Buffer = True
        response.Charset = ""
        response.ContentType = "application/vnd.ms-excel"
        'response.AddHeader("Content-Disposition", "attachment;filename=Arch.xls")
        response.AddHeader("Content-Disposition", "attachment;filename=" & NomArch & ".xls")
        response.Charset = "UTF-8"
        response.ContentEncoding = System.Text.Encoding.Default
        Dim stringWrite As New System.IO.StringWriter



        Dim htmlWrite As New System.Web.UI.HtmlTextWriter(stringWrite)
        'Dim dg As New GridView
        gvDatos.DataSourceID = ""
        gvDatos.DataSource = ds.Tables(0)
        gvDatos.HeaderStyle.Font.Bold = True
        gvDatos.DataBind()
        gvDatos.RenderControl(htmlWrite)
        'all that's left is to output the html
        If (objImagen.Length <> 0) Then
            response.Write(HttpUtility.HtmlDecode("<img alt='left' src='" & objImagen & "' width='345' height='140'/>"))
            response.Write(HttpUtility.HtmlDecode("<br>"))
            response.Write(HttpUtility.HtmlDecode("<br>"))
        End If
        If (parCabeceraRepor <> "") Then
            response.Write(HttpUtility.HtmlDecode(parCabeceraRepor))
        End If
        'response.Write(HttpUtility.HtmlDecode(stringWrite.ToString))
        response.Write(stringWrite.ToString)
        response.Flush()
        response.End()
    End Sub


    Public Function getCabeceraRepor(ByVal parIdPagina As Integer, ByVal parTituloRepor As String, ByVal parFchGeneraRepor As String _
                                     ) As String
        Dim cabecera As New Text.StringBuilder
        If (parIdPagina = 1) Then
            cabecera.Append("<table>")
            cabecera.Append("<tr>")
            cabecera.Append("<td align='left' colspan='5'")
            cabecera.Append("<b>" & parTituloRepor & "</b>")
            cabecera.Append("</td>")
            cabecera.Append("<td align='left'>")
            cabecera.Append("<b>&nbsp;</b>")
            cabecera.Append("</td>")
            cabecera.Append("<td align='left'>")
            cabecera.Append("<b>&nbsp;</b>")
            cabecera.Append("</td>")
            cabecera.Append("<td align='left'>")
            cabecera.Append("<b>&nbsp;</b>")
            cabecera.Append("</td>")
            cabecera.Append("<td align='left'>")
            cabecera.Append("<b>&nbsp;</b>")
            cabecera.Append("</td>")
            cabecera.Append("<td align='left'>")
            cabecera.Append("<b>&nbsp;</b>")
            cabecera.Append("</td>")
            cabecera.Append("<td align='left'>")
            cabecera.Append("<b>&nbsp;</b>")
            cabecera.Append("</td>")
            cabecera.Append("<td align='right'  Width='500'>")
            cabecera.Append("<br><b> Fecha Creación </b> " & parFchGeneraRepor & "</b>")
            cabecera.Append("</td>")
            cabecera.Append("</tr>")
            cabecera.Append("<tr>")
            cabecera.Append("<td align='left' colspan='5'")
            cabecera.Append("<br><b>  </b> " & "<br>")
            cabecera.Append("</td>")
            cabecera.Append("<td align='left'>")
            cabecera.Append("<br><b>  </b> " & "<br>")
            cabecera.Append("</td>")
            cabecera.Append("</tr>")
            cabecera.Append("<tr>")
            cabecera.Append("<td align='left' colspan='5'")
            cabecera.Append("<br><b>  </b> " & "<br>")
            cabecera.Append("</td>")
            cabecera.Append("</tr>")
            cabecera.Append("<tr>")
            cabecera.Append("<td align='left'>")
            cabecera.Append("</td>")
            cabecera.Append("</tr>")
            cabecera.Append("</table>")
            cabecera.Append("<br>")
        End If
        Return cabecera.ToString
    End Function

    Public Shared Sub ExportaExcel(ByVal ds As DataSet, ByVal response As HttpResponse,
                                   ByVal parGvDatos As GridView, Optional ByVal NomArch As String = "Arch")

        Dim gvDatos As New GridView

        For Each gvCol As DataControlField In parGvDatos.Columns
            gvDatos.Columns.Add(gvCol)
        Next
        gvDatos.AutoGenerateColumns = False

        response.Clear()
        response.Buffer = True
        response.Charset = ""
        response.ContentType = "application/vnd.ms-excel"
        'response.AddHeader("Content-Disposition", "attachment;filename=Arch.xls")
        response.AddHeader("Content-Disposition", "attachment;filename=" & NomArch & ".xls")
        response.Charset = "UTF-8"
        response.ContentEncoding = System.Text.Encoding.Default
        Dim stringWrite As New System.IO.StringWriter



        Dim htmlWrite As New System.Web.UI.HtmlTextWriter(stringWrite)
        'Dim dg As New GridView
        gvDatos.DataSourceID = ""
        gvDatos.DataSource = ds.Tables(0)
        gvDatos.HeaderStyle.Font.Bold = True
        gvDatos.DataBind()
        gvDatos.RenderControl(htmlWrite)
        'all that's left is to output the html
        'If (objImagen.Length <> 0) Then
        '    response.Write(HttpUtility.HtmlDecode("<img alt='left' src='" & objImagen & "' width='345' height='140'/>"))
        '    response.Write(HttpUtility.HtmlDecode("<br>"))
        '    response.Write(HttpUtility.HtmlDecode("<br>"))
        'End If
        'If (parCabeceraRepor <> "") Then
        '    response.Write(HttpUtility.HtmlDecode(parCabeceraRepor))
        'End If
        response.Write(HttpUtility.HtmlDecode(stringWrite.ToString))
        response.Write(stringWrite.ToString)
        response.Flush()
        response.End()
    End Sub

End Class

﻿Imports System.Data
Imports System.Data.SqlClient
Imports System.Data.Common

Public Class spFunciones
    Inherits conexion

    Public Function executaSelect(ByVal Query As String) As DataSet
        cnn = conecta()
        Dim da As New SqlDataAdapter(Query, cnn)
        da.SelectCommand.CommandTimeout = 0
        da.SelectCommand.CommandType = CommandType.Text
        Dim ds As New DataSet()
        Try
            da.Fill(ds)
            da.Dispose()
            Return ds
        Catch ex As Exception
            MsgBox(ex.Message)
        Finally
            desconecta(cnn)
        End Try
        Return Nothing
    End Function


    Public Function executUpdate(ByVal query As String)

        Try
            cnn = conecta()
            cmd = New SqlCommand
            cmd.CommandType = CommandType.Text
            cmd.CommandText = query
            cmd.Connection = cnn

            If cmd.ExecuteNonQuery Then
                Return True
            Else
                Return False
            End If
        Catch e As Exception
            Return False
        Finally
            desconecta(cnn)
        End Try

    End Function

    Public Function consecutivoPRM(ByVal codpre As String, ByVal tipo As String) As mensaje
        Dim res As Integer = 0
        Dim message As New mensaje
        message.Codigo = 0
        Try
            cnn = conecta()

            cmd = New SqlCommand("CnsPrmPorTipo")

            cmd.CommandType = CommandType.StoredProcedure
            cmd.Connection = cnn
            Dim paramCodPre As DbParameter = cmd.CreateParameter()
            paramCodPre.Value = codpre
            paramCodPre.ParameterName = "codpre"
            cmd.Parameters.Add(paramCodPre)
            Dim paramTipo As DbParameter = cmd.CreateParameter()
            paramTipo.Value = tipo
            paramTipo.ParameterName = "tipo"
            cmd.Parameters.Add(paramTipo)

            Dim Dr As DbDataReader = cmd.ExecuteReader()
            While Dr.Read
                message.Valor = Dr(0)
            End While

        Catch ex As Exception
            MsgBox(ex.Message)
        End Try

        Return message
    End Function

    Public Function creaAgenda(ByVal codpre As String, ByVal nropme As String)
        Try
            cnn = conecta()

            cmd = New SqlCommand("creaAgenda")

            cmd.CommandType = CommandType.StoredProcedure
            cmd.Connection = cnn
            Dim paramCodPre As DbParameter = cmd.CreateParameter()
            paramCodPre.Value = codpre
            paramCodPre.ParameterName = "codpre"
            cmd.Parameters.Add(paramCodPre)
            Dim paramNroPme As DbParameter = cmd.CreateParameter()
            paramNroPme.Value = nropme
            paramNroPme.ParameterName = "nropme"
            cmd.Parameters.Add(paramNroPme)

            Dim Dr As DbDataReader = cmd.ExecuteReader()
            While Dr.Read

            End While

        Catch ex As Exception

        End Try
        Return True
    End Function

    Public Function guardarAgeDFB(ByVal id As Integer)
        Dim message As New mensaje
        Try
            cnn = conecta()
            cmd = New SqlCommand("cargaAgeDbf")

            cmd.CommandType = CommandType.StoredProcedure
            cmd.Connection = cnn
            Dim paramIdAge As DbParameter = cmd.CreateParameter()
            paramIdAge.Value = id
            paramIdAge.ParameterName = "id_age"
            cmd.Parameters.Add(paramIdAge)

            Dim Dr As DbDataReader = cmd.ExecuteReader()
            While Dr.Read

            End While

        Catch ex As Exception

        End Try
        Return message
    End Function

    Public Function ValidarAgendaMedico(APas As Datos.saih_apas) As mensaje
        Dim msn As New mensaje

        Try
            cnn = conecta()
            cmd = New SqlCommand("ValidarAgendaMedico")
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Connection = cnn

            Dim paramCodPas As DbParameter = cmd.CreateParameter
            paramCodPas.Value = APas.codpas
            paramCodPas.ParameterName = "codpas"

            Dim paramFechaIni As DbParameter = cmd.CreateParameter
            paramFechaIni.Value = APas.fecha_inicial
            paramFechaIni.ParameterName = "fecha_inicial"

            Dim paramFechaFin As DbParameter = cmd.CreateParameter
            paramFechaFin.Value = APas.fecha_final
            paramFechaFin.ParameterName = "fecha_final"

            Dim paramConsultorio As DbParameter = cmd.CreateParameter
            paramConsultorio.Value = APas.consultorio
            paramConsultorio.ParameterName = "consultorio"

            cmd.Parameters.Add(paramCodPas)
            cmd.Parameters.Add(paramFechaIni)
            cmd.Parameters.Add(paramFechaFin)
            cmd.Parameters.Add(paramConsultorio)
            Dim Dr As DbDataReader = cmd.ExecuteReader()
            While Dr.Read
                msn.Codigo = Dr("error")
                msn.Texto = Dr("mensaje")
            End While

        Catch ex As Exception
            msn.Codigo = "-1"
            msn.Texto = "Error Ejecutando Proceso, Error del sistema"
        End Try
        Return msn
    End Function



    Public Function ValidarAgendaMedicoChange(APas As Datos.saih_apas) As mensaje
        Dim msn As New mensaje

        Try
            cnn = conecta()
            cmd = New SqlCommand("ValidarAgendaMedicoChange")
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Connection = cnn

            Dim paramCodPas As DbParameter = cmd.CreateParameter
            paramCodPas.Value = APas.codpas
            paramCodPas.ParameterName = "codpas"

            Dim paramFechaIni As DbParameter = cmd.CreateParameter
            paramFechaIni.Value = APas.fecha_inicial
            paramFechaIni.ParameterName = "fecha_inicial"

            Dim paramFechaFin As DbParameter = cmd.CreateParameter
            paramFechaFin.Value = APas.fecha_final
            paramFechaFin.ParameterName = "fecha_final"

            Dim paramConsultorio As DbParameter = cmd.CreateParameter
            paramConsultorio.Value = APas.consultorio
            paramConsultorio.ParameterName = "consultorio"

            Dim paramCodpre As DbParameter = cmd.CreateParameter
            paramCodpre.Value = APas.codpre
            paramCodpre.ParameterName = "codpre"

            Dim paramNropme As DbParameter = cmd.CreateParameter
            paramNropme.Value = APas.nropme
            paramNropme.ParameterName = "nropme"

            cmd.Parameters.Add(paramCodPas)
            cmd.Parameters.Add(paramFechaIni)
            cmd.Parameters.Add(paramFechaFin)
            cmd.Parameters.Add(paramConsultorio)
            cmd.Parameters.Add(paramCodpre)
            cmd.Parameters.Add(paramNropme)
            Dim Dr As DbDataReader = cmd.ExecuteReader()
            While Dr.Read
                msn.Codigo = Dr("error")
                msn.Texto = Dr("mensaje")
            End While

        Catch ex As Exception
            msn.Codigo = "-1"
            msn.Texto = "Error Ejecutando Proceso, Error del sistema"
        End Try
        Return msn
    End Function
End Class

﻿Imports Ext.Net

Public Class StoreAutoComplete

    Public Function storeSaih_his(ByVal Cond As String) As Paging(Of saih_his)
        Dim pacientes As List(Of saih_his)
        Dim datoSaih As New Datos.Datosaih_his
        Dim qT_rec As String = ""
        If IsNumeric(Cond) Then
            pacientes = datoSaih.Select_Where_saih_his(" where identificacion like '%" & Cond & "%'")
        Else
            pacientes = datoSaih.Select_Where_saih_his(" where razonsocial like '%" & Cond & "%'")
        End If
        
        Return New Paging(Of saih_his)(pacientes, pacientes.Count)
    End Function

    Public Function storeSaih_pcd(ByVal Cond As String) As Paging(Of saih_pcd)
        Dim procedimientos As New List(Of saih_pcd)
        Dim datoPcd As New Datos.Datosaih_pcd
        If IsNumeric(Cond) Then
            procedimientos = datoPcd.Select_Where_saih_pcd(" where codigo like '%" & Cond & "%'")
        Else
            procedimientos = datoPcd.Select_Where_saih_pcd(" where nombre like '%" & Cond & "%'")
        End If
        
        Return New Paging(Of saih_pcd)(procedimientos, procedimientos.Count)
    End Function

End Class

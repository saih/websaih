﻿Imports System.Data
Imports System.Data.SqlClient
Imports System.Data.Common
Imports System.Web
Imports System.Configuration



Public Class conexion
    Public cnn As SqlConnection
    Public cmd As New SqlCommand
    Protected Function conecta() As SqlConnection
        Try
            cnn = New SqlConnection(System.Web.Configuration.WebConfigurationManager.ConnectionStrings("ConexionLocal").ConnectionString)
            cnn.Open()
            Return cnn
        Catch ex As Exception
            Throw
        End Try
    End Function

    Protected Function desconecta(ByVal cnn As SqlConnection)
        Try
            If cnn.State = ConnectionState.Open Then
                cnn.Close()
                Return True
            Else
                Return False
            End If
            Return True
        Catch ex As Exception
            Return False
        End Try
    End Function

    Protected Function ejecutaNonQuery(ByVal StoreProcedure As String, ByVal parametros As List(Of DbParameter))
        Dim Id As Integer = 0
        Dim appsettings = ConfigurationManager.AppSettings
        Dim result As String = appsettings("046")
        Try
            conecta()
            cmd = New SqlCommand(StoreProcedure)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Connection = cnn
            For Each Param As DbParameter In parametros
                cmd.Parameters.Add(Param)
            Next
            If cmd.ExecuteNonQuery Then
                Return True
            Else
                Return False
            End If
        Catch ex As Exception
            'MsgBox(ex.Message)
            Ext.Net.X.Msg.Alert("Información", result).Show()
            Return False
        Finally
            desconecta(cnn)
        End Try
    End Function

End Class

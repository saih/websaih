Public Class saih_hos
     Private _codent As string
     Private _tipdoc As string
     Private _nrodoc As string
     Private _nombre As string
     Private _direccion As string
     Private _telefono As string
     Private _logotipo As string
     Private _resdian As string
     Private _fecha_resolucion As string
     Private _num_inicial As integer
     Private _num_final As integer
     Private _num_factura As string
     Private _prefijo As string
     Private _regimen As string
     Private _gerente As string
     Private _subgerente As string
     Private _indtriage As integer
     Private _indimpformula As integer
     Private _indfechaglosa As integer

     Public Property codent() As string
        Get
            Return _codent
        End Get
        Set(ByVal value As string)
            _codent = value
        End Set
     End Property

     Public Property tipdoc() As string
        Get
            Return _tipdoc
        End Get
        Set(ByVal value As string)
            _tipdoc = value
        End Set
     End Property

     Public Property nrodoc() As string
        Get
            Return _nrodoc
        End Get
        Set(ByVal value As string)
            _nrodoc = value
        End Set
     End Property

     Public Property nombre() As string
        Get
            Return _nombre
        End Get
        Set(ByVal value As string)
            _nombre = value
        End Set
     End Property

     Public Property direccion() As string
        Get
            Return _direccion
        End Get
        Set(ByVal value As string)
            _direccion = value
        End Set
     End Property

     Public Property telefono() As string
        Get
            Return _telefono
        End Get
        Set(ByVal value As string)
            _telefono = value
        End Set
     End Property

     Public Property logotipo() As string
        Get
            Return _logotipo
        End Get
        Set(ByVal value As string)
            _logotipo = value
        End Set
     End Property

     Public Property resdian() As string
        Get
            Return _resdian
        End Get
        Set(ByVal value As string)
            _resdian = value
        End Set
     End Property

     Public Property fecha_resolucion() As string
        Get
            Return _fecha_resolucion
        End Get
        Set(ByVal value As string)
            _fecha_resolucion = value
        End Set
     End Property

     Public Property num_inicial() As integer
        Get
            Return _num_inicial
        End Get
        Set(ByVal value As integer)
            _num_inicial = value
        End Set
     End Property

     Public Property num_final() As integer
        Get
            Return _num_final
        End Get
        Set(ByVal value As integer)
            _num_final = value
        End Set
     End Property

     Public Property num_factura() As string
        Get
            Return _num_factura
        End Get
        Set(ByVal value As string)
            _num_factura = value
        End Set
     End Property

     Public Property prefijo() As string
        Get
            Return _prefijo
        End Get
        Set(ByVal value As string)
            _prefijo = value
        End Set
     End Property

     Public Property regimen() As string
        Get
            Return _regimen
        End Get
        Set(ByVal value As string)
            _regimen = value
        End Set
     End Property

     Public Property gerente() As string
        Get
            Return _gerente
        End Get
        Set(ByVal value As string)
            _gerente = value
        End Set
     End Property

     Public Property subgerente() As string
        Get
            Return _subgerente
        End Get
        Set(ByVal value As string)
            _subgerente = value
        End Set
     End Property

     Public Property indtriage() As integer
        Get
            Return _indtriage
        End Get
        Set(ByVal value As integer)
            _indtriage = value
        End Set
     End Property

     Public Property indimpformula() As integer
        Get
            Return _indimpformula
        End Get
        Set(ByVal value As integer)
            _indimpformula = value
        End Set
     End Property

     Public Property indfechaglosa() As integer
        Get
            Return _indfechaglosa
        End Get
        Set(ByVal value As integer)
            _indfechaglosa = value
        End Set
     End Property

    Public Sub New(
      ByVal codent As string,
      ByVal tipdoc As string,
      ByVal nrodoc As string,
      ByVal nombre As string,
      ByVal direccion As string,
      ByVal telefono As string,
      ByVal logotipo As string,
      ByVal resdian As string,
      ByVal fecha_resolucion As string,
      ByVal num_inicial As integer,
      ByVal num_final As integer,
      ByVal num_factura As string,
      ByVal prefijo As string,
      ByVal regimen As string,
      ByVal gerente As string,
      ByVal subgerente As string,
      ByVal indtriage As integer,
      ByVal indimpformula As integer,
      ByVal indfechaglosa As integer
    )
      Me.codent = codent
      Me.tipdoc = tipdoc
      Me.nrodoc = nrodoc
      Me.nombre = nombre
      Me.direccion = direccion
      Me.telefono = telefono
      Me.logotipo = logotipo
      Me.resdian = resdian
      Me.fecha_resolucion = fecha_resolucion
      Me.num_inicial = num_inicial
      Me.num_final = num_final
      Me.num_factura = num_factura
      Me.prefijo = prefijo
      Me.regimen = regimen
      Me.gerente = gerente
      Me.subgerente = subgerente
      Me.indtriage = indtriage
      Me.indimpformula = indimpformula
      Me.indfechaglosa = indfechaglosa
    End Sub

    Public Sub New()
    End Sub
End Class
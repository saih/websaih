Public Class saih_pas
     Private _codigo As string
     Private _identificacion As string
     Private _id_lugarexp As integer
     Private _num_registro_med As string
     Private _nombres As string
     Private _direccion As string
     Private _telefono As string
     Private _cod_clase As string
     Private _cod_especialidad As string
     Private _direccion_off As string
     Private _telefono_off As string
     Private _num_citas_dia As integer
     Private _estado As string
     Private _duracion As integer
     Private _foto As string
     Private _firma As string
     Private _huella As string

     Public Property codigo() As string
        Get
            Return _codigo
        End Get
        Set(ByVal value As string)
            _codigo = value
        End Set
     End Property

     Public Property identificacion() As string
        Get
            Return _identificacion
        End Get
        Set(ByVal value As string)
            _identificacion = value
        End Set
     End Property

     Public Property id_lugarexp() As integer
        Get
            Return _id_lugarexp
        End Get
        Set(ByVal value As integer)
            _id_lugarexp = value
        End Set
     End Property

     Public Property num_registro_med() As string
        Get
            Return _num_registro_med
        End Get
        Set(ByVal value As string)
            _num_registro_med = value
        End Set
     End Property

     Public Property nombres() As string
        Get
            Return _nombres
        End Get
        Set(ByVal value As string)
            _nombres = value
        End Set
     End Property

     Public Property direccion() As string
        Get
            Return _direccion
        End Get
        Set(ByVal value As string)
            _direccion = value
        End Set
     End Property

     Public Property telefono() As string
        Get
            Return _telefono
        End Get
        Set(ByVal value As string)
            _telefono = value
        End Set
     End Property

     Public Property cod_clase() As string
        Get
            Return _cod_clase
        End Get
        Set(ByVal value As string)
            _cod_clase = value
        End Set
     End Property

     Public Property cod_especialidad() As string
        Get
            Return _cod_especialidad
        End Get
        Set(ByVal value As string)
            _cod_especialidad = value
        End Set
     End Property

     Public Property direccion_off() As string
        Get
            Return _direccion_off
        End Get
        Set(ByVal value As string)
            _direccion_off = value
        End Set
     End Property

     Public Property telefono_off() As string
        Get
            Return _telefono_off
        End Get
        Set(ByVal value As string)
            _telefono_off = value
        End Set
     End Property

     Public Property num_citas_dia() As integer
        Get
            Return _num_citas_dia
        End Get
        Set(ByVal value As integer)
            _num_citas_dia = value
        End Set
     End Property

     Public Property estado() As string
        Get
            Return _estado
        End Get
        Set(ByVal value As string)
            _estado = value
        End Set
     End Property

     Public Property duracion() As integer
        Get
            Return _duracion
        End Get
        Set(ByVal value As integer)
            _duracion = value
        End Set
     End Property

     Public Property foto() As string
        Get
            Return _foto
        End Get
        Set(ByVal value As string)
            _foto = value
        End Set
     End Property

     Public Property firma() As string
        Get
            Return _firma
        End Get
        Set(ByVal value As string)
            _firma = value
        End Set
     End Property

     Public Property huella() As string
        Get
            Return _huella
        End Get
        Set(ByVal value As string)
            _huella = value
        End Set
     End Property

    Public Sub New(
      ByVal codigo As string,
      ByVal identificacion As string,
      ByVal id_lugarexp As integer,
      ByVal num_registro_med As string,
      ByVal nombres As string,
      ByVal direccion As string,
      ByVal telefono As string,
      ByVal cod_clase As string,
      ByVal cod_especialidad As string,
      ByVal direccion_off As string,
      ByVal telefono_off As string,
      ByVal num_citas_dia As integer,
      ByVal estado As string,
      ByVal duracion As integer,
      ByVal foto As string,
      ByVal firma As string,
      ByVal huella As string
    )
      Me.codigo = codigo
      Me.identificacion = identificacion
      Me.id_lugarexp = id_lugarexp
      Me.num_registro_med = num_registro_med
      Me.nombres = nombres
      Me.direccion = direccion
      Me.telefono = telefono
      Me.cod_clase = cod_clase
      Me.cod_especialidad = cod_especialidad
      Me.direccion_off = direccion_off
      Me.telefono_off = telefono_off
      Me.num_citas_dia = num_citas_dia
      Me.estado = estado
      Me.duracion = duracion
      Me.foto = foto
      Me.firma = firma
      Me.huella = huella
    End Sub

    Public Sub New()
    End Sub
End Class
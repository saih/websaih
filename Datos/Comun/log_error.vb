Public Class log_error
     Private _id As integer
     Private _tipo As string
     Private _descripcion As string
     Private _fecha As string
     Private _usuario As string

     Public Property id() As integer
        Get
            Return _id
        End Get
        Set(ByVal value As integer)
            _id = value
        End Set
     End Property

     Public Property tipo() As string
        Get
            Return _tipo
        End Get
        Set(ByVal value As string)
            _tipo = value
        End Set
     End Property

     Public Property descripcion() As string
        Get
            Return _descripcion
        End Get
        Set(ByVal value As string)
            _descripcion = value
        End Set
     End Property

     Public Property fecha() As string
        Get
            Return _fecha
        End Get
        Set(ByVal value As string)
            _fecha = value
        End Set
     End Property

     Public Property usuario() As string
        Get
            Return _usuario
        End Get
        Set(ByVal value As string)
            _usuario = value
        End Set
     End Property

    Public Sub New(
      ByVal id As integer,
      ByVal tipo As string,
      ByVal descripcion As string,
      ByVal fecha As string,
      ByVal usuario As string
    )
      Me.id = id
      Me.tipo = tipo
      Me.descripcion = descripcion
      Me.fecha = fecha
      Me.usuario = usuario
    End Sub

    Public Sub New()
    End Sub
End Class
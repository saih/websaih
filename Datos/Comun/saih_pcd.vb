Public Class saih_pcd
     Private _manual As string
     Private _codigo As string
     Private _nombre As string
     Private _ncomplejidad As string
     Private _cencosto As string
     Private _aplsex As string
     Private _apledadinicio As integer
     Private _apledadfin As integer
     Private _codgrupoq As string
     Private _valor As decimal
     Private _valsmv As decimal
     Private _porincremento As decimal
     Private _cobhonmed As integer
     Private _cobdersal As integer
     Private _cobmatins As integer
     Private _cobhonayu As integer
     Private _cobhonanes As integer
     Private _conhonper As integer
     Private _codunif As string
     Private _codfp As string
     Private _observaciones As string
     Private _codser As string

     Public Property manual() As string
        Get
            Return _manual
        End Get
        Set(ByVal value As string)
            _manual = value
        End Set
     End Property

     Public Property codigo() As string
        Get
            Return _codigo
        End Get
        Set(ByVal value As string)
            _codigo = value
        End Set
     End Property

     Public Property nombre() As string
        Get
            Return _nombre
        End Get
        Set(ByVal value As string)
            _nombre = value
        End Set
     End Property

     Public Property ncomplejidad() As string
        Get
            Return _ncomplejidad
        End Get
        Set(ByVal value As string)
            _ncomplejidad = value
        End Set
     End Property

     Public Property cencosto() As string
        Get
            Return _cencosto
        End Get
        Set(ByVal value As string)
            _cencosto = value
        End Set
     End Property

     Public Property aplsex() As string
        Get
            Return _aplsex
        End Get
        Set(ByVal value As string)
            _aplsex = value
        End Set
     End Property

     Public Property apledadinicio() As integer
        Get
            Return _apledadinicio
        End Get
        Set(ByVal value As integer)
            _apledadinicio = value
        End Set
     End Property

     Public Property apledadfin() As integer
        Get
            Return _apledadfin
        End Get
        Set(ByVal value As integer)
            _apledadfin = value
        End Set
     End Property

     Public Property codgrupoq() As string
        Get
            Return _codgrupoq
        End Get
        Set(ByVal value As string)
            _codgrupoq = value
        End Set
     End Property

     Public Property valor() As decimal
        Get
            Return _valor
        End Get
        Set(ByVal value As decimal)
            _valor = value
        End Set
     End Property

     Public Property valsmv() As decimal
        Get
            Return _valsmv
        End Get
        Set(ByVal value As decimal)
            _valsmv = value
        End Set
     End Property

     Public Property porincremento() As decimal
        Get
            Return _porincremento
        End Get
        Set(ByVal value As decimal)
            _porincremento = value
        End Set
     End Property

     Public Property cobhonmed() As integer
        Get
            Return _cobhonmed
        End Get
        Set(ByVal value As integer)
            _cobhonmed = value
        End Set
     End Property

     Public Property cobdersal() As integer
        Get
            Return _cobdersal
        End Get
        Set(ByVal value As integer)
            _cobdersal = value
        End Set
     End Property

     Public Property cobmatins() As integer
        Get
            Return _cobmatins
        End Get
        Set(ByVal value As integer)
            _cobmatins = value
        End Set
     End Property

     Public Property cobhonayu() As integer
        Get
            Return _cobhonayu
        End Get
        Set(ByVal value As integer)
            _cobhonayu = value
        End Set
     End Property

     Public Property cobhonanes() As integer
        Get
            Return _cobhonanes
        End Get
        Set(ByVal value As integer)
            _cobhonanes = value
        End Set
     End Property

     Public Property conhonper() As integer
        Get
            Return _conhonper
        End Get
        Set(ByVal value As integer)
            _conhonper = value
        End Set
     End Property

     Public Property codunif() As string
        Get
            Return _codunif
        End Get
        Set(ByVal value As string)
            _codunif = value
        End Set
     End Property

     Public Property codfp() As string
        Get
            Return _codfp
        End Get
        Set(ByVal value As string)
            _codfp = value
        End Set
     End Property

     Public Property observaciones() As string
        Get
            Return _observaciones
        End Get
        Set(ByVal value As string)
            _observaciones = value
        End Set
     End Property

     Public Property codser() As string
        Get
            Return _codser
        End Get
        Set(ByVal value As string)
            _codser = value
        End Set
     End Property

    Public Sub New(
      ByVal manual As string,
      ByVal codigo As string,
      ByVal nombre As string,
      ByVal ncomplejidad As string,
      ByVal cencosto As string,
      ByVal aplsex As string,
      ByVal apledadinicio As integer,
      ByVal apledadfin As integer,
      ByVal codgrupoq As string,
      ByVal valor As decimal,
      ByVal valsmv As decimal,
      ByVal porincremento As decimal,
      ByVal cobhonmed As integer,
      ByVal cobdersal As integer,
      ByVal cobmatins As integer,
      ByVal cobhonayu As integer,
      ByVal cobhonanes As integer,
      ByVal conhonper As integer,
      ByVal codunif As string,
      ByVal codfp As string,
      ByVal observaciones As string,
      ByVal codser As string
    )
      Me.manual = manual
      Me.codigo = codigo
      Me.nombre = nombre
      Me.ncomplejidad = ncomplejidad
      Me.cencosto = cencosto
      Me.aplsex = aplsex
      Me.apledadinicio = apledadinicio
      Me.apledadfin = apledadfin
      Me.codgrupoq = codgrupoq
      Me.valor = valor
      Me.valsmv = valsmv
      Me.porincremento = porincremento
      Me.cobhonmed = cobhonmed
      Me.cobdersal = cobdersal
      Me.cobmatins = cobmatins
      Me.cobhonayu = cobhonayu
      Me.cobhonanes = cobhonanes
      Me.conhonper = conhonper
      Me.codunif = codunif
      Me.codfp = codfp
      Me.observaciones = observaciones
      Me.codser = codser
    End Sub

    Public Sub New()
    End Sub
End Class
Public Class saih_dfes
     Private _id As integer
     Private _dia As string
     Private _descripcion As string

     Public Property id() As integer
        Get
            Return _id
        End Get
        Set(ByVal value As integer)
            _id = value
        End Set
     End Property

     Public Property dia() As string
        Get
            Return _dia
        End Get
        Set(ByVal value As string)
            _dia = value
        End Set
     End Property

     Public Property descripcion() As string
        Get
            Return _descripcion
        End Get
        Set(ByVal value As string)
            _descripcion = value
        End Set
     End Property

    Public Sub New(
      ByVal id As integer,
      ByVal dia As string,
      ByVal descripcion As string
    )
      Me.id = id
      Me.dia = dia
      Me.descripcion = descripcion
    End Sub

    Public Sub New()
    End Sub
End Class
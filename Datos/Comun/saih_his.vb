Public Class saih_his
    Private _numhis As String
    Private _identificacion As String
    Private _tipo_identificacion As String
    Private _primer_nombre As String
    Private _segundo_nombre As String
    Private _primer_apellido As String
    Private _segundo_apellido As String
    Private _fecha_nacimiento As String
    Private _id_lugar As Integer
    Private _unidad_medida As String
    Private _sexo As String
    Private _estado_civil As String
    Private _direccion As String
    Private _telefono As String
    Private _telefonoaux As String
    Private _id_lugar_municipio As Integer
    Private _id_lugar_barrio As Integer
    Private _zona_residencia As String
    Private _estrato As String
    Private _codemp As String
    Private _codarp As String
    Private _codccf As String
    Private _apellido_conyuge As String
    Private _nombre_conyuge As String
    Private _nombre_padre As String
    Private _nombre_madre As String
    Private _usuario_creo As String
    Private _fecha_creacion As String
    Private _usuario_modifico As String
    Private _fecha_modificacion As String
    Private _observacion As String
    Private _nrocon As String
    Private _ubica As String
    Private _factor_hr As String
    Private _fecha_muerte As String
    Private _url_foto As String
    Private _etnia As String
    Private _huella As String
    Private _nivel_escolar As String
    Private _responsable_economico As String
    Private _religion As String
    Private _tipo_residencia As String
    Private _escolar As String
    Private _razonsocial As String
    Private _estatura As Decimal
    Private _observaciones As String
    Private _codocu As String
    Private _discapacidad As String

    Public Property numhis() As String
        Get
            Return _numhis
        End Get
        Set(ByVal value As String)
            _numhis = value
        End Set
    End Property

    Public Property identificacion() As String
        Get
            Return _identificacion
        End Get
        Set(ByVal value As String)
            _identificacion = value
        End Set
    End Property

    Public Property tipo_identificacion() As String
        Get
            Return _tipo_identificacion
        End Get
        Set(ByVal value As String)
            _tipo_identificacion = value
        End Set
    End Property

    Public Property primer_nombre() As String
        Get
            Return _primer_nombre
        End Get
        Set(ByVal value As String)
            _primer_nombre = value
        End Set
    End Property

    Public Property segundo_nombre() As String
        Get
            Return _segundo_nombre
        End Get
        Set(ByVal value As String)
            _segundo_nombre = value
        End Set
    End Property

    Public Property primer_apellido() As String
        Get
            Return _primer_apellido
        End Get
        Set(ByVal value As String)
            _primer_apellido = value
        End Set
    End Property

    Public Property segundo_apellido() As String
        Get
            Return _segundo_apellido
        End Get
        Set(ByVal value As String)
            _segundo_apellido = value
        End Set
    End Property

    Public Property fecha_nacimiento() As String
        Get
            Return _fecha_nacimiento
        End Get
        Set(ByVal value As String)
            _fecha_nacimiento = value
        End Set
    End Property

    Public Property id_lugar() As Integer
        Get
            Return _id_lugar
        End Get
        Set(ByVal value As Integer)
            _id_lugar = value
        End Set
    End Property

    Public Property unidad_medida() As String
        Get
            Return _unidad_medida
        End Get
        Set(ByVal value As String)
            _unidad_medida = value
        End Set
    End Property

    Public Property sexo() As String
        Get
            Return _sexo
        End Get
        Set(ByVal value As String)
            _sexo = value
        End Set
    End Property

    Public Property estado_civil() As String
        Get
            Return _estado_civil
        End Get
        Set(ByVal value As String)
            _estado_civil = value
        End Set
    End Property

    Public Property direccion() As String
        Get
            Return _direccion
        End Get
        Set(ByVal value As String)
            _direccion = value
        End Set
    End Property

    Public Property telefono() As String
        Get
            Return _telefono
        End Get
        Set(ByVal value As String)
            _telefono = value
        End Set
    End Property
    Public Property telefonoaux() As String
        Get
            Return _telefonoaux
        End Get
        Set(ByVal value As String)
            _telefonoaux = value
        End Set
    End Property
    Public Property id_lugar_municipio() As Integer
        Get
            Return _id_lugar_municipio
        End Get
        Set(ByVal value As Integer)
            _id_lugar_municipio = value
        End Set
    End Property

    Public Property id_lugar_barrio() As Integer
        Get
            Return _id_lugar_barrio
        End Get
        Set(ByVal value As Integer)
            _id_lugar_barrio = value
        End Set
    End Property

    Public Property zona_residencia() As String
        Get
            Return _zona_residencia
        End Get
        Set(ByVal value As String)
            _zona_residencia = value
        End Set
    End Property

    Public Property estrato() As String
        Get
            Return _estrato
        End Get
        Set(ByVal value As String)
            _estrato = value
        End Set
    End Property

    Public Property codemp() As String
        Get
            Return _codemp
        End Get
        Set(ByVal value As String)
            _codemp = value
        End Set
    End Property

    Public Property codarp() As String
        Get
            Return _codarp
        End Get
        Set(ByVal value As String)
            _codarp = value
        End Set
    End Property

    Public Property codccf() As String
        Get
            Return _codccf
        End Get
        Set(ByVal value As String)
            _codccf = value
        End Set
    End Property

    Public Property apellido_conyuge() As String
        Get
            Return _apellido_conyuge
        End Get
        Set(ByVal value As String)
            _apellido_conyuge = value
        End Set
    End Property

    Public Property nombre_conyuge() As String
        Get
            Return _nombre_conyuge
        End Get
        Set(ByVal value As String)
            _nombre_conyuge = value
        End Set
    End Property

    Public Property nombre_padre() As String
        Get
            Return _nombre_padre
        End Get
        Set(ByVal value As String)
            _nombre_padre = value
        End Set
    End Property

    Public Property nombre_madre() As String
        Get
            Return _nombre_madre
        End Get
        Set(ByVal value As String)
            _nombre_madre = value
        End Set
    End Property

    Public Property usuario_creo() As String
        Get
            Return _usuario_creo
        End Get
        Set(ByVal value As String)
            _usuario_creo = value
        End Set
    End Property

    Public Property fecha_creacion() As String
        Get
            Return _fecha_creacion
        End Get
        Set(ByVal value As String)
            _fecha_creacion = value
        End Set
    End Property

    Public Property usuario_modifico() As String
        Get
            Return _usuario_modifico
        End Get
        Set(ByVal value As String)
            _usuario_modifico = value
        End Set
    End Property

    Public Property fecha_modificacion() As String
        Get
            Return _fecha_modificacion
        End Get
        Set(ByVal value As String)
            _fecha_modificacion = value
        End Set
    End Property

    Public Property observacion() As String
        Get
            Return _observacion
        End Get
        Set(ByVal value As String)
            _observacion = value
        End Set
    End Property

    Public Property nrocon() As String
        Get
            Return _nrocon
        End Get
        Set(ByVal value As String)
            _nrocon = value
        End Set
    End Property

    Public Property ubica() As String
        Get
            Return _ubica
        End Get
        Set(ByVal value As String)
            _ubica = value
        End Set
    End Property

    Public Property factor_hr() As String
        Get
            Return _factor_hr
        End Get
        Set(ByVal value As String)
            _factor_hr = value
        End Set
    End Property

    Public Property fecha_muerte() As String
        Get
            Return _fecha_muerte
        End Get
        Set(ByVal value As String)
            _fecha_muerte = value
        End Set
    End Property

    Public Property url_foto() As String
        Get
            Return _url_foto
        End Get
        Set(ByVal value As String)
            _url_foto = value
        End Set
    End Property

    Public Property etnia() As String
        Get
            Return _etnia
        End Get
        Set(ByVal value As String)
            _etnia = value
        End Set
    End Property

    Public Property huella() As String
        Get
            Return _huella
        End Get
        Set(ByVal value As String)
            _huella = value
        End Set
    End Property

    Public Property nivel_escolar() As String
        Get
            Return _nivel_escolar
        End Get
        Set(ByVal value As String)
            _nivel_escolar = value
        End Set
    End Property

    Public Property responsable_economico() As String
        Get
            Return _responsable_economico
        End Get
        Set(ByVal value As String)
            _responsable_economico = value
        End Set
    End Property

    Public Property religion() As String
        Get
            Return _religion
        End Get
        Set(ByVal value As String)
            _religion = value
        End Set
    End Property

    Public Property tipo_residencia() As String
        Get
            Return _tipo_residencia
        End Get
        Set(ByVal value As String)
            _tipo_residencia = value
        End Set
    End Property

    Public Property escolar() As String
        Get
            Return _escolar
        End Get
        Set(ByVal value As String)
            _escolar = value
        End Set
    End Property

    Public Property razonsocial() As String
        Get
            Return _razonsocial
        End Get
        Set(ByVal value As String)
            _razonsocial = value
        End Set
    End Property

    Public Property estatura() As Decimal
        Get
            Return _estatura
        End Get
        Set(ByVal value As Decimal)
            _estatura = value
        End Set
    End Property

    Public Property observaciones() As String
        Get
            Return _observaciones
        End Get
        Set(ByVal value As String)
            _observaciones = value
        End Set
    End Property

    Public Property codocu() As String
        Get
            Return _codocu
        End Get
        Set(ByVal value As String)
            _codocu = value
        End Set
    End Property
    Public Property discapacidad() As String
        Get
            Return _discapacidad
        End Get
        Set(ByVal value As String)
            _discapacidad = value
        End Set
    End Property
    Public Sub New(
      ByVal numhis As String,
      ByVal identificacion As String,
      ByVal tipo_identificacion As String,
      ByVal primer_nombre As String,
      ByVal segundo_nombre As String,
      ByVal primer_apellido As String,
      ByVal segundo_apellido As String,
      ByVal fecha_nacimiento As String,
      ByVal id_lugar As Integer,
      ByVal unidad_medida As String,
      ByVal sexo As String,
      ByVal estado_civil As String,
      ByVal direccion As String,
      ByVal telefono As String,
      ByVal telefonoaux As String,
      ByVal id_lugar_municipio As Integer,
      ByVal id_lugar_barrio As Integer,
      ByVal zona_residencia As String,
      ByVal estrato As String,
      ByVal codemp As String,
      ByVal codarp As String,
      ByVal codccf As String,
      ByVal apellido_conyuge As String,
      ByVal nombre_conyuge As String,
      ByVal nombre_padre As String,
      ByVal nombre_madre As String,
      ByVal usuario_creo As String,
      ByVal fecha_creacion As String,
      ByVal usuario_modifico As String,
      ByVal fecha_modificacion As String,
      ByVal observacion As String,
      ByVal nrocon As String,
      ByVal ubica As String,
      ByVal factor_hr As String,
      ByVal fecha_muerte As String,
      ByVal url_foto As String,
      ByVal etnia As String,
      ByVal huella As String,
      ByVal nivel_escolar As String,
      ByVal responsable_economico As String,
      ByVal religion As String,
      ByVal tipo_residencia As String,
      ByVal escolar As String,
      ByVal razonsocial As String,
      ByVal estatura As Decimal,
      ByVal observaciones As String,
      ByVal codocu As String,
      ByVal discapacidad As String
    )
        Me.numhis = numhis
        Me.identificacion = identificacion
        Me.tipo_identificacion = tipo_identificacion
        Me.primer_nombre = primer_nombre
        Me.segundo_nombre = segundo_nombre
        Me.primer_apellido = primer_apellido
        Me.segundo_apellido = segundo_apellido
        Me.fecha_nacimiento = fecha_nacimiento
        Me.id_lugar = id_lugar
        Me.unidad_medida = unidad_medida
        Me.sexo = sexo
        Me.estado_civil = estado_civil
        Me.direccion = direccion
        Me.telefono = telefono
        Me.telefonoaux = telefonoaux
        Me.id_lugar_municipio = id_lugar_municipio
        Me.id_lugar_barrio = id_lugar_barrio
        Me.zona_residencia = zona_residencia
        Me.estrato = estrato
        Me.codemp = codemp
        Me.codarp = codarp
        Me.codccf = codccf
        Me.apellido_conyuge = apellido_conyuge
        Me.nombre_conyuge = nombre_conyuge
        Me.nombre_padre = nombre_padre
        Me.nombre_madre = nombre_madre
        Me.usuario_creo = usuario_creo
        Me.fecha_creacion = fecha_creacion
        Me.usuario_modifico = usuario_modifico
        Me.fecha_modificacion = fecha_modificacion
        Me.observacion = observacion
        Me.nrocon = nrocon
        Me.ubica = ubica
        Me.factor_hr = factor_hr
        Me.fecha_muerte = fecha_muerte
        Me.url_foto = url_foto
        Me.etnia = etnia
        Me.huella = huella
        Me.nivel_escolar = nivel_escolar
        Me.responsable_economico = responsable_economico
        Me.religion = religion
        Me.tipo_residencia = tipo_residencia
        Me.escolar = escolar
        Me.razonsocial = razonsocial
        Me.estatura = estatura
        Me.observaciones = observaciones
        Me.codocu = codocu
        Me.discapacidad = discapacidad
    End Sub

    Public Sub New()
    End Sub
End Class
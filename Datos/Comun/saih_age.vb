Public Class saih_age
     Private _id As integer
     Private _codpre As string
     Private _nroage As string
     Private _fecha_adicion As string
     Private _fecha_modificacion As string
     Private _usuario_creo As string
     Private _usuario_modifico As string
     Private _fecha_cita As string
     Private _numhis As string
     Private _codemp As string
     Private _codpas As string
     Private _codpcd As string
     Private _codser As string
     Private _consultorio As string
     Private _estado As string
     Private _cod_cancelacion As string
     Private _es_nueva As integer
     Private _observaciones As string
     Private _fecha_fincita As string
     Private _nropme As string
     Private _telefono As string
     Private _manual As string

     Public Property id() As integer
        Get
            Return _id
        End Get
        Set(ByVal value As integer)
            _id = value
        End Set
     End Property

     Public Property codpre() As string
        Get
            Return _codpre
        End Get
        Set(ByVal value As string)
            _codpre = value
        End Set
     End Property

     Public Property nroage() As string
        Get
            Return _nroage
        End Get
        Set(ByVal value As string)
            _nroage = value
        End Set
     End Property

     Public Property fecha_adicion() As string
        Get
            Return _fecha_adicion
        End Get
        Set(ByVal value As string)
            _fecha_adicion = value
        End Set
     End Property

     Public Property fecha_modificacion() As string
        Get
            Return _fecha_modificacion
        End Get
        Set(ByVal value As string)
            _fecha_modificacion = value
        End Set
     End Property

     Public Property usuario_creo() As string
        Get
            Return _usuario_creo
        End Get
        Set(ByVal value As string)
            _usuario_creo = value
        End Set
     End Property

     Public Property usuario_modifico() As string
        Get
            Return _usuario_modifico
        End Get
        Set(ByVal value As string)
            _usuario_modifico = value
        End Set
     End Property

     Public Property fecha_cita() As string
        Get
            Return _fecha_cita
        End Get
        Set(ByVal value As string)
            _fecha_cita = value
        End Set
     End Property

     Public Property numhis() As string
        Get
            Return _numhis
        End Get
        Set(ByVal value As string)
            _numhis = value
        End Set
     End Property

     Public Property codemp() As string
        Get
            Return _codemp
        End Get
        Set(ByVal value As string)
            _codemp = value
        End Set
     End Property

     Public Property codpas() As string
        Get
            Return _codpas
        End Get
        Set(ByVal value As string)
            _codpas = value
        End Set
     End Property

     Public Property codpcd() As string
        Get
            Return _codpcd
        End Get
        Set(ByVal value As string)
            _codpcd = value
        End Set
     End Property

     Public Property codser() As string
        Get
            Return _codser
        End Get
        Set(ByVal value As string)
            _codser = value
        End Set
     End Property

     Public Property consultorio() As string
        Get
            Return _consultorio
        End Get
        Set(ByVal value As string)
            _consultorio = value
        End Set
     End Property

     Public Property estado() As string
        Get
            Return _estado
        End Get
        Set(ByVal value As string)
            _estado = value
        End Set
     End Property

     Public Property cod_cancelacion() As string
        Get
            Return _cod_cancelacion
        End Get
        Set(ByVal value As string)
            _cod_cancelacion = value
        End Set
     End Property

     Public Property es_nueva() As integer
        Get
            Return _es_nueva
        End Get
        Set(ByVal value As integer)
            _es_nueva = value
        End Set
     End Property

     Public Property observaciones() As string
        Get
            Return _observaciones
        End Get
        Set(ByVal value As string)
            _observaciones = value
        End Set
     End Property

     Public Property fecha_fincita() As string
        Get
            Return _fecha_fincita
        End Get
        Set(ByVal value As string)
            _fecha_fincita = value
        End Set
     End Property

     Public Property nropme() As string
        Get
            Return _nropme
        End Get
        Set(ByVal value As string)
            _nropme = value
        End Set
     End Property

     Public Property telefono() As string
        Get
            Return _telefono
        End Get
        Set(ByVal value As string)
            _telefono = value
        End Set
     End Property

     Public Property manual() As string
        Get
            Return _manual
        End Get
        Set(ByVal value As string)
            _manual = value
        End Set
     End Property

    Public Sub New(
      ByVal id As integer,
      ByVal codpre As string,
      ByVal nroage As string,
      ByVal fecha_adicion As string,
      ByVal fecha_modificacion As string,
      ByVal usuario_creo As string,
      ByVal usuario_modifico As string,
      ByVal fecha_cita As string,
      ByVal numhis As string,
      ByVal codemp As string,
      ByVal codpas As string,
      ByVal codpcd As string,
      ByVal codser As string,
      ByVal consultorio As string,
      ByVal estado As string,
      ByVal cod_cancelacion As string,
      ByVal es_nueva As integer,
      ByVal observaciones As string,
      ByVal fecha_fincita As string,
      ByVal nropme As string,
      ByVal telefono As string,
      ByVal manual As string
    )
      Me.id = id
      Me.codpre = codpre
      Me.nroage = nroage
      Me.fecha_adicion = fecha_adicion
      Me.fecha_modificacion = fecha_modificacion
      Me.usuario_creo = usuario_creo
      Me.usuario_modifico = usuario_modifico
      Me.fecha_cita = fecha_cita
      Me.numhis = numhis
      Me.codemp = codemp
      Me.codpas = codpas
      Me.codpcd = codpcd
      Me.codser = codser
      Me.consultorio = consultorio
      Me.estado = estado
      Me.cod_cancelacion = cod_cancelacion
      Me.es_nueva = es_nueva
      Me.observaciones = observaciones
      Me.fecha_fincita = fecha_fincita
      Me.nropme = nropme
      Me.telefono = telefono
      Me.manual = manual
    End Sub

    Public Sub New()
    End Sub
End Class
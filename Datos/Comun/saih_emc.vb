Public Class saih_emc
     Private _codemp As string
     Private _num_contrato As string
     Private _nombre As string
     Private _fecha As string
     Private _fecha_inicial As string
     Private _fecha_final As string
     Private _monto As decimal
     Private _estado As string
     Private _num_afiliados As integer
     Private _forma_pago As string
     Private _val_ejecutado As decimal

     Public Property codemp() As string
        Get
            Return _codemp
        End Get
        Set(ByVal value As string)
            _codemp = value
        End Set
     End Property

     Public Property num_contrato() As string
        Get
            Return _num_contrato
        End Get
        Set(ByVal value As string)
            _num_contrato = value
        End Set
     End Property

     Public Property nombre() As string
        Get
            Return _nombre
        End Get
        Set(ByVal value As string)
            _nombre = value
        End Set
     End Property

     Public Property fecha() As string
        Get
            Return _fecha
        End Get
        Set(ByVal value As string)
            _fecha = value
        End Set
     End Property

     Public Property fecha_inicial() As string
        Get
            Return _fecha_inicial
        End Get
        Set(ByVal value As string)
            _fecha_inicial = value
        End Set
     End Property

     Public Property fecha_final() As string
        Get
            Return _fecha_final
        End Get
        Set(ByVal value As string)
            _fecha_final = value
        End Set
     End Property

     Public Property monto() As decimal
        Get
            Return _monto
        End Get
        Set(ByVal value As decimal)
            _monto = value
        End Set
     End Property

     Public Property estado() As string
        Get
            Return _estado
        End Get
        Set(ByVal value As string)
            _estado = value
        End Set
     End Property

     Public Property num_afiliados() As integer
        Get
            Return _num_afiliados
        End Get
        Set(ByVal value As integer)
            _num_afiliados = value
        End Set
     End Property

     Public Property forma_pago() As string
        Get
            Return _forma_pago
        End Get
        Set(ByVal value As string)
            _forma_pago = value
        End Set
     End Property

     Public Property val_ejecutado() As decimal
        Get
            Return _val_ejecutado
        End Get
        Set(ByVal value As decimal)
            _val_ejecutado = value
        End Set
     End Property

    Public Sub New(
      ByVal codemp As string,
      ByVal num_contrato As string,
      ByVal nombre As string,
      ByVal fecha As string,
      ByVal fecha_inicial As string,
      ByVal fecha_final As string,
      ByVal monto As decimal,
      ByVal estado As string,
      ByVal num_afiliados As integer,
      ByVal forma_pago As string,
      ByVal val_ejecutado As decimal
    )
      Me.codemp = codemp
      Me.num_contrato = num_contrato
      Me.nombre = nombre
      Me.fecha = fecha
      Me.fecha_inicial = fecha_inicial
      Me.fecha_final = fecha_final
      Me.monto = monto
      Me.estado = estado
      Me.num_afiliados = num_afiliados
      Me.forma_pago = forma_pago
      Me.val_ejecutado = val_ejecutado
    End Sub

    Public Sub New()
    End Sub
End Class
Public Class usuario_log
     Private _id As integer
     Private _Usuario As string
     Private _fecha As string
     Private _ip As string
     Private _host As string
     Private _mac As string
     Private _tipo As string

     Public Property id() As integer
        Get
            Return _id
        End Get
        Set(ByVal value As integer)
            _id = value
        End Set
     End Property

     Public Property Usuario() As string
        Get
            Return _Usuario
        End Get
        Set(ByVal value As string)
            _Usuario = value
        End Set
     End Property

     Public Property fecha() As string
        Get
            Return _fecha
        End Get
        Set(ByVal value As string)
            _fecha = value
        End Set
     End Property

     Public Property ip() As string
        Get
            Return _ip
        End Get
        Set(ByVal value As string)
            _ip = value
        End Set
     End Property

     Public Property host() As string
        Get
            Return _host
        End Get
        Set(ByVal value As string)
            _host = value
        End Set
     End Property

     Public Property mac() As string
        Get
            Return _mac
        End Get
        Set(ByVal value As string)
            _mac = value
        End Set
     End Property

     Public Property tipo() As string
        Get
            Return _tipo
        End Get
        Set(ByVal value As string)
            _tipo = value
        End Set
     End Property

    Public Sub New(
      ByVal id As integer,
      ByVal Usuario As string,
      ByVal fecha As string,
      ByVal ip As string,
      ByVal host As string,
      ByVal mac As string,
      ByVal tipo As string
    )
      Me.id = id
      Me.Usuario = Usuario
      Me.fecha = fecha
      Me.ip = ip
      Me.host = host
      Me.mac = mac
      Me.tipo = tipo
    End Sub

    Public Sub New()
    End Sub
End Class
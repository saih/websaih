Public Class saih_tab
     Private _dominio As string
     Private _codigo As string
     Private _nombre As string
     Private _valor As string
     Private _csc As integer

     Public Property dominio() As string
        Get
            Return _dominio
        End Get
        Set(ByVal value As string)
            _dominio = value
        End Set
     End Property

     Public Property codigo() As string
        Get
            Return _codigo
        End Get
        Set(ByVal value As string)
            _codigo = value
        End Set
     End Property

     Public Property nombre() As string
        Get
            Return _nombre
        End Get
        Set(ByVal value As string)
            _nombre = value
        End Set
     End Property

     Public Property valor() As string
        Get
            Return _valor
        End Get
        Set(ByVal value As string)
            _valor = value
        End Set
     End Property

     Public Property csc() As integer
        Get
            Return _csc
        End Get
        Set(ByVal value As integer)
            _csc = value
        End Set
     End Property

    Public Sub New(
      ByVal dominio As string,
      ByVal codigo As string,
      ByVal nombre As string,
      ByVal valor As string,
      ByVal csc As integer
    )
      Me.dominio = dominio
      Me.codigo = codigo
      Me.nombre = nombre
      Me.valor = valor
      Me.csc = csc
    End Sub

    Public Sub New()
    End Sub
End Class
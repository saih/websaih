﻿Public Class UsuarioSede
    Private _UsuarioSede As Integer
    Private _idUsuario As String
    Private _idSede As String


    Public Property UsuarioSede() As Integer
        Get
            Return _UsuarioSede
        End Get
        Set(ByVal value As Integer)
            _UsuarioSede = value
        End Set
    End Property

    Public Property idUsuario() As String
        Get
            Return _idUsuario
        End Get
        Set(ByVal value As String)
            _idUsuario = value
        End Set
    End Property

    Public Property idSede() As String
        Get
            Return _idSede
        End Get
        Set(ByVal value As String)
            _idSede = value
        End Set
    End Property

    Public Sub New(
      ByVal UsuarioSede As Integer,
      ByVal idUsuario As String,
      ByVal idSede As String
    )
        Me.UsuarioSede = UsuarioSede
        Me.idUsuario = idUsuario
        Me.idSede = idSede
    End Sub

    Public Sub New()
    End Sub
End Class
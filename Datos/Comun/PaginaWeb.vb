Public Class PaginaWeb
     Private _id As integer
     Private _nombre As string

     Public Property id() As integer
        Get
            Return _id
        End Get
        Set(ByVal value As integer)
            _id = value
        End Set
     End Property

     Public Property nombre() As string
        Get
            Return _nombre
        End Get
        Set(ByVal value As string)
            _nombre = value
        End Set
     End Property

    Public Sub New(
      ByVal id As integer,
      ByVal nombre As string
    )
      Me.id = id
      Me.nombre = nombre
    End Sub

    Public Sub New()
    End Sub
End Class
Public Class familia_his
     Private _id_familia As integer
     Private _numhis As string
     Private _rol As string
     Private _fecha_adicion As string
     Private _usuario_creo As string
     Private _fecha_modificacion As string
     Private _usuario_modifico As string

     Public Property id_familia() As integer
        Get
            Return _id_familia
        End Get
        Set(ByVal value As integer)
            _id_familia = value
        End Set
     End Property

     Public Property numhis() As string
        Get
            Return _numhis
        End Get
        Set(ByVal value As string)
            _numhis = value
        End Set
     End Property

     Public Property rol() As string
        Get
            Return _rol
        End Get
        Set(ByVal value As string)
            _rol = value
        End Set
     End Property

     Public Property fecha_adicion() As string
        Get
            Return _fecha_adicion
        End Get
        Set(ByVal value As string)
            _fecha_adicion = value
        End Set
     End Property

     Public Property usuario_creo() As string
        Get
            Return _usuario_creo
        End Get
        Set(ByVal value As string)
            _usuario_creo = value
        End Set
     End Property

     Public Property fecha_modificacion() As string
        Get
            Return _fecha_modificacion
        End Get
        Set(ByVal value As string)
            _fecha_modificacion = value
        End Set
     End Property

     Public Property usuario_modifico() As string
        Get
            Return _usuario_modifico
        End Get
        Set(ByVal value As string)
            _usuario_modifico = value
        End Set
     End Property

    Public Sub New(
      ByVal id_familia As integer,
      ByVal numhis As string,
      ByVal rol As string,
      ByVal fecha_adicion As string,
      ByVal usuario_creo As string,
      ByVal fecha_modificacion As string,
      ByVal usuario_modifico As string
    )
      Me.id_familia = id_familia
      Me.numhis = numhis
      Me.rol = rol
      Me.fecha_adicion = fecha_adicion
      Me.usuario_creo = usuario_creo
      Me.fecha_modificacion = fecha_modificacion
      Me.usuario_modifico = usuario_modifico
    End Sub

    Public Sub New()
    End Sub
End Class
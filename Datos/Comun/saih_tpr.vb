Public Class saih_tpr
     Private _codpro As string
     Private _manual As string
     Private _id As integer
     Private _id_plan As string
     Private _codgruq As string
     Private _valor As decimal
     Private _valsmv As decimal
     Private _porincremento As decimal

     Public Property codpro() As string
        Get
            Return _codpro
        End Get
        Set(ByVal value As string)
            _codpro = value
        End Set
     End Property

     Public Property manual() As string
        Get
            Return _manual
        End Get
        Set(ByVal value As string)
            _manual = value
        End Set
     End Property

     Public Property id() As integer
        Get
            Return _id
        End Get
        Set(ByVal value As integer)
            _id = value
        End Set
     End Property

     Public Property id_plan() As string
        Get
            Return _id_plan
        End Get
        Set(ByVal value As string)
            _id_plan = value
        End Set
     End Property

     Public Property codgruq() As string
        Get
            Return _codgruq
        End Get
        Set(ByVal value As string)
            _codgruq = value
        End Set
     End Property

     Public Property valor() As decimal
        Get
            Return _valor
        End Get
        Set(ByVal value As decimal)
            _valor = value
        End Set
     End Property

     Public Property valsmv() As decimal
        Get
            Return _valsmv
        End Get
        Set(ByVal value As decimal)
            _valsmv = value
        End Set
     End Property

     Public Property porincremento() As decimal
        Get
            Return _porincremento
        End Get
        Set(ByVal value As decimal)
            _porincremento = value
        End Set
     End Property

    Public Sub New(
      ByVal codpro As string,
      ByVal manual As string,
      ByVal id As integer,
      ByVal id_plan As string,
      ByVal codgruq As string,
      ByVal valor As decimal,
      ByVal valsmv As decimal,
      ByVal porincremento As decimal
    )
      Me.codpro = codpro
      Me.manual = manual
      Me.id = id
      Me.id_plan = id_plan
      Me.codgruq = codgruq
      Me.valor = valor
      Me.valsmv = valsmv
      Me.porincremento = porincremento
    End Sub

    Public Sub New()
    End Sub
End Class
Public Class PaginaWebControl
     Private _id As integer
     Private _id_PaginaWeb As integer
     Private _nombre As string
     Private _tipo As string
     Private _descripcion As string

     Public Property id() As integer
        Get
            Return _id
        End Get
        Set(ByVal value As integer)
            _id = value
        End Set
     End Property

     Public Property id_PaginaWeb() As integer
        Get
            Return _id_PaginaWeb
        End Get
        Set(ByVal value As integer)
            _id_PaginaWeb = value
        End Set
     End Property

     Public Property nombre() As string
        Get
            Return _nombre
        End Get
        Set(ByVal value As string)
            _nombre = value
        End Set
     End Property

     Public Property tipo() As string
        Get
            Return _tipo
        End Get
        Set(ByVal value As string)
            _tipo = value
        End Set
     End Property

     Public Property descripcion() As string
        Get
            Return _descripcion
        End Get
        Set(ByVal value As string)
            _descripcion = value
        End Set
     End Property

    Public Sub New(
      ByVal id As integer,
      ByVal id_PaginaWeb As integer,
      ByVal nombre As string,
      ByVal tipo As string,
      ByVal descripcion As string
    )
      Me.id = id
      Me.id_PaginaWeb = id_PaginaWeb
      Me.nombre = nombre
      Me.tipo = tipo
      Me.descripcion = descripcion
    End Sub

    Public Sub New()
    End Sub
End Class
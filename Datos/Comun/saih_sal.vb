Public Class saih_sal
     Private _id As integer
     Private _id_prm As integer
     Private _fecha_inicio As string
     Private _fecha_fin As string
     Private _estado As string
     Private _salario As decimal
     Private _observaciones As string

     Public Property id() As integer
        Get
            Return _id
        End Get
        Set(ByVal value As integer)
            _id = value
        End Set
     End Property

     Public Property id_prm() As integer
        Get
            Return _id_prm
        End Get
        Set(ByVal value As integer)
            _id_prm = value
        End Set
     End Property

     Public Property fecha_inicio() As string
        Get
            Return _fecha_inicio
        End Get
        Set(ByVal value As string)
            _fecha_inicio = value
        End Set
     End Property

     Public Property fecha_fin() As string
        Get
            Return _fecha_fin
        End Get
        Set(ByVal value As string)
            _fecha_fin = value
        End Set
     End Property

     Public Property estado() As string
        Get
            Return _estado
        End Get
        Set(ByVal value As string)
            _estado = value
        End Set
     End Property

     Public Property salario() As decimal
        Get
            Return _salario
        End Get
        Set(ByVal value As decimal)
            _salario = value
        End Set
     End Property

     Public Property observaciones() As string
        Get
            Return _observaciones
        End Get
        Set(ByVal value As string)
            _observaciones = value
        End Set
     End Property

    Public Sub New(
      ByVal id As integer,
      ByVal id_prm As integer,
      ByVal fecha_inicio As string,
      ByVal fecha_fin As string,
      ByVal estado As string,
      ByVal salario As decimal,
      ByVal observaciones As string
    )
      Me.id = id
      Me.id_prm = id_prm
      Me.fecha_inicio = fecha_inicio
      Me.fecha_fin = fecha_fin
      Me.estado = estado
      Me.salario = salario
      Me.observaciones = observaciones
    End Sub

    Public Sub New()
    End Sub
End Class
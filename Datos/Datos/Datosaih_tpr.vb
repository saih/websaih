Imports System.Data
Imports System.Data.SqlClient
Imports System.Data.Common
Imports System.Configuration

Public Class Datosaih_tpr
   Inherits conexion

  Public Function Insertar(ByVal saih_tpr As saih_tpr)
      Dim parametros As New List(Of DbParameter)
      Dim paramcodpro As DbParameter = cmd.CreateParameter()
      paramcodpro.Value = saih_tpr.codpro
      paramcodpro.ParameterName = "codpro"
      parametros.Add(paramcodpro)
      Dim parammanual As DbParameter = cmd.CreateParameter()
      parammanual.Value = saih_tpr.manual
      parammanual.ParameterName = "manual"
      parametros.Add(parammanual)
      Dim paramid_plan As DbParameter = cmd.CreateParameter()
      paramid_plan.Value = saih_tpr.id_plan
      paramid_plan.ParameterName = "id_plan"
      parametros.Add(paramid_plan)
      Dim paramcodgruq As DbParameter = cmd.CreateParameter()
      paramcodgruq.Value = saih_tpr.codgruq
      paramcodgruq.ParameterName = "codgruq"
      parametros.Add(paramcodgruq)
      Dim paramvalor As DbParameter = cmd.CreateParameter()
      paramvalor.Value = saih_tpr.valor
      paramvalor.ParameterName = "valor"
      parametros.Add(paramvalor)
      Dim paramvalsmv As DbParameter = cmd.CreateParameter()
      paramvalsmv.Value = saih_tpr.valsmv
      paramvalsmv.ParameterName = "valsmv"
      parametros.Add(paramvalsmv)
      Dim paramporincremento As DbParameter = cmd.CreateParameter()
      paramporincremento.Value = saih_tpr.porincremento
      paramporincremento.ParameterName = "porincremento"
      parametros.Add(paramporincremento)
      Return ejecutaNonQuery("insertasaih_tpr", parametros)
  End Function

  Public Function InsertarRet(ByVal saih_tpr As saih_tpr)
      Dim dato As Integer = 0
        Dim appSettings = ConfigurationManager.AppSettings
        Dim result As String = appSettings("010")

        Try
            cnn = conecta()
            cmd = New SqlCommand("insertasaih_tpr")
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Connection = cnn
            Dim paramcodpro As DbParameter = cmd.CreateParameter()
            paramcodpro.Value = saih_tpr.codpro
            paramcodpro.ParameterName = "codpro"
            cmd.Parameters.Add(paramcodpro)
            Dim parammanual As DbParameter = cmd.CreateParameter()
            parammanual.Value = saih_tpr.manual
            parammanual.ParameterName = "manual"
            cmd.Parameters.Add(parammanual)
            Dim paramid_plan As DbParameter = cmd.CreateParameter()
            paramid_plan.Value = saih_tpr.id_plan
            paramid_plan.ParameterName = "id_plan"
            cmd.Parameters.Add(paramid_plan)
            Dim paramcodgruq As DbParameter = cmd.CreateParameter()
            paramcodgruq.Value = saih_tpr.codgruq
            paramcodgruq.ParameterName = "codgruq"
            cmd.Parameters.Add(paramcodgruq)
            Dim paramvalor As DbParameter = cmd.CreateParameter()
            paramvalor.Value = saih_tpr.valor
            paramvalor.ParameterName = "valor"
            cmd.Parameters.Add(paramvalor)
            Dim paramvalsmv As DbParameter = cmd.CreateParameter()
            paramvalsmv.Value = saih_tpr.valsmv
            paramvalsmv.ParameterName = "valsmv"
            cmd.Parameters.Add(paramvalsmv)
            Dim paramporincremento As DbParameter = cmd.CreateParameter()
            paramporincremento.Value = saih_tpr.porincremento
            paramporincremento.ParameterName = "porincremento"
            cmd.Parameters.Add(paramporincremento)
            Dim Dr As DbDataReader = cmd.ExecuteReader()
            While Dr.Read
                dato = Dr(0)
            End While
        Catch ex As Exception
            Ext.Net.X.Msg.Alert("Información", result).Show()
        Finally
            desconecta(cnn)
        End Try
      Return dato
  End Function

  Public Function Editar(ByVal saih_tpr As saih_tpr)
      Dim parametros As New List(Of DbParameter)
      Dim paramcodpro As DbParameter = cmd.CreateParameter()
      paramcodpro.Value = saih_tpr.codpro
      paramcodpro.ParameterName = "codpro"
      parametros.Add(paramcodpro)
      Dim parammanual As DbParameter = cmd.CreateParameter()
      parammanual.Value = saih_tpr.manual
      parammanual.ParameterName = "manual"
      parametros.Add(parammanual)
      Dim paramid As DbParameter = cmd.CreateParameter()
      paramid.Value = saih_tpr.id
      paramid.ParameterName = "id"
      parametros.Add(paramid)
      Dim paramid_plan As DbParameter = cmd.CreateParameter()
      paramid_plan.Value = saih_tpr.id_plan
      paramid_plan.ParameterName = "id_plan"
      parametros.Add(paramid_plan)
      Dim paramcodgruq As DbParameter = cmd.CreateParameter()
      paramcodgruq.Value = saih_tpr.codgruq
      paramcodgruq.ParameterName = "codgruq"
      parametros.Add(paramcodgruq)
      Dim paramvalor As DbParameter = cmd.CreateParameter()
      paramvalor.Value = saih_tpr.valor
      paramvalor.ParameterName = "valor"
      parametros.Add(paramvalor)
      Dim paramvalsmv As DbParameter = cmd.CreateParameter()
      paramvalsmv.Value = saih_tpr.valsmv
      paramvalsmv.ParameterName = "valsmv"
      parametros.Add(paramvalsmv)
      Dim paramporincremento As DbParameter = cmd.CreateParameter()
      paramporincremento.Value = saih_tpr.porincremento
      paramporincremento.ParameterName = "porincremento"
      parametros.Add(paramporincremento)
      Return ejecutaNonQuery("cambiarsaih_tpr", parametros)
  End Function

  Public Function Borrar(
      ByVal id As integer)
      Dim parametros As New List(Of DbParameter)
      Dim paramid As DbParameter = cmd.CreateParameter()
      paramid.Value = id
      paramid.ParameterName = "id"
      parametros.Add(paramid)
      Return ejecutaNonQuery("eliminasaih_tpr", parametros)
  End Function

  Public Function Select_Where_saih_tpr(ByVal Cond As String) As List(Of saih_tpr)
        Dim Listsaih_tpr As New List(Of saih_tpr)
        Dim appSettings = ConfigurationManager.AppSettings
        Dim result As String = appSettings("011")

      Try
          cnn = conecta()
          cmd = New SqlCommand("obtenersaih_tprByWhere")
          cmd.CommandType = CommandType.StoredProcedure
          cmd.Connection = cnn
          Dim param As DbParameter = cmd.CreateParameter()
          param.Value = Cond
          param.ParameterName = "where"
          cmd.Parameters.Add(param)
          Dim Dr As DbDataReader = cmd.ExecuteReader()
          While Dr.Read
              Listsaih_tpr.Add(New saih_tpr(
                  Dr("codpro"),
                  Dr("manual"),
                  Dr("id"),
                  Dr("id_plan"),
                  Dr("codgruq"),
                  Dr("valor"),
                  Dr("valsmv"),
                  Dr("porincremento")))
          End While
      Catch ex As Exception
            Ext.Net.X.Msg.Alert("Información", result).Show()
      Finally
          desconecta(cnn)
      End Try
      Return Listsaih_tpr
  End Function

  Public Function Select_ById_saih_tpr(
      ByVal id As integer) As List(Of saih_tpr)
        Dim Listsaih_tpr As New List(Of saih_tpr)
        Dim appSettings = ConfigurationManager.AppSettings
        Dim result As String = appSettings("011")

      Try
          cnn = conecta()
          cmd = New SqlCommand("obtenersaih_tprById")
          cmd.CommandType = CommandType.StoredProcedure
          cmd.Connection = cnn
      Dim paramid As DbParameter = cmd.CreateParameter()
      paramid.Value = id
      paramid.ParameterName = "id"
      cmd.Parameters.Add(paramid)
          Dim Dr As DbDataReader = cmd.ExecuteReader()
          While Dr.Read
              Listsaih_tpr.Add(New saih_tpr(
                  Dr("codpro"),
                  Dr("manual"),
                  Dr("id"),
                  Dr("id_plan"),
                  Dr("codgruq"),
                  Dr("valor"),
                  Dr("valsmv"),
                  Dr("porincremento")))
          End While
      Catch ex As Exception
            Ext.Net.X.Msg.Alert("Información", result).Show()
      Finally
          desconecta(cnn)
      End Try
      Return Listsaih_tpr
  End Function

  Public Function Select_saih_tpr() As List(Of saih_tpr)
        Dim Listsaih_tpr As New List(Of saih_tpr)
        Dim appSettings = ConfigurationManager.AppSettings
        Dim result As String = appSettings("011")

      Try
         cnn = conecta()
          cmd = New SqlCommand("obtenersaih_tpr")
          cmd.CommandType = CommandType.StoredProcedure
          cmd.Connection = cnn
          Dim Dr As DbDataReader = cmd.ExecuteReader()
          While Dr.Read
              Listsaih_tpr.Add(New saih_tpr(
                  Dr("codpro"),
                  Dr("manual"),
                  Dr("id"),
                  Dr("id_plan"),
                  Dr("codgruq"),
                  Dr("valor"),
                  Dr("valsmv"),
                  Dr("porincremento")))
          End While
      Catch ex As Exception
            Ext.Net.X.Msg.Alert("Información", result).Show()
      Finally
          desconecta(cnn)
      End Try
      Return Listsaih_tpr
  End Function

End Class
Imports System.Data
Imports System.Data.SqlClient
Imports System.Data.Common

Public Class Datosaih_apas
   Inherits conexion

  Public Function Insertar(ByVal saih_apas As saih_apas)
      Dim parametros As New List(Of DbParameter)
      Dim paramcodpre As DbParameter = cmd.CreateParameter()
      paramcodpre.Value = saih_apas.codpre
      paramcodpre.ParameterName = "codpre"
      parametros.Add(paramcodpre)
      Dim paramnropme As DbParameter = cmd.CreateParameter()
      paramnropme.Value = saih_apas.nropme
      paramnropme.ParameterName = "nropme"
      parametros.Add(paramnropme)
      Dim paramcodpas As DbParameter = cmd.CreateParameter()
      paramcodpas.Value = saih_apas.codpas
      paramcodpas.ParameterName = "codpas"
      parametros.Add(paramcodpas)
      Dim paramfecha_adicion As DbParameter = cmd.CreateParameter()
      paramfecha_adicion.Value = saih_apas.fecha_adicion
      paramfecha_adicion.ParameterName = "fecha_adicion"
      parametros.Add(paramfecha_adicion)
      Dim paramfecha_modificacion As DbParameter = cmd.CreateParameter()
      paramfecha_modificacion.Value = saih_apas.fecha_modificacion
      paramfecha_modificacion.ParameterName = "fecha_modificacion"
      parametros.Add(paramfecha_modificacion)
      Dim paramusuario_creo As DbParameter = cmd.CreateParameter()
      paramusuario_creo.Value = saih_apas.usuario_creo
      paramusuario_creo.ParameterName = "usuario_creo"
      parametros.Add(paramusuario_creo)
      Dim paramusuario_modifico As DbParameter = cmd.CreateParameter()
      paramusuario_modifico.Value = saih_apas.usuario_modifico
      paramusuario_modifico.ParameterName = "usuario_modifico"
      parametros.Add(paramusuario_modifico)
      Dim paramfecha_inicial As DbParameter = cmd.CreateParameter()
      paramfecha_inicial.Value = saih_apas.fecha_inicial
      paramfecha_inicial.ParameterName = "fecha_inicial"
      parametros.Add(paramfecha_inicial)
      Dim paramfecha_final As DbParameter = cmd.CreateParameter()
      paramfecha_final.Value = saih_apas.fecha_final
      paramfecha_final.ParameterName = "fecha_final"
      parametros.Add(paramfecha_final)
      Dim paramconsultorio As DbParameter = cmd.CreateParameter()
      paramconsultorio.Value = saih_apas.consultorio
      paramconsultorio.ParameterName = "consultorio"
      parametros.Add(paramconsultorio)
      Dim paramintervalo As DbParameter = cmd.CreateParameter()
      paramintervalo.Value = saih_apas.intervalo
      paramintervalo.ParameterName = "intervalo"
      parametros.Add(paramintervalo)
      Dim paramcodser As DbParameter = cmd.CreateParameter()
      paramcodser.Value = saih_apas.codser
      paramcodser.ParameterName = "codser"
      parametros.Add(paramcodser)
      Dim paramestado As DbParameter = cmd.CreateParameter()
      paramestado.Value = saih_apas.estado
      paramestado.ParameterName = "estado"
      parametros.Add(paramestado)
      Dim parammotivo_cancelacion As DbParameter = cmd.CreateParameter()
      parammotivo_cancelacion.Value = saih_apas.motivo_cancelacion
      parammotivo_cancelacion.ParameterName = "motivo_cancelacion"
      parametros.Add(parammotivo_cancelacion)
      Return ejecutaNonQuery("insertasaih_apas", parametros)
  End Function

  Public Function InsertarRet(ByVal saih_apas As saih_apas)
      Dim dato As Integer = 0
      Try
          cnn = conecta()
          cmd = New SqlCommand("insertasaih_apas")
          cmd.CommandType = CommandType.StoredProcedure
          cmd.Connection = cnn
        Dim paramcodpre As DbParameter = cmd.CreateParameter()
        paramcodpre.Value = saih_apas.codpre
        paramcodpre.ParameterName = "codpre"
        cmd.Parameters.Add(paramcodpre)
        Dim paramnropme As DbParameter = cmd.CreateParameter()
        paramnropme.Value = saih_apas.nropme
        paramnropme.ParameterName = "nropme"
        cmd.Parameters.Add(paramnropme)
        Dim paramcodpas As DbParameter = cmd.CreateParameter()
        paramcodpas.Value = saih_apas.codpas
        paramcodpas.ParameterName = "codpas"
        cmd.Parameters.Add(paramcodpas)
        Dim paramfecha_adicion As DbParameter = cmd.CreateParameter()
        paramfecha_adicion.Value = saih_apas.fecha_adicion
        paramfecha_adicion.ParameterName = "fecha_adicion"
        cmd.Parameters.Add(paramfecha_adicion)
        Dim paramfecha_modificacion As DbParameter = cmd.CreateParameter()
        paramfecha_modificacion.Value = saih_apas.fecha_modificacion
        paramfecha_modificacion.ParameterName = "fecha_modificacion"
        cmd.Parameters.Add(paramfecha_modificacion)
        Dim paramusuario_creo As DbParameter = cmd.CreateParameter()
        paramusuario_creo.Value = saih_apas.usuario_creo
        paramusuario_creo.ParameterName = "usuario_creo"
        cmd.Parameters.Add(paramusuario_creo)
        Dim paramusuario_modifico As DbParameter = cmd.CreateParameter()
        paramusuario_modifico.Value = saih_apas.usuario_modifico
        paramusuario_modifico.ParameterName = "usuario_modifico"
        cmd.Parameters.Add(paramusuario_modifico)
        Dim paramfecha_inicial As DbParameter = cmd.CreateParameter()
        paramfecha_inicial.Value = saih_apas.fecha_inicial
        paramfecha_inicial.ParameterName = "fecha_inicial"
        cmd.Parameters.Add(paramfecha_inicial)
        Dim paramfecha_final As DbParameter = cmd.CreateParameter()
        paramfecha_final.Value = saih_apas.fecha_final
        paramfecha_final.ParameterName = "fecha_final"
        cmd.Parameters.Add(paramfecha_final)
        Dim paramconsultorio As DbParameter = cmd.CreateParameter()
        paramconsultorio.Value = saih_apas.consultorio
        paramconsultorio.ParameterName = "consultorio"
        cmd.Parameters.Add(paramconsultorio)
        Dim paramintervalo As DbParameter = cmd.CreateParameter()
        paramintervalo.Value = saih_apas.intervalo
        paramintervalo.ParameterName = "intervalo"
        cmd.Parameters.Add(paramintervalo)
        Dim paramcodser As DbParameter = cmd.CreateParameter()
        paramcodser.Value = saih_apas.codser
        paramcodser.ParameterName = "codser"
        cmd.Parameters.Add(paramcodser)
        Dim paramestado As DbParameter = cmd.CreateParameter()
        paramestado.Value = saih_apas.estado
        paramestado.ParameterName = "estado"
        cmd.Parameters.Add(paramestado)
        Dim parammotivo_cancelacion As DbParameter = cmd.CreateParameter()
        parammotivo_cancelacion.Value = saih_apas.motivo_cancelacion
        parammotivo_cancelacion.ParameterName = "motivo_cancelacion"
        cmd.Parameters.Add(parammotivo_cancelacion)
          Dim Dr As DbDataReader = cmd.ExecuteReader()
          While Dr.Read
            dato = Dr(0)
          End While
      Catch ex As Exception
          Ext.Net.X.Msg.Alert("Información", ex.Message).Show()
      Finally
          desconecta(cnn)
      End Try
      Return dato
  End Function

  Public Function Editar(ByVal saih_apas As saih_apas)
      Dim parametros As New List(Of DbParameter)
      Dim paramcodpre As DbParameter = cmd.CreateParameter()
      paramcodpre.Value = saih_apas.codpre
      paramcodpre.ParameterName = "codpre"
      parametros.Add(paramcodpre)
      Dim paramnropme As DbParameter = cmd.CreateParameter()
      paramnropme.Value = saih_apas.nropme
      paramnropme.ParameterName = "nropme"
      parametros.Add(paramnropme)
      Dim paramcodpas As DbParameter = cmd.CreateParameter()
      paramcodpas.Value = saih_apas.codpas
      paramcodpas.ParameterName = "codpas"
      parametros.Add(paramcodpas)
      Dim paramfecha_adicion As DbParameter = cmd.CreateParameter()
      paramfecha_adicion.Value = saih_apas.fecha_adicion
      paramfecha_adicion.ParameterName = "fecha_adicion"
      parametros.Add(paramfecha_adicion)
      Dim paramfecha_modificacion As DbParameter = cmd.CreateParameter()
      paramfecha_modificacion.Value = saih_apas.fecha_modificacion
      paramfecha_modificacion.ParameterName = "fecha_modificacion"
      parametros.Add(paramfecha_modificacion)
      Dim paramusuario_creo As DbParameter = cmd.CreateParameter()
      paramusuario_creo.Value = saih_apas.usuario_creo
      paramusuario_creo.ParameterName = "usuario_creo"
      parametros.Add(paramusuario_creo)
      Dim paramusuario_modifico As DbParameter = cmd.CreateParameter()
      paramusuario_modifico.Value = saih_apas.usuario_modifico
      paramusuario_modifico.ParameterName = "usuario_modifico"
      parametros.Add(paramusuario_modifico)
      Dim paramfecha_inicial As DbParameter = cmd.CreateParameter()
      paramfecha_inicial.Value = saih_apas.fecha_inicial
      paramfecha_inicial.ParameterName = "fecha_inicial"
      parametros.Add(paramfecha_inicial)
      Dim paramfecha_final As DbParameter = cmd.CreateParameter()
      paramfecha_final.Value = saih_apas.fecha_final
      paramfecha_final.ParameterName = "fecha_final"
      parametros.Add(paramfecha_final)
      Dim paramconsultorio As DbParameter = cmd.CreateParameter()
      paramconsultorio.Value = saih_apas.consultorio
      paramconsultorio.ParameterName = "consultorio"
      parametros.Add(paramconsultorio)
      Dim paramintervalo As DbParameter = cmd.CreateParameter()
      paramintervalo.Value = saih_apas.intervalo
      paramintervalo.ParameterName = "intervalo"
      parametros.Add(paramintervalo)
      Dim paramcodser As DbParameter = cmd.CreateParameter()
      paramcodser.Value = saih_apas.codser
      paramcodser.ParameterName = "codser"
      parametros.Add(paramcodser)
      Dim paramestado As DbParameter = cmd.CreateParameter()
      paramestado.Value = saih_apas.estado
      paramestado.ParameterName = "estado"
      parametros.Add(paramestado)
      Dim parammotivo_cancelacion As DbParameter = cmd.CreateParameter()
      parammotivo_cancelacion.Value = saih_apas.motivo_cancelacion
      parammotivo_cancelacion.ParameterName = "motivo_cancelacion"
      parametros.Add(parammotivo_cancelacion)
      Return ejecutaNonQuery("cambiarsaih_apas", parametros)
  End Function

  Public Function Borrar(
      ByVal codpre As string,
      ByVal nropme As string)
      Dim parametros As New List(Of DbParameter)
      Dim paramcodpre As DbParameter = cmd.CreateParameter()
      paramcodpre.Value = codpre
      paramcodpre.ParameterName = "codpre"
      parametros.Add(paramcodpre)
      Dim paramnropme As DbParameter = cmd.CreateParameter()
      paramnropme.Value = nropme
      paramnropme.ParameterName = "nropme"
      parametros.Add(paramnropme)
      Return ejecutaNonQuery("eliminasaih_apas", parametros)
  End Function

  Public Function Select_Where_saih_apas(ByVal Cond As String) As List(Of saih_apas)
      Dim Listsaih_apas As New List(Of saih_apas)
      Try
          cnn = conecta()
          cmd = New SqlCommand("obtenersaih_apasByWhere")
          cmd.CommandType = CommandType.StoredProcedure
          cmd.Connection = cnn
          Dim param As DbParameter = cmd.CreateParameter()
          param.Value = Cond
          param.ParameterName = "where"
          cmd.Parameters.Add(param)
          Dim Dr As DbDataReader = cmd.ExecuteReader()
          While Dr.Read
              Listsaih_apas.Add(New saih_apas(
                  Dr("codpre"),
                  Dr("nropme"),
                  Dr("codpas"),
                  Dr("fecha_adicion"),
                  Dr("fecha_modificacion"),
                  Dr("usuario_creo"),
                  Dr("usuario_modifico"),
                  Dr("fecha_inicial"),
                  Dr("fecha_final"),
                  Dr("consultorio"),
                  Dr("intervalo"),
                  Dr("codser"),
                  Dr("estado"),
                  Dr("motivo_cancelacion")))
          End While
      Catch ex As Exception
          Ext.Net.X.Msg.Alert("Información", ex.Message).Show()
      Finally
          desconecta(cnn)
      End Try
      Return Listsaih_apas
  End Function

  Public Function Select_ById_saih_apas(
      ByVal codpre As string,
      ByVal nropme As string) As List(Of saih_apas)
      Dim Listsaih_apas As New List(Of saih_apas)
      Try
          cnn = conecta()
          cmd = New SqlCommand("obtenersaih_apasById")
          cmd.CommandType = CommandType.StoredProcedure
          cmd.Connection = cnn
      Dim paramcodpre As DbParameter = cmd.CreateParameter()
      paramcodpre.Value = codpre
      paramcodpre.ParameterName = "codpre"
      cmd.Parameters.Add(paramcodpre)
      Dim paramnropme As DbParameter = cmd.CreateParameter()
      paramnropme.Value = nropme
      paramnropme.ParameterName = "nropme"
      cmd.Parameters.Add(paramnropme)
          Dim Dr As DbDataReader = cmd.ExecuteReader()
          While Dr.Read
              Listsaih_apas.Add(New saih_apas(
                  Dr("codpre"),
                  Dr("nropme"),
                  Dr("codpas"),
                  Dr("fecha_adicion"),
                  Dr("fecha_modificacion"),
                  Dr("usuario_creo"),
                  Dr("usuario_modifico"),
                  Dr("fecha_inicial"),
                  Dr("fecha_final"),
                  Dr("consultorio"),
                  Dr("intervalo"),
                  Dr("codser"),
                  Dr("estado"),
                  Dr("motivo_cancelacion")))
          End While
      Catch ex As Exception
          Ext.Net.X.Msg.Alert("Información", ex.Message).Show()
      Finally
          desconecta(cnn)
      End Try
      Return Listsaih_apas
  End Function

  Public Function Select_saih_apas() As List(Of saih_apas)
      Dim Listsaih_apas As New List(Of saih_apas)
      Try
         cnn = conecta()
          cmd = New SqlCommand("obtenersaih_apas")
          cmd.CommandType = CommandType.StoredProcedure
          cmd.Connection = cnn
          Dim Dr As DbDataReader = cmd.ExecuteReader()
          While Dr.Read
              Listsaih_apas.Add(New saih_apas(
                  Dr("codpre"),
                  Dr("nropme"),
                  Dr("codpas"),
                  Dr("fecha_adicion"),
                  Dr("fecha_modificacion"),
                  Dr("usuario_creo"),
                  Dr("usuario_modifico"),
                  Dr("fecha_inicial"),
                  Dr("fecha_final"),
                  Dr("consultorio"),
                  Dr("intervalo"),
                  Dr("codser"),
                  Dr("estado"),
                  Dr("motivo_cancelacion")))
          End While
      Catch ex As Exception
          Ext.Net.X.Msg.Alert("Información", ex.Message).Show()
      Finally
          desconecta(cnn)
      End Try
      Return Listsaih_apas
  End Function

End Class
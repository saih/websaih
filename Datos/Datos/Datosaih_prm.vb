Imports System.Data
Imports System.Data.SqlClient
Imports System.Data.Common

Public Class Datosaih_prm
   Inherits conexion

  Public Function Insertar(ByVal saih_prm As saih_prm)
      Dim parametros As New List(Of DbParameter)
      Dim paramcodpre As DbParameter = cmd.CreateParameter()
      paramcodpre.Value = saih_prm.codpre
      paramcodpre.ParameterName = "codpre"
      parametros.Add(paramcodpre)
      Dim paramlugar As DbParameter = cmd.CreateParameter()
      paramlugar.Value = saih_prm.lugar
      paramlugar.ParameterName = "lugar"
      parametros.Add(paramlugar)
      Dim paramnumhis As DbParameter = cmd.CreateParameter()
      paramnumhis.Value = saih_prm.numhis
      paramnumhis.ParameterName = "numhis"
      parametros.Add(paramnumhis)
      Dim paramnropac As DbParameter = cmd.CreateParameter()
      paramnropac.Value = saih_prm.nropac
      paramnropac.ParameterName = "nropac"
      parametros.Add(paramnropac)
      Dim paramnumfac As DbParameter = cmd.CreateParameter()
      paramnumfac.Value = saih_prm.numfac
      paramnumfac.ParameterName = "numfac"
      parametros.Add(paramnumfac)
      Dim paramnrocta As DbParameter = cmd.CreateParameter()
      paramnrocta.Value = saih_prm.nrocta
      paramnrocta.ParameterName = "nrocta"
      parametros.Add(paramnrocta)
      Dim paramnroord As DbParameter = cmd.CreateParameter()
      paramnroord.Value = saih_prm.nroord
      paramnroord.ParameterName = "nroord"
      parametros.Add(paramnroord)
      Dim paramnronci As DbParameter = cmd.CreateParameter()
      paramnronci.Value = saih_prm.nronci
      paramnronci.ParameterName = "nronci"
      parametros.Add(paramnronci)
      Dim paramnroglo As DbParameter = cmd.CreateParameter()
      paramnroglo.Value = saih_prm.nroglo
      paramnroglo.ParameterName = "nroglo"
      parametros.Add(paramnroglo)
      Dim paramnroges As DbParameter = cmd.CreateParameter()
      paramnroges.Value = saih_prm.nroges
      paramnroges.ParameterName = "nroges"
      parametros.Add(paramnroges)
      Dim paramnrocit As DbParameter = cmd.CreateParameter()
      paramnrocit.Value = saih_prm.nrocit
      paramnrocit.ParameterName = "nrocit"
      parametros.Add(paramnrocit)
      Dim paramnorage As DbParameter = cmd.CreateParameter()
      paramnorage.Value = saih_prm.norage
      paramnorage.ParameterName = "norage"
      parametros.Add(paramnorage)
      Dim paramnorres As DbParameter = cmd.CreateParameter()
      paramnorres.Value = saih_prm.norres
      paramnorres.ParameterName = "norres"
      parametros.Add(paramnorres)
      Dim paramnroepi As DbParameter = cmd.CreateParameter()
      paramnroepi.Value = saih_prm.nroepi
      paramnroepi.ParameterName = "nroepi"
      parametros.Add(paramnroepi)
      Dim paramnroiqu As DbParameter = cmd.CreateParameter()
      paramnroiqu.Value = saih_prm.nroiqu
      paramnroiqu.ParameterName = "nroiqu"
      parametros.Add(paramnroiqu)
      Dim paramindhuella As DbParameter = cmd.CreateParameter()
      paramindhuella.Value = saih_prm.indhuella
      paramindhuella.ParameterName = "indhuella"
      parametros.Add(paramindhuella)
      Dim paramnrorem As DbParameter = cmd.CreateParameter()
      paramnrorem.Value = saih_prm.nrorem
      paramnrorem.ParameterName = "nrorem"
      parametros.Add(paramnrorem)
      Dim paramnroacm As DbParameter = cmd.CreateParameter()
      paramnroacm.Value = saih_prm.nroacm
      paramnroacm.ParameterName = "nroacm"
      parametros.Add(paramnroacm)
      Dim paramnrofor As DbParameter = cmd.CreateParameter()
      paramnrofor.Value = saih_prm.nrofor
      paramnrofor.ParameterName = "nrofor"
      parametros.Add(paramnrofor)
      Dim paramnropro As DbParameter = cmd.CreateParameter()
      paramnropro.Value = saih_prm.nropro
      paramnropro.ParameterName = "nropro"
      parametros.Add(paramnropro)
      Dim paramnropme As DbParameter = cmd.CreateParameter()
      paramnropme.Value = saih_prm.nropme
      paramnropme.ParameterName = "nropme"
      parametros.Add(paramnropme)
      Dim paramnrofur As DbParameter = cmd.CreateParameter()
      paramnrofur.Value = saih_prm.nrofur
      paramnrofur.ParameterName = "nrofur"
      parametros.Add(paramnrofur)
      Dim paramid_lugar As DbParameter = cmd.CreateParameter()
      paramid_lugar.Value = saih_prm.id_lugar
      paramid_lugar.ParameterName = "id_lugar"
      parametros.Add(paramid_lugar)
      Dim paramcon_pago_ser_no As DbParameter = cmd.CreateParameter()
      paramcon_pago_ser_no.Value = saih_prm.con_pago_ser_no
      paramcon_pago_ser_no.ParameterName = "con_pago_ser_no"
      parametros.Add(paramcon_pago_ser_no)
      Dim paramcod_abono_fac As DbParameter = cmd.CreateParameter()
      paramcod_abono_fac.Value = saih_prm.cod_abono_fac
      paramcod_abono_fac.ParameterName = "cod_abono_fac"
      parametros.Add(paramcod_abono_fac)
      Dim paramcod_pago_act As DbParameter = cmd.CreateParameter()
      paramcod_pago_act.Value = saih_prm.cod_pago_act
      paramcod_pago_act.ParameterName = "cod_pago_act"
      parametros.Add(paramcod_pago_act)
      Dim paramcod_recaudo_copagos As DbParameter = cmd.CreateParameter()
      paramcod_recaudo_copagos.Value = saih_prm.cod_recaudo_copagos
      paramcod_recaudo_copagos.ParameterName = "cod_recaudo_copagos"
      parametros.Add(paramcod_recaudo_copagos)
      Dim paramcns_num_cuenta_cobro As DbParameter = cmd.CreateParameter()
      paramcns_num_cuenta_cobro.Value = saih_prm.cns_num_cuenta_cobro
      paramcns_num_cuenta_cobro.ParameterName = "cns_num_cuenta_cobro"
      parametros.Add(paramcns_num_cuenta_cobro)
      Dim paramcns_pag_cuenta_cobro As DbParameter = cmd.CreateParameter()
      paramcns_pag_cuenta_cobro.Value = saih_prm.cns_pag_cuenta_cobro
      paramcns_pag_cuenta_cobro.ParameterName = "cns_pag_cuenta_cobro"
      parametros.Add(paramcns_pag_cuenta_cobro)
      Dim paramcod_recaudo_cuota_moderadora As DbParameter = cmd.CreateParameter()
      paramcod_recaudo_cuota_moderadora.Value = saih_prm.cod_recaudo_cuota_moderadora
      paramcod_recaudo_cuota_moderadora.ParameterName = "cod_recaudo_cuota_moderadora"
      parametros.Add(paramcod_recaudo_cuota_moderadora)
      Dim paramHoraExpedicion As DbParameter = cmd.CreateParameter()
      paramHoraExpedicion.Value = saih_prm.HoraExpedicion
      paramHoraExpedicion.ParameterName = "HoraExpedicion"
      parametros.Add(paramHoraExpedicion)
      Dim paramHoraInicioAtencion As DbParameter = cmd.CreateParameter()
      paramHoraInicioAtencion.Value = saih_prm.HoraInicioAtencion
      paramHoraInicioAtencion.ParameterName = "HoraInicioAtencion"
      parametros.Add(paramHoraInicioAtencion)
      Dim paramIntervaloCitas As DbParameter = cmd.CreateParameter()
      paramIntervaloCitas.Value = saih_prm.IntervaloCitas
      paramIntervaloCitas.ParameterName = "IntervaloCitas"
      parametros.Add(paramIntervaloCitas)
      Dim paramcodurgencias As DbParameter = cmd.CreateParameter()
      paramcodurgencias.Value = saih_prm.codurgencias
      paramcodurgencias.ParameterName = "codurgencias"
      parametros.Add(paramcodurgencias)
      Dim paramcodambulatorias As DbParameter = cmd.CreateParameter()
      paramcodambulatorias.Value = saih_prm.codambulatorias
      paramcodambulatorias.ParameterName = "codambulatorias"
      parametros.Add(paramcodambulatorias)
      Dim paramcodespecialidad As DbParameter = cmd.CreateParameter()
      paramcodespecialidad.Value = saih_prm.codespecialidad
      paramcodespecialidad.ParameterName = "codespecialidad"
      parametros.Add(paramcodespecialidad)
      Dim paramobservaciones As DbParameter = cmd.CreateParameter()
      paramobservaciones.Value = saih_prm.observaciones
      paramobservaciones.ParameterName = "observaciones"
      parametros.Add(paramobservaciones)
      Return ejecutaNonQuery("insertasaih_prm", parametros)
  End Function

  Public Function InsertarRet(ByVal saih_prm As saih_prm)
      Dim dato As Integer = 0
      Try
          cnn = conecta()
          cmd = New SqlCommand("insertasaih_prm")
          cmd.CommandType = CommandType.StoredProcedure
          cmd.Connection = cnn
        Dim paramcodpre As DbParameter = cmd.CreateParameter()
        paramcodpre.Value = saih_prm.codpre
        paramcodpre.ParameterName = "codpre"
        cmd.Parameters.Add(paramcodpre)
        Dim paramlugar As DbParameter = cmd.CreateParameter()
        paramlugar.Value = saih_prm.lugar
        paramlugar.ParameterName = "lugar"
        cmd.Parameters.Add(paramlugar)
        Dim paramnumhis As DbParameter = cmd.CreateParameter()
        paramnumhis.Value = saih_prm.numhis
        paramnumhis.ParameterName = "numhis"
        cmd.Parameters.Add(paramnumhis)
        Dim paramnropac As DbParameter = cmd.CreateParameter()
        paramnropac.Value = saih_prm.nropac
        paramnropac.ParameterName = "nropac"
        cmd.Parameters.Add(paramnropac)
        Dim paramnumfac As DbParameter = cmd.CreateParameter()
        paramnumfac.Value = saih_prm.numfac
        paramnumfac.ParameterName = "numfac"
        cmd.Parameters.Add(paramnumfac)
        Dim paramnrocta As DbParameter = cmd.CreateParameter()
        paramnrocta.Value = saih_prm.nrocta
        paramnrocta.ParameterName = "nrocta"
        cmd.Parameters.Add(paramnrocta)
        Dim paramnroord As DbParameter = cmd.CreateParameter()
        paramnroord.Value = saih_prm.nroord
        paramnroord.ParameterName = "nroord"
        cmd.Parameters.Add(paramnroord)
        Dim paramnronci As DbParameter = cmd.CreateParameter()
        paramnronci.Value = saih_prm.nronci
        paramnronci.ParameterName = "nronci"
        cmd.Parameters.Add(paramnronci)
        Dim paramnroglo As DbParameter = cmd.CreateParameter()
        paramnroglo.Value = saih_prm.nroglo
        paramnroglo.ParameterName = "nroglo"
        cmd.Parameters.Add(paramnroglo)
        Dim paramnroges As DbParameter = cmd.CreateParameter()
        paramnroges.Value = saih_prm.nroges
        paramnroges.ParameterName = "nroges"
        cmd.Parameters.Add(paramnroges)
        Dim paramnrocit As DbParameter = cmd.CreateParameter()
        paramnrocit.Value = saih_prm.nrocit
        paramnrocit.ParameterName = "nrocit"
        cmd.Parameters.Add(paramnrocit)
        Dim paramnorage As DbParameter = cmd.CreateParameter()
        paramnorage.Value = saih_prm.norage
        paramnorage.ParameterName = "norage"
        cmd.Parameters.Add(paramnorage)
        Dim paramnorres As DbParameter = cmd.CreateParameter()
        paramnorres.Value = saih_prm.norres
        paramnorres.ParameterName = "norres"
        cmd.Parameters.Add(paramnorres)
        Dim paramnroepi As DbParameter = cmd.CreateParameter()
        paramnroepi.Value = saih_prm.nroepi
        paramnroepi.ParameterName = "nroepi"
        cmd.Parameters.Add(paramnroepi)
        Dim paramnroiqu As DbParameter = cmd.CreateParameter()
        paramnroiqu.Value = saih_prm.nroiqu
        paramnroiqu.ParameterName = "nroiqu"
        cmd.Parameters.Add(paramnroiqu)
        Dim paramindhuella As DbParameter = cmd.CreateParameter()
        paramindhuella.Value = saih_prm.indhuella
        paramindhuella.ParameterName = "indhuella"
        cmd.Parameters.Add(paramindhuella)
        Dim paramnrorem As DbParameter = cmd.CreateParameter()
        paramnrorem.Value = saih_prm.nrorem
        paramnrorem.ParameterName = "nrorem"
        cmd.Parameters.Add(paramnrorem)
        Dim paramnroacm As DbParameter = cmd.CreateParameter()
        paramnroacm.Value = saih_prm.nroacm
        paramnroacm.ParameterName = "nroacm"
        cmd.Parameters.Add(paramnroacm)
        Dim paramnrofor As DbParameter = cmd.CreateParameter()
        paramnrofor.Value = saih_prm.nrofor
        paramnrofor.ParameterName = "nrofor"
        cmd.Parameters.Add(paramnrofor)
        Dim paramnropro As DbParameter = cmd.CreateParameter()
        paramnropro.Value = saih_prm.nropro
        paramnropro.ParameterName = "nropro"
        cmd.Parameters.Add(paramnropro)
        Dim paramnropme As DbParameter = cmd.CreateParameter()
        paramnropme.Value = saih_prm.nropme
        paramnropme.ParameterName = "nropme"
        cmd.Parameters.Add(paramnropme)
        Dim paramnrofur As DbParameter = cmd.CreateParameter()
        paramnrofur.Value = saih_prm.nrofur
        paramnrofur.ParameterName = "nrofur"
        cmd.Parameters.Add(paramnrofur)
        Dim paramid_lugar As DbParameter = cmd.CreateParameter()
        paramid_lugar.Value = saih_prm.id_lugar
        paramid_lugar.ParameterName = "id_lugar"
        cmd.Parameters.Add(paramid_lugar)
        Dim paramcon_pago_ser_no As DbParameter = cmd.CreateParameter()
        paramcon_pago_ser_no.Value = saih_prm.con_pago_ser_no
        paramcon_pago_ser_no.ParameterName = "con_pago_ser_no"
        cmd.Parameters.Add(paramcon_pago_ser_no)
        Dim paramcod_abono_fac As DbParameter = cmd.CreateParameter()
        paramcod_abono_fac.Value = saih_prm.cod_abono_fac
        paramcod_abono_fac.ParameterName = "cod_abono_fac"
        cmd.Parameters.Add(paramcod_abono_fac)
        Dim paramcod_pago_act As DbParameter = cmd.CreateParameter()
        paramcod_pago_act.Value = saih_prm.cod_pago_act
        paramcod_pago_act.ParameterName = "cod_pago_act"
        cmd.Parameters.Add(paramcod_pago_act)
        Dim paramcod_recaudo_copagos As DbParameter = cmd.CreateParameter()
        paramcod_recaudo_copagos.Value = saih_prm.cod_recaudo_copagos
        paramcod_recaudo_copagos.ParameterName = "cod_recaudo_copagos"
        cmd.Parameters.Add(paramcod_recaudo_copagos)
        Dim paramcns_num_cuenta_cobro As DbParameter = cmd.CreateParameter()
        paramcns_num_cuenta_cobro.Value = saih_prm.cns_num_cuenta_cobro
        paramcns_num_cuenta_cobro.ParameterName = "cns_num_cuenta_cobro"
        cmd.Parameters.Add(paramcns_num_cuenta_cobro)
        Dim paramcns_pag_cuenta_cobro As DbParameter = cmd.CreateParameter()
        paramcns_pag_cuenta_cobro.Value = saih_prm.cns_pag_cuenta_cobro
        paramcns_pag_cuenta_cobro.ParameterName = "cns_pag_cuenta_cobro"
        cmd.Parameters.Add(paramcns_pag_cuenta_cobro)
        Dim paramcod_recaudo_cuota_moderadora As DbParameter = cmd.CreateParameter()
        paramcod_recaudo_cuota_moderadora.Value = saih_prm.cod_recaudo_cuota_moderadora
        paramcod_recaudo_cuota_moderadora.ParameterName = "cod_recaudo_cuota_moderadora"
        cmd.Parameters.Add(paramcod_recaudo_cuota_moderadora)
        Dim paramHoraExpedicion As DbParameter = cmd.CreateParameter()
        paramHoraExpedicion.Value = saih_prm.HoraExpedicion
        paramHoraExpedicion.ParameterName = "HoraExpedicion"
        cmd.Parameters.Add(paramHoraExpedicion)
        Dim paramHoraInicioAtencion As DbParameter = cmd.CreateParameter()
        paramHoraInicioAtencion.Value = saih_prm.HoraInicioAtencion
        paramHoraInicioAtencion.ParameterName = "HoraInicioAtencion"
        cmd.Parameters.Add(paramHoraInicioAtencion)
        Dim paramIntervaloCitas As DbParameter = cmd.CreateParameter()
        paramIntervaloCitas.Value = saih_prm.IntervaloCitas
        paramIntervaloCitas.ParameterName = "IntervaloCitas"
        cmd.Parameters.Add(paramIntervaloCitas)
        Dim paramcodurgencias As DbParameter = cmd.CreateParameter()
        paramcodurgencias.Value = saih_prm.codurgencias
        paramcodurgencias.ParameterName = "codurgencias"
        cmd.Parameters.Add(paramcodurgencias)
        Dim paramcodambulatorias As DbParameter = cmd.CreateParameter()
        paramcodambulatorias.Value = saih_prm.codambulatorias
        paramcodambulatorias.ParameterName = "codambulatorias"
        cmd.Parameters.Add(paramcodambulatorias)
        Dim paramcodespecialidad As DbParameter = cmd.CreateParameter()
        paramcodespecialidad.Value = saih_prm.codespecialidad
        paramcodespecialidad.ParameterName = "codespecialidad"
        cmd.Parameters.Add(paramcodespecialidad)
        Dim paramobservaciones As DbParameter = cmd.CreateParameter()
        paramobservaciones.Value = saih_prm.observaciones
        paramobservaciones.ParameterName = "observaciones"
        cmd.Parameters.Add(paramobservaciones)
          Dim Dr As DbDataReader = cmd.ExecuteReader()
          While Dr.Read
            dato = Dr(0)
          End While
      Catch ex As Exception
          Ext.Net.X.Msg.Alert("Información", ex.Message).Show()
      Finally
          desconecta(cnn)
      End Try
      Return dato
  End Function

  Public Function Editar(ByVal saih_prm As saih_prm)
      Dim parametros As New List(Of DbParameter)
      Dim paramcodpre As DbParameter = cmd.CreateParameter()
      paramcodpre.Value = saih_prm.codpre
      paramcodpre.ParameterName = "codpre"
      parametros.Add(paramcodpre)
      Dim paramlugar As DbParameter = cmd.CreateParameter()
      paramlugar.Value = saih_prm.lugar
      paramlugar.ParameterName = "lugar"
      parametros.Add(paramlugar)
      Dim paramnumhis As DbParameter = cmd.CreateParameter()
      paramnumhis.Value = saih_prm.numhis
      paramnumhis.ParameterName = "numhis"
      parametros.Add(paramnumhis)
      Dim paramnropac As DbParameter = cmd.CreateParameter()
      paramnropac.Value = saih_prm.nropac
      paramnropac.ParameterName = "nropac"
      parametros.Add(paramnropac)
      Dim paramnumfac As DbParameter = cmd.CreateParameter()
      paramnumfac.Value = saih_prm.numfac
      paramnumfac.ParameterName = "numfac"
      parametros.Add(paramnumfac)
      Dim paramnrocta As DbParameter = cmd.CreateParameter()
      paramnrocta.Value = saih_prm.nrocta
      paramnrocta.ParameterName = "nrocta"
      parametros.Add(paramnrocta)
      Dim paramnroord As DbParameter = cmd.CreateParameter()
      paramnroord.Value = saih_prm.nroord
      paramnroord.ParameterName = "nroord"
      parametros.Add(paramnroord)
      Dim paramnronci As DbParameter = cmd.CreateParameter()
      paramnronci.Value = saih_prm.nronci
      paramnronci.ParameterName = "nronci"
      parametros.Add(paramnronci)
      Dim paramnroglo As DbParameter = cmd.CreateParameter()
      paramnroglo.Value = saih_prm.nroglo
      paramnroglo.ParameterName = "nroglo"
      parametros.Add(paramnroglo)
      Dim paramnroges As DbParameter = cmd.CreateParameter()
      paramnroges.Value = saih_prm.nroges
      paramnroges.ParameterName = "nroges"
      parametros.Add(paramnroges)
      Dim paramnrocit As DbParameter = cmd.CreateParameter()
      paramnrocit.Value = saih_prm.nrocit
      paramnrocit.ParameterName = "nrocit"
      parametros.Add(paramnrocit)
      Dim paramnorage As DbParameter = cmd.CreateParameter()
      paramnorage.Value = saih_prm.norage
      paramnorage.ParameterName = "norage"
      parametros.Add(paramnorage)
      Dim paramnorres As DbParameter = cmd.CreateParameter()
      paramnorres.Value = saih_prm.norres
      paramnorres.ParameterName = "norres"
      parametros.Add(paramnorres)
      Dim paramnroepi As DbParameter = cmd.CreateParameter()
      paramnroepi.Value = saih_prm.nroepi
      paramnroepi.ParameterName = "nroepi"
      parametros.Add(paramnroepi)
      Dim paramnroiqu As DbParameter = cmd.CreateParameter()
      paramnroiqu.Value = saih_prm.nroiqu
      paramnroiqu.ParameterName = "nroiqu"
      parametros.Add(paramnroiqu)
      Dim paramindhuella As DbParameter = cmd.CreateParameter()
      paramindhuella.Value = saih_prm.indhuella
      paramindhuella.ParameterName = "indhuella"
      parametros.Add(paramindhuella)
      Dim paramnrorem As DbParameter = cmd.CreateParameter()
      paramnrorem.Value = saih_prm.nrorem
      paramnrorem.ParameterName = "nrorem"
      parametros.Add(paramnrorem)
      Dim paramnroacm As DbParameter = cmd.CreateParameter()
      paramnroacm.Value = saih_prm.nroacm
      paramnroacm.ParameterName = "nroacm"
      parametros.Add(paramnroacm)
      Dim paramnrofor As DbParameter = cmd.CreateParameter()
      paramnrofor.Value = saih_prm.nrofor
      paramnrofor.ParameterName = "nrofor"
      parametros.Add(paramnrofor)
      Dim paramnropro As DbParameter = cmd.CreateParameter()
      paramnropro.Value = saih_prm.nropro
      paramnropro.ParameterName = "nropro"
      parametros.Add(paramnropro)
      Dim paramnropme As DbParameter = cmd.CreateParameter()
      paramnropme.Value = saih_prm.nropme
      paramnropme.ParameterName = "nropme"
      parametros.Add(paramnropme)
      Dim paramnrofur As DbParameter = cmd.CreateParameter()
      paramnrofur.Value = saih_prm.nrofur
      paramnrofur.ParameterName = "nrofur"
      parametros.Add(paramnrofur)
      Dim paramid_lugar As DbParameter = cmd.CreateParameter()
      paramid_lugar.Value = saih_prm.id_lugar
      paramid_lugar.ParameterName = "id_lugar"
      parametros.Add(paramid_lugar)
      Dim paramcon_pago_ser_no As DbParameter = cmd.CreateParameter()
      paramcon_pago_ser_no.Value = saih_prm.con_pago_ser_no
      paramcon_pago_ser_no.ParameterName = "con_pago_ser_no"
      parametros.Add(paramcon_pago_ser_no)
      Dim paramcod_abono_fac As DbParameter = cmd.CreateParameter()
      paramcod_abono_fac.Value = saih_prm.cod_abono_fac
      paramcod_abono_fac.ParameterName = "cod_abono_fac"
      parametros.Add(paramcod_abono_fac)
      Dim paramcod_pago_act As DbParameter = cmd.CreateParameter()
      paramcod_pago_act.Value = saih_prm.cod_pago_act
      paramcod_pago_act.ParameterName = "cod_pago_act"
      parametros.Add(paramcod_pago_act)
      Dim paramcod_recaudo_copagos As DbParameter = cmd.CreateParameter()
      paramcod_recaudo_copagos.Value = saih_prm.cod_recaudo_copagos
      paramcod_recaudo_copagos.ParameterName = "cod_recaudo_copagos"
      parametros.Add(paramcod_recaudo_copagos)
      Dim paramcns_num_cuenta_cobro As DbParameter = cmd.CreateParameter()
      paramcns_num_cuenta_cobro.Value = saih_prm.cns_num_cuenta_cobro
      paramcns_num_cuenta_cobro.ParameterName = "cns_num_cuenta_cobro"
      parametros.Add(paramcns_num_cuenta_cobro)
      Dim paramcns_pag_cuenta_cobro As DbParameter = cmd.CreateParameter()
      paramcns_pag_cuenta_cobro.Value = saih_prm.cns_pag_cuenta_cobro
      paramcns_pag_cuenta_cobro.ParameterName = "cns_pag_cuenta_cobro"
      parametros.Add(paramcns_pag_cuenta_cobro)
      Dim paramcod_recaudo_cuota_moderadora As DbParameter = cmd.CreateParameter()
      paramcod_recaudo_cuota_moderadora.Value = saih_prm.cod_recaudo_cuota_moderadora
      paramcod_recaudo_cuota_moderadora.ParameterName = "cod_recaudo_cuota_moderadora"
      parametros.Add(paramcod_recaudo_cuota_moderadora)
      Dim paramHoraExpedicion As DbParameter = cmd.CreateParameter()
      paramHoraExpedicion.Value = saih_prm.HoraExpedicion
      paramHoraExpedicion.ParameterName = "HoraExpedicion"
      parametros.Add(paramHoraExpedicion)
      Dim paramHoraInicioAtencion As DbParameter = cmd.CreateParameter()
      paramHoraInicioAtencion.Value = saih_prm.HoraInicioAtencion
      paramHoraInicioAtencion.ParameterName = "HoraInicioAtencion"
      parametros.Add(paramHoraInicioAtencion)
      Dim paramIntervaloCitas As DbParameter = cmd.CreateParameter()
      paramIntervaloCitas.Value = saih_prm.IntervaloCitas
      paramIntervaloCitas.ParameterName = "IntervaloCitas"
      parametros.Add(paramIntervaloCitas)
      Dim paramcodurgencias As DbParameter = cmd.CreateParameter()
      paramcodurgencias.Value = saih_prm.codurgencias
      paramcodurgencias.ParameterName = "codurgencias"
      parametros.Add(paramcodurgencias)
      Dim paramcodambulatorias As DbParameter = cmd.CreateParameter()
      paramcodambulatorias.Value = saih_prm.codambulatorias
      paramcodambulatorias.ParameterName = "codambulatorias"
      parametros.Add(paramcodambulatorias)
      Dim paramcodespecialidad As DbParameter = cmd.CreateParameter()
      paramcodespecialidad.Value = saih_prm.codespecialidad
      paramcodespecialidad.ParameterName = "codespecialidad"
      parametros.Add(paramcodespecialidad)
      Dim paramobservaciones As DbParameter = cmd.CreateParameter()
      paramobservaciones.Value = saih_prm.observaciones
      paramobservaciones.ParameterName = "observaciones"
      parametros.Add(paramobservaciones)
      Return ejecutaNonQuery("cambiarsaih_prm", parametros)
  End Function

  Public Function Borrar(
      ByVal codpre As string)
      Dim parametros As New List(Of DbParameter)
      Dim paramcodpre As DbParameter = cmd.CreateParameter()
      paramcodpre.Value = codpre
      paramcodpre.ParameterName = "codpre"
      parametros.Add(paramcodpre)
      Return ejecutaNonQuery("eliminasaih_prm", parametros)
  End Function

  Public Function Select_Where_saih_prm(ByVal Cond As String) As List(Of saih_prm)
      Dim Listsaih_prm As New List(Of saih_prm)
      Try
          cnn = conecta()
          cmd = New SqlCommand("obtenersaih_prmByWhere")
          cmd.CommandType = CommandType.StoredProcedure
          cmd.Connection = cnn
          Dim param As DbParameter = cmd.CreateParameter()
          param.Value = Cond
          param.ParameterName = "where"
          cmd.Parameters.Add(param)
          Dim Dr As DbDataReader = cmd.ExecuteReader()
          While Dr.Read
              Listsaih_prm.Add(New saih_prm(
                  Dr("codpre"),
                  Dr("lugar"),
                  Dr("numhis"),
                  Dr("nropac"),
                  Dr("numfac"),
                  Dr("nrocta"),
                  Dr("nroord"),
                  Dr("nronci"),
                  Dr("nroglo"),
                  Dr("nroges"),
                  Dr("nrocit"),
                  Dr("norage"),
                  Dr("norres"),
                  Dr("nroepi"),
                  Dr("nroiqu"),
                  Dr("indhuella"),
                  Dr("nrorem"),
                  Dr("nroacm"),
                  Dr("nrofor"),
                  Dr("nropro"),
                  Dr("nropme"),
                  Dr("nrofur"),
                  Dr("id_lugar"),
                  Dr("con_pago_ser_no"),
                  Dr("cod_abono_fac"),
                  Dr("cod_pago_act"),
                  Dr("cod_recaudo_copagos"),
                  Dr("cns_num_cuenta_cobro"),
                  Dr("cns_pag_cuenta_cobro"),
                  Dr("cod_recaudo_cuota_moderadora"),
                  Dr("HoraExpedicion"),
                  Dr("HoraInicioAtencion"),
                  Dr("IntervaloCitas"),
                  Dr("codurgencias"),
                  Dr("codambulatorias"),
                  Dr("codespecialidad"),
                  Dr("observaciones")))
          End While
      Catch ex As Exception
          Ext.Net.X.Msg.Alert("Información", ex.Message).Show()
      Finally
          desconecta(cnn)
      End Try
      Return Listsaih_prm
  End Function

  Public Function Select_ById_saih_prm(
      ByVal codpre As string) As List(Of saih_prm)
      Dim Listsaih_prm As New List(Of saih_prm)
      Try
          cnn = conecta()
          cmd = New SqlCommand("obtenersaih_prmById")
          cmd.CommandType = CommandType.StoredProcedure
          cmd.Connection = cnn
      Dim paramcodpre As DbParameter = cmd.CreateParameter()
      paramcodpre.Value = codpre
      paramcodpre.ParameterName = "codpre"
      cmd.Parameters.Add(paramcodpre)
          Dim Dr As DbDataReader = cmd.ExecuteReader()
          While Dr.Read
              Listsaih_prm.Add(New saih_prm(
                  Dr("codpre"),
                  Dr("lugar"),
                  Dr("numhis"),
                  Dr("nropac"),
                  Dr("numfac"),
                  Dr("nrocta"),
                  Dr("nroord"),
                  Dr("nronci"),
                  Dr("nroglo"),
                  Dr("nroges"),
                  Dr("nrocit"),
                  Dr("norage"),
                  Dr("norres"),
                  Dr("nroepi"),
                  Dr("nroiqu"),
                  Dr("indhuella"),
                  Dr("nrorem"),
                  Dr("nroacm"),
                  Dr("nrofor"),
                  Dr("nropro"),
                  Dr("nropme"),
                  Dr("nrofur"),
                  Dr("id_lugar"),
                  Dr("con_pago_ser_no"),
                  Dr("cod_abono_fac"),
                  Dr("cod_pago_act"),
                  Dr("cod_recaudo_copagos"),
                  Dr("cns_num_cuenta_cobro"),
                  Dr("cns_pag_cuenta_cobro"),
                  Dr("cod_recaudo_cuota_moderadora"),
                  Dr("HoraExpedicion"),
                  Dr("HoraInicioAtencion"),
                  Dr("IntervaloCitas"),
                  Dr("codurgencias"),
                  Dr("codambulatorias"),
                  Dr("codespecialidad"),
                  Dr("observaciones")))
          End While
      Catch ex As Exception
          Ext.Net.X.Msg.Alert("Información", ex.Message).Show()
      Finally
          desconecta(cnn)
      End Try
      Return Listsaih_prm
  End Function

  Public Function Select_saih_prm() As List(Of saih_prm)
      Dim Listsaih_prm As New List(Of saih_prm)
      Try
         cnn = conecta()
          cmd = New SqlCommand("obtenersaih_prm")
          cmd.CommandType = CommandType.StoredProcedure
          cmd.Connection = cnn
          Dim Dr As DbDataReader = cmd.ExecuteReader()
          While Dr.Read
              Listsaih_prm.Add(New saih_prm(
                  Dr("codpre"),
                  Dr("lugar"),
                  Dr("numhis"),
                  Dr("nropac"),
                  Dr("numfac"),
                  Dr("nrocta"),
                  Dr("nroord"),
                  Dr("nronci"),
                  Dr("nroglo"),
                  Dr("nroges"),
                  Dr("nrocit"),
                  Dr("norage"),
                  Dr("norres"),
                  Dr("nroepi"),
                  Dr("nroiqu"),
                  Dr("indhuella"),
                  Dr("nrorem"),
                  Dr("nroacm"),
                  Dr("nrofor"),
                  Dr("nropro"),
                  Dr("nropme"),
                  Dr("nrofur"),
                  Dr("id_lugar"),
                  Dr("con_pago_ser_no"),
                  Dr("cod_abono_fac"),
                  Dr("cod_pago_act"),
                  Dr("cod_recaudo_copagos"),
                  Dr("cns_num_cuenta_cobro"),
                  Dr("cns_pag_cuenta_cobro"),
                  Dr("cod_recaudo_cuota_moderadora"),
                  Dr("HoraExpedicion"),
                  Dr("HoraInicioAtencion"),
                  Dr("IntervaloCitas"),
                  Dr("codurgencias"),
                  Dr("codambulatorias"),
                  Dr("codespecialidad"),
                  Dr("observaciones")))
          End While
      Catch ex As Exception
          Ext.Net.X.Msg.Alert("Información", ex.Message).Show()
      Finally
          desconecta(cnn)
      End Try
      Return Listsaih_prm
  End Function

End Class
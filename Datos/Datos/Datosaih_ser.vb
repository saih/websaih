Imports System.Data
Imports System.Data.SqlClient
Imports System.Data.Common
Imports System.Configuration

Public Class Datosaih_ser
   Inherits conexion

  Public Function Insertar(ByVal saih_ser As saih_ser)
      Dim parametros As New List(Of DbParameter)
      Dim paramcodser As DbParameter = cmd.CreateParameter()
      paramcodser.Value = saih_ser.codser
      paramcodser.ParameterName = "codser"
      parametros.Add(paramcodser)
      Dim paramnombre As DbParameter = cmd.CreateParameter()
      paramnombre.Value = saih_ser.nombre
      paramnombre.ParameterName = "nombre"
      parametros.Add(paramnombre)
      Dim paramtippro As DbParameter = cmd.CreateParameter()
      paramtippro.Value = saih_ser.tippro
      paramtippro.ParameterName = "tippro"
      parametros.Add(paramtippro)
      Return ejecutaNonQuery("insertasaih_ser", parametros)
  End Function

  Public Function InsertarRet(ByVal saih_ser As saih_ser)
      Dim dato As Integer = 0
        Dim appSettings = ConfigurationManager.AppSettings
        Dim result As String = appSettings("014")

        Try
            cnn = conecta()
            cmd = New SqlCommand("insertasaih_ser")
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Connection = cnn
            Dim paramcodser As DbParameter = cmd.CreateParameter()
            paramcodser.Value = saih_ser.codser
            paramcodser.ParameterName = "codser"
            cmd.Parameters.Add(paramcodser)
            Dim paramnombre As DbParameter = cmd.CreateParameter()
            paramnombre.Value = saih_ser.nombre
            paramnombre.ParameterName = "nombre"
            cmd.Parameters.Add(paramnombre)
            Dim paramtippro As DbParameter = cmd.CreateParameter()
            paramtippro.Value = saih_ser.tippro
            paramtippro.ParameterName = "tippro"
            cmd.Parameters.Add(paramtippro)
            Dim Dr As DbDataReader = cmd.ExecuteReader()
            While Dr.Read
                dato = Dr(0)
            End While
        Catch ex As Exception
            Ext.Net.X.Msg.Alert("Información", result).Show()
        Finally
            desconecta(cnn)
        End Try
      Return dato
  End Function

  Public Function Editar(ByVal saih_ser As saih_ser)
      Dim parametros As New List(Of DbParameter)
      Dim paramcodser As DbParameter = cmd.CreateParameter()
      paramcodser.Value = saih_ser.codser
      paramcodser.ParameterName = "codser"
      parametros.Add(paramcodser)
      Dim paramnombre As DbParameter = cmd.CreateParameter()
      paramnombre.Value = saih_ser.nombre
      paramnombre.ParameterName = "nombre"
      parametros.Add(paramnombre)
      Dim paramtippro As DbParameter = cmd.CreateParameter()
      paramtippro.Value = saih_ser.tippro
      paramtippro.ParameterName = "tippro"
      parametros.Add(paramtippro)
      Return ejecutaNonQuery("cambiarsaih_ser", parametros)
  End Function

  Public Function Borrar(
      ByVal codser As string)
      Dim parametros As New List(Of DbParameter)
      Dim paramcodser As DbParameter = cmd.CreateParameter()
      paramcodser.Value = codser
      paramcodser.ParameterName = "codser"
      parametros.Add(paramcodser)
      Return ejecutaNonQuery("eliminasaih_ser", parametros)
  End Function

  Public Function Select_Where_saih_ser(ByVal Cond As String) As List(Of saih_ser)
        Dim Listsaih_ser As New List(Of saih_ser)
        Dim appSettings = ConfigurationManager.AppSettings
        Dim result As String = appSettings("015")

      Try
          cnn = conecta()
          cmd = New SqlCommand("obtenersaih_serByWhere")
          cmd.CommandType = CommandType.StoredProcedure
          cmd.Connection = cnn
          Dim param As DbParameter = cmd.CreateParameter()
          param.Value = Cond
          param.ParameterName = "where"
          cmd.Parameters.Add(param)
          Dim Dr As DbDataReader = cmd.ExecuteReader()
          While Dr.Read
              Listsaih_ser.Add(New saih_ser(
                  Dr("codser"),
                  Dr("nombre"),
                  Dr("tippro")))
          End While
      Catch ex As Exception
            Ext.Net.X.Msg.Alert("Información", result).Show()
      Finally
          desconecta(cnn)
      End Try
      Return Listsaih_ser
  End Function

  Public Function Select_ById_saih_ser(
      ByVal codser As string) As List(Of saih_ser)
        Dim Listsaih_ser As New List(Of saih_ser)
        Dim appSettings = ConfigurationManager.AppSettings
        Dim result As String = appSettings("015")

      Try
          cnn = conecta()
          cmd = New SqlCommand("obtenersaih_serById")
          cmd.CommandType = CommandType.StoredProcedure
          cmd.Connection = cnn
      Dim paramcodser As DbParameter = cmd.CreateParameter()
      paramcodser.Value = codser
      paramcodser.ParameterName = "codser"
      cmd.Parameters.Add(paramcodser)
          Dim Dr As DbDataReader = cmd.ExecuteReader()
          While Dr.Read
              Listsaih_ser.Add(New saih_ser(
                  Dr("codser"),
                  Dr("nombre"),
                  Dr("tippro")))
          End While
      Catch ex As Exception
            Ext.Net.X.Msg.Alert("Información", result).Show()
      Finally
          desconecta(cnn)
      End Try
      Return Listsaih_ser
  End Function

  Public Function Select_saih_ser() As List(Of saih_ser)
        Dim Listsaih_ser As New List(Of saih_ser)
        Dim appSettings = ConfigurationManager.AppSettings
        Dim result As String = appSettings("015")

      Try
         cnn = conecta()
          cmd = New SqlCommand("obtenersaih_ser")
          cmd.CommandType = CommandType.StoredProcedure
          cmd.Connection = cnn
          Dim Dr As DbDataReader = cmd.ExecuteReader()
          While Dr.Read
              Listsaih_ser.Add(New saih_ser(
                  Dr("codser"),
                  Dr("nombre"),
                  Dr("tippro")))
          End While
      Catch ex As Exception
            Ext.Net.X.Msg.Alert("Información", result).Show()
      Finally
          desconecta(cnn)
      End Try
      Return Listsaih_ser
  End Function

End Class
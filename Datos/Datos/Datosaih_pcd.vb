Imports System.Data
Imports System.Data.SqlClient
Imports System.Data.Common
Imports System.Configuration

Public Class Datosaih_pcd
   Inherits conexion

  Public Function Insertar(ByVal saih_pcd As saih_pcd)
      Dim parametros As New List(Of DbParameter)
      Dim parammanual As DbParameter = cmd.CreateParameter()
      parammanual.Value = saih_pcd.manual
      parammanual.ParameterName = "manual"
      parametros.Add(parammanual)
      Dim paramcodigo As DbParameter = cmd.CreateParameter()
      paramcodigo.Value = saih_pcd.codigo
      paramcodigo.ParameterName = "codigo"
      parametros.Add(paramcodigo)
      Dim paramnombre As DbParameter = cmd.CreateParameter()
      paramnombre.Value = saih_pcd.nombre
      paramnombre.ParameterName = "nombre"
      parametros.Add(paramnombre)
      Dim paramncomplejidad As DbParameter = cmd.CreateParameter()
      paramncomplejidad.Value = saih_pcd.ncomplejidad
      paramncomplejidad.ParameterName = "ncomplejidad"
      parametros.Add(paramncomplejidad)
      Dim paramcencosto As DbParameter = cmd.CreateParameter()
      paramcencosto.Value = saih_pcd.cencosto
      paramcencosto.ParameterName = "cencosto"
      parametros.Add(paramcencosto)
      Dim paramaplsex As DbParameter = cmd.CreateParameter()
      paramaplsex.Value = saih_pcd.aplsex
      paramaplsex.ParameterName = "aplsex"
      parametros.Add(paramaplsex)
      Dim paramapledadinicio As DbParameter = cmd.CreateParameter()
      paramapledadinicio.Value = saih_pcd.apledadinicio
      paramapledadinicio.ParameterName = "apledadinicio"
      parametros.Add(paramapledadinicio)
      Dim paramapledadfin As DbParameter = cmd.CreateParameter()
      paramapledadfin.Value = saih_pcd.apledadfin
      paramapledadfin.ParameterName = "apledadfin"
      parametros.Add(paramapledadfin)
      Dim paramcodgrupoq As DbParameter = cmd.CreateParameter()
      paramcodgrupoq.Value = saih_pcd.codgrupoq
      paramcodgrupoq.ParameterName = "codgrupoq"
      parametros.Add(paramcodgrupoq)
      Dim paramvalor As DbParameter = cmd.CreateParameter()
      paramvalor.Value = saih_pcd.valor
      paramvalor.ParameterName = "valor"
      parametros.Add(paramvalor)
      Dim paramvalsmv As DbParameter = cmd.CreateParameter()
      paramvalsmv.Value = saih_pcd.valsmv
      paramvalsmv.ParameterName = "valsmv"
      parametros.Add(paramvalsmv)
      Dim paramporincremento As DbParameter = cmd.CreateParameter()
      paramporincremento.Value = saih_pcd.porincremento
      paramporincremento.ParameterName = "porincremento"
      parametros.Add(paramporincremento)
      Dim paramcobhonmed As DbParameter = cmd.CreateParameter()
      paramcobhonmed.Value = saih_pcd.cobhonmed
      paramcobhonmed.ParameterName = "cobhonmed"
      parametros.Add(paramcobhonmed)
      Dim paramcobdersal As DbParameter = cmd.CreateParameter()
      paramcobdersal.Value = saih_pcd.cobdersal
      paramcobdersal.ParameterName = "cobdersal"
      parametros.Add(paramcobdersal)
      Dim paramcobmatins As DbParameter = cmd.CreateParameter()
      paramcobmatins.Value = saih_pcd.cobmatins
      paramcobmatins.ParameterName = "cobmatins"
      parametros.Add(paramcobmatins)
      Dim paramcobhonayu As DbParameter = cmd.CreateParameter()
      paramcobhonayu.Value = saih_pcd.cobhonayu
      paramcobhonayu.ParameterName = "cobhonayu"
      parametros.Add(paramcobhonayu)
      Dim paramcobhonanes As DbParameter = cmd.CreateParameter()
      paramcobhonanes.Value = saih_pcd.cobhonanes
      paramcobhonanes.ParameterName = "cobhonanes"
      parametros.Add(paramcobhonanes)
      Dim paramconhonper As DbParameter = cmd.CreateParameter()
      paramconhonper.Value = saih_pcd.conhonper
      paramconhonper.ParameterName = "conhonper"
      parametros.Add(paramconhonper)
      Dim paramcodunif As DbParameter = cmd.CreateParameter()
      paramcodunif.Value = saih_pcd.codunif
      paramcodunif.ParameterName = "codunif"
      parametros.Add(paramcodunif)
      Dim paramcodfp As DbParameter = cmd.CreateParameter()
      paramcodfp.Value = saih_pcd.codfp
      paramcodfp.ParameterName = "codfp"
      parametros.Add(paramcodfp)
      Dim paramobservaciones As DbParameter = cmd.CreateParameter()
      paramobservaciones.Value = saih_pcd.observaciones
      paramobservaciones.ParameterName = "observaciones"
      parametros.Add(paramobservaciones)
      Dim paramcodser As DbParameter = cmd.CreateParameter()
      paramcodser.Value = saih_pcd.codser
      paramcodser.ParameterName = "codser"
      parametros.Add(paramcodser)
      Return ejecutaNonQuery("insertasaih_pcd", parametros)
  End Function

  Public Function InsertarRet(ByVal saih_pcd As saih_pcd)
      Dim dato As Integer = 0
        Dim appSettings = ConfigurationManager.AppSettings
        Dim result As String = appSettings("020")

        Try
            cnn = conecta()
            cmd = New SqlCommand("insertasaih_pcd")
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Connection = cnn
            Dim parammanual As DbParameter = cmd.CreateParameter()
            parammanual.Value = saih_pcd.manual
            parammanual.ParameterName = "manual"
            cmd.Parameters.Add(parammanual)
            Dim paramcodigo As DbParameter = cmd.CreateParameter()
            paramcodigo.Value = saih_pcd.codigo
            paramcodigo.ParameterName = "codigo"
            cmd.Parameters.Add(paramcodigo)
            Dim paramnombre As DbParameter = cmd.CreateParameter()
            paramnombre.Value = saih_pcd.nombre
            paramnombre.ParameterName = "nombre"
            cmd.Parameters.Add(paramnombre)
            Dim paramncomplejidad As DbParameter = cmd.CreateParameter()
            paramncomplejidad.Value = saih_pcd.ncomplejidad
            paramncomplejidad.ParameterName = "ncomplejidad"
            cmd.Parameters.Add(paramncomplejidad)
            Dim paramcencosto As DbParameter = cmd.CreateParameter()
            paramcencosto.Value = saih_pcd.cencosto
            paramcencosto.ParameterName = "cencosto"
            cmd.Parameters.Add(paramcencosto)
            Dim paramaplsex As DbParameter = cmd.CreateParameter()
            paramaplsex.Value = saih_pcd.aplsex
            paramaplsex.ParameterName = "aplsex"
            cmd.Parameters.Add(paramaplsex)
            Dim paramapledadinicio As DbParameter = cmd.CreateParameter()
            paramapledadinicio.Value = saih_pcd.apledadinicio
            paramapledadinicio.ParameterName = "apledadinicio"
            cmd.Parameters.Add(paramapledadinicio)
            Dim paramapledadfin As DbParameter = cmd.CreateParameter()
            paramapledadfin.Value = saih_pcd.apledadfin
            paramapledadfin.ParameterName = "apledadfin"
            cmd.Parameters.Add(paramapledadfin)
            Dim paramcodgrupoq As DbParameter = cmd.CreateParameter()
            paramcodgrupoq.Value = saih_pcd.codgrupoq
            paramcodgrupoq.ParameterName = "codgrupoq"
            cmd.Parameters.Add(paramcodgrupoq)
            Dim paramvalor As DbParameter = cmd.CreateParameter()
            paramvalor.Value = saih_pcd.valor
            paramvalor.ParameterName = "valor"
            cmd.Parameters.Add(paramvalor)
            Dim paramvalsmv As DbParameter = cmd.CreateParameter()
            paramvalsmv.Value = saih_pcd.valsmv
            paramvalsmv.ParameterName = "valsmv"
            cmd.Parameters.Add(paramvalsmv)
            Dim paramporincremento As DbParameter = cmd.CreateParameter()
            paramporincremento.Value = saih_pcd.porincremento
            paramporincremento.ParameterName = "porincremento"
            cmd.Parameters.Add(paramporincremento)
            Dim paramcobhonmed As DbParameter = cmd.CreateParameter()
            paramcobhonmed.Value = saih_pcd.cobhonmed
            paramcobhonmed.ParameterName = "cobhonmed"
            cmd.Parameters.Add(paramcobhonmed)
            Dim paramcobdersal As DbParameter = cmd.CreateParameter()
            paramcobdersal.Value = saih_pcd.cobdersal
            paramcobdersal.ParameterName = "cobdersal"
            cmd.Parameters.Add(paramcobdersal)
            Dim paramcobmatins As DbParameter = cmd.CreateParameter()
            paramcobmatins.Value = saih_pcd.cobmatins
            paramcobmatins.ParameterName = "cobmatins"
            cmd.Parameters.Add(paramcobmatins)
            Dim paramcobhonayu As DbParameter = cmd.CreateParameter()
            paramcobhonayu.Value = saih_pcd.cobhonayu
            paramcobhonayu.ParameterName = "cobhonayu"
            cmd.Parameters.Add(paramcobhonayu)
            Dim paramcobhonanes As DbParameter = cmd.CreateParameter()
            paramcobhonanes.Value = saih_pcd.cobhonanes
            paramcobhonanes.ParameterName = "cobhonanes"
            cmd.Parameters.Add(paramcobhonanes)
            Dim paramconhonper As DbParameter = cmd.CreateParameter()
            paramconhonper.Value = saih_pcd.conhonper
            paramconhonper.ParameterName = "conhonper"
            cmd.Parameters.Add(paramconhonper)
            Dim paramcodunif As DbParameter = cmd.CreateParameter()
            paramcodunif.Value = saih_pcd.codunif
            paramcodunif.ParameterName = "codunif"
            cmd.Parameters.Add(paramcodunif)
            Dim paramcodfp As DbParameter = cmd.CreateParameter()
            paramcodfp.Value = saih_pcd.codfp
            paramcodfp.ParameterName = "codfp"
            cmd.Parameters.Add(paramcodfp)
            Dim paramobservaciones As DbParameter = cmd.CreateParameter()
            paramobservaciones.Value = saih_pcd.observaciones
            paramobservaciones.ParameterName = "observaciones"
            cmd.Parameters.Add(paramobservaciones)
            Dim paramcodser As DbParameter = cmd.CreateParameter()
            paramcodser.Value = saih_pcd.codser
            paramcodser.ParameterName = "codser"
            cmd.Parameters.Add(paramcodser)
            Dim Dr As DbDataReader = cmd.ExecuteReader()
            While Dr.Read
                dato = Dr(0)
            End While
        Catch ex As Exception
            Ext.Net.X.Msg.Alert("Información", result).Show()
        Finally
            desconecta(cnn)
        End Try
      Return dato
  End Function

  Public Function Editar(ByVal saih_pcd As saih_pcd)
      Dim parametros As New List(Of DbParameter)
      Dim parammanual As DbParameter = cmd.CreateParameter()
      parammanual.Value = saih_pcd.manual
      parammanual.ParameterName = "manual"
      parametros.Add(parammanual)
      Dim paramcodigo As DbParameter = cmd.CreateParameter()
      paramcodigo.Value = saih_pcd.codigo
      paramcodigo.ParameterName = "codigo"
      parametros.Add(paramcodigo)
      Dim paramnombre As DbParameter = cmd.CreateParameter()
      paramnombre.Value = saih_pcd.nombre
      paramnombre.ParameterName = "nombre"
      parametros.Add(paramnombre)
      Dim paramncomplejidad As DbParameter = cmd.CreateParameter()
      paramncomplejidad.Value = saih_pcd.ncomplejidad
      paramncomplejidad.ParameterName = "ncomplejidad"
      parametros.Add(paramncomplejidad)
      Dim paramcencosto As DbParameter = cmd.CreateParameter()
      paramcencosto.Value = saih_pcd.cencosto
      paramcencosto.ParameterName = "cencosto"
      parametros.Add(paramcencosto)
      Dim paramaplsex As DbParameter = cmd.CreateParameter()
      paramaplsex.Value = saih_pcd.aplsex
      paramaplsex.ParameterName = "aplsex"
      parametros.Add(paramaplsex)
      Dim paramapledadinicio As DbParameter = cmd.CreateParameter()
      paramapledadinicio.Value = saih_pcd.apledadinicio
      paramapledadinicio.ParameterName = "apledadinicio"
      parametros.Add(paramapledadinicio)
      Dim paramapledadfin As DbParameter = cmd.CreateParameter()
      paramapledadfin.Value = saih_pcd.apledadfin
      paramapledadfin.ParameterName = "apledadfin"
      parametros.Add(paramapledadfin)
      Dim paramcodgrupoq As DbParameter = cmd.CreateParameter()
      paramcodgrupoq.Value = saih_pcd.codgrupoq
      paramcodgrupoq.ParameterName = "codgrupoq"
      parametros.Add(paramcodgrupoq)
      Dim paramvalor As DbParameter = cmd.CreateParameter()
      paramvalor.Value = saih_pcd.valor
      paramvalor.ParameterName = "valor"
      parametros.Add(paramvalor)
      Dim paramvalsmv As DbParameter = cmd.CreateParameter()
      paramvalsmv.Value = saih_pcd.valsmv
      paramvalsmv.ParameterName = "valsmv"
      parametros.Add(paramvalsmv)
      Dim paramporincremento As DbParameter = cmd.CreateParameter()
      paramporincremento.Value = saih_pcd.porincremento
      paramporincremento.ParameterName = "porincremento"
      parametros.Add(paramporincremento)
      Dim paramcobhonmed As DbParameter = cmd.CreateParameter()
      paramcobhonmed.Value = saih_pcd.cobhonmed
      paramcobhonmed.ParameterName = "cobhonmed"
      parametros.Add(paramcobhonmed)
      Dim paramcobdersal As DbParameter = cmd.CreateParameter()
      paramcobdersal.Value = saih_pcd.cobdersal
      paramcobdersal.ParameterName = "cobdersal"
      parametros.Add(paramcobdersal)
      Dim paramcobmatins As DbParameter = cmd.CreateParameter()
      paramcobmatins.Value = saih_pcd.cobmatins
      paramcobmatins.ParameterName = "cobmatins"
      parametros.Add(paramcobmatins)
      Dim paramcobhonayu As DbParameter = cmd.CreateParameter()
      paramcobhonayu.Value = saih_pcd.cobhonayu
      paramcobhonayu.ParameterName = "cobhonayu"
      parametros.Add(paramcobhonayu)
      Dim paramcobhonanes As DbParameter = cmd.CreateParameter()
      paramcobhonanes.Value = saih_pcd.cobhonanes
      paramcobhonanes.ParameterName = "cobhonanes"
      parametros.Add(paramcobhonanes)
      Dim paramconhonper As DbParameter = cmd.CreateParameter()
      paramconhonper.Value = saih_pcd.conhonper
      paramconhonper.ParameterName = "conhonper"
      parametros.Add(paramconhonper)
      Dim paramcodunif As DbParameter = cmd.CreateParameter()
      paramcodunif.Value = saih_pcd.codunif
      paramcodunif.ParameterName = "codunif"
      parametros.Add(paramcodunif)
      Dim paramcodfp As DbParameter = cmd.CreateParameter()
      paramcodfp.Value = saih_pcd.codfp
      paramcodfp.ParameterName = "codfp"
      parametros.Add(paramcodfp)
      Dim paramobservaciones As DbParameter = cmd.CreateParameter()
      paramobservaciones.Value = saih_pcd.observaciones
      paramobservaciones.ParameterName = "observaciones"
      parametros.Add(paramobservaciones)
      Dim paramcodser As DbParameter = cmd.CreateParameter()
      paramcodser.Value = saih_pcd.codser
      paramcodser.ParameterName = "codser"
      parametros.Add(paramcodser)
      Return ejecutaNonQuery("cambiarsaih_pcd", parametros)
  End Function

  Public Function Borrar(
      ByVal manual As string,
      ByVal codigo As string)
      Dim parametros As New List(Of DbParameter)
      Dim parammanual As DbParameter = cmd.CreateParameter()
      parammanual.Value = manual
      parammanual.ParameterName = "manual"
      parametros.Add(parammanual)
      Dim paramcodigo As DbParameter = cmd.CreateParameter()
      paramcodigo.Value = codigo
      paramcodigo.ParameterName = "codigo"
      parametros.Add(paramcodigo)
      Return ejecutaNonQuery("eliminasaih_pcd", parametros)
  End Function

  Public Function Select_Where_saih_pcd(ByVal Cond As String) As List(Of saih_pcd)
        Dim Listsaih_pcd As New List(Of saih_pcd)
        Dim appSettings = ConfigurationManager.AppSettings
        Dim result As String = appSettings("021")

      Try
          cnn = conecta()
          cmd = New SqlCommand("obtenersaih_pcdByWhere")
          cmd.CommandType = CommandType.StoredProcedure
          cmd.Connection = cnn
          Dim param As DbParameter = cmd.CreateParameter()
          param.Value = Cond
          param.ParameterName = "where"
          cmd.Parameters.Add(param)
          Dim Dr As DbDataReader = cmd.ExecuteReader()
          While Dr.Read
              Listsaih_pcd.Add(New saih_pcd(
                  Dr("manual"),
                  Dr("codigo"),
                  Dr("nombre"),
                  Dr("ncomplejidad"),
                  Dr("cencosto"),
                  Dr("aplsex"),
                  Dr("apledadinicio"),
                  Dr("apledadfin"),
                  Dr("codgrupoq"),
                  Dr("valor"),
                  Dr("valsmv"),
                  Dr("porincremento"),
                  Dr("cobhonmed"),
                  Dr("cobdersal"),
                  Dr("cobmatins"),
                  Dr("cobhonayu"),
                  Dr("cobhonanes"),
                  Dr("conhonper"),
                  Dr("codunif"),
                  Dr("codfp"),
                  Dr("observaciones"),
                  Dr("codser")))
          End While
      Catch ex As Exception
            Ext.Net.X.Msg.Alert("Información", result).Show()
      Finally
          desconecta(cnn)
      End Try
      Return Listsaih_pcd
  End Function

  Public Function Select_ById_saih_pcd(
      ByVal manual As string,
      ByVal codigo As string) As List(Of saih_pcd)
        Dim Listsaih_pcd As New List(Of saih_pcd)
        Dim appSettings = ConfigurationManager.AppSettings
        Dim result As String = appSettings("021")

      Try
          cnn = conecta()
          cmd = New SqlCommand("obtenersaih_pcdById")
          cmd.CommandType = CommandType.StoredProcedure
          cmd.Connection = cnn
      Dim parammanual As DbParameter = cmd.CreateParameter()
      parammanual.Value = manual
      parammanual.ParameterName = "manual"
      cmd.Parameters.Add(parammanual)
      Dim paramcodigo As DbParameter = cmd.CreateParameter()
      paramcodigo.Value = codigo
      paramcodigo.ParameterName = "codigo"
      cmd.Parameters.Add(paramcodigo)
          Dim Dr As DbDataReader = cmd.ExecuteReader()
          While Dr.Read
              Listsaih_pcd.Add(New saih_pcd(
                  Dr("manual"),
                  Dr("codigo"),
                  Dr("nombre"),
                  Dr("ncomplejidad"),
                  Dr("cencosto"),
                  Dr("aplsex"),
                  Dr("apledadinicio"),
                  Dr("apledadfin"),
                  Dr("codgrupoq"),
                  Dr("valor"),
                  Dr("valsmv"),
                  Dr("porincremento"),
                  Dr("cobhonmed"),
                  Dr("cobdersal"),
                  Dr("cobmatins"),
                  Dr("cobhonayu"),
                  Dr("cobhonanes"),
                  Dr("conhonper"),
                  Dr("codunif"),
                  Dr("codfp"),
                  Dr("observaciones"),
                  Dr("codser")))
          End While
      Catch ex As Exception
            Ext.Net.X.Msg.Alert("Información", result).Show()
      Finally
          desconecta(cnn)
      End Try
      Return Listsaih_pcd
  End Function

  Public Function Select_saih_pcd() As List(Of saih_pcd)
        Dim Listsaih_pcd As New List(Of saih_pcd)
        Dim appSettings = ConfigurationManager.AppSettings
        Dim result As String = appSettings("021")

      Try
         cnn = conecta()
          cmd = New SqlCommand("obtenersaih_pcd")
          cmd.CommandType = CommandType.StoredProcedure
          cmd.Connection = cnn
          Dim Dr As DbDataReader = cmd.ExecuteReader()
          While Dr.Read
              Listsaih_pcd.Add(New saih_pcd(
                  Dr("manual"),
                  Dr("codigo"),
                  Dr("nombre"),
                  Dr("ncomplejidad"),
                  Dr("cencosto"),
                  Dr("aplsex"),
                  Dr("apledadinicio"),
                  Dr("apledadfin"),
                  Dr("codgrupoq"),
                  Dr("valor"),
                  Dr("valsmv"),
                  Dr("porincremento"),
                  Dr("cobhonmed"),
                  Dr("cobdersal"),
                  Dr("cobmatins"),
                  Dr("cobhonayu"),
                  Dr("cobhonanes"),
                  Dr("conhonper"),
                  Dr("codunif"),
                  Dr("codfp"),
                  Dr("observaciones"),
                  Dr("codser")))
          End While
      Catch ex As Exception
            Ext.Net.X.Msg.Alert("Información", result).Show()
      Finally
          desconecta(cnn)
      End Try
      Return Listsaih_pcd
  End Function

End Class
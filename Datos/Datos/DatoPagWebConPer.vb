Imports System.Data
Imports System.Data.SqlClient
Imports System.Data.Common
Imports System.Configuration

Public Class DatoPagWebConPer
   Inherits conexion

  Public Function Insertar(ByVal PagWebConPer As PagWebConPer)
      Dim parametros As New List(Of DbParameter)
      Dim paramid_PaginaWebControl As DbParameter = cmd.CreateParameter()
      paramid_PaginaWebControl.Value = PagWebConPer.id_PaginaWebControl
      paramid_PaginaWebControl.ParameterName = "id_PaginaWebControl"
      parametros.Add(paramid_PaginaWebControl)
      Dim paramidperfil As DbParameter = cmd.CreateParameter()
      paramidperfil.Value = PagWebConPer.idperfil
      paramidperfil.ParameterName = "idperfil"
      parametros.Add(paramidperfil)
      Dim paramaccion As DbParameter = cmd.CreateParameter()
      paramaccion.Value = PagWebConPer.accion
      paramaccion.ParameterName = "accion"
      parametros.Add(paramaccion)
      Return ejecutaNonQuery("insertaPagWebConPer", parametros)
  End Function

  Public Function Editar(ByVal PagWebConPer As PagWebConPer)
      Dim parametros As New List(Of DbParameter)
      Dim paramid As DbParameter = cmd.CreateParameter()
      paramid.Value = PagWebConPer.id
      paramid.ParameterName = "id"
      parametros.Add(paramid)
      Dim paramid_PaginaWebControl As DbParameter = cmd.CreateParameter()
      paramid_PaginaWebControl.Value = PagWebConPer.id_PaginaWebControl
      paramid_PaginaWebControl.ParameterName = "id_PaginaWebControl"
      parametros.Add(paramid_PaginaWebControl)
      Dim paramidperfil As DbParameter = cmd.CreateParameter()
      paramidperfil.Value = PagWebConPer.idperfil
      paramidperfil.ParameterName = "idperfil"
      parametros.Add(paramidperfil)
      Dim paramaccion As DbParameter = cmd.CreateParameter()
      paramaccion.Value = PagWebConPer.accion
      paramaccion.ParameterName = "accion"
      parametros.Add(paramaccion)
      Return ejecutaNonQuery("cambiarPagWebConPer", parametros)
  End Function

  Public Function Borrar(
      ByVal id As integer)
      Dim parametros As New List(Of DbParameter)
      Dim paramid As DbParameter = cmd.CreateParameter()
      paramid.Value = id
      paramid.ParameterName = "id"
      parametros.Add(paramid)
      Return ejecutaNonQuery("eliminaPagWebConPer", parametros)
  End Function

  Public Function Select_Where_PagWebConPer(ByVal Cond As String) As List(Of PagWebConPer)
      Dim ListPagWebConPer As New List(Of PagWebConPer)
        Dim appSettings = ConfigurationManager.AppSettings
        Dim result As String = appSettings("040")

        Try
            cnn = conecta()
            cmd = New SqlCommand("obtenerPagWebConPerByWhere")
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Connection = cnn
            Dim param As DbParameter = cmd.CreateParameter()
            param.Value = Cond
            param.ParameterName = "where"
            cmd.Parameters.Add(param)
            Dim Dr As DbDataReader = cmd.ExecuteReader()
            While Dr.Read
                ListPagWebConPer.Add(New PagWebConPer(
                    Dr("id"),
                    Dr("id_PaginaWebControl"),
                    Dr("idperfil"),
                    Dr("accion")))
            End While
        Catch ex As Exception
            Ext.Net.X.Msg.Alert("Información", result).Show()
        Finally
            desconecta(cnn)
        End Try
      Return ListPagWebConPer
  End Function

  Public Function Select_ById_PagWebConPer(
      ByVal id As integer) As List(Of PagWebConPer)
        Dim ListPagWebConPer As New List(Of PagWebConPer)
        Dim appSettings = ConfigurationManager.AppSettings
        Dim result As String = appSettings("040")

      Try
          cnn = conecta()
          cmd = New SqlCommand("obtenerPagWebConPerById")
          cmd.CommandType = CommandType.StoredProcedure
          cmd.Connection = cnn
      Dim paramid As DbParameter = cmd.CreateParameter()
      paramid.Value = id
      paramid.ParameterName = "id"
      cmd.Parameters.Add(paramid)
          Dim Dr As DbDataReader = cmd.ExecuteReader()
          While Dr.Read
              ListPagWebConPer.Add(New PagWebConPer(
                  Dr("id"),
                  Dr("id_PaginaWebControl"),
                  Dr("idperfil"),
                  Dr("accion")))
          End While
      Catch ex As Exception
            Ext.Net.X.Msg.Alert("Información", result).Show()
      Finally
          desconecta(cnn)
      End Try
      Return ListPagWebConPer
  End Function

  Public Function Select_PagWebConPer() As List(Of PagWebConPer)
        Dim ListPagWebConPer As New List(Of PagWebConPer)
        Dim appSettings = ConfigurationManager.AppSettings
        Dim result As String = appSettings("040")

      Try
         cnn = conecta()
          cmd = New SqlCommand("obtenerPagWebConPer")
          cmd.CommandType = CommandType.StoredProcedure
          cmd.Connection = cnn
          Dim Dr As DbDataReader = cmd.ExecuteReader()
          While Dr.Read
              ListPagWebConPer.Add(New PagWebConPer(
                  Dr("id"),
                  Dr("id_PaginaWebControl"),
                  Dr("idperfil"),
                  Dr("accion")))
          End While
      Catch ex As Exception
            Ext.Net.X.Msg.Alert("Información", result).Show()
      Finally
          desconecta(cnn)
      End Try
      Return ListPagWebConPer
  End Function

End Class
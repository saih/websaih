Imports System.Data
Imports System.Data.SqlClient
Imports System.Data.Common
Imports System.Configuration

Public Class Datosaih_paser
   Inherits conexion

  Public Function Insertar(ByVal saih_paser As saih_paser)
      Dim parametros As New List(Of DbParameter)
      Dim paramcodpas As DbParameter = cmd.CreateParameter()
      paramcodpas.Value = saih_paser.codpas
      paramcodpas.ParameterName = "codpas"
      parametros.Add(paramcodpas)
      Dim paramcodser As DbParameter = cmd.CreateParameter()
      paramcodser.Value = saih_paser.codser
      paramcodser.ParameterName = "codser"
      parametros.Add(paramcodser)
      Return ejecutaNonQuery("insertasaih_paser", parametros)
  End Function

  Public Function InsertarRet(ByVal saih_paser As saih_paser)
        Dim dato As Integer = 0
        Dim appSettings = ConfigurationManager.AppSettings
        Dim result As String = appSettings("044")

      Try
          cnn = conecta()
          cmd = New SqlCommand("insertasaih_paser")
          cmd.CommandType = CommandType.StoredProcedure
          cmd.Connection = cnn
        Dim paramcodpas As DbParameter = cmd.CreateParameter()
        paramcodpas.Value = saih_paser.codpas
        paramcodpas.ParameterName = "codpas"
        cmd.Parameters.Add(paramcodpas)
        Dim paramcodser As DbParameter = cmd.CreateParameter()
        paramcodser.Value = saih_paser.codser
        paramcodser.ParameterName = "codser"
        cmd.Parameters.Add(paramcodser)
          Dim Dr As DbDataReader = cmd.ExecuteReader()
          While Dr.Read
            dato = Dr(0)
          End While
      Catch ex As Exception
            Ext.Net.X.Msg.Alert("Información", result).Show()
      Finally
          desconecta(cnn)
      End Try
      Return dato
  End Function

  Public Function Editar(ByVal saih_paser As saih_paser)
      Dim parametros As New List(Of DbParameter)
      Dim paramcodpas As DbParameter = cmd.CreateParameter()
      paramcodpas.Value = saih_paser.codpas
      paramcodpas.ParameterName = "codpas"
      parametros.Add(paramcodpas)
      Dim paramcodser As DbParameter = cmd.CreateParameter()
      paramcodser.Value = saih_paser.codser
      paramcodser.ParameterName = "codser"
      parametros.Add(paramcodser)
      Return ejecutaNonQuery("cambiarsaih_paser", parametros)
  End Function

  Public Function Borrar(
      ByVal codpas As string,
      ByVal codser As string)
      Dim parametros As New List(Of DbParameter)
      Dim paramcodpas As DbParameter = cmd.CreateParameter()
      paramcodpas.Value = codpas
      paramcodpas.ParameterName = "codpas"
      parametros.Add(paramcodpas)
      Dim paramcodser As DbParameter = cmd.CreateParameter()
      paramcodser.Value = codser
      paramcodser.ParameterName = "codser"
      parametros.Add(paramcodser)
      Return ejecutaNonQuery("eliminasaih_paser", parametros)
  End Function

  Public Function Select_Where_saih_paser(ByVal Cond As String) As List(Of saih_paser)
        Dim Listsaih_paser As New List(Of saih_paser)
        Dim appSettings = ConfigurationManager.AppSettings
        Dim result As String = appSettings("045")

      Try
          cnn = conecta()
          cmd = New SqlCommand("obtenersaih_paserByWhere")
          cmd.CommandType = CommandType.StoredProcedure
          cmd.Connection = cnn
          Dim param As DbParameter = cmd.CreateParameter()
          param.Value = Cond
          param.ParameterName = "where"
          cmd.Parameters.Add(param)
          Dim Dr As DbDataReader = cmd.ExecuteReader()
          While Dr.Read
              Listsaih_paser.Add(New saih_paser(
                  Dr("codpas"),
                  Dr("codser")))
          End While
      Catch ex As Exception
            Ext.Net.X.Msg.Alert("Información", result).Show()
      Finally
          desconecta(cnn)
      End Try
      Return Listsaih_paser
  End Function

  Public Function Select_ById_saih_paser(
      ByVal codpas As string,
      ByVal codser As string) As List(Of saih_paser)
        Dim Listsaih_paser As New List(Of saih_paser)
        Dim appSettings = ConfigurationManager.AppSettings
        Dim result As String = appSettings("045")

      Try
          cnn = conecta()
          cmd = New SqlCommand("obtenersaih_paserById")
          cmd.CommandType = CommandType.StoredProcedure
          cmd.Connection = cnn
      Dim paramcodpas As DbParameter = cmd.CreateParameter()
      paramcodpas.Value = codpas
      paramcodpas.ParameterName = "codpas"
      cmd.Parameters.Add(paramcodpas)
      Dim paramcodser As DbParameter = cmd.CreateParameter()
      paramcodser.Value = codser
      paramcodser.ParameterName = "codser"
      cmd.Parameters.Add(paramcodser)
          Dim Dr As DbDataReader = cmd.ExecuteReader()
          While Dr.Read
              Listsaih_paser.Add(New saih_paser(
                  Dr("codpas"),
                  Dr("codser")))
          End While
      Catch ex As Exception
            Ext.Net.X.Msg.Alert("Información", result).Show()
      Finally
          desconecta(cnn)
      End Try
      Return Listsaih_paser
  End Function

  Public Function Select_saih_paser() As List(Of saih_paser)
        Dim Listsaih_paser As New List(Of saih_paser)
        Dim appSettings = ConfigurationManager.AppSettings
        Dim result As String = appSettings("045")

      Try
         cnn = conecta()
          cmd = New SqlCommand("obtenersaih_paser")
          cmd.CommandType = CommandType.StoredProcedure
          cmd.Connection = cnn
          Dim Dr As DbDataReader = cmd.ExecuteReader()
          While Dr.Read
              Listsaih_paser.Add(New saih_paser(
                  Dr("codpas"),
                  Dr("codser")))
          End While
      Catch ex As Exception
            Ext.Net.X.Msg.Alert("Información", result).Show()
      Finally
          desconecta(cnn)
      End Try
      Return Listsaih_paser
  End Function

End Class
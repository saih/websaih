Imports System.Data
Imports System.Data.SqlClient
Imports System.Data.Common
Imports System.Configuration

Public Class Datosaih_sal
   Inherits conexion

  Public Function Insertar(ByVal saih_sal As saih_sal)
      Dim parametros As New List(Of DbParameter)
      Dim paramid_prm As DbParameter = cmd.CreateParameter()
      paramid_prm.Value = saih_sal.id_prm
      paramid_prm.ParameterName = "id_prm"
      parametros.Add(paramid_prm)
      Dim paramfecha_inicio As DbParameter = cmd.CreateParameter()
      paramfecha_inicio.Value = saih_sal.fecha_inicio
      paramfecha_inicio.ParameterName = "fecha_inicio"
      parametros.Add(paramfecha_inicio)
      Dim paramfecha_fin As DbParameter = cmd.CreateParameter()
      paramfecha_fin.Value = saih_sal.fecha_fin
      paramfecha_fin.ParameterName = "fecha_fin"
      parametros.Add(paramfecha_fin)
      Dim paramestado As DbParameter = cmd.CreateParameter()
      paramestado.Value = saih_sal.estado
      paramestado.ParameterName = "estado"
      parametros.Add(paramestado)
      Dim paramsalario As DbParameter = cmd.CreateParameter()
      paramsalario.Value = saih_sal.salario
      paramsalario.ParameterName = "salario"
      parametros.Add(paramsalario)
      Dim paramobservaciones As DbParameter = cmd.CreateParameter()
      paramobservaciones.Value = saih_sal.observaciones
      paramobservaciones.ParameterName = "observaciones"
      parametros.Add(paramobservaciones)
      Return ejecutaNonQuery("insertasaih_sal", parametros)
  End Function

  Public Function InsertarRet(ByVal saih_sal As saih_sal)
      Dim dato As Integer = 0
        Dim appSettings = ConfigurationManager.AppSettings
        Dim result As String = appSettings("016")

        Try
            cnn = conecta()
            cmd = New SqlCommand("insertasaih_sal")
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Connection = cnn
            Dim paramid_prm As DbParameter = cmd.CreateParameter()
            paramid_prm.Value = saih_sal.id_prm
            paramid_prm.ParameterName = "id_prm"
            cmd.Parameters.Add(paramid_prm)
            Dim paramfecha_inicio As DbParameter = cmd.CreateParameter()
            paramfecha_inicio.Value = saih_sal.fecha_inicio
            paramfecha_inicio.ParameterName = "fecha_inicio"
            cmd.Parameters.Add(paramfecha_inicio)
            Dim paramfecha_fin As DbParameter = cmd.CreateParameter()
            paramfecha_fin.Value = saih_sal.fecha_fin
            paramfecha_fin.ParameterName = "fecha_fin"
            cmd.Parameters.Add(paramfecha_fin)
            Dim paramestado As DbParameter = cmd.CreateParameter()
            paramestado.Value = saih_sal.estado
            paramestado.ParameterName = "estado"
            cmd.Parameters.Add(paramestado)
            Dim paramsalario As DbParameter = cmd.CreateParameter()
            paramsalario.Value = saih_sal.salario
            paramsalario.ParameterName = "salario"
            cmd.Parameters.Add(paramsalario)
            Dim paramobservaciones As DbParameter = cmd.CreateParameter()
            paramobservaciones.Value = saih_sal.observaciones
            paramobservaciones.ParameterName = "observaciones"
            cmd.Parameters.Add(paramobservaciones)
            Dim Dr As DbDataReader = cmd.ExecuteReader()
            While Dr.Read
                dato = Dr(0)
            End While
        Catch ex As Exception
            Ext.Net.X.Msg.Alert("Información", result).Show()
        Finally
            desconecta(cnn)
        End Try
      Return dato
  End Function

  Public Function Editar(ByVal saih_sal As saih_sal)
      Dim parametros As New List(Of DbParameter)
      Dim paramid As DbParameter = cmd.CreateParameter()
      paramid.Value = saih_sal.id
      paramid.ParameterName = "id"
      parametros.Add(paramid)
      Dim paramid_prm As DbParameter = cmd.CreateParameter()
      paramid_prm.Value = saih_sal.id_prm
      paramid_prm.ParameterName = "id_prm"
      parametros.Add(paramid_prm)
      Dim paramfecha_inicio As DbParameter = cmd.CreateParameter()
      paramfecha_inicio.Value = saih_sal.fecha_inicio
      paramfecha_inicio.ParameterName = "fecha_inicio"
      parametros.Add(paramfecha_inicio)
      Dim paramfecha_fin As DbParameter = cmd.CreateParameter()
      paramfecha_fin.Value = saih_sal.fecha_fin
      paramfecha_fin.ParameterName = "fecha_fin"
      parametros.Add(paramfecha_fin)
      Dim paramestado As DbParameter = cmd.CreateParameter()
      paramestado.Value = saih_sal.estado
      paramestado.ParameterName = "estado"
      parametros.Add(paramestado)
      Dim paramsalario As DbParameter = cmd.CreateParameter()
      paramsalario.Value = saih_sal.salario
      paramsalario.ParameterName = "salario"
      parametros.Add(paramsalario)
      Dim paramobservaciones As DbParameter = cmd.CreateParameter()
      paramobservaciones.Value = saih_sal.observaciones
      paramobservaciones.ParameterName = "observaciones"
      parametros.Add(paramobservaciones)
      Return ejecutaNonQuery("cambiarsaih_sal", parametros)
  End Function

  Public Function Borrar(
      ByVal id As integer)
      Dim parametros As New List(Of DbParameter)
      Dim paramid As DbParameter = cmd.CreateParameter()
      paramid.Value = id
      paramid.ParameterName = "id"
      parametros.Add(paramid)
      Return ejecutaNonQuery("eliminasaih_sal", parametros)
  End Function

  Public Function Select_Where_saih_sal(ByVal Cond As String) As List(Of saih_sal)
        Dim Listsaih_sal As New List(Of saih_sal)
        Dim appSettings = ConfigurationManager.AppSettings
        Dim result As String = appSettings("017")

      Try
          cnn = conecta()
          cmd = New SqlCommand("obtenersaih_salByWhere")
          cmd.CommandType = CommandType.StoredProcedure
          cmd.Connection = cnn
          Dim param As DbParameter = cmd.CreateParameter()
          param.Value = Cond
          param.ParameterName = "where"
          cmd.Parameters.Add(param)
          Dim Dr As DbDataReader = cmd.ExecuteReader()
          While Dr.Read
              Listsaih_sal.Add(New saih_sal(
                  Dr("id"),
                  Dr("id_prm"),
                  Dr("fecha_inicio"),
                  Dr("fecha_fin"),
                  Dr("estado"),
                  Dr("salario"),
                  Dr("observaciones")))
          End While
      Catch ex As Exception
            Ext.Net.X.Msg.Alert("Información", result).Show()
      Finally
          desconecta(cnn)
      End Try
      Return Listsaih_sal
  End Function

  Public Function Select_ById_saih_sal(
      ByVal id As integer) As List(Of saih_sal)
        Dim Listsaih_sal As New List(Of saih_sal)
        Dim appSettings = ConfigurationManager.AppSettings
        Dim result As String = appSettings("017")

      Try
          cnn = conecta()
          cmd = New SqlCommand("obtenersaih_salById")
          cmd.CommandType = CommandType.StoredProcedure
          cmd.Connection = cnn
      Dim paramid As DbParameter = cmd.CreateParameter()
      paramid.Value = id
      paramid.ParameterName = "id"
      cmd.Parameters.Add(paramid)
          Dim Dr As DbDataReader = cmd.ExecuteReader()
          While Dr.Read
              Listsaih_sal.Add(New saih_sal(
                  Dr("id"),
                  Dr("id_prm"),
                  Dr("fecha_inicio"),
                  Dr("fecha_fin"),
                  Dr("estado"),
                  Dr("salario"),
                  Dr("observaciones")))
          End While
      Catch ex As Exception
            Ext.Net.X.Msg.Alert("Información", result).Show()
      Finally
          desconecta(cnn)
      End Try
      Return Listsaih_sal
  End Function

  Public Function Select_saih_sal() As List(Of saih_sal)
        Dim Listsaih_sal As New List(Of saih_sal)
        Dim appSettings = ConfigurationManager.AppSettings
        Dim result As String = appSettings("017")

      Try
         cnn = conecta()
          cmd = New SqlCommand("obtenersaih_sal")
          cmd.CommandType = CommandType.StoredProcedure
          cmd.Connection = cnn
          Dim Dr As DbDataReader = cmd.ExecuteReader()
          While Dr.Read
              Listsaih_sal.Add(New saih_sal(
                  Dr("id"),
                  Dr("id_prm"),
                  Dr("fecha_inicio"),
                  Dr("fecha_fin"),
                  Dr("estado"),
                  Dr("salario"),
                  Dr("observaciones")))
          End While
      Catch ex As Exception
            Ext.Net.X.Msg.Alert("Información", result).Show()
      Finally
          desconecta(cnn)
      End Try
      Return Listsaih_sal
  End Function

End Class
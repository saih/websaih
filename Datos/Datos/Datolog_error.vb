Imports System.Data
Imports System.Data.SqlClient
Imports System.Data.Common

Public Class Datolog_error
   Inherits conexion

  Public Function Insertar(ByVal log_error As log_error)
      Dim parametros As New List(Of DbParameter)
      Dim paramtipo As DbParameter = cmd.CreateParameter()
      paramtipo.Value = log_error.tipo
      paramtipo.ParameterName = "tipo"
      parametros.Add(paramtipo)
      Dim paramdescripcion As DbParameter = cmd.CreateParameter()
      paramdescripcion.Value = log_error.descripcion
      paramdescripcion.ParameterName = "descripcion"
      parametros.Add(paramdescripcion)
      Dim paramfecha As DbParameter = cmd.CreateParameter()
      paramfecha.Value = log_error.fecha
      paramfecha.ParameterName = "fecha"
      parametros.Add(paramfecha)
      Dim paramusuario As DbParameter = cmd.CreateParameter()
      paramusuario.Value = log_error.usuario
      paramusuario.ParameterName = "usuario"
      parametros.Add(paramusuario)
      Return ejecutaNonQuery("insertalog_error", parametros)
  End Function

  Public Function InsertarRet(ByVal log_error As log_error)
      Dim dato As Integer = 0
      Try
          cnn = conecta()
          cmd = New SqlCommand("insertalog_error")
          cmd.CommandType = CommandType.StoredProcedure
          cmd.Connection = cnn
        Dim paramtipo As DbParameter = cmd.CreateParameter()
        paramtipo.Value = log_error.tipo
        paramtipo.ParameterName = "tipo"
        cmd.Parameters.Add(paramtipo)
        Dim paramdescripcion As DbParameter = cmd.CreateParameter()
        paramdescripcion.Value = log_error.descripcion
        paramdescripcion.ParameterName = "descripcion"
        cmd.Parameters.Add(paramdescripcion)
        Dim paramfecha As DbParameter = cmd.CreateParameter()
        paramfecha.Value = log_error.fecha
        paramfecha.ParameterName = "fecha"
        cmd.Parameters.Add(paramfecha)
        Dim paramusuario As DbParameter = cmd.CreateParameter()
        paramusuario.Value = log_error.usuario
        paramusuario.ParameterName = "usuario"
        cmd.Parameters.Add(paramusuario)
          Dim Dr As DbDataReader = cmd.ExecuteReader()
          While Dr.Read
            dato = Dr(0)
          End While
      Catch ex As Exception
          Ext.Net.X.Msg.Alert("Información", ex.Message).Show()
      Finally
          desconecta(cnn)
      End Try
      Return dato
  End Function

  Public Function Editar(ByVal log_error As log_error)
      Dim parametros As New List(Of DbParameter)
      Dim paramid As DbParameter = cmd.CreateParameter()
      paramid.Value = log_error.id
      paramid.ParameterName = "id"
      parametros.Add(paramid)
      Dim paramtipo As DbParameter = cmd.CreateParameter()
      paramtipo.Value = log_error.tipo
      paramtipo.ParameterName = "tipo"
      parametros.Add(paramtipo)
      Dim paramdescripcion As DbParameter = cmd.CreateParameter()
      paramdescripcion.Value = log_error.descripcion
      paramdescripcion.ParameterName = "descripcion"
      parametros.Add(paramdescripcion)
      Dim paramfecha As DbParameter = cmd.CreateParameter()
      paramfecha.Value = log_error.fecha
      paramfecha.ParameterName = "fecha"
      parametros.Add(paramfecha)
      Dim paramusuario As DbParameter = cmd.CreateParameter()
      paramusuario.Value = log_error.usuario
      paramusuario.ParameterName = "usuario"
      parametros.Add(paramusuario)
      Return ejecutaNonQuery("cambiarlog_error", parametros)
  End Function

  Public Function Borrar(
      ByVal id As integer)
      Dim parametros As New List(Of DbParameter)
      Dim paramid As DbParameter = cmd.CreateParameter()
      paramid.Value = id
      paramid.ParameterName = "id"
      parametros.Add(paramid)
      Return ejecutaNonQuery("eliminalog_error", parametros)
  End Function

  Public Function Select_Where_log_error(ByVal Cond As String) As List(Of log_error)
      Dim Listlog_error As New List(Of log_error)
      Try
          cnn = conecta()
          cmd = New SqlCommand("obtenerlog_errorByWhere")
          cmd.CommandType = CommandType.StoredProcedure
          cmd.Connection = cnn
          Dim param As DbParameter = cmd.CreateParameter()
          param.Value = Cond
          param.ParameterName = "where"
          cmd.Parameters.Add(param)
          Dim Dr As DbDataReader = cmd.ExecuteReader()
          While Dr.Read
              Listlog_error.Add(New log_error(
                  Dr("id"),
                  Dr("tipo"),
                  Dr("descripcion"),
                  Dr("fecha"),
                  Dr("usuario")))
          End While
      Catch ex As Exception
          Ext.Net.X.Msg.Alert("Información", ex.Message).Show()
      Finally
          desconecta(cnn)
      End Try
      Return Listlog_error
  End Function

  Public Function Select_ById_log_error(
      ByVal id As integer) As List(Of log_error)
      Dim Listlog_error As New List(Of log_error)
      Try
          cnn = conecta()
          cmd = New SqlCommand("obtenerlog_errorById")
          cmd.CommandType = CommandType.StoredProcedure
          cmd.Connection = cnn
      Dim paramid As DbParameter = cmd.CreateParameter()
      paramid.Value = id
      paramid.ParameterName = "id"
      cmd.Parameters.Add(paramid)
          Dim Dr As DbDataReader = cmd.ExecuteReader()
          While Dr.Read
              Listlog_error.Add(New log_error(
                  Dr("id"),
                  Dr("tipo"),
                  Dr("descripcion"),
                  Dr("fecha"),
                  Dr("usuario")))
          End While
      Catch ex As Exception
          Ext.Net.X.Msg.Alert("Información", ex.Message).Show()
      Finally
          desconecta(cnn)
      End Try
      Return Listlog_error
  End Function

  Public Function Select_log_error() As List(Of log_error)
      Dim Listlog_error As New List(Of log_error)
      Try
         cnn = conecta()
          cmd = New SqlCommand("obtenerlog_error")
          cmd.CommandType = CommandType.StoredProcedure
          cmd.Connection = cnn
          Dim Dr As DbDataReader = cmd.ExecuteReader()
          While Dr.Read
              Listlog_error.Add(New log_error(
                  Dr("id"),
                  Dr("tipo"),
                  Dr("descripcion"),
                  Dr("fecha"),
                  Dr("usuario")))
          End While
      Catch ex As Exception
          Ext.Net.X.Msg.Alert("Información", ex.Message).Show()
      Finally
          desconecta(cnn)
      End Try
      Return Listlog_error
  End Function

End Class
Imports System.Data
Imports System.Data.SqlClient
Imports System.Data.Common
Imports System.Configuration

Public Class Datomenu_perfil
   Inherits conexion

  Public Function Insertar(ByVal menu_perfil As menu_perfil)
      Dim parametros As New List(Of DbParameter)
      Dim paramidperfil As DbParameter = cmd.CreateParameter()
      paramidperfil.Value = menu_perfil.idperfil
      paramidperfil.ParameterName = "idperfil"
      parametros.Add(paramidperfil)
      Dim paramdescripcion As DbParameter = cmd.CreateParameter()
      paramdescripcion.Value = menu_perfil.descripcion
      paramdescripcion.ParameterName = "descripcion"
      parametros.Add(paramdescripcion)
      Dim paramPerfil_Csc As DbParameter = cmd.CreateParameter()
      paramPerfil_Csc.Value = menu_perfil.Perfil_Csc
      paramPerfil_Csc.ParameterName = "Perfil_Csc"
      parametros.Add(paramPerfil_Csc)
      Dim parammanAtenciones As DbParameter = cmd.CreateParameter()
      parammanAtenciones.Value = menu_perfil.manAtenciones
      parammanAtenciones.ParameterName = "manAtenciones"
      parametros.Add(parammanAtenciones)
      Return ejecutaNonQuery("insertamenu_perfil", parametros)
  End Function

  Public Function InsertarRet(ByVal menu_perfil As menu_perfil)
        Dim dato As Integer = 0
        Dim appSettings = ConfigurationManager.AppSettings
        Dim result As String = appSettings("043")

      Try
          cnn = conecta()
          cmd = New SqlCommand("insertamenu_perfil")
          cmd.CommandType = CommandType.StoredProcedure
          cmd.Connection = cnn
        Dim paramidperfil As DbParameter = cmd.CreateParameter()
        paramidperfil.Value = menu_perfil.idperfil
        paramidperfil.ParameterName = "idperfil"
        cmd.Parameters.Add(paramidperfil)
        Dim paramdescripcion As DbParameter = cmd.CreateParameter()
        paramdescripcion.Value = menu_perfil.descripcion
        paramdescripcion.ParameterName = "descripcion"
        cmd.Parameters.Add(paramdescripcion)
        Dim paramPerfil_Csc As DbParameter = cmd.CreateParameter()
        paramPerfil_Csc.Value = menu_perfil.Perfil_Csc
        paramPerfil_Csc.ParameterName = "Perfil_Csc"
        cmd.Parameters.Add(paramPerfil_Csc)
        Dim parammanAtenciones As DbParameter = cmd.CreateParameter()
        parammanAtenciones.Value = menu_perfil.manAtenciones
        parammanAtenciones.ParameterName = "manAtenciones"
        cmd.Parameters.Add(parammanAtenciones)
          Dim Dr As DbDataReader = cmd.ExecuteReader()
          While Dr.Read
            dato = Dr(0)
          End While
      Catch ex As Exception
            Ext.Net.X.Msg.Alert("Información", result).Show()
      Finally
          desconecta(cnn)
      End Try
      Return dato
  End Function

  Public Function Editar(ByVal menu_perfil As menu_perfil)
      Dim parametros As New List(Of DbParameter)
      Dim paramidperfil As DbParameter = cmd.CreateParameter()
      paramidperfil.Value = menu_perfil.idperfil
      paramidperfil.ParameterName = "idperfil"
      parametros.Add(paramidperfil)
      Dim paramdescripcion As DbParameter = cmd.CreateParameter()
      paramdescripcion.Value = menu_perfil.descripcion
      paramdescripcion.ParameterName = "descripcion"
      parametros.Add(paramdescripcion)
      Dim paramPerfil_Csc As DbParameter = cmd.CreateParameter()
      paramPerfil_Csc.Value = menu_perfil.Perfil_Csc
      paramPerfil_Csc.ParameterName = "Perfil_Csc"
      parametros.Add(paramPerfil_Csc)
      Dim parammanAtenciones As DbParameter = cmd.CreateParameter()
      parammanAtenciones.Value = menu_perfil.manAtenciones
      parammanAtenciones.ParameterName = "manAtenciones"
      parametros.Add(parammanAtenciones)
      Return ejecutaNonQuery("cambiarmenu_perfil", parametros)
  End Function

  Public Function Borrar(
      ByVal idperfil As string)
      Dim parametros As New List(Of DbParameter)
      Dim paramidperfil As DbParameter = cmd.CreateParameter()
      paramidperfil.Value = idperfil
      paramidperfil.ParameterName = "idperfil"
      parametros.Add(paramidperfil)
      Return ejecutaNonQuery("eliminamenu_perfil", parametros)
  End Function

  Public Function Select_Where_menu_perfil(ByVal Cond As String) As List(Of menu_perfil)
        Dim Listmenu_perfil As New List(Of menu_perfil)
        Dim appSettings = ConfigurationManager.AppSettings
        Dim result As String = appSettings("043")

      Try
          cnn = conecta()
          cmd = New SqlCommand("obtenermenu_perfilByWhere")
          cmd.CommandType = CommandType.StoredProcedure
          cmd.Connection = cnn
          Dim param As DbParameter = cmd.CreateParameter()
          param.Value = Cond
          param.ParameterName = "where"
          cmd.Parameters.Add(param)
          Dim Dr As DbDataReader = cmd.ExecuteReader()
          While Dr.Read
              Listmenu_perfil.Add(New menu_perfil(
                  Dr("idperfil"),
                  Dr("descripcion"),
                  Dr("Perfil_Csc"),
                  Dr("manAtenciones")))
          End While
      Catch ex As Exception
            Ext.Net.X.Msg.Alert("Información", result).Show()
      Finally
          desconecta(cnn)
      End Try
      Return Listmenu_perfil
  End Function

  Public Function Select_ById_menu_perfil(
      ByVal idperfil As string) As List(Of menu_perfil)
        Dim Listmenu_perfil As New List(Of menu_perfil)
        Dim appSettings = ConfigurationManager.AppSettings
        Dim result As String = appSettings("043")

      Try
          cnn = conecta()
          cmd = New SqlCommand("obtenermenu_perfilById")
          cmd.CommandType = CommandType.StoredProcedure
          cmd.Connection = cnn
      Dim paramidperfil As DbParameter = cmd.CreateParameter()
      paramidperfil.Value = idperfil
      paramidperfil.ParameterName = "idperfil"
      cmd.Parameters.Add(paramidperfil)
          Dim Dr As DbDataReader = cmd.ExecuteReader()
          While Dr.Read
              Listmenu_perfil.Add(New menu_perfil(
                  Dr("idperfil"),
                  Dr("descripcion"),
                  Dr("Perfil_Csc"),
                  Dr("manAtenciones")))
          End While
      Catch ex As Exception
            Ext.Net.X.Msg.Alert("Información", result).Show()
      Finally
          desconecta(cnn)
      End Try
      Return Listmenu_perfil
  End Function

  Public Function Select_menu_perfil() As List(Of menu_perfil)
        Dim Listmenu_perfil As New List(Of menu_perfil)
        Dim appSettings = ConfigurationManager.AppSettings
        Dim result As String = appSettings("043")

      Try
         cnn = conecta()
          cmd = New SqlCommand("obtenermenu_perfil")
          cmd.CommandType = CommandType.StoredProcedure
          cmd.Connection = cnn
          Dim Dr As DbDataReader = cmd.ExecuteReader()
          While Dr.Read
              Listmenu_perfil.Add(New menu_perfil(
                  Dr("idperfil"),
                  Dr("descripcion"),
                  Dr("Perfil_Csc"),
                  Dr("manAtenciones")))
          End While
      Catch ex As Exception
            Ext.Net.X.Msg.Alert("Información", result).Show()
      Finally
          desconecta(cnn)
      End Try
      Return Listmenu_perfil
  End Function

End Class
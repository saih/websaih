Imports System.Data
Imports System.Data.SqlClient
Imports System.Data.Common

Public Class Datosaih_age
   Inherits conexion

  Public Function Insertar(ByVal saih_age As saih_age)
      Dim parametros As New List(Of DbParameter)
      Dim paramcodpre As DbParameter = cmd.CreateParameter()
      paramcodpre.Value = saih_age.codpre
      paramcodpre.ParameterName = "codpre"
      parametros.Add(paramcodpre)
      Dim paramnroage As DbParameter = cmd.CreateParameter()
      paramnroage.Value = saih_age.nroage
      paramnroage.ParameterName = "nroage"
      parametros.Add(paramnroage)
      Dim paramfecha_adicion As DbParameter = cmd.CreateParameter()
      paramfecha_adicion.Value = saih_age.fecha_adicion
      paramfecha_adicion.ParameterName = "fecha_adicion"
      parametros.Add(paramfecha_adicion)
      Dim paramfecha_modificacion As DbParameter = cmd.CreateParameter()
      paramfecha_modificacion.Value = saih_age.fecha_modificacion
      paramfecha_modificacion.ParameterName = "fecha_modificacion"
      parametros.Add(paramfecha_modificacion)
      Dim paramusuario_creo As DbParameter = cmd.CreateParameter()
      paramusuario_creo.Value = saih_age.usuario_creo
      paramusuario_creo.ParameterName = "usuario_creo"
      parametros.Add(paramusuario_creo)
      Dim paramusuario_modifico As DbParameter = cmd.CreateParameter()
      paramusuario_modifico.Value = saih_age.usuario_modifico
      paramusuario_modifico.ParameterName = "usuario_modifico"
      parametros.Add(paramusuario_modifico)
      Dim paramfecha_cita As DbParameter = cmd.CreateParameter()
      paramfecha_cita.Value = saih_age.fecha_cita
      paramfecha_cita.ParameterName = "fecha_cita"
      parametros.Add(paramfecha_cita)
      Dim paramnumhis As DbParameter = cmd.CreateParameter()
      paramnumhis.Value = saih_age.numhis
      paramnumhis.ParameterName = "numhis"
      parametros.Add(paramnumhis)
      Dim paramcodemp As DbParameter = cmd.CreateParameter()
      paramcodemp.Value = saih_age.codemp
      paramcodemp.ParameterName = "codemp"
      parametros.Add(paramcodemp)
      Dim paramcodpas As DbParameter = cmd.CreateParameter()
      paramcodpas.Value = saih_age.codpas
      paramcodpas.ParameterName = "codpas"
      parametros.Add(paramcodpas)
      Dim paramcodpcd As DbParameter = cmd.CreateParameter()
      paramcodpcd.Value = saih_age.codpcd
      paramcodpcd.ParameterName = "codpcd"
      parametros.Add(paramcodpcd)
      Dim paramcodser As DbParameter = cmd.CreateParameter()
      paramcodser.Value = saih_age.codser
      paramcodser.ParameterName = "codser"
      parametros.Add(paramcodser)
      Dim paramconsultorio As DbParameter = cmd.CreateParameter()
      paramconsultorio.Value = saih_age.consultorio
      paramconsultorio.ParameterName = "consultorio"
      parametros.Add(paramconsultorio)
      Dim paramestado As DbParameter = cmd.CreateParameter()
      paramestado.Value = saih_age.estado
      paramestado.ParameterName = "estado"
      parametros.Add(paramestado)
      Dim paramcod_cancelacion As DbParameter = cmd.CreateParameter()
      paramcod_cancelacion.Value = saih_age.cod_cancelacion
      paramcod_cancelacion.ParameterName = "cod_cancelacion"
      parametros.Add(paramcod_cancelacion)
      Dim parames_nueva As DbParameter = cmd.CreateParameter()
      parames_nueva.Value = saih_age.es_nueva
      parames_nueva.ParameterName = "es_nueva"
      parametros.Add(parames_nueva)
      Dim paramobservaciones As DbParameter = cmd.CreateParameter()
      paramobservaciones.Value = saih_age.observaciones
      paramobservaciones.ParameterName = "observaciones"
      parametros.Add(paramobservaciones)
      Dim paramfecha_fincita As DbParameter = cmd.CreateParameter()
      paramfecha_fincita.Value = saih_age.fecha_fincita
      paramfecha_fincita.ParameterName = "fecha_fincita"
      parametros.Add(paramfecha_fincita)
      Dim paramnropme As DbParameter = cmd.CreateParameter()
      paramnropme.Value = saih_age.nropme
      paramnropme.ParameterName = "nropme"
      parametros.Add(paramnropme)
      Dim paramtelefono As DbParameter = cmd.CreateParameter()
      paramtelefono.Value = saih_age.telefono
      paramtelefono.ParameterName = "telefono"
      parametros.Add(paramtelefono)
      Dim parammanual As DbParameter = cmd.CreateParameter()
      parammanual.Value = saih_age.manual
      parammanual.ParameterName = "manual"
      parametros.Add(parammanual)
      Return ejecutaNonQuery("insertasaih_age", parametros)
  End Function

  Public Function InsertarRet(ByVal saih_age As saih_age)
      Dim dato As Integer = 0
      Try
          cnn = conecta()
          cmd = New SqlCommand("insertasaih_age")
          cmd.CommandType = CommandType.StoredProcedure
          cmd.Connection = cnn
        Dim paramcodpre As DbParameter = cmd.CreateParameter()
        paramcodpre.Value = saih_age.codpre
        paramcodpre.ParameterName = "codpre"
        cmd.Parameters.Add(paramcodpre)
        Dim paramnroage As DbParameter = cmd.CreateParameter()
        paramnroage.Value = saih_age.nroage
        paramnroage.ParameterName = "nroage"
        cmd.Parameters.Add(paramnroage)
        Dim paramfecha_adicion As DbParameter = cmd.CreateParameter()
        paramfecha_adicion.Value = saih_age.fecha_adicion
        paramfecha_adicion.ParameterName = "fecha_adicion"
        cmd.Parameters.Add(paramfecha_adicion)
        Dim paramfecha_modificacion As DbParameter = cmd.CreateParameter()
        paramfecha_modificacion.Value = saih_age.fecha_modificacion
        paramfecha_modificacion.ParameterName = "fecha_modificacion"
        cmd.Parameters.Add(paramfecha_modificacion)
        Dim paramusuario_creo As DbParameter = cmd.CreateParameter()
        paramusuario_creo.Value = saih_age.usuario_creo
        paramusuario_creo.ParameterName = "usuario_creo"
        cmd.Parameters.Add(paramusuario_creo)
        Dim paramusuario_modifico As DbParameter = cmd.CreateParameter()
        paramusuario_modifico.Value = saih_age.usuario_modifico
        paramusuario_modifico.ParameterName = "usuario_modifico"
        cmd.Parameters.Add(paramusuario_modifico)
        Dim paramfecha_cita As DbParameter = cmd.CreateParameter()
        paramfecha_cita.Value = saih_age.fecha_cita
        paramfecha_cita.ParameterName = "fecha_cita"
        cmd.Parameters.Add(paramfecha_cita)
        Dim paramnumhis As DbParameter = cmd.CreateParameter()
        paramnumhis.Value = saih_age.numhis
        paramnumhis.ParameterName = "numhis"
        cmd.Parameters.Add(paramnumhis)
        Dim paramcodemp As DbParameter = cmd.CreateParameter()
        paramcodemp.Value = saih_age.codemp
        paramcodemp.ParameterName = "codemp"
        cmd.Parameters.Add(paramcodemp)
        Dim paramcodpas As DbParameter = cmd.CreateParameter()
        paramcodpas.Value = saih_age.codpas
        paramcodpas.ParameterName = "codpas"
        cmd.Parameters.Add(paramcodpas)
        Dim paramcodpcd As DbParameter = cmd.CreateParameter()
        paramcodpcd.Value = saih_age.codpcd
        paramcodpcd.ParameterName = "codpcd"
        cmd.Parameters.Add(paramcodpcd)
        Dim paramcodser As DbParameter = cmd.CreateParameter()
        paramcodser.Value = saih_age.codser
        paramcodser.ParameterName = "codser"
        cmd.Parameters.Add(paramcodser)
        Dim paramconsultorio As DbParameter = cmd.CreateParameter()
        paramconsultorio.Value = saih_age.consultorio
        paramconsultorio.ParameterName = "consultorio"
        cmd.Parameters.Add(paramconsultorio)
        Dim paramestado As DbParameter = cmd.CreateParameter()
        paramestado.Value = saih_age.estado
        paramestado.ParameterName = "estado"
        cmd.Parameters.Add(paramestado)
        Dim paramcod_cancelacion As DbParameter = cmd.CreateParameter()
        paramcod_cancelacion.Value = saih_age.cod_cancelacion
        paramcod_cancelacion.ParameterName = "cod_cancelacion"
        cmd.Parameters.Add(paramcod_cancelacion)
        Dim parames_nueva As DbParameter = cmd.CreateParameter()
        parames_nueva.Value = saih_age.es_nueva
        parames_nueva.ParameterName = "es_nueva"
        cmd.Parameters.Add(parames_nueva)
        Dim paramobservaciones As DbParameter = cmd.CreateParameter()
        paramobservaciones.Value = saih_age.observaciones
        paramobservaciones.ParameterName = "observaciones"
        cmd.Parameters.Add(paramobservaciones)
        Dim paramfecha_fincita As DbParameter = cmd.CreateParameter()
        paramfecha_fincita.Value = saih_age.fecha_fincita
        paramfecha_fincita.ParameterName = "fecha_fincita"
        cmd.Parameters.Add(paramfecha_fincita)
        Dim paramnropme As DbParameter = cmd.CreateParameter()
        paramnropme.Value = saih_age.nropme
        paramnropme.ParameterName = "nropme"
        cmd.Parameters.Add(paramnropme)
        Dim paramtelefono As DbParameter = cmd.CreateParameter()
        paramtelefono.Value = saih_age.telefono
        paramtelefono.ParameterName = "telefono"
        cmd.Parameters.Add(paramtelefono)
        Dim parammanual As DbParameter = cmd.CreateParameter()
        parammanual.Value = saih_age.manual
        parammanual.ParameterName = "manual"
        cmd.Parameters.Add(parammanual)
          Dim Dr As DbDataReader = cmd.ExecuteReader()
          While Dr.Read
            dato = Dr(0)
          End While
      Catch ex As Exception
          Ext.Net.X.Msg.Alert("Información", ex.Message).Show()
      Finally
          desconecta(cnn)
      End Try
      Return dato
  End Function

  Public Function Editar(ByVal saih_age As saih_age)
      Dim parametros As New List(Of DbParameter)
      Dim paramid As DbParameter = cmd.CreateParameter()
      paramid.Value = saih_age.id
      paramid.ParameterName = "id"
      parametros.Add(paramid)
      Dim paramcodpre As DbParameter = cmd.CreateParameter()
      paramcodpre.Value = saih_age.codpre
      paramcodpre.ParameterName = "codpre"
      parametros.Add(paramcodpre)
      Dim paramnroage As DbParameter = cmd.CreateParameter()
      paramnroage.Value = saih_age.nroage
      paramnroage.ParameterName = "nroage"
      parametros.Add(paramnroage)
      Dim paramfecha_adicion As DbParameter = cmd.CreateParameter()
      paramfecha_adicion.Value = saih_age.fecha_adicion
      paramfecha_adicion.ParameterName = "fecha_adicion"
      parametros.Add(paramfecha_adicion)
      Dim paramfecha_modificacion As DbParameter = cmd.CreateParameter()
      paramfecha_modificacion.Value = saih_age.fecha_modificacion
      paramfecha_modificacion.ParameterName = "fecha_modificacion"
      parametros.Add(paramfecha_modificacion)
      Dim paramusuario_creo As DbParameter = cmd.CreateParameter()
      paramusuario_creo.Value = saih_age.usuario_creo
      paramusuario_creo.ParameterName = "usuario_creo"
      parametros.Add(paramusuario_creo)
      Dim paramusuario_modifico As DbParameter = cmd.CreateParameter()
      paramusuario_modifico.Value = saih_age.usuario_modifico
      paramusuario_modifico.ParameterName = "usuario_modifico"
      parametros.Add(paramusuario_modifico)
      Dim paramfecha_cita As DbParameter = cmd.CreateParameter()
      paramfecha_cita.Value = saih_age.fecha_cita
      paramfecha_cita.ParameterName = "fecha_cita"
      parametros.Add(paramfecha_cita)
      Dim paramnumhis As DbParameter = cmd.CreateParameter()
      paramnumhis.Value = saih_age.numhis
      paramnumhis.ParameterName = "numhis"
      parametros.Add(paramnumhis)
      Dim paramcodemp As DbParameter = cmd.CreateParameter()
      paramcodemp.Value = saih_age.codemp
      paramcodemp.ParameterName = "codemp"
      parametros.Add(paramcodemp)
      Dim paramcodpas As DbParameter = cmd.CreateParameter()
      paramcodpas.Value = saih_age.codpas
      paramcodpas.ParameterName = "codpas"
      parametros.Add(paramcodpas)
      Dim paramcodpcd As DbParameter = cmd.CreateParameter()
      paramcodpcd.Value = saih_age.codpcd
      paramcodpcd.ParameterName = "codpcd"
      parametros.Add(paramcodpcd)
      Dim paramcodser As DbParameter = cmd.CreateParameter()
      paramcodser.Value = saih_age.codser
      paramcodser.ParameterName = "codser"
      parametros.Add(paramcodser)
      Dim paramconsultorio As DbParameter = cmd.CreateParameter()
      paramconsultorio.Value = saih_age.consultorio
      paramconsultorio.ParameterName = "consultorio"
      parametros.Add(paramconsultorio)
      Dim paramestado As DbParameter = cmd.CreateParameter()
      paramestado.Value = saih_age.estado
      paramestado.ParameterName = "estado"
      parametros.Add(paramestado)
      Dim paramcod_cancelacion As DbParameter = cmd.CreateParameter()
      paramcod_cancelacion.Value = saih_age.cod_cancelacion
      paramcod_cancelacion.ParameterName = "cod_cancelacion"
      parametros.Add(paramcod_cancelacion)
      Dim parames_nueva As DbParameter = cmd.CreateParameter()
      parames_nueva.Value = saih_age.es_nueva
      parames_nueva.ParameterName = "es_nueva"
      parametros.Add(parames_nueva)
      Dim paramobservaciones As DbParameter = cmd.CreateParameter()
      paramobservaciones.Value = saih_age.observaciones
      paramobservaciones.ParameterName = "observaciones"
      parametros.Add(paramobservaciones)
      Dim paramfecha_fincita As DbParameter = cmd.CreateParameter()
      paramfecha_fincita.Value = saih_age.fecha_fincita
      paramfecha_fincita.ParameterName = "fecha_fincita"
      parametros.Add(paramfecha_fincita)
      Dim paramnropme As DbParameter = cmd.CreateParameter()
      paramnropme.Value = saih_age.nropme
      paramnropme.ParameterName = "nropme"
      parametros.Add(paramnropme)
      Dim paramtelefono As DbParameter = cmd.CreateParameter()
      paramtelefono.Value = saih_age.telefono
      paramtelefono.ParameterName = "telefono"
      parametros.Add(paramtelefono)
      Dim parammanual As DbParameter = cmd.CreateParameter()
      parammanual.Value = saih_age.manual
      parammanual.ParameterName = "manual"
      parametros.Add(parammanual)
      Return ejecutaNonQuery("cambiarsaih_age", parametros)
  End Function

  Public Function Borrar(
      ByVal id As integer)
      Dim parametros As New List(Of DbParameter)
      Dim paramid As DbParameter = cmd.CreateParameter()
      paramid.Value = id
      paramid.ParameterName = "id"
      parametros.Add(paramid)
      Return ejecutaNonQuery("eliminasaih_age", parametros)
  End Function

  Public Function Select_Where_saih_age(ByVal Cond As String) As List(Of saih_age)
      Dim Listsaih_age As New List(Of saih_age)
      Try
          cnn = conecta()
          cmd = New SqlCommand("obtenersaih_ageByWhere")
          cmd.CommandType = CommandType.StoredProcedure
          cmd.Connection = cnn
          Dim param As DbParameter = cmd.CreateParameter()
          param.Value = Cond
          param.ParameterName = "where"
          cmd.Parameters.Add(param)
          Dim Dr As DbDataReader = cmd.ExecuteReader()
          While Dr.Read
              Listsaih_age.Add(New saih_age(
                  Dr("id"),
                  Dr("codpre"),
                  Dr("nroage"),
                  Dr("fecha_adicion"),
                  Dr("fecha_modificacion"),
                  Dr("usuario_creo"),
                  Dr("usuario_modifico"),
                  Dr("fecha_cita"),
                  Dr("numhis"),
                  Dr("codemp"),
                  Dr("codpas"),
                  Dr("codpcd"),
                  Dr("codser"),
                  Dr("consultorio"),
                  Dr("estado"),
                  Dr("cod_cancelacion"),
                  Dr("es_nueva"),
                  Dr("observaciones"),
                  Dr("fecha_fincita"),
                  Dr("nropme"),
                  Dr("telefono"),
                  Dr("manual")))
          End While
      Catch ex As Exception
          Ext.Net.X.Msg.Alert("Información", ex.Message).Show()
      Finally
          desconecta(cnn)
      End Try
      Return Listsaih_age
  End Function

  Public Function Select_ById_saih_age(
      ByVal id As integer) As List(Of saih_age)
      Dim Listsaih_age As New List(Of saih_age)
      Try
          cnn = conecta()
          cmd = New SqlCommand("obtenersaih_ageById")
          cmd.CommandType = CommandType.StoredProcedure
          cmd.Connection = cnn
      Dim paramid As DbParameter = cmd.CreateParameter()
      paramid.Value = id
      paramid.ParameterName = "id"
      cmd.Parameters.Add(paramid)
          Dim Dr As DbDataReader = cmd.ExecuteReader()
          While Dr.Read
              Listsaih_age.Add(New saih_age(
                  Dr("id"),
                  Dr("codpre"),
                  Dr("nroage"),
                  Dr("fecha_adicion"),
                  Dr("fecha_modificacion"),
                  Dr("usuario_creo"),
                  Dr("usuario_modifico"),
                  Dr("fecha_cita"),
                  Dr("numhis"),
                  Dr("codemp"),
                  Dr("codpas"),
                  Dr("codpcd"),
                  Dr("codser"),
                  Dr("consultorio"),
                  Dr("estado"),
                  Dr("cod_cancelacion"),
                  Dr("es_nueva"),
                  Dr("observaciones"),
                  Dr("fecha_fincita"),
                  Dr("nropme"),
                  Dr("telefono"),
                  Dr("manual")))
          End While
      Catch ex As Exception
          Ext.Net.X.Msg.Alert("Información", ex.Message).Show()
      Finally
          desconecta(cnn)
      End Try
      Return Listsaih_age
  End Function

  Public Function Select_saih_age() As List(Of saih_age)
      Dim Listsaih_age As New List(Of saih_age)
      Try
         cnn = conecta()
          cmd = New SqlCommand("obtenersaih_age")
          cmd.CommandType = CommandType.StoredProcedure
          cmd.Connection = cnn
          Dim Dr As DbDataReader = cmd.ExecuteReader()
          While Dr.Read
              Listsaih_age.Add(New saih_age(
                  Dr("id"),
                  Dr("codpre"),
                  Dr("nroage"),
                  Dr("fecha_adicion"),
                  Dr("fecha_modificacion"),
                  Dr("usuario_creo"),
                  Dr("usuario_modifico"),
                  Dr("fecha_cita"),
                  Dr("numhis"),
                  Dr("codemp"),
                  Dr("codpas"),
                  Dr("codpcd"),
                  Dr("codser"),
                  Dr("consultorio"),
                  Dr("estado"),
                  Dr("cod_cancelacion"),
                  Dr("es_nueva"),
                  Dr("observaciones"),
                  Dr("fecha_fincita"),
                  Dr("nropme"),
                  Dr("telefono"),
                  Dr("manual")))
          End While
      Catch ex As Exception
          Ext.Net.X.Msg.Alert("Información", ex.Message).Show()
      Finally
          desconecta(cnn)
      End Try
      Return Listsaih_age
  End Function

End Class
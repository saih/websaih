Imports System.Data
Imports System.Data.SqlClient
Imports System.Data.Common
Imports System.Configuration

Public Class Datosaih_his
    Inherits conexion

    Public Function Insertar(ByVal saih_his As saih_his)
        Dim parametros As New List(Of DbParameter)
        Dim paramnumhis As DbParameter = cmd.CreateParameter()
        paramnumhis.Value = saih_his.numhis
        paramnumhis.ParameterName = "numhis"
        parametros.Add(paramnumhis)
        Dim paramidentificacion As DbParameter = cmd.CreateParameter()
        paramidentificacion.Value = saih_his.identificacion
        paramidentificacion.ParameterName = "identificacion"
        parametros.Add(paramidentificacion)
        Dim paramtipo_identificacion As DbParameter = cmd.CreateParameter()
        paramtipo_identificacion.Value = saih_his.tipo_identificacion
        paramtipo_identificacion.ParameterName = "tipo_identificacion"
        parametros.Add(paramtipo_identificacion)
        Dim paramprimer_nombre As DbParameter = cmd.CreateParameter()
        paramprimer_nombre.Value = saih_his.primer_nombre
        paramprimer_nombre.ParameterName = "primer_nombre"
        parametros.Add(paramprimer_nombre)
        Dim paramsegundo_nombre As DbParameter = cmd.CreateParameter()
        paramsegundo_nombre.Value = saih_his.segundo_nombre
        paramsegundo_nombre.ParameterName = "segundo_nombre"
        parametros.Add(paramsegundo_nombre)
        Dim paramprimer_apellido As DbParameter = cmd.CreateParameter()
        paramprimer_apellido.Value = saih_his.primer_apellido
        paramprimer_apellido.ParameterName = "primer_apellido"
        parametros.Add(paramprimer_apellido)
        Dim paramsegundo_apellido As DbParameter = cmd.CreateParameter()
        paramsegundo_apellido.Value = saih_his.segundo_apellido
        paramsegundo_apellido.ParameterName = "segundo_apellido"
        parametros.Add(paramsegundo_apellido)
        Dim paramfecha_nacimiento As DbParameter = cmd.CreateParameter()
        paramfecha_nacimiento.Value = saih_his.fecha_nacimiento
        paramfecha_nacimiento.ParameterName = "fecha_nacimiento"
        parametros.Add(paramfecha_nacimiento)
        Dim paramid_lugar As DbParameter = cmd.CreateParameter()
        paramid_lugar.Value = saih_his.id_lugar
        paramid_lugar.ParameterName = "id_lugar"
        parametros.Add(paramid_lugar)
        Dim paramunidad_medida As DbParameter = cmd.CreateParameter()
        paramunidad_medida.Value = saih_his.unidad_medida
        paramunidad_medida.ParameterName = "unidad_medida"
        parametros.Add(paramunidad_medida)
        Dim paramsexo As DbParameter = cmd.CreateParameter()
        paramsexo.Value = saih_his.sexo
        paramsexo.ParameterName = "sexo"
        parametros.Add(paramsexo)
        Dim paramestado_civil As DbParameter = cmd.CreateParameter()
        paramestado_civil.Value = saih_his.estado_civil
        paramestado_civil.ParameterName = "estado_civil"
        parametros.Add(paramestado_civil)
        Dim paramdireccion As DbParameter = cmd.CreateParameter()
        paramdireccion.Value = saih_his.direccion
        paramdireccion.ParameterName = "direccion"
        parametros.Add(paramdireccion)
        Dim paramtelefono As DbParameter = cmd.CreateParameter()
        paramtelefono.Value = saih_his.telefono
        paramtelefono.ParameterName = "telefono"
        parametros.Add(paramtelefono)
        Dim paramtelefonoaux As DbParameter = cmd.CreateParameter()
        paramtelefonoaux.Value = saih_his.telefonoaux
        paramtelefonoaux.ParameterName = "telefonoaux"
        parametros.Add(paramtelefonoaux)
        Dim paramid_lugar_municipio As DbParameter = cmd.CreateParameter()
        paramid_lugar_municipio.Value = saih_his.id_lugar_municipio
        paramid_lugar_municipio.ParameterName = "id_lugar_municipio"
        parametros.Add(paramid_lugar_municipio)
        Dim paramid_lugar_barrio As DbParameter = cmd.CreateParameter()
        paramid_lugar_barrio.Value = saih_his.id_lugar_barrio
        paramid_lugar_barrio.ParameterName = "id_lugar_barrio"
        parametros.Add(paramid_lugar_barrio)
        Dim paramzona_residencia As DbParameter = cmd.CreateParameter()
        paramzona_residencia.Value = saih_his.zona_residencia
        paramzona_residencia.ParameterName = "zona_residencia"
        parametros.Add(paramzona_residencia)
        Dim paramestrato As DbParameter = cmd.CreateParameter()
        paramestrato.Value = saih_his.estrato
        paramestrato.ParameterName = "estrato"
        parametros.Add(paramestrato)
        Dim paramcodemp As DbParameter = cmd.CreateParameter()
        paramcodemp.Value = saih_his.codemp
        paramcodemp.ParameterName = "codemp"
        parametros.Add(paramcodemp)
        Dim paramcodarp As DbParameter = cmd.CreateParameter()
        paramcodarp.Value = saih_his.codarp
        paramcodarp.ParameterName = "codarp"
        parametros.Add(paramcodarp)
        Dim paramcodccf As DbParameter = cmd.CreateParameter()
        paramcodccf.Value = saih_his.codccf
        paramcodccf.ParameterName = "codccf"
        parametros.Add(paramcodccf)
        Dim paramapellido_conyuge As DbParameter = cmd.CreateParameter()
        paramapellido_conyuge.Value = saih_his.apellido_conyuge
        paramapellido_conyuge.ParameterName = "apellido_conyuge"
        parametros.Add(paramapellido_conyuge)
        Dim paramnombre_conyuge As DbParameter = cmd.CreateParameter()
        paramnombre_conyuge.Value = saih_his.nombre_conyuge
        paramnombre_conyuge.ParameterName = "nombre_conyuge"
        parametros.Add(paramnombre_conyuge)
        Dim paramnombre_padre As DbParameter = cmd.CreateParameter()
        paramnombre_padre.Value = saih_his.nombre_padre
        paramnombre_padre.ParameterName = "nombre_padre"
        parametros.Add(paramnombre_padre)
        Dim paramnombre_madre As DbParameter = cmd.CreateParameter()
        paramnombre_madre.Value = saih_his.nombre_madre
        paramnombre_madre.ParameterName = "nombre_madre"
        parametros.Add(paramnombre_madre)
        Dim paramusuario_creo As DbParameter = cmd.CreateParameter()
        paramusuario_creo.Value = saih_his.usuario_creo
        paramusuario_creo.ParameterName = "usuario_creo"
        parametros.Add(paramusuario_creo)
        Dim paramfecha_creacion As DbParameter = cmd.CreateParameter()
        paramfecha_creacion.Value = saih_his.fecha_creacion
        paramfecha_creacion.ParameterName = "fecha_creacion"
        parametros.Add(paramfecha_creacion)
        Dim paramusuario_modifico As DbParameter = cmd.CreateParameter()
        paramusuario_modifico.Value = saih_his.usuario_modifico
        paramusuario_modifico.ParameterName = "usuario_modifico"
        parametros.Add(paramusuario_modifico)
        Dim paramfecha_modificacion As DbParameter = cmd.CreateParameter()
        paramfecha_modificacion.Value = saih_his.fecha_modificacion
        paramfecha_modificacion.ParameterName = "fecha_modificacion"
        parametros.Add(paramfecha_modificacion)
        Dim paramobservacion As DbParameter = cmd.CreateParameter()
        paramobservacion.Value = saih_his.observacion
        paramobservacion.ParameterName = "observacion"
        parametros.Add(paramobservacion)
        Dim paramnrocon As DbParameter = cmd.CreateParameter()
        paramnrocon.Value = saih_his.nrocon
        paramnrocon.ParameterName = "nrocon"
        parametros.Add(paramnrocon)
        Dim paramubica As DbParameter = cmd.CreateParameter()
        paramubica.Value = saih_his.ubica
        paramubica.ParameterName = "ubica"
        parametros.Add(paramubica)
        Dim paramfactor_hr As DbParameter = cmd.CreateParameter()
        paramfactor_hr.Value = saih_his.factor_hr
        paramfactor_hr.ParameterName = "factor_hr"
        parametros.Add(paramfactor_hr)
        Dim paramfecha_muerte As DbParameter = cmd.CreateParameter()
        paramfecha_muerte.Value = saih_his.fecha_muerte
        paramfecha_muerte.ParameterName = "fecha_muerte"
        parametros.Add(paramfecha_muerte)
        Dim paramurl_foto As DbParameter = cmd.CreateParameter()
        paramurl_foto.Value = saih_his.url_foto
        paramurl_foto.ParameterName = "url_foto"
        parametros.Add(paramurl_foto)
        Dim parametnia As DbParameter = cmd.CreateParameter()
        parametnia.Value = saih_his.etnia
        parametnia.ParameterName = "etnia"
        parametros.Add(parametnia)
        Dim paramhuella As DbParameter = cmd.CreateParameter()
        paramhuella.Value = saih_his.huella
        paramhuella.ParameterName = "huella"
        parametros.Add(paramhuella)
        Dim paramnivel_escolar As DbParameter = cmd.CreateParameter()
        paramnivel_escolar.Value = saih_his.nivel_escolar
        paramnivel_escolar.ParameterName = "nivel_escolar"
        parametros.Add(paramnivel_escolar)
        Dim paramresponsable_economico As DbParameter = cmd.CreateParameter()
        paramresponsable_economico.Value = saih_his.responsable_economico
        paramresponsable_economico.ParameterName = "responsable_economico"
        parametros.Add(paramresponsable_economico)
        Dim paramreligion As DbParameter = cmd.CreateParameter()
        paramreligion.Value = saih_his.religion
        paramreligion.ParameterName = "religion"
        parametros.Add(paramreligion)
        Dim paramtipo_residencia As DbParameter = cmd.CreateParameter()
        paramtipo_residencia.Value = saih_his.tipo_residencia
        paramtipo_residencia.ParameterName = "tipo_residencia"
        parametros.Add(paramtipo_residencia)
        Dim paramescolar As DbParameter = cmd.CreateParameter()
        paramescolar.Value = saih_his.escolar
        paramescolar.ParameterName = "escolar"
        parametros.Add(paramescolar)
        Dim paramrazonsocial As DbParameter = cmd.CreateParameter()
        paramrazonsocial.Value = saih_his.razonsocial
        paramrazonsocial.ParameterName = "razonsocial"
        parametros.Add(paramrazonsocial)
        Dim paramestatura As DbParameter = cmd.CreateParameter()
        paramestatura.Value = saih_his.estatura
        paramestatura.ParameterName = "estatura"
        parametros.Add(paramestatura)
        Dim paramobservaciones As DbParameter = cmd.CreateParameter()
        paramobservaciones.Value = saih_his.observaciones
        paramobservaciones.ParameterName = "observaciones"
        parametros.Add(paramobservaciones)
        Dim paramcodocu As DbParameter = cmd.CreateParameter()
        paramcodocu.Value = saih_his.codocu
        paramcodocu.ParameterName = "codocu"
        parametros.Add(paramcodocu)
        Dim paramdiscapacidad As DbParameter = cmd.CreateParameter()
        paramdiscapacidad.Value = saih_his.discapacidad
        paramdiscapacidad.ParameterName = "discapacidad"
        parametros.Add(paramdiscapacidad)
        Return ejecutaNonQuery("insertasaih_his", parametros)
    End Function

    Public Function InsertarRet(ByVal saih_his As saih_his)
        Dim dato As Integer = 0
        Dim appSettings = ConfigurationManager.AppSettings
        Dim result As String = appSettings("028")

        Try
            cnn = conecta()
            cmd = New SqlCommand("insertasaih_his")
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Connection = cnn
            Dim paramnumhis As DbParameter = cmd.CreateParameter()
            paramnumhis.Value = saih_his.numhis
            paramnumhis.ParameterName = "numhis"
            cmd.Parameters.Add(paramnumhis)
            Dim paramidentificacion As DbParameter = cmd.CreateParameter()
            paramidentificacion.Value = saih_his.identificacion
            paramidentificacion.ParameterName = "identificacion"
            cmd.Parameters.Add(paramidentificacion)
            Dim paramtipo_identificacion As DbParameter = cmd.CreateParameter()
            paramtipo_identificacion.Value = saih_his.tipo_identificacion
            paramtipo_identificacion.ParameterName = "tipo_identificacion"
            cmd.Parameters.Add(paramtipo_identificacion)
            Dim paramprimer_nombre As DbParameter = cmd.CreateParameter()
            paramprimer_nombre.Value = saih_his.primer_nombre
            paramprimer_nombre.ParameterName = "primer_nombre"
            cmd.Parameters.Add(paramprimer_nombre)
            Dim paramsegundo_nombre As DbParameter = cmd.CreateParameter()
            paramsegundo_nombre.Value = saih_his.segundo_nombre
            paramsegundo_nombre.ParameterName = "segundo_nombre"
            cmd.Parameters.Add(paramsegundo_nombre)
            Dim paramprimer_apellido As DbParameter = cmd.CreateParameter()
            paramprimer_apellido.Value = saih_his.primer_apellido
            paramprimer_apellido.ParameterName = "primer_apellido"
            cmd.Parameters.Add(paramprimer_apellido)
            Dim paramsegundo_apellido As DbParameter = cmd.CreateParameter()
            paramsegundo_apellido.Value = saih_his.segundo_apellido
            paramsegundo_apellido.ParameterName = "segundo_apellido"
            cmd.Parameters.Add(paramsegundo_apellido)
            Dim paramfecha_nacimiento As DbParameter = cmd.CreateParameter()
            paramfecha_nacimiento.Value = saih_his.fecha_nacimiento
            paramfecha_nacimiento.ParameterName = "fecha_nacimiento"
            cmd.Parameters.Add(paramfecha_nacimiento)
            Dim paramid_lugar As DbParameter = cmd.CreateParameter()
            paramid_lugar.Value = saih_his.id_lugar
            paramid_lugar.ParameterName = "id_lugar"
            cmd.Parameters.Add(paramid_lugar)
            Dim paramunidad_medida As DbParameter = cmd.CreateParameter()
            paramunidad_medida.Value = saih_his.unidad_medida
            paramunidad_medida.ParameterName = "unidad_medida"
            cmd.Parameters.Add(paramunidad_medida)
            Dim paramsexo As DbParameter = cmd.CreateParameter()
            paramsexo.Value = saih_his.sexo
            paramsexo.ParameterName = "sexo"
            cmd.Parameters.Add(paramsexo)
            Dim paramestado_civil As DbParameter = cmd.CreateParameter()
            paramestado_civil.Value = saih_his.estado_civil
            paramestado_civil.ParameterName = "estado_civil"
            cmd.Parameters.Add(paramestado_civil)
            Dim paramdireccion As DbParameter = cmd.CreateParameter()
            paramdireccion.Value = saih_his.direccion
            paramdireccion.ParameterName = "direccion"
            cmd.Parameters.Add(paramdireccion)
            Dim paramtelefono As DbParameter = cmd.CreateParameter()
            paramtelefono.Value = saih_his.telefono
            paramtelefono.ParameterName = "telefono"
            cmd.Parameters.Add(paramtelefono)
            Dim paramtelefonoaux As DbParameter = cmd.CreateParameter()
            paramtelefonoaux.Value = saih_his.telefonoaux
            paramtelefonoaux.ParameterName = "telefonoaux"
            cmd.Parameters.Add(paramtelefonoaux)
            Dim paramid_lugar_municipio As DbParameter = cmd.CreateParameter()
            paramid_lugar_municipio.Value = saih_his.id_lugar_municipio
            paramid_lugar_municipio.ParameterName = "id_lugar_municipio"
            cmd.Parameters.Add(paramid_lugar_municipio)
            Dim paramid_lugar_barrio As DbParameter = cmd.CreateParameter()
            paramid_lugar_barrio.Value = saih_his.id_lugar_barrio
            paramid_lugar_barrio.ParameterName = "id_lugar_barrio"
            cmd.Parameters.Add(paramid_lugar_barrio)
            Dim paramzona_residencia As DbParameter = cmd.CreateParameter()
            paramzona_residencia.Value = saih_his.zona_residencia
            paramzona_residencia.ParameterName = "zona_residencia"
            cmd.Parameters.Add(paramzona_residencia)
            Dim paramestrato As DbParameter = cmd.CreateParameter()
            paramestrato.Value = saih_his.estrato
            paramestrato.ParameterName = "estrato"
            cmd.Parameters.Add(paramestrato)
            Dim paramcodemp As DbParameter = cmd.CreateParameter()
            paramcodemp.Value = saih_his.codemp
            paramcodemp.ParameterName = "codemp"
            cmd.Parameters.Add(paramcodemp)
            Dim paramcodarp As DbParameter = cmd.CreateParameter()
            paramcodarp.Value = saih_his.codarp
            paramcodarp.ParameterName = "codarp"
            cmd.Parameters.Add(paramcodarp)
            Dim paramcodccf As DbParameter = cmd.CreateParameter()
            paramcodccf.Value = saih_his.codccf
            paramcodccf.ParameterName = "codccf"
            cmd.Parameters.Add(paramcodccf)
            Dim paramapellido_conyuge As DbParameter = cmd.CreateParameter()
            paramapellido_conyuge.Value = saih_his.apellido_conyuge
            paramapellido_conyuge.ParameterName = "apellido_conyuge"
            cmd.Parameters.Add(paramapellido_conyuge)
            Dim paramnombre_conyuge As DbParameter = cmd.CreateParameter()
            paramnombre_conyuge.Value = saih_his.nombre_conyuge
            paramnombre_conyuge.ParameterName = "nombre_conyuge"
            cmd.Parameters.Add(paramnombre_conyuge)
            Dim paramnombre_padre As DbParameter = cmd.CreateParameter()
            paramnombre_padre.Value = saih_his.nombre_padre
            paramnombre_padre.ParameterName = "nombre_padre"
            cmd.Parameters.Add(paramnombre_padre)
            Dim paramnombre_madre As DbParameter = cmd.CreateParameter()
            paramnombre_madre.Value = saih_his.nombre_madre
            paramnombre_madre.ParameterName = "nombre_madre"
            cmd.Parameters.Add(paramnombre_madre)
            Dim paramusuario_creo As DbParameter = cmd.CreateParameter()
            paramusuario_creo.Value = saih_his.usuario_creo
            paramusuario_creo.ParameterName = "usuario_creo"
            cmd.Parameters.Add(paramusuario_creo)
            Dim paramfecha_creacion As DbParameter = cmd.CreateParameter()
            paramfecha_creacion.Value = saih_his.fecha_creacion
            paramfecha_creacion.ParameterName = "fecha_creacion"
            cmd.Parameters.Add(paramfecha_creacion)
            Dim paramusuario_modifico As DbParameter = cmd.CreateParameter()
            paramusuario_modifico.Value = saih_his.usuario_modifico
            paramusuario_modifico.ParameterName = "usuario_modifico"
            cmd.Parameters.Add(paramusuario_modifico)
            Dim paramfecha_modificacion As DbParameter = cmd.CreateParameter()
            paramfecha_modificacion.Value = saih_his.fecha_modificacion
            paramfecha_modificacion.ParameterName = "fecha_modificacion"
            cmd.Parameters.Add(paramfecha_modificacion)
            Dim paramobservacion As DbParameter = cmd.CreateParameter()
            paramobservacion.Value = saih_his.observacion
            paramobservacion.ParameterName = "observacion"
            cmd.Parameters.Add(paramobservacion)
            Dim paramnrocon As DbParameter = cmd.CreateParameter()
            paramnrocon.Value = saih_his.nrocon
            paramnrocon.ParameterName = "nrocon"
            cmd.Parameters.Add(paramnrocon)
            Dim paramubica As DbParameter = cmd.CreateParameter()
            paramubica.Value = saih_his.ubica
            paramubica.ParameterName = "ubica"
            cmd.Parameters.Add(paramubica)
            Dim paramfactor_hr As DbParameter = cmd.CreateParameter()
            paramfactor_hr.Value = saih_his.factor_hr
            paramfactor_hr.ParameterName = "factor_hr"
            cmd.Parameters.Add(paramfactor_hr)
            Dim paramfecha_muerte As DbParameter = cmd.CreateParameter()
            paramfecha_muerte.Value = saih_his.fecha_muerte
            paramfecha_muerte.ParameterName = "fecha_muerte"
            cmd.Parameters.Add(paramfecha_muerte)
            Dim paramurl_foto As DbParameter = cmd.CreateParameter()
            paramurl_foto.Value = saih_his.url_foto
            paramurl_foto.ParameterName = "url_foto"
            cmd.Parameters.Add(paramurl_foto)
            Dim parametnia As DbParameter = cmd.CreateParameter()
            parametnia.Value = saih_his.etnia
            parametnia.ParameterName = "etnia"
            cmd.Parameters.Add(parametnia)
            Dim paramhuella As DbParameter = cmd.CreateParameter()
            paramhuella.Value = saih_his.huella
            paramhuella.ParameterName = "huella"
            cmd.Parameters.Add(paramhuella)
            Dim paramnivel_escolar As DbParameter = cmd.CreateParameter()
            paramnivel_escolar.Value = saih_his.nivel_escolar
            paramnivel_escolar.ParameterName = "nivel_escolar"
            cmd.Parameters.Add(paramnivel_escolar)
            Dim paramresponsable_economico As DbParameter = cmd.CreateParameter()
            paramresponsable_economico.Value = saih_his.responsable_economico
            paramresponsable_economico.ParameterName = "responsable_economico"
            cmd.Parameters.Add(paramresponsable_economico)
            Dim paramreligion As DbParameter = cmd.CreateParameter()
            paramreligion.Value = saih_his.religion
            paramreligion.ParameterName = "religion"
            cmd.Parameters.Add(paramreligion)
            Dim paramtipo_residencia As DbParameter = cmd.CreateParameter()
            paramtipo_residencia.Value = saih_his.tipo_residencia
            paramtipo_residencia.ParameterName = "tipo_residencia"
            cmd.Parameters.Add(paramtipo_residencia)
            Dim paramescolar As DbParameter = cmd.CreateParameter()
            paramescolar.Value = saih_his.escolar
            paramescolar.ParameterName = "escolar"
            cmd.Parameters.Add(paramescolar)
            Dim paramrazonsocial As DbParameter = cmd.CreateParameter()
            paramrazonsocial.Value = saih_his.razonsocial
            paramrazonsocial.ParameterName = "razonsocial"
            cmd.Parameters.Add(paramrazonsocial)
            Dim paramestatura As DbParameter = cmd.CreateParameter()
            paramestatura.Value = saih_his.estatura
            paramestatura.ParameterName = "estatura"
            cmd.Parameters.Add(paramestatura)
            Dim paramobservaciones As DbParameter = cmd.CreateParameter()
            paramobservaciones.Value = saih_his.observaciones
            paramobservaciones.ParameterName = "observaciones"
            cmd.Parameters.Add(paramobservaciones)
            Dim paramcodocu As DbParameter = cmd.CreateParameter()
            paramcodocu.Value = saih_his.codocu
            paramcodocu.ParameterName = "codocu"
            Dim paramdiscapacidad As DbParameter = cmd.CreateParameter()
            paramdiscapacidad.Value = saih_his.discapacidad
            paramdiscapacidad.ParameterName = "codocu"

            cmd.Parameters.Add(paramdiscapacidad)
            Dim Dr As DbDataReader = cmd.ExecuteReader()
            While Dr.Read
                dato = Dr(0)
            End While
        Catch ex As Exception
            Ext.Net.X.Msg.Alert("Información", result).Show()
        Finally
            desconecta(cnn)
        End Try
        Return dato
    End Function

    Public Function Editar(ByVal saih_his As saih_his)
        Dim parametros As New List(Of DbParameter)
        Dim paramnumhis As DbParameter = cmd.CreateParameter()
        paramnumhis.Value = saih_his.numhis
        paramnumhis.ParameterName = "numhis"
        parametros.Add(paramnumhis)
        Dim paramidentificacion As DbParameter = cmd.CreateParameter()
        paramidentificacion.Value = saih_his.identificacion
        paramidentificacion.ParameterName = "identificacion"
        parametros.Add(paramidentificacion)
        Dim paramtipo_identificacion As DbParameter = cmd.CreateParameter()
        paramtipo_identificacion.Value = saih_his.tipo_identificacion
        paramtipo_identificacion.ParameterName = "tipo_identificacion"
        parametros.Add(paramtipo_identificacion)
        Dim paramprimer_nombre As DbParameter = cmd.CreateParameter()
        paramprimer_nombre.Value = saih_his.primer_nombre
        paramprimer_nombre.ParameterName = "primer_nombre"
        parametros.Add(paramprimer_nombre)
        Dim paramsegundo_nombre As DbParameter = cmd.CreateParameter()
        paramsegundo_nombre.Value = saih_his.segundo_nombre
        paramsegundo_nombre.ParameterName = "segundo_nombre"
        parametros.Add(paramsegundo_nombre)
        Dim paramprimer_apellido As DbParameter = cmd.CreateParameter()
        paramprimer_apellido.Value = saih_his.primer_apellido
        paramprimer_apellido.ParameterName = "primer_apellido"
        parametros.Add(paramprimer_apellido)
        Dim paramsegundo_apellido As DbParameter = cmd.CreateParameter()
        paramsegundo_apellido.Value = saih_his.segundo_apellido
        paramsegundo_apellido.ParameterName = "segundo_apellido"
        parametros.Add(paramsegundo_apellido)
        Dim paramfecha_nacimiento As DbParameter = cmd.CreateParameter()
        paramfecha_nacimiento.Value = saih_his.fecha_nacimiento
        paramfecha_nacimiento.ParameterName = "fecha_nacimiento"
        parametros.Add(paramfecha_nacimiento)
        Dim paramid_lugar As DbParameter = cmd.CreateParameter()
        paramid_lugar.Value = saih_his.id_lugar
        paramid_lugar.ParameterName = "id_lugar"
        parametros.Add(paramid_lugar)
        Dim paramunidad_medida As DbParameter = cmd.CreateParameter()
        paramunidad_medida.Value = saih_his.unidad_medida
        paramunidad_medida.ParameterName = "unidad_medida"
        parametros.Add(paramunidad_medida)
        Dim paramsexo As DbParameter = cmd.CreateParameter()
        paramsexo.Value = saih_his.sexo
        paramsexo.ParameterName = "sexo"
        parametros.Add(paramsexo)
        Dim paramestado_civil As DbParameter = cmd.CreateParameter()
        paramestado_civil.Value = saih_his.estado_civil
        paramestado_civil.ParameterName = "estado_civil"
        parametros.Add(paramestado_civil)
        Dim paramdireccion As DbParameter = cmd.CreateParameter()
        paramdireccion.Value = saih_his.direccion
        paramdireccion.ParameterName = "direccion"
        parametros.Add(paramdireccion)
        Dim paramtelefono As DbParameter = cmd.CreateParameter()
        paramtelefono.Value = saih_his.telefono
        paramtelefono.ParameterName = "telefono"
        parametros.Add(paramtelefono)
        Dim paramtelefonoaux As DbParameter = cmd.CreateParameter()
        paramtelefonoaux.Value = saih_his.telefonoaux
        paramtelefonoaux.ParameterName = "telefonoaux"
        parametros.Add(paramtelefonoaux)
        Dim paramid_lugar_municipio As DbParameter = cmd.CreateParameter()
        paramid_lugar_municipio.Value = saih_his.id_lugar_municipio
        paramid_lugar_municipio.ParameterName = "id_lugar_municipio"
        parametros.Add(paramid_lugar_municipio)
        Dim paramid_lugar_barrio As DbParameter = cmd.CreateParameter()
        paramid_lugar_barrio.Value = saih_his.id_lugar_barrio
        paramid_lugar_barrio.ParameterName = "id_lugar_barrio"
        parametros.Add(paramid_lugar_barrio)
        Dim paramzona_residencia As DbParameter = cmd.CreateParameter()
        paramzona_residencia.Value = saih_his.zona_residencia
        paramzona_residencia.ParameterName = "zona_residencia"
        parametros.Add(paramzona_residencia)
        Dim paramestrato As DbParameter = cmd.CreateParameter()
        paramestrato.Value = saih_his.estrato
        paramestrato.ParameterName = "estrato"
        parametros.Add(paramestrato)
        Dim paramcodemp As DbParameter = cmd.CreateParameter()
        paramcodemp.Value = saih_his.codemp
        paramcodemp.ParameterName = "codemp"
        parametros.Add(paramcodemp)
        Dim paramcodarp As DbParameter = cmd.CreateParameter()
        paramcodarp.Value = saih_his.codarp
        paramcodarp.ParameterName = "codarp"
        parametros.Add(paramcodarp)
        Dim paramcodccf As DbParameter = cmd.CreateParameter()
        paramcodccf.Value = saih_his.codccf
        paramcodccf.ParameterName = "codccf"
        parametros.Add(paramcodccf)
        Dim paramapellido_conyuge As DbParameter = cmd.CreateParameter()
        paramapellido_conyuge.Value = saih_his.apellido_conyuge
        paramapellido_conyuge.ParameterName = "apellido_conyuge"
        parametros.Add(paramapellido_conyuge)
        Dim paramnombre_conyuge As DbParameter = cmd.CreateParameter()
        paramnombre_conyuge.Value = saih_his.nombre_conyuge
        paramnombre_conyuge.ParameterName = "nombre_conyuge"
        parametros.Add(paramnombre_conyuge)
        Dim paramnombre_padre As DbParameter = cmd.CreateParameter()
        paramnombre_padre.Value = saih_his.nombre_padre
        paramnombre_padre.ParameterName = "nombre_padre"
        parametros.Add(paramnombre_padre)
        Dim paramnombre_madre As DbParameter = cmd.CreateParameter()
        paramnombre_madre.Value = saih_his.nombre_madre
        paramnombre_madre.ParameterName = "nombre_madre"
        parametros.Add(paramnombre_madre)
        Dim paramusuario_creo As DbParameter = cmd.CreateParameter()
        paramusuario_creo.Value = saih_his.usuario_creo
        paramusuario_creo.ParameterName = "usuario_creo"
        parametros.Add(paramusuario_creo)
        Dim paramfecha_creacion As DbParameter = cmd.CreateParameter()
        paramfecha_creacion.Value = saih_his.fecha_creacion
        paramfecha_creacion.ParameterName = "fecha_creacion"
        parametros.Add(paramfecha_creacion)
        Dim paramusuario_modifico As DbParameter = cmd.CreateParameter()
        paramusuario_modifico.Value = saih_his.usuario_modifico
        paramusuario_modifico.ParameterName = "usuario_modifico"
        parametros.Add(paramusuario_modifico)
        Dim paramfecha_modificacion As DbParameter = cmd.CreateParameter()
        paramfecha_modificacion.Value = saih_his.fecha_modificacion
        paramfecha_modificacion.ParameterName = "fecha_modificacion"
        parametros.Add(paramfecha_modificacion)
        Dim paramobservacion As DbParameter = cmd.CreateParameter()
        paramobservacion.Value = saih_his.observacion
        paramobservacion.ParameterName = "observacion"
        parametros.Add(paramobservacion)
        Dim paramnrocon As DbParameter = cmd.CreateParameter()
        paramnrocon.Value = saih_his.nrocon
        paramnrocon.ParameterName = "nrocon"
        parametros.Add(paramnrocon)
        Dim paramubica As DbParameter = cmd.CreateParameter()
        paramubica.Value = saih_his.ubica
        paramubica.ParameterName = "ubica"
        parametros.Add(paramubica)
        Dim paramfactor_hr As DbParameter = cmd.CreateParameter()
        paramfactor_hr.Value = saih_his.factor_hr
        paramfactor_hr.ParameterName = "factor_hr"
        parametros.Add(paramfactor_hr)
        Dim paramfecha_muerte As DbParameter = cmd.CreateParameter()
        paramfecha_muerte.Value = saih_his.fecha_muerte
        paramfecha_muerte.ParameterName = "fecha_muerte"
        parametros.Add(paramfecha_muerte)
        Dim paramurl_foto As DbParameter = cmd.CreateParameter()
        paramurl_foto.Value = saih_his.url_foto
        paramurl_foto.ParameterName = "url_foto"
        parametros.Add(paramurl_foto)
        Dim parametnia As DbParameter = cmd.CreateParameter()
        parametnia.Value = saih_his.etnia
        parametnia.ParameterName = "etnia"
        parametros.Add(parametnia)
        Dim paramhuella As DbParameter = cmd.CreateParameter()
        paramhuella.Value = saih_his.huella
        paramhuella.ParameterName = "huella"
        parametros.Add(paramhuella)
        Dim paramnivel_escolar As DbParameter = cmd.CreateParameter()
        paramnivel_escolar.Value = saih_his.nivel_escolar
        paramnivel_escolar.ParameterName = "nivel_escolar"
        parametros.Add(paramnivel_escolar)
        Dim paramresponsable_economico As DbParameter = cmd.CreateParameter()
        paramresponsable_economico.Value = saih_his.responsable_economico
        paramresponsable_economico.ParameterName = "responsable_economico"
        parametros.Add(paramresponsable_economico)
        Dim paramreligion As DbParameter = cmd.CreateParameter()
        paramreligion.Value = saih_his.religion
        paramreligion.ParameterName = "religion"
        parametros.Add(paramreligion)
        Dim paramtipo_residencia As DbParameter = cmd.CreateParameter()
        paramtipo_residencia.Value = saih_his.tipo_residencia
        paramtipo_residencia.ParameterName = "tipo_residencia"
        parametros.Add(paramtipo_residencia)
        Dim paramescolar As DbParameter = cmd.CreateParameter()
        paramescolar.Value = saih_his.escolar
        paramescolar.ParameterName = "escolar"
        parametros.Add(paramescolar)
        Dim paramrazonsocial As DbParameter = cmd.CreateParameter()
        paramrazonsocial.Value = saih_his.razonsocial
        paramrazonsocial.ParameterName = "razonsocial"
        parametros.Add(paramrazonsocial)
        Dim paramestatura As DbParameter = cmd.CreateParameter()
        paramestatura.Value = saih_his.estatura
        paramestatura.ParameterName = "estatura"
        parametros.Add(paramestatura)
        Dim paramobservaciones As DbParameter = cmd.CreateParameter()
        paramobservaciones.Value = saih_his.observaciones
        paramobservaciones.ParameterName = "observaciones"
        parametros.Add(paramobservaciones)
        Dim paramcodocu As DbParameter = cmd.CreateParameter()
        paramcodocu.Value = saih_his.codocu
        paramcodocu.ParameterName = "codocu"
        parametros.Add(paramcodocu)
        Dim paramdiscapacidad As DbParameter = cmd.CreateParameter()
        paramdiscapacidad.Value = saih_his.discapacidad
        paramdiscapacidad.ParameterName = "discapacidad"
        parametros.Add(paramdiscapacidad)

        Return ejecutaNonQuery("cambiarsaih_his", parametros)
    End Function

    Public Function Borrar(
        ByVal numhis As String)
        Dim parametros As New List(Of DbParameter)
        Dim paramnumhis As DbParameter = cmd.CreateParameter()
        paramnumhis.Value = numhis
        paramnumhis.ParameterName = "numhis"
        parametros.Add(paramnumhis)
        Return ejecutaNonQuery("eliminasaih_his", parametros)
    End Function

    Public Function Select_Where_saih_his(ByVal Cond As String) As List(Of saih_his)
        Dim Listsaih_his As New List(Of saih_his)
        Dim appSettings = ConfigurationManager.AppSettings
        Dim result As String = appSettings("029")

        Try
            cnn = conecta()
            cmd = New SqlCommand("obtenersaih_hisByWhere")
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Connection = cnn
            Dim param As DbParameter = cmd.CreateParameter()
            param.Value = Cond
            param.ParameterName = "where"
            cmd.Parameters.Add(param)
            Dim Dr As DbDataReader = cmd.ExecuteReader()
            While Dr.Read
                Listsaih_his.Add(New saih_his(
                    Dr("numhis"),
                    Dr("identificacion"),
                    Dr("tipo_identificacion"),
                    Dr("primer_nombre"),
                    Dr("segundo_nombre"),
                    Dr("primer_apellido"),
                    Dr("segundo_apellido"),
                    Dr("fecha_nacimiento"),
                    Dr("id_lugar"),
                    Dr("unidad_medida"),
                    Dr("sexo"),
                    Dr("estado_civil"),
                    Dr("direccion"),
                    Dr("telefono"),
                    Dr("telefonoaux"),
                    Dr("id_lugar_municipio"),
                    Dr("id_lugar_barrio"),
                    Dr("zona_residencia"),
                    Dr("estrato"),
                    Dr("codemp"),
                    Dr("codarp"),
                    Dr("codccf"),
                    Dr("apellido_conyuge"),
                    Dr("nombre_conyuge"),
                    Dr("nombre_padre"),
                    Dr("nombre_madre"),
                    Dr("usuario_creo"),
                    Dr("fecha_creacion"),
                    Dr("usuario_modifico"),
                    Dr("fecha_modificacion"),
                    Dr("observacion"),
                    Dr("nrocon"),
                    Dr("ubica"),
                    Dr("factor_hr"),
                    Dr("fecha_muerte"),
                    Dr("url_foto"),
                    Dr("etnia"),
                    Dr("huella"),
                    Dr("nivel_escolar"),
                    Dr("responsable_economico"),
                    Dr("religion"),
                    Dr("tipo_residencia"),
                    Dr("escolar"),
                    Dr("razonsocial"),
                    Dr("estatura"),
                    Dr("observaciones"),
                    Dr("codocu"),
                Dr("discapacidad")))
            End While
        Catch ex As Exception
            Ext.Net.X.Msg.Alert("Información", result).Show()
        Finally
            desconecta(cnn)
        End Try
        Return Listsaih_his
    End Function

    Public Function Select_ById_saih_his(
        ByVal numhis As String) As List(Of saih_his)
        Dim Listsaih_his As New List(Of saih_his)
        Dim appSettings = ConfigurationManager.AppSettings
        Dim result As String = appSettings("029")

        Try
            cnn = conecta()
            cmd = New SqlCommand("obtenersaih_hisById")
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Connection = cnn
            Dim paramnumhis As DbParameter = cmd.CreateParameter()
            paramnumhis.Value = numhis
            paramnumhis.ParameterName = "numhis"
            cmd.Parameters.Add(paramnumhis)
            Dim Dr As DbDataReader = cmd.ExecuteReader()
            While Dr.Read
                Listsaih_his.Add(New saih_his(
                    Dr("numhis"),
                    Dr("identificacion"),
                    Dr("tipo_identificacion"),
                    Dr("primer_nombre"),
                    Dr("segundo_nombre"),
                    Dr("primer_apellido"),
                    Dr("segundo_apellido"),
                    Dr("fecha_nacimiento"),
                    Dr("id_lugar"),
                    Dr("unidad_medida"),
                    Dr("sexo"),
                    Dr("estado_civil"),
                    Dr("direccion"),
                    Dr("telefono"),
                    Dr("telefonoaux"),
                    Dr("id_lugar_municipio"),
                    Dr("id_lugar_barrio"),
                    Dr("zona_residencia"),
                    Dr("estrato"),
                    Dr("codemp"),
                    Dr("codarp"),
                    Dr("codccf"),
                    Dr("apellido_conyuge"),
                    Dr("nombre_conyuge"),
                    Dr("nombre_padre"),
                    Dr("nombre_madre"),
                    Dr("usuario_creo"),
                    Dr("fecha_creacion"),
                    Dr("usuario_modifico"),
                    Dr("fecha_modificacion"),
                    Dr("observacion"),
                    Dr("nrocon"),
                    Dr("ubica"),
                    Dr("factor_hr"),
                    Dr("fecha_muerte"),
                    Dr("url_foto"),
                    Dr("etnia"),
                    Dr("huella"),
                    Dr("nivel_escolar"),
                    Dr("responsable_economico"),
                    Dr("religion"),
                    Dr("tipo_residencia"),
                    Dr("escolar"),
                    Dr("razonsocial"),
                    Dr("estatura"),
                    Dr("observaciones"),
                    Dr("codocu"),
                    Dr("discapacidad")))
            End While
        Catch ex As Exception
            Ext.Net.X.Msg.Alert("Información", result).Show()
        Finally
            desconecta(cnn)
        End Try
        Return Listsaih_his
    End Function

    Public Function Select_saih_his() As List(Of saih_his)
        Dim Listsaih_his As New List(Of saih_his)
        Dim appSettings = ConfigurationManager.AppSettings
        Dim result As String = appSettings("029")

        Try
            cnn = conecta()
            cmd = New SqlCommand("obtenersaih_his")
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Connection = cnn
            Dim Dr As DbDataReader = cmd.ExecuteReader()
            While Dr.Read
                Listsaih_his.Add(New saih_his(
                    Dr("numhis"),
                    Dr("identificacion"),
                    Dr("tipo_identificacion"),
                    Dr("primer_nombre"),
                    Dr("segundo_nombre"),
                    Dr("primer_apellido"),
                    Dr("segundo_apellido"),
                    Dr("fecha_nacimiento"),
                    Dr("id_lugar"),
                    Dr("unidad_medida"),
                    Dr("sexo"),
                    Dr("estado_civil"),
                    Dr("direccion"),
                    Dr("telefono"),
                    Dr("telefonoaux"),
                    Dr("id_lugar_municipio"),
                    Dr("id_lugar_barrio"),
                    Dr("zona_residencia"),
                    Dr("estrato"),
                    Dr("codemp"),
                    Dr("codarp"),
                    Dr("codccf"),
                    Dr("apellido_conyuge"),
                    Dr("nombre_conyuge"),
                    Dr("nombre_padre"),
                    Dr("nombre_madre"),
                    Dr("usuario_creo"),
                    Dr("fecha_creacion"),
                    Dr("usuario_modifico"),
                    Dr("fecha_modificacion"),
                    Dr("observacion"),
                    Dr("nrocon"),
                    Dr("ubica"),
                    Dr("factor_hr"),
                    Dr("fecha_muerte"),
                    Dr("url_foto"),
                    Dr("etnia"),
                    Dr("huella"),
                    Dr("nivel_escolar"),
                    Dr("responsable_economico"),
                    Dr("religion"),
                    Dr("tipo_residencia"),
                    Dr("escolar"),
                    Dr("razonsocial"),
                    Dr("estatura"),
                    Dr("observaciones"),
                    Dr("codocu"),
                    Dr("discapacidad")))
            End While
        Catch ex As Exception
            Ext.Net.X.Msg.Alert("Información", result).Show()
        Finally
            desconecta(cnn)
        End Try
        Return Listsaih_his
    End Function

End Class
Imports System.Data
Imports System.Data.SqlClient
Imports System.Data.Common
Imports System.Configuration

Public Class DatoPaginaWeb
    Inherits conexion

    Public Function Insertar(ByVal PaginaWeb As PaginaWeb)
        Dim parametros As New List(Of DbParameter)
        Dim paramnombre As DbParameter = cmd.CreateParameter()
        paramnombre.Value = PaginaWeb.nombre
        paramnombre.ParameterName = "nombre"
        parametros.Add(paramnombre)
        Return ejecutaNonQuery("insertaPaginaWeb", parametros)
    End Function

    Public Function InsertarRet(ByVal PaginaWeb As PaginaWeb)
        Dim dato As Integer = 0
        Dim appSettings = ConfigurationManager.AppSettings
        Dim result As String = appSettings("042")

        Try
            cnn = conecta()
            cmd = New SqlCommand("insertaPaginaWeb")
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Connection = cnn
            Dim paramnombre As DbParameter = cmd.CreateParameter()
            paramnombre.Value = PaginaWeb.nombre
            paramnombre.ParameterName = "nombre"
            cmd.Parameters.Add(paramnombre)
            Dim Dr As DbDataReader = cmd.ExecuteReader()
            While Dr.Read
                dato = Dr(0)
            End While
        Catch ex As Exception
            Ext.Net.X.Msg.Alert("Información", result).Show()
        Finally
            desconecta(cnn)
        End Try
        Return dato
    End Function

    Public Function Editar(ByVal PaginaWeb As PaginaWeb)
        Dim parametros As New List(Of DbParameter)
        Dim paramid As DbParameter = cmd.CreateParameter()
        paramid.Value = PaginaWeb.id
        paramid.ParameterName = "id"
        parametros.Add(paramid)
        Dim paramnombre As DbParameter = cmd.CreateParameter()
        paramnombre.Value = PaginaWeb.nombre
        paramnombre.ParameterName = "nombre"
        parametros.Add(paramnombre)
        Return ejecutaNonQuery("cambiarPaginaWeb", parametros)
    End Function

    Public Function Borrar(
        ByVal id As Integer)
        Dim parametros As New List(Of DbParameter)
        Dim paramid As DbParameter = cmd.CreateParameter()
        paramid.Value = id
        paramid.ParameterName = "id"
        parametros.Add(paramid)
        Return ejecutaNonQuery("eliminaPaginaWeb", parametros)
    End Function

    Public Function Select_Where_PaginaWeb(ByVal Cond As String) As List(Of PaginaWeb)
        Dim ListPaginaWeb As New List(Of PaginaWeb)
        Dim appSettings = ConfigurationManager.AppSettings
        Dim result As String = appSettings("042")

        Try
            cnn = conecta()
            cmd = New SqlCommand("obtenerPaginaWebByWhere")
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Connection = cnn
            Dim param As DbParameter = cmd.CreateParameter()
            param.Value = Cond
            param.ParameterName = "where"
            cmd.Parameters.Add(param)
            Dim Dr As DbDataReader = cmd.ExecuteReader()
            While Dr.Read
                ListPaginaWeb.Add(New PaginaWeb(
                    Dr("id"),
                    Dr("nombre")))
            End While
        Catch ex As Exception
            Ext.Net.X.Msg.Alert("Información", result).Show()
        Finally
            desconecta(cnn)
        End Try
        Return ListPaginaWeb
    End Function

    Public Function Select_ById_PaginaWeb(
        ByVal id As Integer) As List(Of PaginaWeb)
        Dim ListPaginaWeb As New List(Of PaginaWeb)
        Dim appSettings = ConfigurationManager.AppSettings
        Dim result As String = appSettings("042")

        Try
            cnn = conecta()
            cmd = New SqlCommand("obtenerPaginaWebById")
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Connection = cnn
            Dim paramid As DbParameter = cmd.CreateParameter()
            paramid.Value = id
            paramid.ParameterName = "id"
            cmd.Parameters.Add(paramid)
            Dim Dr As DbDataReader = cmd.ExecuteReader()
            While Dr.Read
                ListPaginaWeb.Add(New PaginaWeb(
                    Dr("id"),
                    Dr("nombre")))
            End While
        Catch ex As Exception
            Ext.Net.X.Msg.Alert("Información", result).Show()
        Finally
            desconecta(cnn)
        End Try
        Return ListPaginaWeb
    End Function

    Public Function Select_PaginaWeb() As List(Of PaginaWeb)
        Dim ListPaginaWeb As New List(Of PaginaWeb)
        Dim appSettings = ConfigurationManager.AppSettings
        Dim result As String = appSettings("042")

        Try
            cnn = conecta()
            cmd = New SqlCommand("obtenerPaginaWeb")
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Connection = cnn
            Dim Dr As DbDataReader = cmd.ExecuteReader()
            While Dr.Read
                ListPaginaWeb.Add(New PaginaWeb(
                    Dr("id"),
                    Dr("nombre")))
            End While
        Catch ex As Exception
            Ext.Net.X.Msg.Alert("Información", result).Show()
        Finally
            desconecta(cnn)
        End Try
        Return ListPaginaWeb
    End Function

End Class
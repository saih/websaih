Imports System.Data
Imports System.Data.SqlClient
Imports System.Data.Common
Imports System.Configuration

Public Class Datosaih_emp
   Inherits conexion

  Public Function Insertar(ByVal saih_emp As saih_emp)
      Dim parametros As New List(Of DbParameter)
      Dim paramcodigo As DbParameter = cmd.CreateParameter()
      paramcodigo.Value = saih_emp.codigo
      paramcodigo.ParameterName = "codigo"
      parametros.Add(paramcodigo)
      Dim paramnit As DbParameter = cmd.CreateParameter()
      paramnit.Value = saih_emp.nit
      paramnit.ParameterName = "nit"
      parametros.Add(paramnit)
      Dim paramrazonsocial As DbParameter = cmd.CreateParameter()
      paramrazonsocial.Value = saih_emp.razonsocial
      paramrazonsocial.ParameterName = "razonsocial"
      parametros.Add(paramrazonsocial)
      Dim paramdireccion As DbParameter = cmd.CreateParameter()
      paramdireccion.Value = saih_emp.direccion
      paramdireccion.ParameterName = "direccion"
      parametros.Add(paramdireccion)
      Dim paramtelefono As DbParameter = cmd.CreateParameter()
      paramtelefono.Value = saih_emp.telefono
      paramtelefono.ParameterName = "telefono"
      parametros.Add(paramtelefono)
      Dim paramfax As DbParameter = cmd.CreateParameter()
      paramfax.Value = saih_emp.fax
      paramfax.ParameterName = "fax"
      parametros.Add(paramfax)
      Dim paramcontacto As DbParameter = cmd.CreateParameter()
      paramcontacto.Value = saih_emp.contacto
      paramcontacto.ParameterName = "contacto"
      parametros.Add(paramcontacto)
      Dim paramtipo_fact As DbParameter = cmd.CreateParameter()
      paramtipo_fact.Value = saih_emp.tipo_fact
      paramtipo_fact.ParameterName = "tipo_fact"
      parametros.Add(paramtipo_fact)
      Dim paramcod_contable As DbParameter = cmd.CreateParameter()
      paramcod_contable.Value = saih_emp.cod_contable
      paramcod_contable.ParameterName = "cod_contable"
      parametros.Add(paramcod_contable)
      Dim paramcod_empFctProc As DbParameter = cmd.CreateParameter()
      paramcod_empFctProc.Value = saih_emp.cod_empFctProc
      paramcod_empFctProc.ParameterName = "cod_empFctProc"
      parametros.Add(paramcod_empFctProc)
      Dim paramcod_empSolCuentas As DbParameter = cmd.CreateParameter()
      paramcod_empSolCuentas.Value = saih_emp.cod_empSolCuentas
      paramcod_empSolCuentas.ParameterName = "cod_empSolCuentas"
      parametros.Add(paramcod_empSolCuentas)
      Return ejecutaNonQuery("insertasaih_emp", parametros)
  End Function

  Public Function InsertarRet(ByVal saih_emp As saih_emp)
        Dim dato As Integer = 0
        Dim appSettings = ConfigurationManager.AppSettings
        Dim result As String = appSettings("030")

      Try
          cnn = conecta()
          cmd = New SqlCommand("insertasaih_emp")
          cmd.CommandType = CommandType.StoredProcedure
          cmd.Connection = cnn
        Dim paramcodigo As DbParameter = cmd.CreateParameter()
        paramcodigo.Value = saih_emp.codigo
        paramcodigo.ParameterName = "codigo"
        cmd.Parameters.Add(paramcodigo)
        Dim paramnit As DbParameter = cmd.CreateParameter()
        paramnit.Value = saih_emp.nit
        paramnit.ParameterName = "nit"
        cmd.Parameters.Add(paramnit)
        Dim paramrazonsocial As DbParameter = cmd.CreateParameter()
        paramrazonsocial.Value = saih_emp.razonsocial
        paramrazonsocial.ParameterName = "razonsocial"
        cmd.Parameters.Add(paramrazonsocial)
        Dim paramdireccion As DbParameter = cmd.CreateParameter()
        paramdireccion.Value = saih_emp.direccion
        paramdireccion.ParameterName = "direccion"
        cmd.Parameters.Add(paramdireccion)
        Dim paramtelefono As DbParameter = cmd.CreateParameter()
        paramtelefono.Value = saih_emp.telefono
        paramtelefono.ParameterName = "telefono"
        cmd.Parameters.Add(paramtelefono)
        Dim paramfax As DbParameter = cmd.CreateParameter()
        paramfax.Value = saih_emp.fax
        paramfax.ParameterName = "fax"
        cmd.Parameters.Add(paramfax)
        Dim paramcontacto As DbParameter = cmd.CreateParameter()
        paramcontacto.Value = saih_emp.contacto
        paramcontacto.ParameterName = "contacto"
        cmd.Parameters.Add(paramcontacto)
        Dim paramtipo_fact As DbParameter = cmd.CreateParameter()
        paramtipo_fact.Value = saih_emp.tipo_fact
        paramtipo_fact.ParameterName = "tipo_fact"
        cmd.Parameters.Add(paramtipo_fact)
        Dim paramcod_contable As DbParameter = cmd.CreateParameter()
        paramcod_contable.Value = saih_emp.cod_contable
        paramcod_contable.ParameterName = "cod_contable"
        cmd.Parameters.Add(paramcod_contable)
        Dim paramcod_empFctProc As DbParameter = cmd.CreateParameter()
        paramcod_empFctProc.Value = saih_emp.cod_empFctProc
        paramcod_empFctProc.ParameterName = "cod_empFctProc"
        cmd.Parameters.Add(paramcod_empFctProc)
        Dim paramcod_empSolCuentas As DbParameter = cmd.CreateParameter()
        paramcod_empSolCuentas.Value = saih_emp.cod_empSolCuentas
        paramcod_empSolCuentas.ParameterName = "cod_empSolCuentas"
        cmd.Parameters.Add(paramcod_empSolCuentas)
          Dim Dr As DbDataReader = cmd.ExecuteReader()
          While Dr.Read
            dato = Dr(0)
          End While
      Catch ex As Exception
            Ext.Net.X.Msg.Alert("Información", result).Show()
      Finally
          desconecta(cnn)
      End Try
      Return dato
  End Function

  Public Function Editar(ByVal saih_emp As saih_emp)
      Dim parametros As New List(Of DbParameter)
      Dim paramcodigo As DbParameter = cmd.CreateParameter()
      paramcodigo.Value = saih_emp.codigo
      paramcodigo.ParameterName = "codigo"
      parametros.Add(paramcodigo)
      Dim paramnit As DbParameter = cmd.CreateParameter()
      paramnit.Value = saih_emp.nit
      paramnit.ParameterName = "nit"
      parametros.Add(paramnit)
      Dim paramrazonsocial As DbParameter = cmd.CreateParameter()
      paramrazonsocial.Value = saih_emp.razonsocial
      paramrazonsocial.ParameterName = "razonsocial"
      parametros.Add(paramrazonsocial)
      Dim paramdireccion As DbParameter = cmd.CreateParameter()
      paramdireccion.Value = saih_emp.direccion
      paramdireccion.ParameterName = "direccion"
      parametros.Add(paramdireccion)
      Dim paramtelefono As DbParameter = cmd.CreateParameter()
      paramtelefono.Value = saih_emp.telefono
      paramtelefono.ParameterName = "telefono"
      parametros.Add(paramtelefono)
      Dim paramfax As DbParameter = cmd.CreateParameter()
      paramfax.Value = saih_emp.fax
      paramfax.ParameterName = "fax"
      parametros.Add(paramfax)
      Dim paramcontacto As DbParameter = cmd.CreateParameter()
      paramcontacto.Value = saih_emp.contacto
      paramcontacto.ParameterName = "contacto"
      parametros.Add(paramcontacto)
      Dim paramtipo_fact As DbParameter = cmd.CreateParameter()
      paramtipo_fact.Value = saih_emp.tipo_fact
      paramtipo_fact.ParameterName = "tipo_fact"
      parametros.Add(paramtipo_fact)
      Dim paramcod_contable As DbParameter = cmd.CreateParameter()
      paramcod_contable.Value = saih_emp.cod_contable
      paramcod_contable.ParameterName = "cod_contable"
      parametros.Add(paramcod_contable)
      Dim paramcod_empFctProc As DbParameter = cmd.CreateParameter()
      paramcod_empFctProc.Value = saih_emp.cod_empFctProc
      paramcod_empFctProc.ParameterName = "cod_empFctProc"
      parametros.Add(paramcod_empFctProc)
      Dim paramcod_empSolCuentas As DbParameter = cmd.CreateParameter()
      paramcod_empSolCuentas.Value = saih_emp.cod_empSolCuentas
      paramcod_empSolCuentas.ParameterName = "cod_empSolCuentas"
      parametros.Add(paramcod_empSolCuentas)
      Return ejecutaNonQuery("cambiarsaih_emp", parametros)
  End Function

  Public Function Borrar(
      ByVal codigo As string)
      Dim parametros As New List(Of DbParameter)
      Dim paramcodigo As DbParameter = cmd.CreateParameter()
      paramcodigo.Value = codigo
      paramcodigo.ParameterName = "codigo"
      parametros.Add(paramcodigo)
      Return ejecutaNonQuery("eliminasaih_emp", parametros)
  End Function

  Public Function Select_Where_saih_emp(ByVal Cond As String) As List(Of saih_emp)
        Dim Listsaih_emp As New List(Of saih_emp)
        Dim appSettings = ConfigurationManager.AppSettings
        Dim result As String = appSettings("031")

      Try
          cnn = conecta()
          cmd = New SqlCommand("obtenersaih_empByWhere")
          cmd.CommandType = CommandType.StoredProcedure
          cmd.Connection = cnn
          Dim param As DbParameter = cmd.CreateParameter()
          param.Value = Cond
          param.ParameterName = "where"
          cmd.Parameters.Add(param)
          Dim Dr As DbDataReader = cmd.ExecuteReader()
          While Dr.Read
              Listsaih_emp.Add(New saih_emp(
                  Dr("codigo"),
                  Dr("nit"),
                  Dr("razonsocial"),
                  Dr("direccion"),
                  Dr("telefono"),
                  Dr("fax"),
                  Dr("contacto"),
                  Dr("tipo_fact"),
                  Dr("cod_contable"),
                  Dr("cod_empFctProc"),
                  Dr("cod_empSolCuentas")))
          End While
      Catch ex As Exception
            Ext.Net.X.Msg.Alert("Información", result).Show()
      Finally
          desconecta(cnn)
      End Try
      Return Listsaih_emp
  End Function

  Public Function Select_ById_saih_emp(
      ByVal codigo As string) As List(Of saih_emp)
        Dim Listsaih_emp As New List(Of saih_emp)
        Dim appSettings = ConfigurationManager.AppSettings
        Dim result As String = appSettings("031")

      Try
          cnn = conecta()
          cmd = New SqlCommand("obtenersaih_empById")
          cmd.CommandType = CommandType.StoredProcedure
          cmd.Connection = cnn
      Dim paramcodigo As DbParameter = cmd.CreateParameter()
      paramcodigo.Value = codigo
      paramcodigo.ParameterName = "codigo"
      cmd.Parameters.Add(paramcodigo)
          Dim Dr As DbDataReader = cmd.ExecuteReader()
          While Dr.Read
              Listsaih_emp.Add(New saih_emp(
                  Dr("codigo"),
                  Dr("nit"),
                  Dr("razonsocial"),
                  Dr("direccion"),
                  Dr("telefono"),
                  Dr("fax"),
                  Dr("contacto"),
                  Dr("tipo_fact"),
                  Dr("cod_contable"),
                  Dr("cod_empFctProc"),
                  Dr("cod_empSolCuentas")))
          End While
      Catch ex As Exception
            Ext.Net.X.Msg.Alert("Información", result).Show()
      Finally
          desconecta(cnn)
      End Try
      Return Listsaih_emp
  End Function

  Public Function Select_saih_emp() As List(Of saih_emp)
        Dim Listsaih_emp As New List(Of saih_emp)
        Dim appSettings = ConfigurationManager.AppSettings
        Dim result As String = appSettings("031")

      Try
         cnn = conecta()
          cmd = New SqlCommand("obtenersaih_emp")
          cmd.CommandType = CommandType.StoredProcedure
          cmd.Connection = cnn
          Dim Dr As DbDataReader = cmd.ExecuteReader()
          While Dr.Read
              Listsaih_emp.Add(New saih_emp(
                  Dr("codigo"),
                  Dr("nit"),
                  Dr("razonsocial"),
                  Dr("direccion"),
                  Dr("telefono"),
                  Dr("fax"),
                  Dr("contacto"),
                  Dr("tipo_fact"),
                  Dr("cod_contable"),
                  Dr("cod_empFctProc"),
                  Dr("cod_empSolCuentas")))
          End While
      Catch ex As Exception
            Ext.Net.X.Msg.Alert("Información", result).Show()
      Finally
          desconecta(cnn)
      End Try
      Return Listsaih_emp
  End Function

End Class
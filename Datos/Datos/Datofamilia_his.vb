Imports System.Data
Imports System.Data.SqlClient
Imports System.Data.Common

Public Class Datofamilia_his
   Inherits conexion

  Public Function Insertar(ByVal familia_his As familia_his)
      Dim parametros As New List(Of DbParameter)
      Dim paramid_familia As DbParameter = cmd.CreateParameter()
      paramid_familia.Value = familia_his.id_familia
      paramid_familia.ParameterName = "id_familia"
      parametros.Add(paramid_familia)
      Dim paramnumhis As DbParameter = cmd.CreateParameter()
      paramnumhis.Value = familia_his.numhis
      paramnumhis.ParameterName = "numhis"
      parametros.Add(paramnumhis)
      Dim paramrol As DbParameter = cmd.CreateParameter()
      paramrol.Value = familia_his.rol
      paramrol.ParameterName = "rol"
      parametros.Add(paramrol)
      Dim paramfecha_adicion As DbParameter = cmd.CreateParameter()
      paramfecha_adicion.Value = familia_his.fecha_adicion
      paramfecha_adicion.ParameterName = "fecha_adicion"
      parametros.Add(paramfecha_adicion)
      Dim paramusuario_creo As DbParameter = cmd.CreateParameter()
      paramusuario_creo.Value = familia_his.usuario_creo
      paramusuario_creo.ParameterName = "usuario_creo"
      parametros.Add(paramusuario_creo)
      Dim paramfecha_modificacion As DbParameter = cmd.CreateParameter()
      paramfecha_modificacion.Value = familia_his.fecha_modificacion
      paramfecha_modificacion.ParameterName = "fecha_modificacion"
      parametros.Add(paramfecha_modificacion)
      Dim paramusuario_modifico As DbParameter = cmd.CreateParameter()
      paramusuario_modifico.Value = familia_his.usuario_modifico
      paramusuario_modifico.ParameterName = "usuario_modifico"
      parametros.Add(paramusuario_modifico)
      Return ejecutaNonQuery("insertafamilia_his", parametros)
  End Function

  Public Function InsertarRet(ByVal familia_his As familia_his)
      Dim dato As Integer = 0
      Try
          cnn = conecta()
          cmd = New SqlCommand("insertafamilia_his")
          cmd.CommandType = CommandType.StoredProcedure
          cmd.Connection = cnn
        Dim paramid_familia As DbParameter = cmd.CreateParameter()
        paramid_familia.Value = familia_his.id_familia
        paramid_familia.ParameterName = "id_familia"
        cmd.Parameters.Add(paramid_familia)
        Dim paramnumhis As DbParameter = cmd.CreateParameter()
        paramnumhis.Value = familia_his.numhis
        paramnumhis.ParameterName = "numhis"
        cmd.Parameters.Add(paramnumhis)
        Dim paramrol As DbParameter = cmd.CreateParameter()
        paramrol.Value = familia_his.rol
        paramrol.ParameterName = "rol"
        cmd.Parameters.Add(paramrol)
        Dim paramfecha_adicion As DbParameter = cmd.CreateParameter()
        paramfecha_adicion.Value = familia_his.fecha_adicion
        paramfecha_adicion.ParameterName = "fecha_adicion"
        cmd.Parameters.Add(paramfecha_adicion)
        Dim paramusuario_creo As DbParameter = cmd.CreateParameter()
        paramusuario_creo.Value = familia_his.usuario_creo
        paramusuario_creo.ParameterName = "usuario_creo"
        cmd.Parameters.Add(paramusuario_creo)
        Dim paramfecha_modificacion As DbParameter = cmd.CreateParameter()
        paramfecha_modificacion.Value = familia_his.fecha_modificacion
        paramfecha_modificacion.ParameterName = "fecha_modificacion"
        cmd.Parameters.Add(paramfecha_modificacion)
        Dim paramusuario_modifico As DbParameter = cmd.CreateParameter()
        paramusuario_modifico.Value = familia_his.usuario_modifico
        paramusuario_modifico.ParameterName = "usuario_modifico"
        cmd.Parameters.Add(paramusuario_modifico)
          Dim Dr As DbDataReader = cmd.ExecuteReader()
          While Dr.Read
            dato = Dr(0)
          End While
      Catch ex As Exception
          Ext.Net.X.Msg.Alert("Información", ex.Message).Show()
      Finally
          desconecta(cnn)
      End Try
      Return dato
  End Function

  Public Function Editar(ByVal familia_his As familia_his)
      Dim parametros As New List(Of DbParameter)
      Dim paramid_familia As DbParameter = cmd.CreateParameter()
      paramid_familia.Value = familia_his.id_familia
      paramid_familia.ParameterName = "id_familia"
      parametros.Add(paramid_familia)
      Dim paramnumhis As DbParameter = cmd.CreateParameter()
      paramnumhis.Value = familia_his.numhis
      paramnumhis.ParameterName = "numhis"
      parametros.Add(paramnumhis)
      Dim paramrol As DbParameter = cmd.CreateParameter()
      paramrol.Value = familia_his.rol
      paramrol.ParameterName = "rol"
      parametros.Add(paramrol)
      Dim paramfecha_adicion As DbParameter = cmd.CreateParameter()
      paramfecha_adicion.Value = familia_his.fecha_adicion
      paramfecha_adicion.ParameterName = "fecha_adicion"
      parametros.Add(paramfecha_adicion)
      Dim paramusuario_creo As DbParameter = cmd.CreateParameter()
      paramusuario_creo.Value = familia_his.usuario_creo
      paramusuario_creo.ParameterName = "usuario_creo"
      parametros.Add(paramusuario_creo)
      Dim paramfecha_modificacion As DbParameter = cmd.CreateParameter()
      paramfecha_modificacion.Value = familia_his.fecha_modificacion
      paramfecha_modificacion.ParameterName = "fecha_modificacion"
      parametros.Add(paramfecha_modificacion)
      Dim paramusuario_modifico As DbParameter = cmd.CreateParameter()
      paramusuario_modifico.Value = familia_his.usuario_modifico
      paramusuario_modifico.ParameterName = "usuario_modifico"
      parametros.Add(paramusuario_modifico)
      Return ejecutaNonQuery("cambiarfamilia_his", parametros)
  End Function

  Public Function Borrar(
      ByVal id_familia As integer,
      ByVal numhis As string)
      Dim parametros As New List(Of DbParameter)
      Dim paramid_familia As DbParameter = cmd.CreateParameter()
      paramid_familia.Value = id_familia
      paramid_familia.ParameterName = "id_familia"
      parametros.Add(paramid_familia)
      Dim paramnumhis As DbParameter = cmd.CreateParameter()
      paramnumhis.Value = numhis
      paramnumhis.ParameterName = "numhis"
      parametros.Add(paramnumhis)
      Return ejecutaNonQuery("eliminafamilia_his", parametros)
  End Function

  Public Function Select_Where_familia_his(ByVal Cond As String) As List(Of familia_his)
      Dim Listfamilia_his As New List(Of familia_his)
      Try
          cnn = conecta()
          cmd = New SqlCommand("obtenerfamilia_hisByWhere")
          cmd.CommandType = CommandType.StoredProcedure
          cmd.Connection = cnn
          Dim param As DbParameter = cmd.CreateParameter()
          param.Value = Cond
          param.ParameterName = "where"
          cmd.Parameters.Add(param)
          Dim Dr As DbDataReader = cmd.ExecuteReader()
          While Dr.Read
              Listfamilia_his.Add(New familia_his(
                  Dr("id_familia"),
                  Dr("numhis"),
                  Dr("rol"),
                  Dr("fecha_adicion"),
                  Dr("usuario_creo"),
                  Dr("fecha_modificacion"),
                  Dr("usuario_modifico")))
          End While
      Catch ex As Exception
          Ext.Net.X.Msg.Alert("Información", ex.Message).Show()
      Finally
          desconecta(cnn)
      End Try
      Return Listfamilia_his
  End Function

  Public Function Select_ById_familia_his(
      ByVal id_familia As integer,
      ByVal numhis As string) As List(Of familia_his)
      Dim Listfamilia_his As New List(Of familia_his)
      Try
          cnn = conecta()
          cmd = New SqlCommand("obtenerfamilia_hisById")
          cmd.CommandType = CommandType.StoredProcedure
          cmd.Connection = cnn
      Dim paramid_familia As DbParameter = cmd.CreateParameter()
      paramid_familia.Value = id_familia
      paramid_familia.ParameterName = "id_familia"
      cmd.Parameters.Add(paramid_familia)
      Dim paramnumhis As DbParameter = cmd.CreateParameter()
      paramnumhis.Value = numhis
      paramnumhis.ParameterName = "numhis"
      cmd.Parameters.Add(paramnumhis)
          Dim Dr As DbDataReader = cmd.ExecuteReader()
          While Dr.Read
              Listfamilia_his.Add(New familia_his(
                  Dr("id_familia"),
                  Dr("numhis"),
                  Dr("rol"),
                  Dr("fecha_adicion"),
                  Dr("usuario_creo"),
                  Dr("fecha_modificacion"),
                  Dr("usuario_modifico")))
          End While
      Catch ex As Exception
          Ext.Net.X.Msg.Alert("Información", ex.Message).Show()
      Finally
          desconecta(cnn)
      End Try
      Return Listfamilia_his
  End Function

  Public Function Select_familia_his() As List(Of familia_his)
      Dim Listfamilia_his As New List(Of familia_his)
      Try
         cnn = conecta()
          cmd = New SqlCommand("obtenerfamilia_his")
          cmd.CommandType = CommandType.StoredProcedure
          cmd.Connection = cnn
          Dim Dr As DbDataReader = cmd.ExecuteReader()
          While Dr.Read
              Listfamilia_his.Add(New familia_his(
                  Dr("id_familia"),
                  Dr("numhis"),
                  Dr("rol"),
                  Dr("fecha_adicion"),
                  Dr("usuario_creo"),
                  Dr("fecha_modificacion"),
                  Dr("usuario_modifico")))
          End While
      Catch ex As Exception
          Ext.Net.X.Msg.Alert("Información", ex.Message).Show()
      Finally
          desconecta(cnn)
      End Try
      Return Listfamilia_his
  End Function

End Class
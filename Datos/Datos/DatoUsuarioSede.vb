﻿Imports System.Data
Imports System.Data.SqlClient
Imports System.Data.Common
Imports System.Configuration

Public Class DatoUsuarioSede
    Inherits conexion

    Public Function Insertar(ByVal UsuarioSede As UsuarioSede)
        Dim parametros As New List(Of DbParameter)
        Dim paramIdUsuario As DbParameter = cmd.CreateParameter()
        paramIdUsuario.Value = UsuarioSede.idUsuario
        paramIdUsuario.ParameterName = "idUsuario"
        parametros.Add(paramIdUsuario)
        Dim paramIdsede As DbParameter = cmd.CreateParameter()
        paramIdsede.Value = UsuarioSede.idSede
        paramIdsede.ParameterName = "idSede"
        parametros.Add(paramIdsede)
        Return ejecutaNonQuery("insertaUsuarioSede", parametros)
    End Function

    Public Function InsertarRet(ByVal UsuarioSede As UsuarioSede)
        Dim dato As Integer = 0
        Dim appSettings = ConfigurationManager.AppSettings
        Dim result As String = appSettings("002")
        Try
            cnn = conecta()
            cmd = New SqlCommand("insertaUsuarioSede")
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Connection = cnn
            Dim paramIdUsuario As DbParameter = cmd.CreateParameter()
            paramIdUsuario.Value = UsuarioSede.idUsuario
            paramIdUsuario.ParameterName = "idUsuario"
            cmd.Parameters.Add(paramIdUsuario)
            Dim paramIdsede As DbParameter = cmd.CreateParameter()
            paramIdsede.Value = UsuarioSede.idSede
            paramIdsede.ParameterName = "idSede"
            cmd.Parameters.Add(paramIdsede)

            Dim Dr As DbDataReader = cmd.ExecuteReader()
            While Dr.Read
                dato = Dr(0)
            End While
        Catch ex As Exception
            Ext.Net.X.Msg.Alert("Información", result).Show()
        Finally
            desconecta(cnn)
        End Try
        Return dato
    End Function

    Public Function Editar(ByVal UsuarioSede As UsuarioSede)
        Dim parametros As New List(Of DbParameter)
        Dim paramUsuarioSede As DbParameter = cmd.CreateParameter()
        paramUsuarioSede.Value = UsuarioSede.UsuarioSede
        paramUsuarioSede.ParameterName = "UsuarioSede"
        parametros.Add(paramUsuarioSede)
        Dim paramIdUsuario As DbParameter = cmd.CreateParameter()
        paramIdUsuario.Value = UsuarioSede.idUsuario
        paramIdUsuario.ParameterName = "idUsuario"
        parametros.Add(paramIdUsuario)
        Dim paramIdsede As DbParameter = cmd.CreateParameter()
        paramIdsede.Value = UsuarioSede.idSede
        paramIdsede.ParameterName = "idSede"
        parametros.Add(paramIdsede)
        Return ejecutaNonQuery("cambiarUsuarioSede", parametros)
    End Function

    Public Function Borrar(
        ByVal id As Integer)
        Dim parametros As New List(Of DbParameter)
        Dim paramid As DbParameter = cmd.CreateParameter()
        paramid.Value = id
        paramid.ParameterName = "UsuarioSede"
        parametros.Add(paramid)
        Return ejecutaNonQuery("eliminaUsuarioSede", parametros)
    End Function

    Public Function Select_Where_UsuarioSede(ByVal Cond As String) As List(Of UsuarioSede)
        Dim ListUsuarioSede As New List(Of UsuarioSede)
        Dim appSettings = ConfigurationManager.AppSettings
        Dim result As String = appSettings("003")

        Try
            cnn = conecta()
            cmd = New SqlCommand("obtenerUsuarioSedeByWhere")
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Connection = cnn
            Dim param As DbParameter = cmd.CreateParameter()
            param.Value = Cond
            param.ParameterName = "where"
            cmd.Parameters.Add(param)
            Dim Dr As DbDataReader = cmd.ExecuteReader()
            While Dr.Read
                ListUsuarioSede.Add(New UsuarioSede(
                    Dr("UsuarioSede"),
                    Dr("idUsuario"),
                    Dr("idSede")))
            End While
        Catch ex As Exception
            Ext.Net.X.Msg.Alert("Información", result).Show()
        Finally
            desconecta(cnn)
        End Try
        Return ListUsuarioSede
    End Function

    Public Function Select_ById_UsuarioSede(
        ByVal id As Integer) As List(Of UsuarioSede)
        Dim ListUsuarioSede As New List(Of UsuarioSede)
        Dim appSettings = ConfigurationManager.AppSettings
        Dim result As String = appSettings("003")

        Try
            cnn = conecta()
            cmd = New SqlCommand("obtenerUsuarioSedeById")
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Connection = cnn
            Dim paramid As DbParameter = cmd.CreateParameter()
            paramid.Value = id
            paramid.ParameterName = "UsuarioSede"
            cmd.Parameters.Add(paramid)
            Dim Dr As DbDataReader = cmd.ExecuteReader()
            While Dr.Read
                ListUsuarioSede.Add(New UsuarioSede(
                    Dr("UsuarioSede"),
                    Dr("idUsuario"),
                    Dr("idSede")))
            End While
        Catch ex As Exception
            Ext.Net.X.Msg.Alert("Información", result).Show()
        Finally
            desconecta(cnn)
        End Try
        Return ListUsuarioSede
    End Function

    Public Function Select_UsuarioSede() As List(Of UsuarioSede)
        Dim ListUsuarioSede As New List(Of UsuarioSede)
        Dim appSettings = ConfigurationManager.AppSettings
        Dim result As String = appSettings("003")

        Try
            cnn = conecta()
            cmd = New SqlCommand("obtenerUsuarioSede")
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Connection = cnn
            Dim Dr As DbDataReader = cmd.ExecuteReader()
            While Dr.Read
                ListUsuarioSede.Add(New UsuarioSede(
                    Dr("UsuarioSede"),
                    Dr("idUsuario"),
                    Dr("idSede")))
            End While
        Catch ex As Exception
            Ext.Net.X.Msg.Alert("Información", result).Show()
        Finally
            desconecta(cnn)
        End Try
        Return ListUsuarioSede
    End Function

End Class
Imports System.Data
Imports System.Data.SqlClient
Imports System.Data.Common
Imports System.Configuration

Public Class DatoPaginaWebControl
   Inherits conexion

  Public Function Insertar(ByVal PaginaWebControl As PaginaWebControl)
      Dim parametros As New List(Of DbParameter)
      Dim paramid_PaginaWeb As DbParameter = cmd.CreateParameter()
      paramid_PaginaWeb.Value = PaginaWebControl.id_PaginaWeb
      paramid_PaginaWeb.ParameterName = "id_PaginaWeb"
      parametros.Add(paramid_PaginaWeb)
      Dim paramnombre As DbParameter = cmd.CreateParameter()
      paramnombre.Value = PaginaWebControl.nombre
      paramnombre.ParameterName = "nombre"
      parametros.Add(paramnombre)
      Dim paramtipo As DbParameter = cmd.CreateParameter()
      paramtipo.Value = PaginaWebControl.tipo
      paramtipo.ParameterName = "tipo"
      parametros.Add(paramtipo)
      Dim paramdescripcion As DbParameter = cmd.CreateParameter()
      paramdescripcion.Value = PaginaWebControl.descripcion
      paramdescripcion.ParameterName = "descripcion"
      parametros.Add(paramdescripcion)
      Return ejecutaNonQuery("insertaPaginaWebControl", parametros)
  End Function

  Public Function Editar(ByVal PaginaWebControl As PaginaWebControl)
      Dim parametros As New List(Of DbParameter)
      Dim paramid As DbParameter = cmd.CreateParameter()
      paramid.Value = PaginaWebControl.id
      paramid.ParameterName = "id"
      parametros.Add(paramid)
      Dim paramid_PaginaWeb As DbParameter = cmd.CreateParameter()
      paramid_PaginaWeb.Value = PaginaWebControl.id_PaginaWeb
      paramid_PaginaWeb.ParameterName = "id_PaginaWeb"
      parametros.Add(paramid_PaginaWeb)
      Dim paramnombre As DbParameter = cmd.CreateParameter()
      paramnombre.Value = PaginaWebControl.nombre
      paramnombre.ParameterName = "nombre"
      parametros.Add(paramnombre)
      Dim paramtipo As DbParameter = cmd.CreateParameter()
      paramtipo.Value = PaginaWebControl.tipo
      paramtipo.ParameterName = "tipo"
      parametros.Add(paramtipo)
      Dim paramdescripcion As DbParameter = cmd.CreateParameter()
      paramdescripcion.Value = PaginaWebControl.descripcion
      paramdescripcion.ParameterName = "descripcion"
      parametros.Add(paramdescripcion)
      Return ejecutaNonQuery("cambiarPaginaWebControl", parametros)
  End Function

  Public Function Borrar(
      ByVal id As integer)
      Dim parametros As New List(Of DbParameter)
      Dim paramid As DbParameter = cmd.CreateParameter()
      paramid.Value = id
      paramid.ParameterName = "id"
      parametros.Add(paramid)
      Return ejecutaNonQuery("eliminaPaginaWebControl", parametros)
  End Function

  Public Function Select_Where_PaginaWebControl(ByVal Cond As String) As List(Of PaginaWebControl)
      Dim ListPaginaWebControl As New List(Of PaginaWebControl)
        Dim appSettings = ConfigurationManager.AppSettings
        Dim result As String = appSettings("041")

        Try
            cnn = conecta()
            cmd = New SqlCommand("obtenerPaginaWebControlByWhere")
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Connection = cnn
            Dim param As DbParameter = cmd.CreateParameter()
            param.Value = Cond
            param.ParameterName = "where"
            cmd.Parameters.Add(param)
            Dim Dr As DbDataReader = cmd.ExecuteReader()
            While Dr.Read
                ListPaginaWebControl.Add(New PaginaWebControl(
                    Dr("id"),
                    Dr("id_PaginaWeb"),
                    Dr("nombre"),
                    Dr("tipo"),
                    Dr("descripcion")))
            End While
        Catch ex As Exception
            Ext.Net.X.Msg.Alert("Información", result).Show()
        Finally
            desconecta(cnn)
        End Try
      Return ListPaginaWebControl
  End Function

  Public Function Select_ById_PaginaWebControl(
      ByVal id As integer) As List(Of PaginaWebControl)
        Dim ListPaginaWebControl As New List(Of PaginaWebControl)
        Dim appSettings = ConfigurationManager.AppSettings
        Dim result As String = appSettings("041")

      Try
          cnn = conecta()
          cmd = New SqlCommand("obtenerPaginaWebControlById")
          cmd.CommandType = CommandType.StoredProcedure
          cmd.Connection = cnn
      Dim paramid As DbParameter = cmd.CreateParameter()
      paramid.Value = id
      paramid.ParameterName = "id"
      cmd.Parameters.Add(paramid)
          Dim Dr As DbDataReader = cmd.ExecuteReader()
          While Dr.Read
              ListPaginaWebControl.Add(New PaginaWebControl(
                  Dr("id"),
                  Dr("id_PaginaWeb"),
                  Dr("nombre"),
                  Dr("tipo"),
                  Dr("descripcion")))
          End While
      Catch ex As Exception
            Ext.Net.X.Msg.Alert("Información", result).Show()
      Finally
          desconecta(cnn)
      End Try
      Return ListPaginaWebControl
  End Function

  Public Function Select_PaginaWebControl() As List(Of PaginaWebControl)
        Dim ListPaginaWebControl As New List(Of PaginaWebControl)
        Dim appSettings = ConfigurationManager.AppSettings
        Dim result As String = appSettings("041")

      Try
         cnn = conecta()
          cmd = New SqlCommand("obtenerPaginaWebControl")
          cmd.CommandType = CommandType.StoredProcedure
          cmd.Connection = cnn
          Dim Dr As DbDataReader = cmd.ExecuteReader()
          While Dr.Read
              ListPaginaWebControl.Add(New PaginaWebControl(
                  Dr("id"),
                  Dr("id_PaginaWeb"),
                  Dr("nombre"),
                  Dr("tipo"),
                  Dr("descripcion")))
          End While
      Catch ex As Exception
            Ext.Net.X.Msg.Alert("Información", result).Show()
      Finally
          desconecta(cnn)
      End Try
      Return ListPaginaWebControl
  End Function

End Class
Imports System.Data
Imports System.Data.SqlClient
Imports System.Data.Common
Imports System.Configuration

Public Class Datosaih_hos
   Inherits conexion

  Public Function Insertar(ByVal saih_hos As saih_hos)
      Dim parametros As New List(Of DbParameter)
      Dim paramcodent As DbParameter = cmd.CreateParameter()
      paramcodent.Value = saih_hos.codent
      paramcodent.ParameterName = "codent"
      parametros.Add(paramcodent)
      Dim paramtipdoc As DbParameter = cmd.CreateParameter()
      paramtipdoc.Value = saih_hos.tipdoc
      paramtipdoc.ParameterName = "tipdoc"
      parametros.Add(paramtipdoc)
      Dim paramnrodoc As DbParameter = cmd.CreateParameter()
      paramnrodoc.Value = saih_hos.nrodoc
      paramnrodoc.ParameterName = "nrodoc"
      parametros.Add(paramnrodoc)
      Dim paramnombre As DbParameter = cmd.CreateParameter()
      paramnombre.Value = saih_hos.nombre
      paramnombre.ParameterName = "nombre"
      parametros.Add(paramnombre)
      Dim paramdireccion As DbParameter = cmd.CreateParameter()
      paramdireccion.Value = saih_hos.direccion
      paramdireccion.ParameterName = "direccion"
      parametros.Add(paramdireccion)
      Dim paramtelefono As DbParameter = cmd.CreateParameter()
      paramtelefono.Value = saih_hos.telefono
      paramtelefono.ParameterName = "telefono"
      parametros.Add(paramtelefono)
      Dim paramlogotipo As DbParameter = cmd.CreateParameter()
      paramlogotipo.Value = saih_hos.logotipo
      paramlogotipo.ParameterName = "logotipo"
      parametros.Add(paramlogotipo)
      Dim paramresdian As DbParameter = cmd.CreateParameter()
      paramresdian.Value = saih_hos.resdian
      paramresdian.ParameterName = "resdian"
      parametros.Add(paramresdian)
      Dim paramfecha_resolucion As DbParameter = cmd.CreateParameter()
      paramfecha_resolucion.Value = saih_hos.fecha_resolucion
      paramfecha_resolucion.ParameterName = "fecha_resolucion"
      parametros.Add(paramfecha_resolucion)
      Dim paramnum_inicial As DbParameter = cmd.CreateParameter()
      paramnum_inicial.Value = saih_hos.num_inicial
      paramnum_inicial.ParameterName = "num_inicial"
      parametros.Add(paramnum_inicial)
      Dim paramnum_final As DbParameter = cmd.CreateParameter()
      paramnum_final.Value = saih_hos.num_final
      paramnum_final.ParameterName = "num_final"
      parametros.Add(paramnum_final)
      Dim paramnum_factura As DbParameter = cmd.CreateParameter()
      paramnum_factura.Value = saih_hos.num_factura
      paramnum_factura.ParameterName = "num_factura"
      parametros.Add(paramnum_factura)
      Dim paramprefijo As DbParameter = cmd.CreateParameter()
      paramprefijo.Value = saih_hos.prefijo
      paramprefijo.ParameterName = "prefijo"
      parametros.Add(paramprefijo)
      Dim paramregimen As DbParameter = cmd.CreateParameter()
      paramregimen.Value = saih_hos.regimen
      paramregimen.ParameterName = "regimen"
      parametros.Add(paramregimen)
      Dim paramgerente As DbParameter = cmd.CreateParameter()
      paramgerente.Value = saih_hos.gerente
      paramgerente.ParameterName = "gerente"
      parametros.Add(paramgerente)
      Dim paramsubgerente As DbParameter = cmd.CreateParameter()
      paramsubgerente.Value = saih_hos.subgerente
      paramsubgerente.ParameterName = "subgerente"
      parametros.Add(paramsubgerente)
      Dim paramindtriage As DbParameter = cmd.CreateParameter()
      paramindtriage.Value = saih_hos.indtriage
      paramindtriage.ParameterName = "indtriage"
      parametros.Add(paramindtriage)
      Dim paramindimpformula As DbParameter = cmd.CreateParameter()
      paramindimpformula.Value = saih_hos.indimpformula
      paramindimpformula.ParameterName = "indimpformula"
      parametros.Add(paramindimpformula)
      Dim paramindfechaglosa As DbParameter = cmd.CreateParameter()
      paramindfechaglosa.Value = saih_hos.indfechaglosa
      paramindfechaglosa.ParameterName = "indfechaglosa"
      parametros.Add(paramindfechaglosa)
      Return ejecutaNonQuery("insertasaih_hos", parametros)
  End Function

  Public Function InsertarRet(ByVal saih_hos As saih_hos)
      Dim dato As Integer = 0
        Dim appSettings = ConfigurationManager.AppSettings
        Dim result As String = appSettings("026")

        Try
            cnn = conecta()
            cmd = New SqlCommand("insertasaih_hos")
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Connection = cnn
            Dim paramcodent As DbParameter = cmd.CreateParameter()
            paramcodent.Value = saih_hos.codent
            paramcodent.ParameterName = "codent"
            cmd.Parameters.Add(paramcodent)
            Dim paramtipdoc As DbParameter = cmd.CreateParameter()
            paramtipdoc.Value = saih_hos.tipdoc
            paramtipdoc.ParameterName = "tipdoc"
            cmd.Parameters.Add(paramtipdoc)
            Dim paramnrodoc As DbParameter = cmd.CreateParameter()
            paramnrodoc.Value = saih_hos.nrodoc
            paramnrodoc.ParameterName = "nrodoc"
            cmd.Parameters.Add(paramnrodoc)
            Dim paramnombre As DbParameter = cmd.CreateParameter()
            paramnombre.Value = saih_hos.nombre
            paramnombre.ParameterName = "nombre"
            cmd.Parameters.Add(paramnombre)
            Dim paramdireccion As DbParameter = cmd.CreateParameter()
            paramdireccion.Value = saih_hos.direccion
            paramdireccion.ParameterName = "direccion"
            cmd.Parameters.Add(paramdireccion)
            Dim paramtelefono As DbParameter = cmd.CreateParameter()
            paramtelefono.Value = saih_hos.telefono
            paramtelefono.ParameterName = "telefono"
            cmd.Parameters.Add(paramtelefono)
            Dim paramlogotipo As DbParameter = cmd.CreateParameter()
            paramlogotipo.Value = saih_hos.logotipo
            paramlogotipo.ParameterName = "logotipo"
            cmd.Parameters.Add(paramlogotipo)
            Dim paramresdian As DbParameter = cmd.CreateParameter()
            paramresdian.Value = saih_hos.resdian
            paramresdian.ParameterName = "resdian"
            cmd.Parameters.Add(paramresdian)
            Dim paramfecha_resolucion As DbParameter = cmd.CreateParameter()
            paramfecha_resolucion.Value = saih_hos.fecha_resolucion
            paramfecha_resolucion.ParameterName = "fecha_resolucion"
            cmd.Parameters.Add(paramfecha_resolucion)
            Dim paramnum_inicial As DbParameter = cmd.CreateParameter()
            paramnum_inicial.Value = saih_hos.num_inicial
            paramnum_inicial.ParameterName = "num_inicial"
            cmd.Parameters.Add(paramnum_inicial)
            Dim paramnum_final As DbParameter = cmd.CreateParameter()
            paramnum_final.Value = saih_hos.num_final
            paramnum_final.ParameterName = "num_final"
            cmd.Parameters.Add(paramnum_final)
            Dim paramnum_factura As DbParameter = cmd.CreateParameter()
            paramnum_factura.Value = saih_hos.num_factura
            paramnum_factura.ParameterName = "num_factura"
            cmd.Parameters.Add(paramnum_factura)
            Dim paramprefijo As DbParameter = cmd.CreateParameter()
            paramprefijo.Value = saih_hos.prefijo
            paramprefijo.ParameterName = "prefijo"
            cmd.Parameters.Add(paramprefijo)
            Dim paramregimen As DbParameter = cmd.CreateParameter()
            paramregimen.Value = saih_hos.regimen
            paramregimen.ParameterName = "regimen"
            cmd.Parameters.Add(paramregimen)
            Dim paramgerente As DbParameter = cmd.CreateParameter()
            paramgerente.Value = saih_hos.gerente
            paramgerente.ParameterName = "gerente"
            cmd.Parameters.Add(paramgerente)
            Dim paramsubgerente As DbParameter = cmd.CreateParameter()
            paramsubgerente.Value = saih_hos.subgerente
            paramsubgerente.ParameterName = "subgerente"
            cmd.Parameters.Add(paramsubgerente)
            Dim paramindtriage As DbParameter = cmd.CreateParameter()
            paramindtriage.Value = saih_hos.indtriage
            paramindtriage.ParameterName = "indtriage"
            cmd.Parameters.Add(paramindtriage)
            Dim paramindimpformula As DbParameter = cmd.CreateParameter()
            paramindimpformula.Value = saih_hos.indimpformula
            paramindimpformula.ParameterName = "indimpformula"
            cmd.Parameters.Add(paramindimpformula)
            Dim paramindfechaglosa As DbParameter = cmd.CreateParameter()
            paramindfechaglosa.Value = saih_hos.indfechaglosa
            paramindfechaglosa.ParameterName = "indfechaglosa"
            cmd.Parameters.Add(paramindfechaglosa)
            Dim Dr As DbDataReader = cmd.ExecuteReader()
            While Dr.Read
                dato = Dr(0)
            End While
        Catch ex As Exception
            Ext.Net.X.Msg.Alert("Información", result).Show()
        Finally
            desconecta(cnn)
        End Try
      Return dato
  End Function

  Public Function Editar(ByVal saih_hos As saih_hos)
      Dim parametros As New List(Of DbParameter)
      Dim paramcodent As DbParameter = cmd.CreateParameter()
      paramcodent.Value = saih_hos.codent
      paramcodent.ParameterName = "codent"
      parametros.Add(paramcodent)
      Dim paramtipdoc As DbParameter = cmd.CreateParameter()
      paramtipdoc.Value = saih_hos.tipdoc
      paramtipdoc.ParameterName = "tipdoc"
      parametros.Add(paramtipdoc)
      Dim paramnrodoc As DbParameter = cmd.CreateParameter()
      paramnrodoc.Value = saih_hos.nrodoc
      paramnrodoc.ParameterName = "nrodoc"
      parametros.Add(paramnrodoc)
      Dim paramnombre As DbParameter = cmd.CreateParameter()
      paramnombre.Value = saih_hos.nombre
      paramnombre.ParameterName = "nombre"
      parametros.Add(paramnombre)
      Dim paramdireccion As DbParameter = cmd.CreateParameter()
      paramdireccion.Value = saih_hos.direccion
      paramdireccion.ParameterName = "direccion"
      parametros.Add(paramdireccion)
      Dim paramtelefono As DbParameter = cmd.CreateParameter()
      paramtelefono.Value = saih_hos.telefono
      paramtelefono.ParameterName = "telefono"
      parametros.Add(paramtelefono)
      Dim paramlogotipo As DbParameter = cmd.CreateParameter()
      paramlogotipo.Value = saih_hos.logotipo
      paramlogotipo.ParameterName = "logotipo"
      parametros.Add(paramlogotipo)
      Dim paramresdian As DbParameter = cmd.CreateParameter()
      paramresdian.Value = saih_hos.resdian
      paramresdian.ParameterName = "resdian"
      parametros.Add(paramresdian)
      Dim paramfecha_resolucion As DbParameter = cmd.CreateParameter()
      paramfecha_resolucion.Value = saih_hos.fecha_resolucion
      paramfecha_resolucion.ParameterName = "fecha_resolucion"
      parametros.Add(paramfecha_resolucion)
      Dim paramnum_inicial As DbParameter = cmd.CreateParameter()
      paramnum_inicial.Value = saih_hos.num_inicial
      paramnum_inicial.ParameterName = "num_inicial"
      parametros.Add(paramnum_inicial)
      Dim paramnum_final As DbParameter = cmd.CreateParameter()
      paramnum_final.Value = saih_hos.num_final
      paramnum_final.ParameterName = "num_final"
      parametros.Add(paramnum_final)
      Dim paramnum_factura As DbParameter = cmd.CreateParameter()
      paramnum_factura.Value = saih_hos.num_factura
      paramnum_factura.ParameterName = "num_factura"
      parametros.Add(paramnum_factura)
      Dim paramprefijo As DbParameter = cmd.CreateParameter()
      paramprefijo.Value = saih_hos.prefijo
      paramprefijo.ParameterName = "prefijo"
      parametros.Add(paramprefijo)
      Dim paramregimen As DbParameter = cmd.CreateParameter()
      paramregimen.Value = saih_hos.regimen
      paramregimen.ParameterName = "regimen"
      parametros.Add(paramregimen)
      Dim paramgerente As DbParameter = cmd.CreateParameter()
      paramgerente.Value = saih_hos.gerente
      paramgerente.ParameterName = "gerente"
      parametros.Add(paramgerente)
      Dim paramsubgerente As DbParameter = cmd.CreateParameter()
      paramsubgerente.Value = saih_hos.subgerente
      paramsubgerente.ParameterName = "subgerente"
      parametros.Add(paramsubgerente)
      Dim paramindtriage As DbParameter = cmd.CreateParameter()
      paramindtriage.Value = saih_hos.indtriage
      paramindtriage.ParameterName = "indtriage"
      parametros.Add(paramindtriage)
      Dim paramindimpformula As DbParameter = cmd.CreateParameter()
      paramindimpformula.Value = saih_hos.indimpformula
      paramindimpformula.ParameterName = "indimpformula"
      parametros.Add(paramindimpformula)
      Dim paramindfechaglosa As DbParameter = cmd.CreateParameter()
      paramindfechaglosa.Value = saih_hos.indfechaglosa
      paramindfechaglosa.ParameterName = "indfechaglosa"
      parametros.Add(paramindfechaglosa)
      Return ejecutaNonQuery("cambiarsaih_hos", parametros)
  End Function

  Public Function Borrar(
      ByVal codent As string)
      Dim parametros As New List(Of DbParameter)
      Dim paramcodent As DbParameter = cmd.CreateParameter()
      paramcodent.Value = codent
      paramcodent.ParameterName = "codent"
      parametros.Add(paramcodent)
      Return ejecutaNonQuery("eliminasaih_hos", parametros)
  End Function

  Public Function Select_Where_saih_hos(ByVal Cond As String) As List(Of saih_hos)
        Dim Listsaih_hos As New List(Of saih_hos)
        Dim appSettings = ConfigurationManager.AppSettings
        Dim result As String = appSettings("027")

      Try
          cnn = conecta()
          cmd = New SqlCommand("obtenersaih_hosByWhere")
          cmd.CommandType = CommandType.StoredProcedure
          cmd.Connection = cnn
          Dim param As DbParameter = cmd.CreateParameter()
          param.Value = Cond
          param.ParameterName = "where"
          cmd.Parameters.Add(param)
          Dim Dr As DbDataReader = cmd.ExecuteReader()
          While Dr.Read
              Listsaih_hos.Add(New saih_hos(
                  Dr("codent"),
                  Dr("tipdoc"),
                  Dr("nrodoc"),
                  Dr("nombre"),
                  Dr("direccion"),
                  Dr("telefono"),
                  Dr("logotipo"),
                  Dr("resdian"),
                  Dr("fecha_resolucion"),
                  Dr("num_inicial"),
                  Dr("num_final"),
                  Dr("num_factura"),
                  Dr("prefijo"),
                  Dr("regimen"),
                  Dr("gerente"),
                  Dr("subgerente"),
                  Dr("indtriage"),
                  Dr("indimpformula"),
                  Dr("indfechaglosa")))
          End While
      Catch ex As Exception
            Ext.Net.X.Msg.Alert("Información", result).Show()
      Finally
          desconecta(cnn)
      End Try
      Return Listsaih_hos
  End Function

  Public Function Select_ById_saih_hos(
      ByVal codent As string) As List(Of saih_hos)
        Dim Listsaih_hos As New List(Of saih_hos)
        Dim appSettings = ConfigurationManager.AppSettings
        Dim result As String = appSettings("027")

      Try
          cnn = conecta()
          cmd = New SqlCommand("obtenersaih_hosById")
          cmd.CommandType = CommandType.StoredProcedure
          cmd.Connection = cnn
      Dim paramcodent As DbParameter = cmd.CreateParameter()
      paramcodent.Value = codent
      paramcodent.ParameterName = "codent"
      cmd.Parameters.Add(paramcodent)
          Dim Dr As DbDataReader = cmd.ExecuteReader()
          While Dr.Read
              Listsaih_hos.Add(New saih_hos(
                  Dr("codent"),
                  Dr("tipdoc"),
                  Dr("nrodoc"),
                  Dr("nombre"),
                  Dr("direccion"),
                  Dr("telefono"),
                  Dr("logotipo"),
                  Dr("resdian"),
                  Dr("fecha_resolucion"),
                  Dr("num_inicial"),
                  Dr("num_final"),
                  Dr("num_factura"),
                  Dr("prefijo"),
                  Dr("regimen"),
                  Dr("gerente"),
                  Dr("subgerente"),
                  Dr("indtriage"),
                  Dr("indimpformula"),
                  Dr("indfechaglosa")))
          End While
      Catch ex As Exception
            Ext.Net.X.Msg.Alert("Información", result).Show()
      Finally
          desconecta(cnn)
      End Try
      Return Listsaih_hos
  End Function

  Public Function Select_saih_hos() As List(Of saih_hos)
        Dim Listsaih_hos As New List(Of saih_hos)
        Dim appSettings = ConfigurationManager.AppSettings
        Dim result As String = appSettings("027")

      Try
         cnn = conecta()
          cmd = New SqlCommand("obtenersaih_hos")
          cmd.CommandType = CommandType.StoredProcedure
          cmd.Connection = cnn
          Dim Dr As DbDataReader = cmd.ExecuteReader()
          While Dr.Read
              Listsaih_hos.Add(New saih_hos(
                  Dr("codent"),
                  Dr("tipdoc"),
                  Dr("nrodoc"),
                  Dr("nombre"),
                  Dr("direccion"),
                  Dr("telefono"),
                  Dr("logotipo"),
                  Dr("resdian"),
                  Dr("fecha_resolucion"),
                  Dr("num_inicial"),
                  Dr("num_final"),
                  Dr("num_factura"),
                  Dr("prefijo"),
                  Dr("regimen"),
                  Dr("gerente"),
                  Dr("subgerente"),
                  Dr("indtriage"),
                  Dr("indimpformula"),
                  Dr("indfechaglosa")))
          End While
      Catch ex As Exception
            Ext.Net.X.Msg.Alert("Información", result).Show()
      Finally
          desconecta(cnn)
      End Try
      Return Listsaih_hos
  End Function

End Class
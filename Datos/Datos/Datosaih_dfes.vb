Imports System.Data
Imports System.Data.SqlClient
Imports System.Data.Common
Imports System.Configuration

Public Class Datosaih_dfes
   Inherits conexion

  Public Function Insertar(ByVal saih_dfes As saih_dfes)
      Dim parametros As New List(Of DbParameter)
      Dim paramdia As DbParameter = cmd.CreateParameter()
      paramdia.Value = saih_dfes.dia
      paramdia.ParameterName = "dia"
      parametros.Add(paramdia)
      Dim paramdescripcion As DbParameter = cmd.CreateParameter()
      paramdescripcion.Value = saih_dfes.descripcion
      paramdescripcion.ParameterName = "descripcion"
      parametros.Add(paramdescripcion)
      Return ejecutaNonQuery("insertasaih_dfes", parametros)
  End Function

  Public Function InsertarRet(ByVal saih_dfes As saih_dfes)
        Dim dato As Integer = 0
        Dim appSettings = ConfigurationManager.AppSettings
        Dim result As String = appSettings("034")

      Try
          cnn = conecta()
          cmd = New SqlCommand("insertasaih_dfes")
          cmd.CommandType = CommandType.StoredProcedure
          cmd.Connection = cnn
        Dim paramdia As DbParameter = cmd.CreateParameter()
        paramdia.Value = saih_dfes.dia
        paramdia.ParameterName = "dia"
        cmd.Parameters.Add(paramdia)
        Dim paramdescripcion As DbParameter = cmd.CreateParameter()
        paramdescripcion.Value = saih_dfes.descripcion
        paramdescripcion.ParameterName = "descripcion"
        cmd.Parameters.Add(paramdescripcion)
          Dim Dr As DbDataReader = cmd.ExecuteReader()
          While Dr.Read
            dato = Dr(0)
          End While
      Catch ex As Exception
            Ext.Net.X.Msg.Alert("Información", result).Show()
      Finally
          desconecta(cnn)
      End Try
      Return dato
  End Function

  Public Function Editar(ByVal saih_dfes As saih_dfes)
      Dim parametros As New List(Of DbParameter)
      Dim paramid As DbParameter = cmd.CreateParameter()
      paramid.Value = saih_dfes.id
      paramid.ParameterName = "id"
      parametros.Add(paramid)
      Dim paramdia As DbParameter = cmd.CreateParameter()
      paramdia.Value = saih_dfes.dia
      paramdia.ParameterName = "dia"
      parametros.Add(paramdia)
      Dim paramdescripcion As DbParameter = cmd.CreateParameter()
      paramdescripcion.Value = saih_dfes.descripcion
      paramdescripcion.ParameterName = "descripcion"
      parametros.Add(paramdescripcion)
      Return ejecutaNonQuery("cambiarsaih_dfes", parametros)
  End Function

  Public Function Borrar(
      ByVal id As integer)
      Dim parametros As New List(Of DbParameter)
      Dim paramid As DbParameter = cmd.CreateParameter()
      paramid.Value = id
      paramid.ParameterName = "id"
      parametros.Add(paramid)
      Return ejecutaNonQuery("eliminasaih_dfes", parametros)
  End Function

  Public Function Select_Where_saih_dfes(ByVal Cond As String) As List(Of saih_dfes)
        Dim Listsaih_dfes As New List(Of saih_dfes)
        Dim appSettings = ConfigurationManager.AppSettings
        Dim result As String = appSettings("035")

      Try
          cnn = conecta()
          cmd = New SqlCommand("obtenersaih_dfesByWhere")
          cmd.CommandType = CommandType.StoredProcedure
          cmd.Connection = cnn
          Dim param As DbParameter = cmd.CreateParameter()
          param.Value = Cond
          param.ParameterName = "where"
          cmd.Parameters.Add(param)
          Dim Dr As DbDataReader = cmd.ExecuteReader()
          While Dr.Read
              Listsaih_dfes.Add(New saih_dfes(
                  Dr("id"),
                  Dr("dia"),
                  Dr("descripcion")))
          End While
      Catch ex As Exception
            Ext.Net.X.Msg.Alert("Información", result).Show()
      Finally
          desconecta(cnn)
      End Try
      Return Listsaih_dfes
  End Function

  Public Function Select_ById_saih_dfes(
      ByVal id As integer) As List(Of saih_dfes)
        Dim Listsaih_dfes As New List(Of saih_dfes)
        Dim appSettings = ConfigurationManager.AppSettings
        Dim result As String = appSettings("035")

      Try
          cnn = conecta()
          cmd = New SqlCommand("obtenersaih_dfesById")
          cmd.CommandType = CommandType.StoredProcedure
          cmd.Connection = cnn
      Dim paramid As DbParameter = cmd.CreateParameter()
      paramid.Value = id
      paramid.ParameterName = "id"
      cmd.Parameters.Add(paramid)
          Dim Dr As DbDataReader = cmd.ExecuteReader()
          While Dr.Read
              Listsaih_dfes.Add(New saih_dfes(
                  Dr("id"),
                  Dr("dia"),
                  Dr("descripcion")))
          End While
      Catch ex As Exception
            Ext.Net.X.Msg.Alert("Información", result).Show()
      Finally
          desconecta(cnn)
      End Try
      Return Listsaih_dfes
  End Function

  Public Function Select_saih_dfes() As List(Of saih_dfes)
        Dim Listsaih_dfes As New List(Of saih_dfes)
        Dim appSettings = ConfigurationManager.AppSettings
        Dim result As String = appSettings("035")

      Try
         cnn = conecta()
          cmd = New SqlCommand("obtenersaih_dfes")
          cmd.CommandType = CommandType.StoredProcedure
          cmd.Connection = cnn
          Dim Dr As DbDataReader = cmd.ExecuteReader()
          While Dr.Read
              Listsaih_dfes.Add(New saih_dfes(
                  Dr("id"),
                  Dr("dia"),
                  Dr("descripcion")))
          End While
      Catch ex As Exception
            Ext.Net.X.Msg.Alert("Información", result).Show()
      Finally
          desconecta(cnn)
      End Try
      Return Listsaih_dfes
  End Function

End Class
Imports System.Data
Imports System.Data.SqlClient
Imports System.Data.Common
Imports System.Configuration

Public Class Datosaih_emc
   Inherits conexion

  Public Function Insertar(ByVal saih_emc As saih_emc)
      Dim parametros As New List(Of DbParameter)
      Dim paramcodemp As DbParameter = cmd.CreateParameter()
      paramcodemp.Value = saih_emc.codemp
      paramcodemp.ParameterName = "codemp"
      parametros.Add(paramcodemp)
      Dim paramnum_contrato As DbParameter = cmd.CreateParameter()
      paramnum_contrato.Value = saih_emc.num_contrato
      paramnum_contrato.ParameterName = "num_contrato"
      parametros.Add(paramnum_contrato)
      Dim paramnombre As DbParameter = cmd.CreateParameter()
      paramnombre.Value = saih_emc.nombre
      paramnombre.ParameterName = "nombre"
      parametros.Add(paramnombre)
      Dim paramfecha As DbParameter = cmd.CreateParameter()
      paramfecha.Value = saih_emc.fecha
      paramfecha.ParameterName = "fecha"
      parametros.Add(paramfecha)
      Dim paramfecha_inicial As DbParameter = cmd.CreateParameter()
      paramfecha_inicial.Value = saih_emc.fecha_inicial
      paramfecha_inicial.ParameterName = "fecha_inicial"
      parametros.Add(paramfecha_inicial)
      Dim paramfecha_final As DbParameter = cmd.CreateParameter()
      paramfecha_final.Value = saih_emc.fecha_final
      paramfecha_final.ParameterName = "fecha_final"
      parametros.Add(paramfecha_final)
      Dim parammonto As DbParameter = cmd.CreateParameter()
      parammonto.Value = saih_emc.monto
      parammonto.ParameterName = "monto"
      parametros.Add(parammonto)
      Dim paramestado As DbParameter = cmd.CreateParameter()
      paramestado.Value = saih_emc.estado
      paramestado.ParameterName = "estado"
      parametros.Add(paramestado)
      Dim paramnum_afiliados As DbParameter = cmd.CreateParameter()
      paramnum_afiliados.Value = saih_emc.num_afiliados
      paramnum_afiliados.ParameterName = "num_afiliados"
      parametros.Add(paramnum_afiliados)
      Dim paramforma_pago As DbParameter = cmd.CreateParameter()
      paramforma_pago.Value = saih_emc.forma_pago
      paramforma_pago.ParameterName = "forma_pago"
      parametros.Add(paramforma_pago)
      Dim paramval_ejecutado As DbParameter = cmd.CreateParameter()
      paramval_ejecutado.Value = saih_emc.val_ejecutado
      paramval_ejecutado.ParameterName = "val_ejecutado"
      parametros.Add(paramval_ejecutado)
      Return ejecutaNonQuery("insertasaih_emc", parametros)
  End Function

  Public Function InsertarRet(ByVal saih_emc As saih_emc)
        Dim dato As Integer = 0
        Dim appSettings = ConfigurationManager.AppSettings
        Dim result As String = appSettings("032")

      Try
          cnn = conecta()
          cmd = New SqlCommand("insertasaih_emc")
          cmd.CommandType = CommandType.StoredProcedure
          cmd.Connection = cnn
        Dim paramcodemp As DbParameter = cmd.CreateParameter()
        paramcodemp.Value = saih_emc.codemp
        paramcodemp.ParameterName = "codemp"
        cmd.Parameters.Add(paramcodemp)
        Dim paramnum_contrato As DbParameter = cmd.CreateParameter()
        paramnum_contrato.Value = saih_emc.num_contrato
        paramnum_contrato.ParameterName = "num_contrato"
        cmd.Parameters.Add(paramnum_contrato)
        Dim paramnombre As DbParameter = cmd.CreateParameter()
        paramnombre.Value = saih_emc.nombre
        paramnombre.ParameterName = "nombre"
        cmd.Parameters.Add(paramnombre)
        Dim paramfecha As DbParameter = cmd.CreateParameter()
        paramfecha.Value = saih_emc.fecha
        paramfecha.ParameterName = "fecha"
        cmd.Parameters.Add(paramfecha)
        Dim paramfecha_inicial As DbParameter = cmd.CreateParameter()
        paramfecha_inicial.Value = saih_emc.fecha_inicial
        paramfecha_inicial.ParameterName = "fecha_inicial"
        cmd.Parameters.Add(paramfecha_inicial)
        Dim paramfecha_final As DbParameter = cmd.CreateParameter()
        paramfecha_final.Value = saih_emc.fecha_final
        paramfecha_final.ParameterName = "fecha_final"
        cmd.Parameters.Add(paramfecha_final)
        Dim parammonto As DbParameter = cmd.CreateParameter()
        parammonto.Value = saih_emc.monto
        parammonto.ParameterName = "monto"
        cmd.Parameters.Add(parammonto)
        Dim paramestado As DbParameter = cmd.CreateParameter()
        paramestado.Value = saih_emc.estado
        paramestado.ParameterName = "estado"
        cmd.Parameters.Add(paramestado)
        Dim paramnum_afiliados As DbParameter = cmd.CreateParameter()
        paramnum_afiliados.Value = saih_emc.num_afiliados
        paramnum_afiliados.ParameterName = "num_afiliados"
        cmd.Parameters.Add(paramnum_afiliados)
        Dim paramforma_pago As DbParameter = cmd.CreateParameter()
        paramforma_pago.Value = saih_emc.forma_pago
        paramforma_pago.ParameterName = "forma_pago"
        cmd.Parameters.Add(paramforma_pago)
        Dim paramval_ejecutado As DbParameter = cmd.CreateParameter()
        paramval_ejecutado.Value = saih_emc.val_ejecutado
        paramval_ejecutado.ParameterName = "val_ejecutado"
        cmd.Parameters.Add(paramval_ejecutado)
          Dim Dr As DbDataReader = cmd.ExecuteReader()
          While Dr.Read
            dato = Dr(0)
          End While
      Catch ex As Exception
            Ext.Net.X.Msg.Alert("Información", result).Show()
      Finally
          desconecta(cnn)
      End Try
      Return dato
  End Function

  Public Function Editar(ByVal saih_emc As saih_emc)
      Dim parametros As New List(Of DbParameter)
      Dim paramcodemp As DbParameter = cmd.CreateParameter()
      paramcodemp.Value = saih_emc.codemp
      paramcodemp.ParameterName = "codemp"
      parametros.Add(paramcodemp)
      Dim paramnum_contrato As DbParameter = cmd.CreateParameter()
      paramnum_contrato.Value = saih_emc.num_contrato
      paramnum_contrato.ParameterName = "num_contrato"
      parametros.Add(paramnum_contrato)
      Dim paramnombre As DbParameter = cmd.CreateParameter()
      paramnombre.Value = saih_emc.nombre
      paramnombre.ParameterName = "nombre"
      parametros.Add(paramnombre)
      Dim paramfecha As DbParameter = cmd.CreateParameter()
      paramfecha.Value = saih_emc.fecha
      paramfecha.ParameterName = "fecha"
      parametros.Add(paramfecha)
      Dim paramfecha_inicial As DbParameter = cmd.CreateParameter()
      paramfecha_inicial.Value = saih_emc.fecha_inicial
      paramfecha_inicial.ParameterName = "fecha_inicial"
      parametros.Add(paramfecha_inicial)
      Dim paramfecha_final As DbParameter = cmd.CreateParameter()
      paramfecha_final.Value = saih_emc.fecha_final
      paramfecha_final.ParameterName = "fecha_final"
      parametros.Add(paramfecha_final)
      Dim parammonto As DbParameter = cmd.CreateParameter()
      parammonto.Value = saih_emc.monto
      parammonto.ParameterName = "monto"
      parametros.Add(parammonto)
      Dim paramestado As DbParameter = cmd.CreateParameter()
      paramestado.Value = saih_emc.estado
      paramestado.ParameterName = "estado"
      parametros.Add(paramestado)
      Dim paramnum_afiliados As DbParameter = cmd.CreateParameter()
      paramnum_afiliados.Value = saih_emc.num_afiliados
      paramnum_afiliados.ParameterName = "num_afiliados"
      parametros.Add(paramnum_afiliados)
      Dim paramforma_pago As DbParameter = cmd.CreateParameter()
      paramforma_pago.Value = saih_emc.forma_pago
      paramforma_pago.ParameterName = "forma_pago"
      parametros.Add(paramforma_pago)
      Dim paramval_ejecutado As DbParameter = cmd.CreateParameter()
      paramval_ejecutado.Value = saih_emc.val_ejecutado
      paramval_ejecutado.ParameterName = "val_ejecutado"
      parametros.Add(paramval_ejecutado)
      Return ejecutaNonQuery("cambiarsaih_emc", parametros)
  End Function

  Public Function Borrar(
      ByVal codemp As string,
      ByVal num_contrato As string)
      Dim parametros As New List(Of DbParameter)
      Dim paramcodemp As DbParameter = cmd.CreateParameter()
      paramcodemp.Value = codemp
      paramcodemp.ParameterName = "codemp"
      parametros.Add(paramcodemp)
      Dim paramnum_contrato As DbParameter = cmd.CreateParameter()
      paramnum_contrato.Value = num_contrato
      paramnum_contrato.ParameterName = "num_contrato"
      parametros.Add(paramnum_contrato)
      Return ejecutaNonQuery("eliminasaih_emc", parametros)
  End Function

  Public Function Select_Where_saih_emc(ByVal Cond As String) As List(Of saih_emc)
        Dim Listsaih_emc As New List(Of saih_emc)
        Dim appSettings = ConfigurationManager.AppSettings
        Dim result As String = appSettings("033")

      Try
          cnn = conecta()
          cmd = New SqlCommand("obtenersaih_emcByWhere")
          cmd.CommandType = CommandType.StoredProcedure
          cmd.Connection = cnn
          Dim param As DbParameter = cmd.CreateParameter()
          param.Value = Cond
          param.ParameterName = "where"
          cmd.Parameters.Add(param)
          Dim Dr As DbDataReader = cmd.ExecuteReader()
          While Dr.Read
              Listsaih_emc.Add(New saih_emc(
                  Dr("codemp"),
                  Dr("num_contrato"),
                  Dr("nombre"),
                  Dr("fecha"),
                  Dr("fecha_inicial"),
                  Dr("fecha_final"),
                  Dr("monto"),
                  Dr("estado"),
                  Dr("num_afiliados"),
                  Dr("forma_pago"),
                  Dr("val_ejecutado")))
          End While
      Catch ex As Exception
            Ext.Net.X.Msg.Alert("Información", result).Show()
      Finally
          desconecta(cnn)
      End Try
      Return Listsaih_emc
  End Function

  Public Function Select_ById_saih_emc(
      ByVal codemp As string,
      ByVal num_contrato As string) As List(Of saih_emc)
        Dim Listsaih_emc As New List(Of saih_emc)
        Dim appSettings = ConfigurationManager.AppSettings
        Dim result As String = appSettings("033")

      Try
          cnn = conecta()
          cmd = New SqlCommand("obtenersaih_emcById")
          cmd.CommandType = CommandType.StoredProcedure
          cmd.Connection = cnn
      Dim paramcodemp As DbParameter = cmd.CreateParameter()
      paramcodemp.Value = codemp
      paramcodemp.ParameterName = "codemp"
      cmd.Parameters.Add(paramcodemp)
      Dim paramnum_contrato As DbParameter = cmd.CreateParameter()
      paramnum_contrato.Value = num_contrato
      paramnum_contrato.ParameterName = "num_contrato"
      cmd.Parameters.Add(paramnum_contrato)
          Dim Dr As DbDataReader = cmd.ExecuteReader()
          While Dr.Read
              Listsaih_emc.Add(New saih_emc(
                  Dr("codemp"),
                  Dr("num_contrato"),
                  Dr("nombre"),
                  Dr("fecha"),
                  Dr("fecha_inicial"),
                  Dr("fecha_final"),
                  Dr("monto"),
                  Dr("estado"),
                  Dr("num_afiliados"),
                  Dr("forma_pago"),
                  Dr("val_ejecutado")))
          End While
      Catch ex As Exception
            Ext.Net.X.Msg.Alert("Información", result).Show()
      Finally
          desconecta(cnn)
      End Try
      Return Listsaih_emc
  End Function

  Public Function Select_saih_emc() As List(Of saih_emc)
        Dim Listsaih_emc As New List(Of saih_emc)
        Dim appSettings = ConfigurationManager.AppSettings
        Dim result As String = appSettings("033")

      Try
         cnn = conecta()
          cmd = New SqlCommand("obtenersaih_emc")
          cmd.CommandType = CommandType.StoredProcedure
          cmd.Connection = cnn
          Dim Dr As DbDataReader = cmd.ExecuteReader()
          While Dr.Read
              Listsaih_emc.Add(New saih_emc(
                  Dr("codemp"),
                  Dr("num_contrato"),
                  Dr("nombre"),
                  Dr("fecha"),
                  Dr("fecha_inicial"),
                  Dr("fecha_final"),
                  Dr("monto"),
                  Dr("estado"),
                  Dr("num_afiliados"),
                  Dr("forma_pago"),
                  Dr("val_ejecutado")))
          End While
      Catch ex As Exception
            Ext.Net.X.Msg.Alert("Información", result).Show()
      Finally
          desconecta(cnn)
      End Try
      Return Listsaih_emc
  End Function

End Class
﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="PerfilesUsuarios.aspx.vb" Inherits="Presentacion.PerfilesUsuarios" %>

<%@ Register assembly="Ext.Net" namespace="Ext.Net" tagprefix="ext" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <link href="../../public/css/estilos.css" type="text/css" rel="stylesheet" />
    <script src="../../public/js/jquery-1.9.0.js" type="text/javascript"></script>
    <script type="text/javascript" src="../../public/js/app.js"></script>
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
    <ext:Store ID="stPerfiles" runat="server" AutoLoad="false" >
        <Reader>
            <ext:JsonReader IDProperty="idperfil">
                <Fields>
                    <ext:RecordField Name="idperfil" />
                    <ext:RecordField Name="descripcion" />
                </Fields>
            </ext:JsonReader>
        </Reader>
    </ext:Store>


    <ext:Store ID="stUsuarios" runat="server" AutoLoad="false" >
        <Reader>
            <ext:JsonReader IDProperty="Usuario">
                <Fields>
                    <ext:RecordField Name="Usuario" />                    
                    <ext:RecordField Name="Identificacion" />
                    <ext:RecordField Name="razonsocial" />
                    <ext:RecordField Name="estado" />
                </Fields>
            </ext:JsonReader>
        </Reader>
    </ext:Store>

    <div>
        <ext:Panel ID="Panel3" runat="server" Frame="true" PaddingSummary="5px 5px 0" AutoWidth="true" Height="650" ButtonAlign="Center" Style="text-align: left" Title="Perfiles Usuarios">
            <Items>
                <ext:FieldSet ID="FieldSet3" runat="server" Title="Campos de Busqueda" Padding="10" Height="90">
                    <Items>
                        <ext:Container ID="Container7" runat="server" Layout="ColumnLayout" Height="80">
                            <Items>
                                <ext:Container ID="Container8" runat="server" LabelAlign="Right" Layout="FormLayout" ColumnWidth=".5">
                                    <Items>
                                        <ext:TextField ID="txtIdPerfil" runat="server" FieldLabel="Id.Perfil" MaskRe="[A-Z]" />
                                        <ext:TextField ID="txtDescripción" runat="server" FieldLabel="Descripción" AnchorHorizontal="90%" />
                                    </Items>
                                </ext:Container>
                                <ext:Container ID="Container9" runat="server" LabelAlign="Right" Layout="FormLayout"
                                    ColumnWidth=".5">
                                    <Items>
                                        <ext:Button runat="server" ID="btnBuscar" Text="Buscar" Icon="ApplicationGo" />
                                    </Items>
                                </ext:Container>
                            </Items>
                        </ext:Container>
                    </Items>
                </ext:FieldSet>
                <ext:GridPanel
                    ID="grdPerfiles"
                    runat="server"                                            
                    Margins="0 0 5 5"
                    Icon="UserSuit"
                    Frame="true"
                    Height="200"
                    StoreId="stPerfiles"
                    Title="Perfiles"
                    >
                    <ColumnModel ID="ColumnModel3" runat="server">
                        <Columns>
                            <ext:Column Header="Id.Perfil" DataIndex="idperfil" />                            
                            <ext:Column Header="Descripción" DataIndex="descripcion" Width="380"/>
                        </Columns>                                
                    </ColumnModel>
                    <SelectionModel>
                        <ext:RowSelectionModel ID="RowSelectionModel1" runat="server" SingleSelect="true" >
                            <DirectEvents>
                                <RowSelect OnEvent="rowSelect" Buffer="100">
                                    <ExtraParams>                                    
                                        <ext:Parameter Name="ID" Value="this.getSelected().id" Mode="Raw" />
                                    </ExtraParams>
                                </RowSelect>
                            </DirectEvents>
                        </ext:RowSelectionModel>
                    </SelectionModel>
                    <LoadMask ShowMask="true" Msg="Loading...." />
                    <BottomBar>
                        <ext:StatusBar runat="server" ID="StatusBar2">
                            <Items>                                                        
                                <ext:Button runat="server" Text="Nuevo Perfil" ID="btnNuevoPerfil" Icon="Add" />
                                <ext:Button runat="server" Text="Editar Perfil" ID="btnEditaPerfil" Icon="ApplicationEdit" />
                            </Items>
                        </ext:StatusBar>
                    </BottomBar>
                </ext:GridPanel>
                <ext:GridPanel
                    ID="grdUsuario"
                    runat="server"                                            
                    Margins="0 0 5 5"
                    Icon="UserSuit"
                    Frame="true"
                    Height="280"
                    StoreId="stUsuarios"
                    Title="Usuarios"
                    >
                    <ColumnModel ID="ColumnModel1" runat="server">
                        <Columns>                            
                            <ext:Column Header="Usuario" DataIndex="Usuario" />
                            <ext:Column Header="Identificación" DataIndex="Identificacion" />
                            <ext:Column Header="Razon Social" DataIndex="razonsocial" Width="380"/>
                            <ext:Column Header="Estado" DataIndex="estado" />
                        </Columns>                                
                    </ColumnModel>
                    <SelectionModel>
                        <ext:RowSelectionModel ID="RowSelectionModel2" runat="server" SingleSelect="true" >
                            <DirectEvents>
                                <RowSelect OnEvent="rowSelectUsu" Buffer="100">
                                    <ExtraParams>                                    
                                        <ext:Parameter Name="ID" Value="this.getSelected().id" Mode="Raw" />
                                    </ExtraParams>
                                </RowSelect>
                            </DirectEvents>
                        </ext:RowSelectionModel>
                    </SelectionModel>
                    <LoadMask ShowMask="true" Msg="Loading...." />
                    <BottomBar>
                        <ext:StatusBar runat="server" ID="StatusBar1">
                            <Items>                                                        
                                <ext:Button runat="server" Text="Nuevo Usuario" ID="btnNuevoUsuario" Icon="Add" />
                                <ext:Button runat="server" Text="Editar Usuario" ID="btnEditarUsuario" Icon="ApplicationEdit" />
                            </Items>
                        </ext:StatusBar>
                    </BottomBar>
                </ext:GridPanel>
                
            </Items>
        </ext:Panel>
    </div>

    <ext:Window 
        ID="VntUsuarios"
        runat="server" 
        Icon="ApplicationFormEdit" 
        Width="750" 
        Height="350" 
        Hidden="true" 
        Modal="true"            
        Title="Usuarios"
        Constrain="true">
        <Items>
            <ext:FormPanel ID="FormaUsuario" runat="server" Padding="10" Height="320">
                <Items>
                    <ext:Container ID="Container4" runat="server" Layout="ColumnLayout" Height="30" >
                        <Items>
                            <ext:Container ID="Container6" runat="server" LabelAlign="Right" Layout="FormLayout" ColumnWidth=".5" >
                                <Items>
                                    <ext:TextField runat="server" ID="txtUsuario" DataIndex="Usuario" FieldLabel="Usuario" AllowBlank="false" />
                                </Items>
                            </ext:Container>
                            <ext:Container ID="Container16" runat="server" LabelAlign="Right" Layout="FormLayout" ColumnWidth=".5" >
                                <Items>
                                    <ext:TextField runat="server" ID="txtIdentificación" DataIndex="Identificacion" FieldLabel="Identificación" AllowBlank="false" />
                                </Items>
                            </ext:Container>
                        </Items>
                    </ext:Container>
                    <ext:Container ID="Container3" runat="server" Layout="ColumnLayout" Height="30" >
                        <Items>
                            <ext:Container ID="Container5" runat="server" LabelAlign="Right" Layout="FormLayout" ColumnWidth=".5" >
                                <Items>
                                    <ext:TextField runat="server" ID="txtNombre1" DataIndex="Nombre1" FieldLabel="Primer Nombre" AllowBlank="false" AnchorHorizontal="95%" />
                                </Items>
                            </ext:Container>
                            <ext:Container ID="Container10" runat="server" LabelAlign="Right" Layout="FormLayout" ColumnWidth=".5" >
                                <Items>
                                    <ext:TextField runat="server" ID="txtNombre2" DataIndex="Nombre2" FieldLabel="Segundo Nombre" AnchorHorizontal="95%" />
                                </Items>
                            </ext:Container>
                        </Items>
                    </ext:Container>
                    <ext:Container ID="Container11" runat="server" Layout="ColumnLayout" Height="30" >
                        <Items>
                            <ext:Container ID="Container12" runat="server" LabelAlign="Right" Layout="FormLayout" ColumnWidth=".5" >
                                <Items>
                                    <ext:TextField runat="server" ID="txtApellido1" DataIndex="Apellido1" FieldLabel="Primer Apellido" AllowBlank="false" AnchorHorizontal="95%" />
                                </Items>
                            </ext:Container>
                            <ext:Container ID="Container13" runat="server" LabelAlign="Right" Layout="FormLayout" ColumnWidth=".5" >
                                <Items>
                                    <ext:TextField runat="server" ID="txtApellido2" DataIndex="Apellido2" FieldLabel="Segundo Apellido" AnchorHorizontal="95%" />
                                </Items>
                            </ext:Container>
                        </Items>
                    </ext:Container>
                    <ext:Container ID="Container14" runat="server" Layout="ColumnLayout" Height="30" >
                        <Items>
                            <ext:Container ID="Container15" runat="server" LabelAlign="Right" Layout="FormLayout" ColumnWidth=".5" >
                                <Items>
                                    <ext:TextField runat="server" ID="txtCorreo" FieldLabel="Correo" DataIndex="Correo" AllowBlank="false" AnchorHorizontal="95%" />
                                </Items>
                            </ext:Container>
                        </Items>
                    </ext:Container>
                    <ext:Container ID="Container17" runat="server" Layout="ColumnLayout" Height="30" >
                        <Items>
                            <ext:Container ID="Container18" runat="server" LabelAlign="Right" Layout="FormLayout" ColumnWidth=".5" >
                                <Items>
                                    <ext:ComboBox     
                                        ID="cbPerfil"                                                                                   
                                        runat="server" 
                                        Shadow="Drop" 
                                        Mode="Local" 
                                        TriggerAction="All" 
                                        ForceSelection="true"
                                        DisplayField="descripcion"
                                        ValueField="idperfil"
                                        FieldLabel="Perfil"
                                        AllowBlank="false"
                                        DataIndex="idperfil"
                                        EmptyText="Seleccione Perfil...">    
                                        <Store>
                                            <ext:Store ID="stUPerfil" runat="server" AutoLoad="true">
                                                <Reader>
                                                    <ext:JsonReader IDProperty="idperfil">
                                                        <Fields>
                                                            <ext:RecordField Name="idperfil" />
                                                            <ext:RecordField Name="descripcion" />                                                
                                                        </Fields>
                                                    </ext:JsonReader>
                                                </Reader>            
                                            </ext:Store>    
                                        </Store>    
                                    </ext:ComboBox>
                                </Items>
                            </ext:Container>
                        </Items>
                    </ext:Container>
                    <ext:Container ID="Container19" runat="server" Layout="ColumnLayout" Height="30" >
                        <Items>
                            <ext:Container ID="Container20" runat="server" LabelAlign="Right" Layout="FormLayout" ColumnWidth=".5" >
                                <Items>
                                    <ext:ComboBox     
	                                    ID="cbPAsistencial"                                                                              
	                                    runat="server" 
	                                    Shadow="Drop" 
	                                    Mode="Local" 
	                                    TriggerAction="All" 
	                                    ForceSelection="true"
	                                    DisplayField="nombres"
	                                    ValueField="codigo"	
	                                    EmptyText="Seleccione Profesional"
	                                    AnchorHorizontal="99%"
	                                    FieldLabel="Medico"                                        
                                        DataIndex="cod_pas"
	                                    >
	                                    <Store>
		                                    <ext:Store ID="stPAsistencial" runat="server" AutoLoad="true">
			                                    <Reader>
				                                    <ext:JsonReader IDProperty="codigo">
					                                    <Fields>
						                                    <ext:RecordField Name="codigo" />
						                                    <ext:RecordField Name="nombres" />
					                                    </Fields>
				                                    </ext:JsonReader>
			                                    </Reader>            
		                                    </ext:Store>    
	                                    </Store>    
                                    </ext:ComboBox>
                                </Items>
                            </ext:Container>
                            <ext:Container ID="Container21" runat="server" LabelAlign="Right" Layout="FormLayout" ColumnWidth=".5" >
                                <Items>
                                    <ext:Button runat="server" ID="btnLimpiaMedico" Text="Limpia Medico" Icon="ApplicationDelete" />
                                </Items>
                            </ext:Container>
                        </Items>
                    </ext:Container>
                    <ext:Container ID="Container22" runat="server" Layout="ColumnLayout" Height="30" >
                        <Items>
                            <ext:Container ID="Container23" runat="server" LabelAlign="Right" Layout="FormLayout" ColumnWidth=".5" >
                                <Items>
                                    <ext:ComboBox ID="cbEstado" runat="server" DataIndex="Estado" FieldLabel="Estado" Width="160" AllowBlank="false" >                                            
                                        <Items>
                                            <ext:ListItem Text="Activo" Value="1" />
                                            <ext:ListItem Text="Inactivo" Value="0" />                                                    
                                        </Items>
                                    </ext:ComboBox>
                                </Items>
                            </ext:Container>
                        </Items>
                    </ext:Container>
                    
                </Items>
            </ext:FormPanel>
        </Items>
        <Buttons>
            <ext:Button runat="server" ID="btnGuardaUsuario" Text="Guardar" >
                <DirectEvents>
                    <Click 
                        OnEvent="btnGuardaUsuario_DirectClick" 
                        Before="return obligatorios('FormaUsuario');">
                        <EventMask 
                            ShowMask="true" 
                            MinDelay="1000"                             
                            />
                    </Click>
                </DirectEvents>
            </ext:Button>
            <ext:Button runat="server" ID="btnCerrarUsuario" Text="Cerrar" />
        </Buttons>
    </ext:Window>

    <ext:Window 
        ID="VnPerfiles"
        runat="server"
        Icon="ApplicationFormEdit"
        Width="450"
        Height="300"
        Hidden="true" 
        Modal="true"            
        Title="Perfiles"
        Constrain="true">
        <Items>
        <ext:Panel ID="Panel1" runat="server" Frame="true" PaddingSummary="5px 2px 0" AutoWidth="true"
        Height="599" ButtonAlign="Center" Style="text-align: left">
            <Items>
                <ext:FieldSet ID="FieldSet2" runat="server" Title="" Padding="10" Height="599">
                    <Items>
                        <ext:Container ID="Container1" runat="server" Layout="ColumnLayout" Height="599">
                            <Items>
                                <ext:Container ID="Container2" runat="server" LabelAlign="Right" Layout="FormLayout"
                                    ColumnWidth=".99">
                                    <Items>                                                            
                                        <ext:TextField runat="server" ID="TIdPerfil" FieldLabel="idperfil" AllowBlank="false"  />                                            
                                        <ext:TextField runat="server" ID="TDescripcion" FieldLabel="Descripción" Width="200" AllowBlank="false"  />
                                        <ext:Checkbox runat="server" ID="ckmanAtenciones" FieldLabel="Maneja Atenciones?" />
                                    </Items>
                                </ext:Container>
                            </Items>
                        </ext:Container>
                    </Items>
                </ext:FieldSet>
            </Items>
        </ext:Panel>
        </Items>
        <Buttons>
            <ext:Button runat="server" ID="btnGuardaPerfil" Text="Guardar" >
                <DirectEvents>
                    <Click 
                        OnEvent="btnGuardaPerfil_DirectClick" 
                        Before="return obligatorios('VnPerfiles');">
                        <EventMask 
                            ShowMask="true" 
                            MinDelay="1000"                             
                            />
                    </Click>
                </DirectEvents>
            </ext:Button>
            <ext:Button runat="server" ID="btnCerrarPerfil" Text="Cerrar" />
        </Buttons>
    </ext:Window>


    <ext:ResourceManager ID="ResourceManager1" runat="server" />
    </form>
</body>
</html>

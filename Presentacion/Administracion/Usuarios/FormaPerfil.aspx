﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="FormaPerfil.aspx.vb" Inherits="Presentacion.FormaPerfil" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<%@ Register assembly="Ext.Net" namespace="Ext.Net" tagprefix="ext" %>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
    <ext:ResourceManager ID="ResourceManager1" runat="server" />

    <ext:Store ID="stSegVentana" runat="server" AutoLoad="false" >
        <Reader>
            <ext:JsonReader IDProperty="id">
                <Fields>
                    <ext:RecordField Name="id" />
                    <ext:RecordField Name="idPagWebConPer" />
                    <ext:RecordField Name="nombre" />
                    <ext:RecordField Name="descripcion" />
                    <ext:RecordField Name="accion" />
                </Fields>
            </ext:JsonReader>
        </Reader>
    </ext:Store>

    <div>
        <ext:Panel ID="PanelPrin" runat="server" AutoWidth="true" Title="Configuración Menus por Perfil" Frame="true"
        PaddingSummary="5px 5px 0" Height="810" ButtonAlign="Center" Style="text-align: left;"  >
            <Items>
                <ext:Container ID="Container1" runat="server"  Layout="ColumnLayout" Height="590" AnchorHorizontal="100%">
                    <Items>
                        <ext:Container ID="Container2" runat="server" LabelAlign="Left" Layout="FormLayout" ColumnWidth=".3">
                            <Items>
                                <ext:Panel ID="PMenu" runat="server" Collapsible="true" Layout="accordion"
                                        Split="true" Title="Menu Principal" Width="250" Height="580">
                                </ext:Panel>
                            </Items>
                        </ext:Container>
                        <ext:Container ID="Container6" runat="server" LabelAlign="Left" Layout="FormLayout" ColumnWidth=".7">
                            <Items>
                                <ext:FieldSet ID="FieldSet1" runat="server" Title="Campos" Padding="10" Height="60" >
                                    <Items>
                                        <ext:Container ID="Container7" runat="server" Layout="ColumnLayout" Height="30">
                                            <Items>
                                                <ext:Container ID="Container3" runat="server" LabelAlign="Left" Layout="FormLayout" ColumnWidth=".28">
                                                    <Items>
                                                        <ext:Label ID="lblMenu" runat="server" FieldLabel="Menu" />
                                                    </Items>
                                                </ext:Container>
                                                <ext:Container ID="Container8" runat="server" LabelAlign="Left" Layout="FormLayout" ColumnWidth=".39
                                                ">
                                                    <Items>
                                                        <ext:ComboBox     
                                                            ID="cbPerfil"                                                                                   
                                                            runat="server" 
                                                            Shadow="Drop" 
                                                            Mode="Local" 
                                                            TriggerAction="All" 
                                                            ForceSelection="true"
                                                            DisplayField="descripcion"
                                                            ValueField="idperfil"
                                                            FieldLabel="Perfil"
                                                            EmptyText="Seleccione Perfil..."                                                                                                
                                                            >
                                                            <Store>
                                                                <ext:Store ID="stPerfil" runat="server" AutoLoad="true">
                                                                    <Reader>
                                                                        <ext:JsonReader IDProperty="idperfil">
                                                                            <Fields>
                                                                                <ext:RecordField Name="idperfil" />
                                                                                <ext:RecordField Name="descripcion" />                                                
                                                                            </Fields>
                                                                        </ext:JsonReader>
                                                                    </Reader>            
                                                                </ext:Store>    
                                                            </Store>    
                                                        </ext:ComboBox>
                                                    </Items>
                                                </ext:Container>
                                                <ext:Container ID="Container9" runat="server" LabelAlign="Left" Layout="FormLayout" ColumnWidth=".33">
                                                    <Items>
                                                        <ext:Button ID="btnCargar" runat="server" Text="Carga" Icon="ApplicationKey" />
                                                    </Items>
                                                </ext:Container>
                                            </Items>               
                                        </ext:Container>                                                         
                                       </Items>
                                </ext:FieldSet>
                                <ext:GridPanel
                                    ID="grdSegVentana"
                                    runat="server"
                                    StoreId="stSegVentana"
                                    Border="false"
                                    Height="410"
                                    Title="Componentes">                                            
                                    <ColumnModel ID="ColumnModel1" runat="server">
                                        <Columns>
                                            <ext:Column Header="id" DataIndex="id" Hidden="true"/>
                                            <ext:Column Header="idPagWebConPer" DataIndex="idPagWebConPer" Hidden="true"/>
                                            <ext:Column Header="Nombre" DataIndex="nombre" Width="220"/>
                                            <ext:Column Header="Descripción" DataIndex="descripcion" Width="250"/>                                            
                                            <ext:Column Header="Acción" DataIndex="accion" Width="170">
                                                <Editor>
                                                    <ext:ComboBox ID="ComboBox1" 
                                                        runat="server" 
                                                        Shadow="Drop" 
                                                        Mode="Local"
                                                        TriggerAction="All" 
                                                        ForceSelection="true"
                                                        >
                                                        <Items>
                                                            <ext:ListItem Value="Ninguna" Text="Ninguna" />
                                                            <ext:ListItem Value="Deshabilitar" Text="Deshabilitar" />
                                                            <ext:ListItem Value="Desaparecer" Text="Desaparecer" />                                                            
                                                        </Items>    
                                                     </ext:ComboBox>                                                    
                                                </Editor>                                             
                                            </ext:Column>                                            
                                        </Columns>                                                
                                    </ColumnModel>
                                    <Buttons>
                                        <ext:Button ID="btnEjecutar" runat="server" Text="Ejecutar Cambios" Icon="Star" >
                                            <DirectEvents>
                                                <Click OnEvent="componentesSeg">
                                                    <ExtraParams>
                                                        <ext:Parameter
                                                            Name="Componentes"
                                                            Value="grdSegVentana.getRowsValues()"
                                                            Mode="Raw"
                                                            Encode="true" />                                                        
                                                    </ExtraParams>
                                                    <EventMask ShowMask="true" />
                                                </Click>
                                            </DirectEvents>
                                        </ext:Button>
                                    </Buttons>

                                </ext:GridPanel>
                                
                            </Items>
                        </ext:Container>                        
                    </Items>
                </ext:Container>
            </Items>
        </ext:Panel>
        
    
    </div>  
    </form>
</body>
</html>

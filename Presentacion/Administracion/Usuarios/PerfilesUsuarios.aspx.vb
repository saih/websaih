﻿Imports Ext.Net

Public Class PerfilesUsuarios
    Inherits System.Web.UI.Page

    Protected Property idperfil As String
        Get
            Return Me.ViewState("idperfil")
        End Get
        Set(ByVal value As String)
            Me.ViewState.Add("idperfil", value)
        End Set
    End Property
    Protected Property sUsuario As String
        Get
            Return Me.ViewState("sUsuario")
        End Get
        Set(ByVal value As String)
            Me.ViewState.Add("sUsuario", value)
        End Set
    End Property

    Protected Property Accion As String
        Get
            Return Me.ViewState("Accion")
        End Get
        Set(ByVal value As String)
            Me.ViewState.Add("Accion", value)
        End Set
    End Property

    Protected Property Accion1 As String
        Get
            Return Me.ViewState("Accion1")
        End Get
        Set(ByVal value As String)
            Me.ViewState.Add("Accion1", value)
        End Set
    End Property

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If IsPostBack = False Then
            idperfil = ""
            sUsuario = ""
        End If
    End Sub

    Protected Sub rowSelect(ByVal sender As Object, ByVal e As DirectEventArgs)
        idperfil = e.ExtraParams("ID")
        carga_usuarios()
    End Sub

    Protected Sub rowSelectUsu(ByVal sender As Object, ByVal e As DirectEventArgs)
        sUsuario = e.ExtraParams("ID")
    End Sub

    Private Function carga_usuarios()
        Dim spFunc As New Datos.spFunciones
        Dim sql As String
        sql = "select Usuario,Identificacion,razonsocial," & _
            "case when estado = 1 then 'Activo' else 'Inactivo' end estado from usuario " & _
            " where idperfil = '" & idperfil & "'"
        stUsuarios.DataSource = spFunc.executaSelect(sql)
        stUsuarios.DataBind()
        Return True
    End Function


    Private Sub btnBuscar_DirectClick(ByVal sender As Object, ByVal e As Ext.Net.DirectEventArgs) Handles btnBuscar.DirectClick
        Dim query As String = "where idperfil <> ''"
        If txtIdPerfil.Text <> "" Then
            query &= " and idperfil = '" & txtIdPerfil.Text & "'"
        End If
        If txtDescripción.Text <> "" Then
            Dim palDescripcion As String()
            palDescripcion = txtDescripción.Text.Split(" ")
            For Each palabra As String In palDescripcion
                query &= " AND descripcion LIKE '%" & palabra & "%'"
            Next
        End If

        Dim negmenu_perfil As New Negocio.Negociomenu_perfil
        Dim listMenuPerfil As New List(Of Datos.menu_perfil)
        listMenuPerfil = negmenu_perfil.Obtenermenu_perfilByWhere(query)
        stPerfiles.DataSource = listMenuPerfil
        stPerfiles.DataBind()
    End Sub

    Private Sub btnNuevoPerfil_DirectClick(ByVal sender As Object, ByVal e As Ext.Net.DirectEventArgs) Handles btnNuevoPerfil.DirectClick
        Accion = "Insert"
        TIdPerfil.Clear()
        TDescripcion.Clear()
        ckmanAtenciones.Clear()
        TIdPerfil.Disabled = False
        VnPerfiles.Show()
    End Sub

    Private Sub btnEditaPerfil_DirectClick(ByVal sender As Object, ByVal e As Ext.Net.DirectEventArgs) Handles btnEditaPerfil.DirectClick
        Accion = "Edit"
        Dim negPerfil As New Negocio.Negociomenu_perfil
        Dim Perfil As New Datos.menu_perfil
        Perfil = negPerfil.Obtenermenu_perfilById(idperfil)
        TIdPerfil.Text = Perfil.idperfil
        TIdPerfil.Disabled = True
        TDescripcion.Text = Perfil.descripcion
        ckmanAtenciones.Value = Funciones.IntToBool(Perfil.manAtenciones)
        VnPerfiles.Show()
    End Sub

    Private Sub btnCerrarPerfil_DirectClick(ByVal sender As Object, ByVal e As Ext.Net.DirectEventArgs) Handles btnCerrarPerfil.DirectClick
        VnPerfiles.Hide()
    End Sub

    Protected Sub btnGuardaPerfil_DirectClick(ByVal sender As Object, ByVal e As Ext.Net.DirectEventArgs)
        Dim negPerfil As New Negocio.Negociomenu_perfil

        If Accion = "Insert" Then
            negPerfil.Altamenu_perfil(TIdPerfil.Text, TDescripcion.Text, 0, Funciones.BoolToInt(ckmanAtenciones.Value))
            Ext.Net.X.Msg.Alert("Información", "Registro Guardado Correctamente").Show()
        Else
            negPerfil.Editamenu_perfil(TIdPerfil.Text, TDescripcion.Text, 0, Funciones.BoolToInt(ckmanAtenciones.Value))
            Ext.Net.X.Msg.Alert("Información", "Registro Editado Correctamente").Show()
        End If
        btnBuscar_DirectClick(sender, e)
        VnPerfiles.Hide()
    End Sub

    Private Function cargarCombos()
        Dim negPerfiles As New Negocio.Negociomenu_perfil
        stUPerfil.DataSource = negPerfiles.Obtenermenu_perfil
        stUPerfil.DataBind()
        Dim negPas As New Negocio.Negociosaih_pas
        stPAsistencial.DataSource = negPas.Obtenersaih_pas
        stPAsistencial.DataBind()
        Return True
    End Function

    Private Sub btnNuevoUsuario_DirectClick(ByVal sender As Object, ByVal e As Ext.Net.DirectEventArgs) Handles btnNuevoUsuario.DirectClick
        Accion = "Insert"
        presentacionUtil.limpiarObjetos(VntUsuarios)
        cargarCombos()
        VntUsuarios.Show()
    End Sub

    Private Sub btnEditarUsuario_DirectClick(ByVal sender As Object, ByVal e As Ext.Net.DirectEventArgs) Handles btnEditarUsuario.DirectClick
        Accion = "Edit"

        Dim negUsuarios As New Negocio.NegocioUsuario
        Dim usuario As New Datos.Usuario
        usuario = negUsuarios.ObtenerUsuarioById(sUsuario)
        cargarCombos()
        Reflexion.formaValores(usuario, VntUsuarios)
        VntUsuarios.Show()
    End Sub

    Protected Sub btnGuardaUsuario_DirectClick(ByVal sender As Object, ByVal e As Ext.Net.DirectEventArgs)

        Dim negUsuario As New Negocio.NegocioUsuario
        Dim usuario As New Datos.Usuario
        If Accion = "Insert" Then
            Reflexion.valoresForma(usuario, VntUsuarios)
            'usuario.cod_pas = If(usuario.cod_pas = Nothing, "0", usuario.cod_pas)
            usuario.razonsocial = usuario.Nombre1 + " " + usuario.Nombre2 + " " + usuario.Apellido1 + " " + usuario.Apellido2
            usuario.Password = usuario.Identificacion
            If negUsuario.AltaUsuario(usuario) Then
                Ext.Net.X.Msg.Alert("Información", "Registro Guardado Correctamente").Show()
                carga_usuarios()
                VntUsuarios.Hide()
            End If
        Else
            usuario = negUsuario.ObtenerUsuarioById(sUsuario)
            Reflexion.valoresForma(usuario, VntUsuarios)
            usuario.razonsocial = usuario.Nombre1 + " " + usuario.Nombre2 + " " + usuario.Apellido1 + " " + usuario.Apellido2
            If negUsuario.EditaUsuario(usuario) Then
                Ext.Net.X.Msg.Alert("Información", "Registro Editado Correctamente").Show()
                carga_usuarios()
                VntUsuarios.Hide()
            End If
        End If
        
    End Sub

    Private Sub btnCerrarUsuario_DirectClick(ByVal sender As Object, ByVal e As Ext.Net.DirectEventArgs) Handles btnCerrarUsuario.DirectClick
        VntUsuarios.Hide()
    End Sub
    
    Private Sub btnLimpiaMedico_DirectClick(sender As Object, e As Ext.Net.DirectEventArgs) Handles btnLimpiaMedico.DirectClick
        cbPAsistencial.Clear()
    End Sub
End Class
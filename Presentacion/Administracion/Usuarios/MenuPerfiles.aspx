﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="MenuPerfiles.aspx.vb" Inherits="Presentacion.MenuPerfiles" %>

<%@ Register assembly="Ext.Net" namespace="Ext.Net" tagprefix="ext" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
     <style type="text/css">
        .complete .x-tree-node-anchor span {
            text-decoration : line-through;
            color : #777;
        }
    </style>
    
    <script type="text/javascript">
        var getTasks = function () {
            var msg = "",
                selNodes = PMenu.getChecked();

            Ext.each(selNodes, function (node) {
                if (msg.length > 0) {
                    msg += ", ";
                }

                msg += node.text;
            });

            Ext.Msg.show({
                title: "Completed Tasks",
                msg: msg.length > 0 ? msg : "None",
                icon: Ext.Msg.INFO,
                minWidth: 200,
                buttons: Ext.Msg.OK
            });
        };


        function stateNode(node) {
            
            
        }
        
    </script>
</head>
<body>
    <form id="form1" runat="server">
    <ext:ResourceManager ID="ResourceManager1" runat="server" />
    <div>
        <ext:Panel ID="PanelPrin" runat="server" AutoWidth="true" Title="Configuración Menus por Perfil" Frame="true"
        PaddingSummary="5px 5px 0" Height="810" ButtonAlign="Center" Style="text-align: left;"  >
            <Items>
                <ext:Container ID="Container1" runat="server"  Layout="ColumnLayout" Height="590">
                    <Items>
                        <ext:Container ID="Container2" runat="server" LabelAlign="Left" Layout="FormLayout" ColumnWidth=".4">
                            <Items>
                                <ext:Panel ID="PMenu" runat="server" Collapsible="true" Layout="accordion"
                                        Split="true" Title="Menu Principal" Width="250" Height="580">
                                </ext:Panel>
                            </Items>
                        </ext:Container>
                        <ext:Container ID="Container3" runat="server" LabelAlign="Left" Layout="FormLayout" ColumnWidth=".3">
                            <Items>
                                <ext:ComboBox     
                                    ID="cbPerfil"                                                                                   
                                    runat="server" 
                                    Shadow="Drop" 
                                    Mode="Local" 
                                    TriggerAction="All" 
                                    ForceSelection="true"
                                    DisplayField="descripcion"
                                    ValueField="idperfil"
                                    FieldLabel="Perfil"
                                    EmptyText="Seleccione Perfil..."                                    
                                    AutoPostBack="true"
                                    >
                                    <Store>
                                        <ext:Store ID="stPerfil" runat="server" AutoLoad="true">
                                            <Reader>
                                                <ext:JsonReader IDProperty="idperfil">
                                                    <Fields>
                                                        <ext:RecordField Name="idperfil" />
                                                        <ext:RecordField Name="descripcion" />                                                
                                                    </Fields>
                                                </ext:JsonReader>
                                            </Reader>            
                                        </ext:Store>    
                                    </Store>    
                                </ext:ComboBox>
                            </Items>
                        </ext:Container>
                        <ext:Container ID="Container4" runat="server" LabelAlign="Left" Layout="FormLayout" ColumnWidth=".3">
                            <Items>
                                <ext:Button ID="btnEjecutar" runat="server" Text="Ejecutar Cambios" Icon="Star" />
                            </Items>
                        </ext:Container>
                    </Items>
                </ext:Container>
            </Items>
        </ext:Panel>
        
    
    </div>
    
    </form>
</body>
</html>

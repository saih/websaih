﻿Imports Ext.Net

Public Class FormaPerfil
    Inherits System.Web.UI.Page

    Public Shared listMenuVtn As New List(Of menu)

    Protected Property estItem As Integer
        Get
            Return Me.ViewState("estItem")
        End Get
        Set(ByVal value As Integer)
            Me.ViewState.Add("estItem", value)
        End Set
    End Property

    Protected Property idPagina As Integer
        Get
            Return Me.ViewState("idPagina")
        End Get
        Set(ByVal value As Integer)
            Me.ViewState.Add("idPagina", value)
        End Set
    End Property



    Protected Property nombreMenu As String
        Get
            Return Me.ViewState("nombreMenu")
        End Get
        Set(ByVal value As String)
            Me.ViewState.Add("nombreMenu", value)
        End Set
    End Property

    Protected Property idPerfil As String
        Get
            Return Me.ViewState("idPerfil")
        End Get
        Set(ByVal value As String)
            Me.ViewState.Add("idPerfil", value)
        End Set
    End Property


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If IsPostBack = False Then
            estItem = 0
            cargaMenu()
            Dim FuncDat As New Datos.spFunciones
            Dim queryPerfiles As String = "select idperfil, descripcion from menu_perfil"
            Dim ds As DataSet
            ds = FuncDat.executaSelect(queryPerfiles)
            stPerfil.DataSource = ds
            stPerfil.DataBind()
        End If
    End Sub

    Private Function cargaMenu()

        Dim Query As String = "select a.id_menu,a.descripcion,a.icono,a.tipo,a.padre_menu,a.orden,a.accion,a.nivel," & _
                        "case when b.idperfil is null then 0 else 1 end escoje " & _
                        "from menu_prin as a left join menu_perfil_control as b on a.id_menu = b.id_menu and b.idperfil = '" & cbPerfil.Value & "' " & _
                        "where a.padre_menu = 'PMenu' order by orden"

        Dim listMenu As New List(Of menu)
        Dim cadenaCon As String = ConfigurationManager.ConnectionStrings("ConexionLocal").ConnectionString
        Dim conn As SqlClient.SqlConnection = New SqlClient.SqlConnection(cadenaCon)
        conn.Open()
        Dim cmd As SqlClient.SqlCommand = New SqlClient.SqlCommand(Query, conn)
        Dim reader As SqlClient.SqlDataReader = cmd.ExecuteReader

        While (reader.Read)
            listMenu.Add(New menu(reader("id_menu"), reader("descripcion"), reader("icono"), reader("tipo"), reader("padre_menu"), reader("orden"), ""))
        End While
        conn.Close()

        For Each menus As menu In listMenu
            Dim PanelMenu As New Ext.Net.Panel()
            PanelMenu.Title = menus.Descripcion
            PanelMenu.Icon = menus.Icono
            Me.PMenu.Items.Add(PanelMenu)
            'nivelMenu(menus.Id_menu, 1)
            recorreMenu(menus.Id_menu, menus.Tipo, PanelMenu, New Ext.Net.TreeNode, 1)
        Next

        Return True
    End Function

    Public Function recorreMenu(ByVal idMenuPadre As String, ByVal tipoPadre As String, ByVal control As Control, ByVal padreNode As Ext.Net.TreeNode, ByVal nivel As Integer) As Boolean

        nivel += 1

        Dim Query As String


        Query = "select a.id_menu,a.descripcion,a.icono,a.tipo,a.padre_menu,a.accion,a.orden " & _
                             "from menu_prin as a where a.padre_menu = '" & idMenuPadre & "' order by orden"


        Dim listMenu As New List(Of menu)
        Dim cadenaCon As String = ConfigurationManager.ConnectionStrings("ConexionLocal").ConnectionString
        Dim conn As SqlClient.SqlConnection = New SqlClient.SqlConnection(cadenaCon)
        conn.Open()
        Dim cmd As SqlClient.SqlCommand = New SqlClient.SqlCommand(Query, conn)
        Dim reader As SqlClient.SqlDataReader = cmd.ExecuteReader
        Dim root As New Ext.Net.TreeNode()
        While (reader.Read)
            listMenu.Add(New menu(reader("id_menu"), reader("descripcion"), reader("icono"), reader("tipo"), reader("padre_menu"), reader("orden"), reader("accion")))
        End While
        conn.Close()


        For Each menus As menu In listMenu
            'nivelMenu(menus.Id_menu, nivel)
            If tipoPadre = "Panel" Then
                If menus.Tipo = "TreePanel" Then
                    Dim TreePanel As New Ext.Net.TreePanel
                    TreePanel.ID = menus.Id_menu
                    TreePanel.Width = 297
                    TreePanel.RootVisible = False
                    TreePanel.AutoScroll = True
                    TreePanel.Border = False
                    CType(control, Ext.Net.Panel).Items.Add(TreePanel)
                    recorreMenu(menus.Id_menu, menus.Tipo, TreePanel, New Ext.Net.TreeNode, nivel)
                    estItem = 0
                End If
            End If
            If tipoPadre = "TreePanel" Then
                If menus.Tipo = "TreeNode" Then

                    If estItem = 0 Then
                        root.Expanded = True
                        CType(control, Ext.Net.TreePanel).Root.Add(root)
                        estItem = 1
                    End If
                    Dim Node As New Ext.Net.TreeNode()
                    Node.Icon = menus.Icono
                    Node.Text = menus.Descripcion
                    Node.SingleClickExpand = False
                    root.Nodes.Add(Node)
                    recorreMenu(menus.Id_menu, menus.Tipo, New Control, Node, nivel)
                    If menus.Accion <> "" Then
                        Node.Listeners.Click.Handler = "Ext.net.DirectMethods.clickMenu('" & menus.Accion & "');"
                    End If
                End If
            End If
            If tipoPadre = "TreeNode" Then
                If menus.Tipo = "TreeNode" Then
                    Dim Node As New Ext.Net.TreeNode()
                    Node.Icon = menus.Icono
                    Node.Text = menus.Descripcion
                    Node.SingleClickExpand = False
                    padreNode.Nodes.Add(Node)
                    If menus.Accion <> "" Then
                        Node.Listeners.Click.Handler = "Ext.net.DirectMethods.clickMenu('" & menus.Accion & "');"
                    End If
                End If
            End If
        Next
        Return True
    End Function

    <DirectMethod()>
    Public Function clickMenu(ByVal urlPagina As String)

        Dim datos As String()
        datos = urlPagina.Split("/")
        datos = datos(datos.Length - 1).Split(".")
        nombreMenu = datos(0)
        lblMenu.Text = nombreMenu
        idPagina = id_Pagina(nombreMenu)
        Return True
    End Function


    Class menu
        Private _id_menu As String
        Private _descripcion As String
        Private _icono As Integer
        Private _tipo As String
        Private _padre_menu As String
        Private _orden As String
        Private _accion As String
        Public Property Accion() As String
            Get
                Return _accion
            End Get
            Set(ByVal value As String)
                _accion = value
            End Set
        End Property
        Public Property Descripcion() As String
            Get
                Return _descripcion
            End Get
            Set(ByVal value As String)
                _descripcion = value
            End Set
        End Property
        Public Property Icono() As Integer
            Get
                Return _icono
            End Get
            Set(ByVal value As Integer)
                _icono = value
            End Set
        End Property
        Public Property Id_menu() As String
            Get
                Return _id_menu
            End Get
            Set(ByVal value As String)
                _id_menu = value
            End Set
        End Property
        Public Property Orden() As String
            Get
                Return _orden
            End Get
            Set(ByVal value As String)
                _orden = value
            End Set
        End Property
        Public Property Padre_menu() As String
            Get
                Return _padre_menu
            End Get
            Set(ByVal value As String)
                _padre_menu = value
            End Set
        End Property
        Public Property Tipo() As String
            Get
                Return _tipo
            End Get
            Set(ByVal value As String)
                _tipo = value
            End Set
        End Property

        Public Sub New(ByVal id_menu As String,
            ByVal descripcion As String,
            ByVal icono As Integer,
            ByVal tipo As String,
            ByVal padre_menu As String,
            ByVal orden As String,
            ByVal accion As String)
            Me.Id_menu = id_menu
            Me.Descripcion = descripcion
            Me.Icono = icono
            Me.Tipo = tipo
            Me.Padre_menu = padre_menu
            Me.Orden = orden
            Me.Accion = accion
        End Sub
    End Class

    Private Sub btnCargar_DirectClick(ByVal sender As Object, ByVal e As Ext.Net.DirectEventArgs) Handles btnCargar.DirectClick
        Dim spFunc As New Datos.spFunciones
        Dim query As String

        query = "select a.id,isnull(b.id,0) idPagWebConPer,a.nombre,a.descripcion," & _
                "case when b.accion is null then 'Ninguna' else b.accion end accion from PaginaWebControl as a " & _
                "left join PagWebConPer as b on a.id = b.id_PaginaWebControl and b.idperfil = '" & cbPerfil.Value & "' " & _
                "where a.id_PaginaWeb = " & idPagina
        stSegVentana.DataSource = spFunc.executaSelect(query)
        stSegVentana.DataBind()
    End Sub

    Private Function id_Pagina(ByVal nomPag As String) As Integer
        Dim dato As Integer = 0
        Dim negPag As New Negocio.NegocioPaginaWeb
        For Each Pag As Datos.PaginaWeb In negPag.ObtenerPaginaWebbyWhere(" where nombre = '" + nomPag + "'")
            dato = Pag.id
        Next
        Return dato
    End Function

    Protected Sub componentesSeg(ByVal sender As Object, ByVal e As DirectEventArgs)
        Dim jsonValues As String = e.ExtraParams("Componentes")
        Dim records As List(Of Dictionary(Of String, String)) = JSON.Deserialize(Of List(Of Dictionary(Of String, String)))(jsonValues)
        Dim result As String = ""
        For Each record As Dictionary(Of String, String) In records
            Dim negControl As New Negocio.NegocioPagWebConPer
            Dim pagControl As New Datos.PagWebConPer
            If record("idPagWebConPer") = "0" Then
                If record("accion") <> "Ninguna" Then
                    negControl.AltaPagWebConPer(0, record("id"), cbPerfil.Value, record("accion"))
                End If
            Else
                pagControl = negControl.ObtenerPagWebConPerById(record("idPagWebConPer"))
                negControl.EditaPagWebConPer(pagControl.id, record("id"), cbPerfil.Value, record("accion"))
            End If
        Next

        Ext.Net.X.Msg.Alert("Información", "Datos Actualizados Correctamente").Show()

    End Sub

End Class
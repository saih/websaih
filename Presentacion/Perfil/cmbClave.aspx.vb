﻿Public Class cmbClave
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

    End Sub

    Private Sub btnEjecutar_DirectClick(ByVal sender As Object, ByVal e As Ext.Net.DirectEventArgs) Handles btnEjecutar.DirectClick
        'Dim func As Datos.deTablas
        If txtPassNew.Text <> txtPass2.Text Then
            Ext.Net.X.Msg.Alert("Información", "Las Claves no Coinciden, Rectifique los datos").Show()
            Return
        End If

        Dim dato As Integer = Datos.deTablas.datoCont("Usuario", " where PassWord = '" & txtPass1.Text & "' and usuario = '" & Session("Usuario") & "'")

        If dato = 0 Then
            Ext.Net.X.Msg.Alert("Información", "La clave no coincide con la registrada al Usuario").Show()
            Return
        End If

        If txtPassNew.Text.Length < 5 Then
            Ext.Net.X.Msg.Alert("Información", "La clave debe tener al menos 5 caracteres").Show()
            Return
        End If

        Dim funcSp As New Datos.spFunciones
        funcSp.executUpdate("update usuario set PassWord = '" & txtPassNew.Text & "' where Usuario = '" & Session("Usuario") & "'")
        Dim negUsuarioClave As New Negocio.NegocioUsuarioClave
        negUsuarioClave.AltaUsuarioClave(0, Session("Usuario"), Funciones.FechaActual, txtPassNew.Text)
        Ext.Net.X.Msg.Alert("Información", "Clave Modificada correctamente").Show()

    End Sub

    

End Class
﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="cmbClave.aspx.vb" Inherits="Presentacion.cmbClave" %>

<%@ Register assembly="Ext.Net" namespace="Ext.Net" tagprefix="ext" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
    <ext:ResourceManager ID="ResourceManager1" runat="server" />
    <div>
        <ext:Panel ID="Panel3" runat="server" Frame="true" PaddingSummary="5px 5px 0" AutoWidth="true"
            Height="650" ButtonAlign="Center" Style="text-align: left" Title="Cambio Clave">
            <Items>
                <ext:FieldSet ID="FieldSet3" runat="server" Title="" Padding="10" Height="150">
                    <Items>
                        <ext:Container ID="Container7" runat="server" Layout="ColumnLayout" Height="150">
                            <Items>
                                <ext:Container ID="Container8" runat="server" LabelAlign="Right" Layout="FormLayout"
                                    ColumnWidth=".5">
                                    <Items>
                                        <ext:TextField runat="server" ID="txtPass1" FieldLabel="Clave Actual" InputType="Password" />
                                        <ext:TextField runat="server" ID="txtPassNew" FieldLabel="Nueva Clave" InputType="Password" />
                                        <ext:TextField runat="server" ID="txtPass2" FieldLabel="Repetir Clave" InputType="Password" />
                                        <ext:Button ID="btnEjecutar" runat="server" Text="Ejecutar Cambios" Icon="Star" />
                                        
                                    </Items>
                                </ext:Container>
                            </Items>
                        </ext:Container>
                    </Items>
                </ext:FieldSet>
            </Items>
        </ext:Panel>
    </div>
    
    </form>
</body>
</html>

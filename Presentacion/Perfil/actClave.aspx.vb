﻿Public Class actClave
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If IsPostBack = False Then
            Dim usu As Datos.Usuario
            Dim negUsua As New Negocio.NegocioUsuario
            usu = negUsua.ObtenerUsuarioById(Session("Usuario"))
            If usu.Usuario <> "" Or usu.Usuario <> Nothing Then
                txtPassActual.Text = usu.Password
                txtEmail.Text = usu.Correo
            Else
                Session.RemoveAll()
                Response.Redirect("../Default.aspx")
            End If
        End If
    End Sub

    Private Sub btnEjecutar_DirectClick(sender As Object, e As Ext.Net.DirectEventArgs) Handles btnEjecutar.DirectClick
        If txtPassNew.Text <> txtPassNew2.Text Then
            Ext.Net.X.Msg.Alert("Información", "Las Claves no Coinciden, Rectifique los datos").Show()
            Return
        End If

        Dim dato As Integer = Datos.deTablas.datoCont("Usuario", " where PassWord = '" & txtPassActual.Text & "' and Usuario = '" & Session("Usuario") & "'")

        If dato = 0 Then
            Ext.Net.X.Msg.Alert("Información", "La clave no coincide con la registrada al Usuario").Show()
            Return
        End If

        If txtPassNew.Text.Length < 5 Then
            Ext.Net.X.Msg.Alert("Información", "La clave debe tener al menos 5 caracteres").Show()
            Return
        End If

        If txtPassNew.Text = txtPassActual.Text Then
            Ext.Net.X.Msg.Alert("Información", "La clave debe ser diferente a la actual").Show()
            Return
        End If

        If txtEmail.Text.Length = 0 Then
            Ext.Net.X.Msg.Alert("Información", "Se debe definir el correo Electronico").Show()
            Return
        End If

        Dim funcSp As New Datos.spFunciones
        funcSp.executUpdate("update usuario set PassWord = '" & txtPassNew.Text & "',correo = '" & txtEmail.Text & "' where Usuario = '" & Session("Usuario") & "'")
        Dim negUsuarioClave As New Negocio.NegocioUsuarioClave
        negUsuarioClave.AltaUsuarioClave(0, Session("Usuario"), Funciones.FechaActual, txtPassNew.Text)
        Ext.Net.X.Msg.Alert("Información", "Clave Modificada correctamente").Show()
        Session.RemoveAll()
        Response.Redirect("../Default.aspx")
    End Sub
End Class
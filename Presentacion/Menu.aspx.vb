﻿Imports Ext.Net

Public Class Menu
    Inherits System.Web.UI.Page


    Shared estItem As Integer = 0

    Protected Property Usuario_Inicio As String
        Get
            Return ViewState("Usuario_Inicio")
        End Get
        Set(ByVal value As String)
            ViewState.Add("Usuario_Inicio", value)
        End Set
    End Property

    Protected Property VerfSession As Integer
        Get
            Return ViewState("VerfSession")
        End Get
        Set(ByVal value As Integer)
            ViewState.Add("VerfSession", value)
        End Set
    End Property


    Protected Sub RefreshTime(ByVal sender As Object, ByVal e As DirectEventArgs)

        Try
            If Session("USUARIO") = "" Then
                'Response.Redirect("Default.aspx")

                Dim script As String = " Ext.MessageBox.alert('Información', 'Su Session ha Caducado.', showResult);" & _
                                       " Ext.MessageBox.alert('Status', 'Prueba Data.', showResult);"

                Ext.Net.X.Js.AddScript(script)
            End If
        Catch ex As Exception

        End Try

        
    End Sub

    <DirectMethod()>
    Public Function verificaSession()

        Try
            If VerfSession = 1 Then
                If Session("USUARIO") = "" Then
                    'Response.Redirect("CaducoSession.aspx")
                    VerfSession = 0
                    VentanaIngreso.Show()
                End If
            End If
            
        Catch ex As Exception

        End Try

        Return True
    End Function

    <DirectMethod()>
    Public Function reedireccionar()
        Response.Redirect("Default.aspx")
        Return True
    End Function

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If IsPostBack = False Then

            If Session("PANTALLA") = "COMPLETA" Then
                PMenu.Collapse()
                PMenu.Collapsed = False
            End If

            If Session("USUARIO") = "" Then
                Response.Redirect("Default.aspx")
            End If
            If Session("FEC_SERVER") = "" Then
                fechaServidor()
            End If
            'Session.Timeout = 360
            Usuario_Inicio = Session("USUARIO")
            Dim PanelIni As New Ext.Net.Panel()
            PanelIni.ID = "idInicio"
            PanelIni.Title = "Inicio"
            PanelIni.Closable = False
            PanelIni.AutoLoad.Mode = Ext.Net.LoadMode.IFrame
            PanelIni.AutoLoad.ShowMask = True
            PanelIni.AutoLoad.MaskMsg = "Cargando Inicio..."
            'PanelIni.AutoLoad.Url = "Inicio.aspx"
            PanelIni.AutoLoad.Url = "Inicio.aspx"
            TabMain.Add(PanelIni)
            VerfSession = 1
            'PanelIni.AddTo(Me.TabMain)
            'Me.TabMain.SetActiveTab(PanelIni.ClientID)


            'sessionesAsp()



            Dim Query As String

            If Session("USUARIO") <> "admin" Then

                Query = "select a.id_menu,a.descripcion,a.icono,a.tipo,a.padre_menu,a.orden,a.accion " & _
                        "from menu_prin as a inner join menu_perfil_control as b on a.id_menu = b.id_menu " & _
                        "where a.padre_menu = 'PMenu' and b.idperfil = '" & Session("PERFIL") & "'order by orden"

            Else
                Query = "select a.id_menu,a.descripcion,a.icono,a.tipo,a.padre_menu,a.orden,a.accion " & _
                        "from menu_prin as a where a.padre_menu = 'PMenu' order by orden"

            End If


            Dim listMenu As New List(Of menu)
            Dim cadenaCon As String = ConfigurationManager.ConnectionStrings("ConexionLocal").ConnectionString
            Dim conn As SqlClient.SqlConnection = New SqlClient.SqlConnection(cadenaCon)
            conn.Open()
            Dim cmd As SqlClient.SqlCommand = New SqlClient.SqlCommand(Query, conn)
            Dim reader As SqlClient.SqlDataReader = cmd.ExecuteReader

            While (reader.Read)
                listMenu.Add(New menu(reader("id_menu"), reader("descripcion"), reader("icono"), reader("tipo"), reader("padre_menu"), reader("orden"), ""))
            End While
            conn.Close()

            For Each menus As menu In listMenu
                Dim PanelMenu As New Ext.Net.Panel()
                PanelMenu.Title = menus.Descripcion
                PanelMenu.Icon = menus.Icono
                Me.PMenu.Items.Add(PanelMenu)
                'nivelMenu(menus.Id_menu, 1)
                recorreMenu(menus.Id_menu, menus.Tipo, PanelMenu, New Ext.Net.TreeNode, 1)
            Next

        End If



        'Ext.Net.X.Js.AddScript("#{TaskManager1}.startAll();")

    End Sub


    Private Function nivelMenu(ByVal id_menu As String, ByVal nivel As Integer)
        Dim Query As String = "update menu_prin set nivel = " & nivel & " where id_menu = '" & id_menu & "'"
        Dim cadenaCon As String = System.Web.Configuration.WebConfigurationManager.ConnectionStrings("ConexionLocal").ConnectionString
        Dim conn As SqlClient.SqlConnection = New SqlClient.SqlConnection(cadenaCon)
        conn.Open()
        Dim cmd As SqlClient.SqlCommand = New SqlClient.SqlCommand(Query, conn)
        cmd.ExecuteNonQuery()
        conn.Close()
        Return True
    End Function


    Public Function recorreMenu(ByVal idMenuPadre As String, ByVal tipoPadre As String, ByVal control As Control, ByVal padreNode As Ext.Net.TreeNode, ByVal nivel As Integer) As Boolean

        nivel += 1

        Dim Query As String
        If Session("USUARIO") <> "admin" Then

            Query = "select a.id_menu,a.descripcion,a.icono,a.tipo,a.padre_menu,a.accion,a.orden " & _
                              "from menu_prin as a inner join menu_perfil_control as b on a.id_menu = b.id_menu " & _
                              "where a.padre_menu = '" & idMenuPadre & "' and b.idperfil = '" & Session("PERFIL") & "'order by orden"

        Else
            Query = "select a.id_menu,a.descripcion,a.icono,a.tipo,a.padre_menu,a.accion,a.orden " & _
                             "from menu_prin as a where a.padre_menu = '" & idMenuPadre & "' order by orden"

        End If

        Dim listMenu As New List(Of menu)
        Dim cadenaCon As String = ConfigurationManager.ConnectionStrings("ConexionLocal").ConnectionString
        Dim conn As SqlClient.SqlConnection = New SqlClient.SqlConnection(cadenaCon)
        conn.Open()
        Dim cmd As SqlClient.SqlCommand = New SqlClient.SqlCommand(Query, conn)
        Dim reader As SqlClient.SqlDataReader = cmd.ExecuteReader
        Dim root As New Ext.Net.TreeNode()
        While (reader.Read)
            listMenu.Add(New menu(reader("id_menu"), reader("descripcion"), reader("icono"), reader("tipo"), reader("padre_menu"), reader("orden"), reader("accion")))
        End While
        conn.Close()


        For Each menus As menu In listMenu
            'nivelMenu(menus.Id_menu, nivel)
            If tipoPadre = "Panel" Then
                If menus.Tipo = "TreePanel" Then
                    Dim TreePanel As New Ext.Net.TreePanel
                    TreePanel.ID = menus.Id_menu
                    TreePanel.Width = 297
                    TreePanel.RootVisible = False
                    TreePanel.AutoScroll = True
                    TreePanel.Border = False
                    CType(control, Ext.Net.Panel).Items.Add(TreePanel)
                    recorreMenu(menus.Id_menu, menus.Tipo, TreePanel, New Ext.Net.TreeNode, nivel)
                    estItem = 0
                End If
            End If
            If tipoPadre = "TreePanel" Then
                If menus.Tipo = "TreeNode" Then

                    If estItem = 0 Then
                        root.Expanded = True
                        CType(control, Ext.Net.TreePanel).Root.Add(root)
                        estItem = 1
                    End If
                    Dim Node As New Ext.Net.TreeNode()
                    Node.Icon = menus.Icono
                    Node.Text = menus.Descripcion
                    Node.SingleClickExpand = False
                    root.Nodes.Add(Node)
                    recorreMenu(menus.Id_menu, menus.Tipo, New Control, Node, nivel)
                    If menus.Accion <> "" Then
                        Node.Listeners.Click.Handler = "addTab(#{TabMain}, '" & menus.Id_menu & "', '" & menus.Accion & "?TipoDoc=" & Session("Csc_Usuario") & "', '" & menus.Descripcion & "');"
                    End If
                End If
            End If
            If tipoPadre = "TreeNode" Then
                If menus.Tipo = "TreeNode" Then
                    Dim Node As New Ext.Net.TreeNode()
                    Node.Icon = menus.Icono
                    Node.Text = menus.Descripcion
                    Node.SingleClickExpand = False
                    padreNode.Nodes.Add(Node)
                    If menus.Accion <> "" Then
                        Node.Listeners.Click.Handler = "addTab(#{TabMain}, '" & menus.Id_menu & "', '" & menus.Accion & "?TipoDoc=" & Session("Csc_Usuario") & "', '" & menus.Descripcion & "');"
                    End If
                End If
            End If

        Next
        Return True
    End Function



    Class menu
        Private _id_menu As String
        Private _descripcion As String
        Private _icono As Integer
        Private _tipo As String
        Private _padre_menu As String
        Private _orden As String
        Private _accion As String
        Public Property Accion() As String
            Get
                Return _accion
            End Get
            Set(ByVal value As String)
                _accion = value
            End Set
        End Property
        Public Property Descripcion() As String
            Get
                Return _descripcion
            End Get
            Set(ByVal value As String)
                _descripcion = value
            End Set
        End Property
        Public Property Icono() As Integer
            Get
                Return _icono
            End Get
            Set(ByVal value As Integer)
                _icono = value
            End Set
        End Property
        Public Property Id_menu() As String
            Get
                Return _id_menu
            End Get
            Set(ByVal value As String)
                _id_menu = value
            End Set
        End Property
        Public Property Orden() As String
            Get
                Return _orden
            End Get
            Set(ByVal value As String)
                _orden = value
            End Set
        End Property
        Public Property Padre_menu() As String
            Get
                Return _padre_menu
            End Get
            Set(ByVal value As String)
                _padre_menu = value
            End Set
        End Property
        Public Property Tipo() As String
            Get
                Return _tipo
            End Get
            Set(ByVal value As String)
                _tipo = value
            End Set
        End Property

        Public Sub New(ByVal id_menu As String,
            ByVal descripcion As String,
            ByVal icono As Integer,
            ByVal tipo As String,
            ByVal padre_menu As String,
            ByVal orden As String,
            ByVal accion As String)
            Me.Id_menu = id_menu
            Me.Descripcion = descripcion
            Me.Icono = icono
            Me.Tipo = tipo
            Me.Padre_menu = padre_menu
            Me.Orden = orden
            Me.Accion = accion
        End Sub
    End Class


    Private Sub btnCerrarSesion_DirectClick(ByVal sender As Object, ByVal e As Ext.Net.DirectEventArgs) Handles btnCerrarSesion.DirectClick

        'Ext.getBody().mask('Good Bye!', 'x-mask-loading');

        'Ext.Net.X.Mask("Good Bye!", "x-mask-loading")



        Session.RemoveAll()
        Response.Redirect("Default.aspx")

    End Sub

    Private Function fechaServidor()
        Dim Query As String = "select CONVERT(varchar(10),getdate(),110) fecha "

        Dim cadenaCon As String = ConfigurationManager.ConnectionStrings("ConexionLocal").ConnectionString
        Dim conn As SqlClient.SqlConnection = New SqlClient.SqlConnection(cadenaCon)
        conn.Open()
        Dim cmd As SqlClient.SqlCommand = New SqlClient.SqlCommand(Query, conn)
        Dim reader As SqlClient.SqlDataReader = cmd.ExecuteReader
        Dim fecha As String = ""
        Dim fechas As String()
        While (reader.Read)
            fecha = reader("fecha")
        End While
        fecha = fecha.Replace("-", "/")

        fechas = fecha.Split("/")
        fecha = fechas(1) & "/" & fechas(0) & "/" & fechas(2)
        Session.Add("FEC_SERVER", fecha)
        Return True
    End Function


    Private Function sessionesAsp()
        Dim fileName As New System.IO.StreamWriter(Server.MapPath("Datos" & Session("UserName") & ".txt"))
        fileName.WriteLine(Session("Csc_Usuario"))
        fileName.WriteLine(Session("UserName"))
        fileName.WriteLine(Session("Nombre1"))
        fileName.WriteLine(Session("Nombre2"))
        fileName.WriteLine(Session("Apellido1"))
        fileName.WriteLine(Session("Apellido2"))
        fileName.WriteLine(Session("Email"))
        fileName.WriteLine(Session("Csc_Perfil"))
        fileName.WriteLine(Session("Empresa"))
        fileName.WriteLine(Session("PERFIL"))
        fileName.Close()
        Return True
    End Function

    <DirectMethod()>
    Public Function cambioTema(ByVal Tema As Integer)

        'ResourceManager1.Theme = Tema
        'ResourceManager1.SetTheme(Tema)
        ResourceManager1.SetTheme(Tema)

        Return True
    End Function


    <DirectMethod()>
    Public Function impresionVentana(url As String)
        WinImpresion.LoadContent(url)
        VentanaImpresion.Show()
        Return True
    End Function

    Private Sub btnLogin_DirectClick(ByVal sender As Object, ByVal e As Ext.Net.DirectEventArgs) Handles btnLogin.DirectClick
        Try


            If txtUsername.Text <> Usuario_Inicio Then
                Ext.Net.X.Msg.Alert("Información", "El Usuario no coincide con el que inicialmente ingreso").Show()
                Return
            End If

            Dim ct As Integer = 0
            Dim usuarios As List(Of Datos.Usuario)
            Dim NgUsuario As New Negocio.NegocioUsuario

            usuarios = NgUsuario.ObtenerUsuariobyWhere(" where Usuario = '" & txtUsername.Text & "' and PassWord = '" & txtPassword.Text & "'")

            Dim usuario As Datos.Usuario

            For Each usuario In usuarios
                ct += 1
            Next

            If ct = 0 Then
                Ext.Net.X.Msg.Alert("Información", "Usuario o Clave Incorrectas").Show()
            Else
                Dim negUsuarioLog As New Negocio.Negociousuario_log
                Dim ipHost = Request.ServerVariables("REMOTE_ADDR")
                negUsuarioLog.Altausuario_log(0, usuario.Usuario, Funciones.FechaActual, ipHost, "", "", "ReIngreso")
                Session.Add("Usuario", usuario.Usuario)
                Session.Add("NombreCompleto", usuario.Nombre1 & " " & usuario.Nombre2 & " " & usuario.Apellido1 & " " & usuario.Apellido2)
                Session.Add("Nombre1", usuario.Nombre1)
                Session.Add("Nombre2", usuario.Nombre2)
                Session.Add("Apellido1", usuario.Apellido1)
                Session.Add("Apellido2", usuario.Apellido2)
                Session.Add("Email", "")
                Session.Add("Csc_Perfil", "")
                Session.Add("Empresa", "")
                Session.Add("PERFIL", usuario.idperfil)
                VerfSession = 1
                VentanaIngreso.Hide()
            End If

        Catch ex As Exception

            Ext.Net.X.Msg.Alert("Información", "Usuario o clave incorrecta").Show()
        End Try
    End Sub
End Class
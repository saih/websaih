﻿Public Class _Default
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If IsPostBack = False Then
            Dim negPrm As New Negocio.Negociosaih_prm
            stSede.DataSource = negPrm.Obtenersaih_prm
            stSede.DataBind()
            txtUsername.Focus(True)

        End If

    End Sub

    Protected Sub btnLogin_DirectClick(ByVal sender As Object, ByVal e As Ext.Net.DirectEventArgs)
        Try
            If cbSede.Value = "" Or cbSede.Value = Nothing Then
                Ext.Net.X.Msg.Alert("Información", "Debe seleccionar una sede").Show()
                Return
            End If

            Dim ct As Integer = 0
            Dim usuarios As List(Of Datos.Usuario)
            Dim NgUsuario As New Negocio.NegocioUsuario
            Dim usuariosSede As List(Of Datos.UsuarioSede)
            Dim NegUsuarioSede As New Negocio.NegocioUsuarioSede

            If txtPassword.Text.Length = 0 Then
                Ext.Net.X.Msg.Alert("Información", "Su Clave no puede ser vacia").Show()
                Return
            End If

            usuarios = NgUsuario.ObtenerUsuariobyWhere(" where Usuario = '" & txtUsername.Text & "' and PassWord = '" & txtPassword.Text & "'")

            Dim usuario As New Datos.Usuario

            For Each usuario In usuarios
                ct += 1
            Next

            If ct = 0 Then
                Ext.Net.X.Msg.Alert("Información", "Usuario o Clave Incorrectas").Show()
            Else
                usuariosSede = NegUsuarioSede.ObtenerUsuarioSedebyWhere(" where idUsuario = '" & usuario.Usuario & "' and idSede = '" & cbSede.Value & "'")
                If usuariosSede.Count = 0 Then
                    Ext.Net.X.Msg.Alert("Información", "El Usuario no tiene permisos para acceder a la Sede").Show()
                    Return
                End If
                Dim negUsuarioLog As New Negocio.Negociousuario_log
                Dim ipHost = Request.ServerVariables("REMOTE_ADDR")
                Dim ctCla As Integer = Datos.deTablas.datoCont("UsuarioClave", " where Usuario = '" & usuario.Usuario & "'")

                If ctCla = 0 Then
                    Ext.Net.X.Msg.Alert("Información", "Su Clave ha Caducado debe Modificarla").Show()
                    Session.Add("Usuario", usuario.Usuario)
                    Response.Redirect("./Perfil/actClave.Aspx")
                Else
                    Dim cantDias As Integer = System.Web.Configuration.WebConfigurationManager.AppSettings("diasVenceClave").ToString
                    Dim idUsuClave As Integer = Datos.deTablas.maxCampoTabla1("id", "UsuarioClave", " Usuario = '" & usuario.Usuario & "'")
                    Dim usuClave As Datos.UsuarioClave
                    Dim negUsuClave As New Negocio.NegocioUsuarioClave
                    usuClave = negUsuClave.ObtenerUsuarioClaveById(idUsuClave)
                    Dim difFechas As Integer = Datos.deTablas.difFechas("dd", Funciones.Fecha(usuClave.fecha), Funciones.FechaActual)
                    If difFechas >= cantDias Then
                        Ext.Net.X.Msg.Alert("Información", "Su Clave ha Caducado debe Modificarla").Show()
                        Session.Add("IdUsuario", usuario.Usuario)
                        Response.Redirect("./Perfil/actClave.aspx")
                    End If
                End If
                negUsuarioLog.Altausuario_log(0, usuario.Usuario, Funciones.FechaActual, ipHost, "", "", "Ingreso")
                Session.Add("Usuario", usuario.Usuario)
                Session.Add("NombreCompleto", usuario.Nombre1 & " " & usuario.Nombre2 & " " & usuario.Apellido1 & " " & usuario.Apellido2)
                Session.Add("Nombre1", usuario.Nombre1)
                Session.Add("Nombre2", usuario.Nombre2)
                Session.Add("Apellido1", usuario.Apellido1)
                Session.Add("Apellido2", usuario.Apellido2)
                Session.Add("Email", "")
                Session.Add("Csc_Perfil", "")
                Session.Add("Empresa", "")
                Session.Add("Sede", cbSede.Value)
                Session.Add("PERFIL", usuario.idperfil)
                Session.Add("PANTALLA", cbTipoPantalla.Value)
                If usuario.Estado = 1 Then
                    Response.Redirect("Menu.aspx")
                Else
                    Ext.Net.X.Msg.Alert("Información", "Su Usuario se encuentra en estado Inactivo, Comuniquese con el Administrador").Show()
                End If
            End If

        Catch ex As Exception

            Ext.Net.X.Msg.Alert("Información", "Usuario o clave incorrecta").Show()
        End Try
    End Sub

    Protected Sub btnRecuperaClave_DirectClick(sender As Object, e As Ext.Net.DirectEventArgs)

    End Sub
End Class
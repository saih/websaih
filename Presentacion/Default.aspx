﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="Default.aspx.vb" Inherits="Presentacion._Default" %>

<%@ Register assembly="Ext.Net" namespace="Ext.Net" tagprefix="ext" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Saih Gestión en Salud</title>        
</head>
<body background="Imagenes/fondoInicio.jpg">
    <form id="form1" runat="server">
    <ext:Window ID="VentanaIngreso" runat="server" Closable="false" Resizable="false"
        Height="320" Icon="Lock" Title="Gestión Hospitalaria " Draggable="false" Width="355"
        Modal="true" BodyPadding="5" Layout="AnchorLayout">
        <Items>
            <ext:FormPanel ID="Window2" runat="server" Layout="AnchorLayout" Border="false" >
                <Items>
                <ext:Toolbar ID="Toolbar1" runat="server">
                <Items>
                    <ext:Image ID="Image1" runat="server" ImageUrl="imagenes/logo.png" Width="335" Height="120" />
                    </Items>
                 </ext:Toolbar>   
                </Items>
            </ext:FormPanel>
            <ext:FieldSet runat="server" Border="false" AutoHeight="true" AutoWidth="False" X="0"
                Y="0" LabelPad="0" MonitorResize="True" Frame="True" Padding="35" LabelWidth="100"
                ID="ctl38">
                <Items>
                    <ext:TextField ID="txtUsername" runat="server" FieldLabel="Usuario" AllowBlank="false"
                        BlankText="El nombre de usuario es requerido" AnchorHorizontal="90%" Width="200" AutoFocus="true" AutoFocusDelay="50" >
                        <Listeners>
                            <Show Handler="#{txtUsername}.focus();" Delay="50"  />
                        </Listeners>
                    </ext:TextField>
                    <ext:TextField ID="txtPassword" runat="server" InputType="Password" FieldLabel="Contraseña"
                        AllowBlank="false" BlankText="La clave es requerida." AnchorHorizontal="90%"
                        Width="200" >
                    </ext:TextField>
                    <ext:ComboBox     
	                    ID="cbSede"
	                    runat="server" 
	                    Shadow="Drop" 
	                    Mode="Local" 
	                    TriggerAction="All" 
	                    ForceSelection="true"
	                    DisplayField="lugar"
	                    ValueField="codpre"	
	                    EmptyText="Seleccione Sede"
	                    AnchorHorizontal="99%"
	                    AllowBlank="false"
                        FieldLabel="Sede"
	                    >
	                    <Store>
		                    <ext:Store ID="stSede" runat="server" AutoLoad="true">
			                    <Reader>
				                    <ext:JsonReader IDProperty="codpre">
					                    <Fields>
						                    <ext:RecordField Name="codpre" />
						                    <ext:RecordField Name="lugar" />
					                    </Fields>
				                    </ext:JsonReader>
			                    </Reader>            
		                    </ext:Store>    
	                    </Store>    
                    </ext:ComboBox>
                    <ext:ComboBox ID="cbTipoPantalla" runat="server" DataIndex="cbTipoPantalla" FieldLabel="Pantalla" AnchorHorizontal="90%" Hidden="true" >
                        <Items>
                            <ext:ListItem Text="Pantalla Normal" Value="NORMAL" />
                            <ext:ListItem Text="Pantalla Completa" Value="COMPLETA" />                            
                        </Items>
                    </ext:ComboBox>
                    
                </Items>
            </ext:FieldSet>
        </Items>
        <Buttons>
            <ext:Button ID="btnLogin" runat="server" Text="Ingresar"  Icon="Accept">
                <DirectEvents>
                    <Click OnEvent="btnLogin_DirectClick">
                        <EventMask ShowMask="true" />
                    </Click>
                </DirectEvents>
            </ext:Button>

            <ext:Button ID="btnCancel" runat="server" Text="Cancelar" Icon="Decline" >
                <Listeners>
                   
                    <Click Handler="#{Window1}.hide();#{lblMessage}.setText('LOGIN CANCELED')" />
                    
                </Listeners>
            </ext:Button>
            <ext:Button runat="server" ID="btnRecuperaClave" Text="Recuperar Mi Contraseña" Icon="BulletKey" Hidden="true" >
                <DirectEvents>
                    <Click OnEvent="btnRecuperaClave_DirectClick">
                        <EventMask ShowMask="true" />
                    </Click>
                </DirectEvents>
            </ext:Button>
        </Buttons>
        <KeyMap>
     <ext:KeyBinding StopEvent="true">
           <Keys>
                <ext:Key Code="ENTER" />
           </Keys>
           <Listeners>
                <Event Handler="#{btnLogin}.fireEvent('click')" />
           </Listeners>
      </ext:KeyBinding>
</KeyMap>
    </ext:Window>
    <ext:ResourceManager ID="ResourceManager1" runat="server" />
    
    </form>
</body>
</html>

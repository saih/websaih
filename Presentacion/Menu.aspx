﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="Menu.aspx.vb" Inherits="Presentacion.Menu" %>



<%@ Register assembly="Ext.Net" namespace="Ext.Net" tagprefix="ext" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <script type="text/javascript" src="/public/js/jquery-1.9.0.js"></script>
    <script type="text/javascript" src="/public/js/jquery.timer.js"></script>
    <title>Saih Gestión en Salud</title>
    <script type="text/javascript">
        var addTab = function (tabPanel, id, url, description) {
            var tab = tabPanel.getComponent(id);
            if (!tab) {
                tab = tabPanel.add({
                    id: id,
                    title: description,
                    closable: true,
                    autoLoad: {
                        showMask: true,
                        url: url,
                        mode: "iframe",
                        maskMsg: "Cargando " + description + "..."
                    }
                });

            }

            tabPanel.setActiveTab(tab);
        }

        function Hola() {
            Alert("Hola");
        }

        
     </script>

     <script type="text/javascript">

         var timer = $.timer(
			function () {
			    verifica();
			},
			5000,
			true
		);

			function verifica() {
			    
			    var imgsrc = 'http://www.google.es/intl/en_com/images/logo_plain.png';
			    var img = new Image();

			    img.onerror = function () {
			        //alert("No hay conexion a internet.");
			    }
			    img.onload = function () {
			        //alert("Hay conexion a internet.");
			        Ext.net.DirectMethods.verificaSession();
			    }

			    img.src = imgsrc; 
			}	
	</script>
</head>

<body>
    <form id="form1" runat="server">
    <ext:ResourceManager ID="ResourceManager1" runat="server" ShowWarningOnAjaxFailure="false" />
    <ext:Viewport ID="Viewport1" runat="server" Layout="border">
        <Items>
            <ext:Toolbar ID="Toolbar1" runat="server" Region="North" Height="25" Margins="0 0 4 0">
                <Items>                    
                    <ext:Label ID="Label1" runat="server" Html='Saih Gestión en Salud' Icon="Application" Cls="title-label"
                        AutoDataBind="true" />
                    <ext:ToolbarFill ID="ToolbarFill1" runat="server" />
                    <ext:Button ID="Button1" runat="server" Icon="StatusOnline" Text='' AutoDataBind="true" Hidden="true">
                        <Menu runat="server">
                            <ext:Menu ID="Menu1" runat="server">
                                <Items>
                                    <ext:MenuItem Text="Profile" Icon="Magnifier" Disabled="true" />
                                    <ext:MenuItem Text="Opciones" Icon="Wrench">
                                        <Menu runat="server">
                                            <ext:Menu ID="Menu2" runat="server">
                                                <Items>
                                                    <ext:MenuItem Text="Escoja Tema...">
                                                        <Menu runat="server">
                                                            <ext:Menu ID="Menu3" runat="server">
                                                                <Items>
                                                                    <ext:MenuItem Text="Default">
                                                                        <Listeners>
                                                                            <Click Handler="Ext.net.DirectMethods.cambioTema(0);" />
                                                                        </Listeners>
                                                                    </ext:MenuItem>
                                                                    <ext:MenuItem Text="Gray">
                                                                        <Listeners>
                                                                            <Click Handler="Ext.net.DirectMethods.cambioTema(1);" />
                                                                        </Listeners>
                                                                    </ext:MenuItem>
                                                                    <ext:MenuItem Text="Slate">
                                                                        <Listeners>
                                                                            <Click Handler="Ext.net.DirectMethods.cambioTema(3);" />
                                                                        </Listeners>
                                                                    </ext:MenuItem>
                                                                </Items>
                                                            </ext:Menu>
                                                        </Menu>
                                                    </ext:MenuItem>
                                                </Items>
                                            </ext:Menu>
                                        </Menu>
                                    </ext:MenuItem>
                                </Items>
                            </ext:Menu>
                        </Menu>
                    </ext:Button>
                    <ext:Button ID="Button2" runat="server" Icon="Help" Text="Help" Hidden="true">
                        <Menu runat="server">
                            <ext:Menu ID="Menu4" runat="server">
                                <Items>
                                    <ext:MenuItem Text="Report a Defect" Icon="Bug">
                                        <Listeners>
                                            <Click Handler="Northwind.addTab({ title : el.text, url : '/Home/Form/', icon : el.iconCls });" />
                                        </Listeners>
                                    </ext:MenuItem>
                                    <ext:MenuItem Text="About" Icon="Information">
                                        <Listeners>
                                            <Click Handler="#{winAbout}.show();" />
                                        </Listeners>
                                    </ext:MenuItem>
                                </Items>
                            </ext:Menu>
                        </Menu>
                    </ext:Button>
                    <ext:ToolbarSeparator ID="ToolbarSeparator1" runat="server" />
                    <ext:Button ID="btnCerrarSesion" runat="server" Icon="LockGo" Text="Cerrar Sesion" />
                </Items>
            </ext:Toolbar>
            <ext:Panel ID="PMenu" runat="server" Collapsible="true" Layout="accordion" Region="West"
                Split="true" Title="Menu Principal" Width="250">
            </ext:Panel>
            <ext:Panel ID="PMain" runat="server" Layout="Fit" Region="Center">
                <Items>
                    <ext:TabPanel ID="TabMain" runat="server" ActiveTabIndex="0" Border="false" Title="Center"
                        ResizeTabs="true">
                        <Items>
                            <ext:Panel ID="PInicio" runat="server" Closable="False" Title="Inicio" Hidden="true">
                                <AutoLoad Mode="IFrame" Scripts="true" Url="http://www.google.com/" />
                            </ext:Panel>
                        </Items>
                    </ext:TabPanel>
                </Items>
            </ext:Panel>
        </Items>
    </ext:Viewport>


    <ext:Window ID="VentanaIngreso" runat="server" Closable="false" Resizable="false"
        Height="320" Icon="Lock" Title="SAIH" Draggable="false" Width="355"
        Modal="true" BodyPadding="5" Layout="AnchorLayout" Hidden="true">
        <Items>
            <ext:FormPanel ID="Window2" runat="server" Layout="AnchorLayout" Border="false" >
                <Items>
                <ext:Toolbar ID="Toolbar2" runat="server">
                <Items>
                    <ext:Image ID="Image1" runat="server" ImageUrl="imagenes/logo.png" AutoHeight="true"
                        AutoWidth="true" >
                        <ResizeConfig ID="ResizeConfig1" runat="server" Adjustments="0, 0" EnableViewState="true"
                            Visible="False">
                        </ResizeConfig>
                    </ext:Image>
                    </Items>
                 </ext:Toolbar>   
                </Items>
            </ext:FormPanel>
            <ext:FieldSet runat="server" Border="false" AutoHeight="true" AutoWidth="False" X="0"
                Y="0" LabelPad="0" MonitorResize="True" Frame="True" Padding="35" LabelWidth="100">
                <Items>
                    <ext:TextField ID="txtUsername" runat="server" FieldLabel="Usuario" AllowBlank="false"
                        BlankText="El nombre de usuario es requerido" AnchorHorizontal="90%" Width="200" AutoFocus="true" AutoFocusDelay="50" >
                        <Listeners>
                    <Show Handler="#{txtUsername}.focus();" Delay="50"  />
                </Listeners>
                    </ext:TextField>
                    <ext:TextField ID="txtPassword" runat="server" InputType="Password" FieldLabel="Contraseña"
                        AllowBlank="false" BlankText="La clave es requerida." AnchorHorizontal="90%"
                        Width="200" >
                    </ext:TextField>                    
                </Items>
            </ext:FieldSet>
        </Items>
        <Buttons>
            <ext:Button ID="btnLogin" runat="server" Text="Ingresar"  Icon="Accept" />                      
        </Buttons>
        <KeyMap>
         <ext:KeyBinding StopEvent="true">
               <Keys>
                    <ext:Key Code="ENTER" />
               </Keys>
               <Listeners>
                    <Event Handler="#{btnLogin}.fireEvent('click')" />
               </Listeners>
          </ext:KeyBinding>
        </KeyMap>
    </ext:Window>
    <ext:Window ID="VentanaImpresion" runat="server" Closable="true" Resizable="false" Height="520" Width="820" Icon="Lock" Title="FORMATO IMPRESION " Draggable="false" Modal="true" BodyPadding="5" Layout="AnchorLayout" Hidden="true">
    <Items>
    </Items> 
    <KeyMap>
    <ext:KeyBinding StopEvent="true">
    <Keys>
    <ext:Key Code="ENTER" />
    </Keys>
    <Listeners>
    <Event Handler="#{_btnClose}.fireEvent('click')" />
    </Listeners>
    </ext:KeyBinding>
    </KeyMap>
    </ext:Window>

    <ext:Window
            ID="WinImpresion"
            runat="server"
            Icon="ApplicationFormEdit"
            Width="700" 
            Height="400" 
            Hidden="true" 
            Modal="true"
            Constrain="true"
            Title="Impresion de Documento"
        >
            <AutoLoad 
                Url="" 
                Mode="IFrame" 
                TriggerEvent="show" 
                ReloadOnEvent="true" 
                ShowMask="true" 
                MaskMsg="Cargando Impresion">
                <Params>
                    <ext:Parameter Name="Csc" Value="" Mode="Value" />
                </Params>
            </AutoLoad>        
        </ext:Window>

    </form>

    
    

</body>
</html>

function recorreObjeto(objeto) {
    var objExt = Ext.getCmp(objeto.id);
    console.log(objeto.id + " - " + $('#' + objeto.id).attr("type") + " - " + objExt.xtype);

}

function obligatorios(forma) {
    var correcto = true;
    var errores = "";

    
    $('#' + forma).find('[type=text],[type=checkbox]').each(function () {
        var elemento = this;
        var objExt = Ext.getCmp(elemento.id);
        if (objExt.allowBlank === false) {
            if (objExt.getValue() === "") {
                objExt.validate();
                errores += "<h4>Campo " + objExt.fieldLabel + " Obligatorio</h4>";
                correcto = false;
            }
        }
        if (objExt.minLength > 0) {
            objExt.validate();
            if (objExt.getValue().length < objExt.minLength) {
                if (objExt.getValue() !== "") {
                    errores += "<h4>El tama�o minimo del campo  " + objExt.fieldLabel + " es " + objExt.minLength + "</h4>";
                    correcto = false;
                }

            }
        }
    });

    if (correcto === false) {
        Ext.net.Notification.show({
            bringToFront: true,
            pinEvent: 'click',
            width: 300,
            iconCls: 'icon-information',            
            showFx     : {
            fxName : 'slideIn', 
            args   : [ 
            'tr', { 
                easing   : 'bounceOut',
                duration : 1.0
                }
            ]},
            alignToCfg: {
                offset: [-10, -10],
                position: 'br-br',
                el: Ext.net.getEl(forma)
            },
            html: errores,
            title: 'Errores del Formulario'
        });
    }


    return correcto;
}


﻿Imports Ext.Net
Imports System.Net

Public Class Funciones


    Public Shared Function Fecha(ByVal fecha_1 As String) As String
        'Funcion que me devuelve el formato de la fecha asi '2012-05-24'
        '""2013-01-02T00:00:00""
        fecha_1 = fecha_1.Replace("""", "")
        Dim fechas As String()
        fecha_1 = fecha_1.Substring(0, 10)
        fechas = fecha_1.Split("/")
        Try
            fecha_1 = fechas(2) & fechas(1) & fechas(0)
        Catch ex As Exception
            Try
                fechas = fecha_1.Split("-")
                fecha_1 = fechas(0) & fechas(1) & fechas(2)
            Catch ex1 As Exception
                fecha_1 = "null"
            End Try
        End Try

        If fecha_1 = "00010101" Then
            Return "19900101"
        End If

        Return fecha_1

    End Function

    Public Shared Function FechaRpt(ByVal fecha_1 As String) As String
        Dim fecha As String = ""
        fecha_1 = fecha_1.Replace("""", "")
        Dim fechas As String()
        fecha_1 = fecha_1.Substring(0, 10)
        fechas = fecha_1.Split("/")
        fecha = fechas(2) & "-" & fechas(1) & "-" & fechas(0)
        Return fecha
    End Function

    Public Shared Function FechaAgendaMedico(ByVal fecha_1 As String) As String
        'Funcion que me devuelve el formato de la fecha asi '2012-05-24'
        '""2013-01-02T00:00:00""
        fecha_1 = fecha_1.Replace("""", "")
        fecha_1 = fecha_1.Trim()
        Dim fechas As String()
        fechas = fecha_1.Split("/")
        Try
            fecha_1 = fechas(2) & fechas(0) & fechas(1)
        Catch ex As Exception
            Try
                fechas = fecha_1.Split("-")
                fecha_1 = fechas(0) & fechas(1) & fechas(2)
            Catch ex1 As Exception
                fecha_1 = "null"
            End Try

        End Try

        If fecha_1 = "00010101" Then
            Return "19900101"
        End If

        Return fecha_1

    End Function

    Public Shared Function FechaAgenda(ByVal fecha_1 As String) As String
        'Funcion que me devuelve el formato de la fecha asi '2012-05-24'
        '""2013-01-02T00:00:00""
        fecha_1 = fecha_1.Replace("""", "")
        Dim fechas As String()
        fecha_1 = fecha_1.Substring(0, 10)
        fechas = fecha_1.Split("/")
        Try
            fecha_1 = fechas(2) & fechas(0) & fechas(1)
        Catch ex As Exception
            Try
                fechas = fecha_1.Split("-")
                fecha_1 = fechas(0) & fechas(1) & fechas(2)
            Catch ex1 As Exception
                fecha_1 = "null"
            End Try

        End Try

        If fecha_1 = "00010101" Then
            Return "19900101"
        End If

        Return fecha_1

    End Function

    Public Shared Function FechaDB(ByVal fecha_1 As String) As String
        'Funcion que me devuelve el formato de la fecha asi '2012-05-24'
        '""2013-01-02T00:00:00""
        fecha_1 = fecha_1.Replace("""", "")
        Dim fechas As String()
        fecha_1 = fecha_1.Substring(0, 10)
        fechas = fecha_1.Split("/")
        Try
            fecha_1 = fechas(2) & fechas(1) & fechas(0)
        Catch ex As Exception
            Try
                fechas = fecha_1.Split("-")
                fecha_1 = fechas(0) & fechas(1) & fechas(2)
            Catch ex1 As Exception
                fecha_1 = "null"
            End Try

        End Try

        If fecha_1 = "00010101" Then
            Return "19900101"
        End If

        Return fecha_1

    End Function

    Public Shared Function fechaHoraDB(fecha As String) As String
        Dim sFecha As String = FechaDB(fecha)
        Dim sHora As String = Hora(fecha)
        Return sFecha & " " & sHora
    End Function

    Public Shared Function FechaWindow(ByVal fecha As String) As String
        Dim fechaRet As String = ""
        Dim fechas As String()
        fechas = fecha.Split(" ")
        Return fechas(0)
    End Function

    Public Shared Function HoraWindow(ByVal fecha As String) As String
        Dim fechaRet As String = ""
        Dim fechas As String()
        Dim dtFecha As String
        fechas = fecha.Split(" ")
        Try
            If fechas(1).Length = 7 Then
                dtFecha = fechas(1).Substring(0, 4)
            Else
                dtFecha = fechas(1).Substring(0, 5)
            End If

        Catch ex As Exception
            dtFecha = "00:00"
        End Try
        Return dtFecha
    End Function

    Public Shared Function FechaCalendar(ByVal fecha As String) As String
        Dim fechaRet As String = ""
        Dim fechas As String()
        fechas = fecha.Split("T")
        Dim strFechas As String()
        strFechas = fechas(0).Split("-")
        fechaRet = strFechas(2) & "-" & strFechas(1) & "-" & strFechas(0)
        Return fechaRet
    End Function

    Public Shared Function HoraCalendar(ByVal fecha As String) As String
        Dim horaRet As String = ""
        Dim fechaRet As String = ""
        Dim fechas As String()
        fechas = fecha.Split("T")
        Dim strHoras As String()
        strHoras = fechas(1).Split(":")
        horaRet = strHoras(0) + ":" + strHoras(1)
        Return horaRet
    End Function

    Public Shared Function FechaJsonAgenda(ByVal fecha As String) As String
        '2011-02-07 13:00
        Dim FechaJson As String = ""
        Dim fechas As String()
        fechas = fecha.Split(" ")
        Dim strHora As String = ""
        Dim horas As String()
        Dim strFechas As String()
        Dim strFecha As String = ""
        Try
            horas = fechas(1).Split(":")
            strHora = horas(0) & ":" & horas(1)
            If horas(0).Length = 1 Then
                strHora = "0" & strHora
            End If
        Catch ex As Exception
            strHora = "01:30"
        End Try

        strFechas = fechas(0).Split("/")
        strFecha = strFechas(2) & "-" & strFechas(1) & "-" & strFechas(0)
        'If fechas(2) = "PM" Then
        '    Dim hora As Integer = Convert.ToInt32(horas(0))
        '    hora = hora + 12
        '    strHora = hora & ":" & horas(1)
        'Else

        'End If

        FechaJson = strFecha & " " & strHora
        '2011-02-07 13:00
        Return FechaJson
    End Function

    Public Shared Function FechaAgregaEvent(ByVal fecha As String) As String
        '2011-02-07 13:00
        Dim fechas As String()
        fechas = fecha.Split(" ")
        Dim strFechas As String()
        Dim strFecha As String = ""
        strFechas = fechas(0).Split("/")
        strFecha = strFechas(1) & "/" & strFechas(0) & "-" & strFechas(2)
        Return strFecha
    End Function

    Public Shared Function HoraAgregaEvent(ByVal fecha As String) As String
        '2011-02-07 13:00
        Dim fechas As String()
        fechas = fecha.Split(" ")
        Dim strHora As String = ""
        Dim horas As String()
        horas = fechas(1).Split(":")
        If fechas(2) = "PM" And horas(0) <> "12" Then
            Dim nHora As Integer = Convert.ToInt32(horas(0)) + 12
            strHora = nHora & ":" & horas(1)
        Else
            strHora = horas(0) & ":" & horas(1)
        End If
        Return strHora
    End Function


    Public Shared Function Hora(ByVal fecha_1 As String) As String
        'Funcion que me devuelve el formato de la fecha asi '2012-05-24'
        '""2013-01-02T00:00:00""
        fecha_1 = fecha_1.Replace("""", "")
        'Dim fechas As String()
        Try
            fecha_1 = fecha_1.Substring(11, 7)
            Return fecha_1 & ".000"
        Catch ex As Exception
            Return "00:00:00.000"
        End Try
    End Function

    Public Shared Function Hora1(ByVal fecha_1 As String) As String
        'Funcion que me devuelve el formato de la fecha asi '2012-05-24'
        '""2013-01-02T00:00:00""
        fecha_1 = fecha_1.Replace("""", "")
        'Dim fechas As String()
        Try
            fecha_1 = fecha_1.Substring(11, 7)
            Return fecha_1 & ".000"
        Catch ex As Exception
            Return fecha_1 & ".000"
        End Try
    End Function

    Public Shared Function ValorRadioGroup(ByVal grupo As Ext.Net.RadioGroup) As String
        Dim valor As String = ""
        Dim Radio As New Ext.Net.Radio
        Dim listRadio As New List(Of Ext.Net.Radio)
        listRadio = grupo.CheckedItems
        For Each Radio In listRadio
            valor = Radio.InputValue
        Next
        Return valor
    End Function


    Public Shared Function BoolToInt(ByVal valor As Boolean) As Integer
        Dim res As Integer = 0
        If valor = True Then
            res = 1
        End If
        Return res
    End Function

    Public Shared Function IntToBool(ByVal valor As Integer) As Boolean
        Dim res As Boolean = False
        If valor = 1 Then
            res = True
        End If
        Return res
    End Function

    Public Shared Function ipHost() As String
        Dim strHostName As String = Dns.GetHostName()
        Dim ipEntry As IPHostEntry = Dns.GetHostEntry(strHostName)
        Dim ip As String = Convert.ToString(ipEntry.AddressList(ipEntry.AddressList.Length - 1))
        Return ip
    End Function

    Public Shared Function cargaStoreDominio(ByVal store As Ext.Net.Store, dominio As String)
        Dim negDominio As New Negocio.Negociosaih_tab
        store.DataSource = negDominio.Obtenersaih_tabbyWhere(" where dominio = '" & dominio & "' order by nombre asc")
        store.DataBind()
        Return True
    End Function

    Public Shared Function cargaStoreDominio(ByVal store As Ext.Net.Store, dominio As String, valor As String)
        Dim negDominio As New Negocio.Negociosaih_tab
        store.DataSource = negDominio.Obtenersaih_tabbyWhere(" where dominio = '" & dominio & "' and valor = '" & valor & "' order by nombre asc")
        store.DataBind()
        Return True
    End Function

    Public Shared Function cargaStoreDominioIn(ByVal store As Ext.Net.Store, dominio As String)
        Dim negDominio As New Negocio.Negociosaih_tab
        store.DataSource = negDominio.Obtenersaih_tabbyWhere(" where dominio in('" & dominio & "')")
        store.DataBind()
        Return True
    End Function


    Public Shared Function cargaStoreDominioCodNom(ByVal store As Ext.Net.Store, dominio As String)
        Dim spFunc As New Datos.spFunciones
        store.DataSource = spFunc.executaSelect("select codigo,'('+codigo+') ' + nombre nombre from saih_tab where dominio = '" & dominio & "'")
        store.DataBind()
        Return True
    End Function

    Public Shared Function FechaActual() As String
        Dim fecha As String = ""
        Dim dtDate As Date = Now
        fecha = Funciones.Fecha(dtDate) & " " & Funciones.Hora(dtDate)
        Return fecha
    End Function


    

    Public Shared Function horaFecha(ByVal fecha As String) As String
        Dim hora As String = "00:00"

        Try
            hora = fecha.Substring(11, 5)

            If hora.Substring(4, 1) = ":" Then
                hora = hora.Substring(0, hora.Length - 1)
            End If

        Catch ex As Exception

        End Try

        Return hora
    End Function

    Public Shared Function estadoColorCalendar(estado As String) As Integer
        Dim dato As Integer = 1

        If estado = "Pendiente" Then
            dato = 1
        End If
        If estado = "Programada" Then
            dato = 2
        End If


        Return dato
    End Function

End Class

﻿Public Class Seguridad

    Private idPag As Integer
    Private nomPag As String
    Private listaComponentes As New List(Of componentes)
    Private idperfil As String


    Public Sub New(ByVal Objeto As Control, ByVal idperfil As String)
        Try
            Me.idperfil = idperfil
            nomPag = nombrePagina(Objeto.ToString)
            idPag = idPagina()
            Dim cargaControles As String = System.Web.Configuration.WebConfigurationManager.AppSettings("cargaControles").ToString
            If cargaControles = "1" And
                idperfil.ToUpper = "ADMON" Then
                Dim negPag As New Negocio.NegocioPaginaWeb
                If idPag = 0 Then
                    idPag = negPag.AltaRetPaginaWeb(0, nomPag)
                End If
                guardaComponentes(Objeto)
            End If

            If idPag = 0 Then
                Return
            End If

            If idperfil.ToUpper <> "ADMON" Then
                ComponeWeb()
                recorreObjetos(Objeto)
            End If
        Catch ex As Exception
            Ext.Net.X.Msg.Alert("Información", ex.Message).Show()
        End Try





    End Sub

    Private Function recorreObjetos(ByVal Padre As Control)

        For Each hijo As Control In Padre.Controls
            Dim dato As String = hijo.GetType().ToString
            'Globales.listaObjetos.Add(dato)
            recorreObjetos(hijo)
            If dato = "Ext.Net.Button" Then
                'CType(hijo, Ext.Net.Button).Disabled = True
                Dim accion As String = accionEnControl(CType(hijo, Ext.Net.Button).ID)
                If accion <> "Ninguna" Then
                    If accion = "Deshabilitar" Then
                        CType(hijo, Ext.Net.Button).Disabled = True
                    Else
                        CType(hijo, Ext.Net.Button).Hidden = True
                    End If
                End If
            End If

            If dato = "Ext.Net.ComboBox" Then
                Dim accion As String = accionEnControl(CType(hijo, Ext.Net.ComboBox).ID)
                If accion <> "Ninguna" Then
                    If accion = "Deshabilitar" Then
                        CType(hijo, Ext.Net.ComboBox).Disabled = True
                    Else
                        CType(hijo, Ext.Net.ComboBox).Hidden = True
                    End If
                End If
            End If
        Next

        Return True
    End Function

    Private Function guardaComponentes(ByVal padre As Control)
        For Each hijo As Control In padre.Controls
            Dim dato As String = hijo.GetType().ToString
            guardaComponentes(hijo)
            If dato = "Ext.Net.Button" Then
                If idControl(CType(hijo, Ext.Net.Button).ID.ToString) = 0 Then
                    Dim negControl As New Negocio.NegocioPaginaWebControl
                    negControl.AltaPaginaWebControl(0, idPag, CType(hijo, Ext.Net.Button).ID.ToString, "Boton",
                                                    CType(hijo, Ext.Net.Button).Text)
                End If
            End If

            If dato = "Ext.Net.ComboBox" Then
                If idControl(CType(hijo, Ext.Net.ComboBox).ID.ToString) = 0 Then
                    Dim negControl As New Negocio.NegocioPaginaWebControl
                    negControl.AltaPaginaWebControl(0, idPag, CType(hijo, Ext.Net.ComboBox).ID.ToString, "Combo",
                                                    CType(hijo, Ext.Net.ComboBox).Text)
                End If
            End If


        Next

        Return True
    End Function


    Private Function nombrePagina(ByVal cadena As String) As String
        Dim datos As String()
        datos = cadena.Split("_")
        Return datos(datos.Length - 2)
    End Function

    Public Function segWin(ByVal idperfil As String, ByVal Objeto As Control)
        'MsgBox(nombrePagina(Objeto.ToString))



        Return True
    End Function

    Private Function accionEnControl(ByVal nombre As String) As String

        For Each componente As componentes In listaComponentes
            If nombre = componente.Nombre Then
                Return componente.Accion
            End If
        Next

        Return "Ninguna"
    End Function




    Private Function idPagina() As Integer
        Dim dato As Integer = 0
        Dim negPag As New Negocio.NegocioPaginaWeb
        For Each Pag As Datos.PaginaWeb In negPag.ObtenerPaginaWebbyWhere(" where nombre = '" + nomPag + "'")
            dato = Pag.id
        Next
        Return dato
    End Function

    Private Function idControl(ByVal nombre As String) As Integer
        Dim dato As Integer = 0
        Dim negControl As New Negocio.NegocioPaginaWebControl
        For Each cont As Datos.PaginaWebControl In negControl.ObtenerPaginaWebControlbyWhere(" where nombre = '" & _
                                                   nombre + "' and id_PaginaWeb = " & idPag)
            dato = cont.id
        Next
        Return dato
    End Function

    Private Function ComponeWeb()
        Dim Query As String = ""
        Query = "select a.nombre,b.accion from PaginaWebControl as a inner join PagWebConPer as b on a.id = b.id_PaginaWebControl " & _
            "where a.id_PaginaWeb = " & idPag & " and b.idperfil = '" & idperfil & "'"
        Dim lista As New List(Of String())
        Dim cadenaCon As String = System.Web.Configuration.WebConfigurationManager.ConnectionStrings("ConexionLocal").ConnectionString
        Dim conn As SqlClient.SqlConnection = New SqlClient.SqlConnection(cadenaCon)
        conn.Open()
        Dim cmd As SqlClient.SqlCommand = New SqlClient.SqlCommand(Query, conn)
        Dim reader As SqlClient.SqlDataReader = cmd.ExecuteReader
        While (reader.Read)
            listaComponentes.Add(New componentes(reader(0), reader(1)))
        End While
        conn.Close()
        Return lista
        Return True
    End Function



    Class componentes

        Public Nombre As String
        Public Accion As String

        Public Sub New(ByVal Nombre As String, ByVal Accion As String)
            Me.Nombre = Nombre
            Me.Accion = Accion
        End Sub

    End Class

End Class

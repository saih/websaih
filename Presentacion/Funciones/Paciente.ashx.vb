﻿Imports System.Web
Imports System.Web.Services
Imports Ext.Net

Public Class Paciente1
    Implements System.Web.IHttpHandler

    Sub ProcessRequest(ByVal context As HttpContext) Implements IHttpHandler.ProcessRequest

        context.Response.ContentType = "text/json"

        Dim query = String.Empty

        If Not String.IsNullOrEmpty(context.Request("query")) Then
            query = context.Request("query")
        End If

        Dim storeAutCom = New Datos.StoreAutoComplete
        Dim pacientes As Ext.Net.Paging(Of Datos.saih_his)
        pacientes = storeAutCom.storeSaih_his(query)
        context.Response.Write(String.Format("{{total:{1},'paciente':{0}}}", JSON.Serialize(pacientes.Data), pacientes.TotalRecords))

    End Sub

    ReadOnly Property IsReusable() As Boolean Implements IHttpHandler.IsReusable
        Get
            Return False
        End Get
    End Property

End Class
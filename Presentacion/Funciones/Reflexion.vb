﻿Imports System.Reflection

Public Class Reflexion

    Public Function infoTipo(ByVal t As Type)
        Dim flags As BindingFlags = BindingFlags.Instance _
                        Or BindingFlags.Public _
                        Or BindingFlags.DeclaredOnly _
                        Or BindingFlags.Static
        Dim s As String = String.Format("Los miembros públicos de: {0}", t.Name)
        Console.WriteLine(s)
        Console.WriteLine(New String("="c, s.Length))
        '
        Console.WriteLine("Los campos")
        Dim cs As FieldInfo() = t.GetFields(flags)
        For Each m As FieldInfo In cs
            Console.WriteLine("   {0}", m.Name)
        Next
        Console.WriteLine()
        '
        Console.WriteLine("Los métodos")
        Dim mi As MethodInfo() = t.GetMethods(flags)
        For Each m As MethodInfo In mi
            ' no mostrar los que tienen nombres especiales
            ' por ejemplo, las propiedades tendrán dos métodos asociados:
            ' get_NombrePropiedad y set_NomprePropiedad
            If m.IsSpecialName = False Then
                Console.WriteLine("   {0}", m.Name)
            End If
        Next
        Console.WriteLine()
        '
        Console.WriteLine("Las propiedades")
        Dim pr As PropertyInfo() = t.GetProperties(flags)
        For Each m As PropertyInfo In pr
            Console.WriteLine("   {0}", m.Name)
        Next
        Console.WriteLine()
        '
        Console.WriteLine("Los eventos")
        Dim evs As EventInfo() = t.GetEvents(flags)
        For Each m As EventInfo In evs
            Console.WriteLine("   {0}", m.Name)
        Next
        Console.WriteLine()
        Return True
    End Function


    Public Shared Function toUpper(ByVal objeto As Object)

        If System.Web.Configuration.WebConfigurationManager.AppSettings("manejaMayusculas").ToString = "1" Then
            For Each pi As PropertyInfo In objeto.GetType.GetProperties()
                If pi.PropertyType.ToString = "System.String" Then
                    Dim strResultado As String = pi.GetValue(objeto, Nothing)
                    pi.SetValue(objeto, strResultado.ToUpper, Nothing)
                End If
            Next
        End If

        Return True

    End Function

    Public Shared Function valoresForma(ByVal objeto As Object, ByVal Padre As Control)
        correrObjetos(objeto, Padre)
        vacios(objeto)
        Return True
    End Function

    Public Shared Function correrObjetos(ByVal objeto As Object, ByVal Padre As Control)
        For Each hijo As Control In Padre.Controls
            Dim dato As String = hijo.GetType().ToString
            Dim negLogError As New Negocio.Negociolog_error
            correrObjetos(objeto, hijo)
            Dim pi As PropertyInfo
            If dato = "Ext.Net.ComboBox" Then
                If CType(hijo, Ext.Net.ComboBox).DataIndex.ToString <> "" Then
                    pi = objeto.GetType.GetProperty(CType(hijo, Ext.Net.ComboBox).DataIndex.ToString)
                    If pi.PropertyType.ToString = "System.Int32" Then
                        Try
                            pi.SetValue(objeto, Integer.Parse(CType(hijo, Ext.Net.ComboBox).Value), Nothing)
                        Catch ex As Exception
                            pi.SetValue(objeto, Integer.Parse(0), Nothing)
                            negLogError.Altalog_error(0, "Reflexion",
                                    "Conversión: " & CType(hijo, Ext.Net.ComboBox).DataIndex.ToString & " - " & CType(hijo, Ext.Net.ComboBox).Value,
                                    Funciones.FechaActual, "")
                        End Try
                    Else
                        pi.SetValue(objeto, CType(hijo, Ext.Net.ComboBox).Value, Nothing)
                    End If

                End If
            End If
            If dato = "Ext.Net.TextField" Then
                If CType(hijo, Ext.Net.TextField).DataIndex.ToString <> "" Then
                    pi = objeto.GetType.GetProperty(CType(hijo, Ext.Net.TextField).DataIndex.ToString)
                    pi.SetValue(objeto, CType(hijo, Ext.Net.TextField).Text, Nothing)
                End If
            End If

            If dato = "Ext.Net.TextArea" Then
                If CType(hijo, Ext.Net.TextArea).DataIndex.ToString <> "" Then
                    pi = objeto.GetType.GetProperty(CType(hijo, Ext.Net.TextArea).DataIndex.ToString)
                    pi.SetValue(objeto, CType(hijo, Ext.Net.TextArea).Text, Nothing)
                End If
            End If

            If dato = "Ext.Net.DateField" Then
                If CType(hijo, Ext.Net.DateField).DataIndex.ToString <> "" Then
                    pi = objeto.GetType.GetProperty(CType(hijo, Ext.Net.DateField).DataIndex.ToString)
                    pi.SetValue(objeto, Funciones.Fecha(CType(hijo, Ext.Net.DateField).Text), Nothing)
                End If
            End If
            If dato = "Ext.Net.NumberField" Then
                If CType(hijo, Ext.Net.NumberField).DataIndex.ToString <> "" Then
                    pi = objeto.GetType.GetProperty(CType(hijo, Ext.Net.NumberField).DataIndex.ToString)

                    If pi.PropertyType.ToString = "System.Int32" Then
                        Try
                            pi.SetValue(objeto, Integer.Parse(CType(hijo, Ext.Net.NumberField).Value), Nothing)
                        Catch ex As Exception
                            pi.SetValue(objeto, 0, Nothing)
                        End Try
                    Else
                        If pi.PropertyType.ToString = "System.Decimal" Then
                            Try
                                pi.SetValue(objeto, Decimal.Parse(CType(hijo, Ext.Net.NumberField).Value), Nothing)
                            Catch ex As Exception
                                pi.SetValue(objeto, 0.0, Nothing)
                            End Try
                        Else
                            pi.SetValue(objeto, CType(hijo, Ext.Net.NumberField).Value.ToString, Nothing)
                        End If
                    End If
                End If
            End If
            If dato = "Ext.Net.Checkbox" Then
                If CType(hijo, Ext.Net.Checkbox).DataIndex.ToString <> "" Then
                    pi = objeto.GetType.GetProperty(CType(hijo, Ext.Net.Checkbox).DataIndex.ToString)
                    pi.SetValue(objeto, Funciones.BoolToInt(CType(hijo, Ext.Net.Checkbox).Value), Nothing)
                End If
            End If
        Next
        Return True
    End Function

    Public Shared Function vacios(ByVal objeto As Object)

        If System.Web.Configuration.WebConfigurationManager.AppSettings("manejaMayusculas").ToString = "1" Then
            For Each pi As PropertyInfo In objeto.GetType.GetProperties()
                If pi.PropertyType.ToString = "System.String" Then
                    Dim strResultado As String = pi.GetValue(objeto, Nothing)
                    If strResultado = Nothing Then
                        pi.SetValue(objeto, "", Nothing)
                    End If
                End If
            Next
        End If
        Return True
    End Function

    Public Shared Function formaValores(ByVal objeto As Object, ByVal Padre As Control)
        correrControl(objeto, Padre)
        Return True
    End Function

    Public Shared Function correrControl(ByVal objeto As Object, ByVal Padre As Control)
        For Each hijo As Control In Padre.Controls
            Dim dato As String = hijo.GetType().ToString
            correrControl(objeto, hijo)
            Dim pi As PropertyInfo
            If dato = "Ext.Net.ComboBox" Then
                If CType(hijo, Ext.Net.ComboBox).DataIndex.ToString <> "" Then
                    pi = objeto.GetType.GetProperty(CType(hijo, Ext.Net.ComboBox).DataIndex.ToString)
                    Dim val As String = pi.GetValue(objeto, Nothing)
                    CType(hijo, Ext.Net.ComboBox).Value = val
                End If
            End If
            If dato = "Ext.Net.TextField" Then
                If CType(hijo, Ext.Net.TextField).DataIndex.ToString <> "" Then
                    pi = objeto.GetType.GetProperty(CType(hijo, Ext.Net.TextField).DataIndex.ToString)
                    Dim val As String = pi.GetValue(objeto, Nothing)
                    CType(hijo, Ext.Net.TextField).Text = val
                End If

            End If

            If dato = "Ext.Net.TextArea" Then
                If CType(hijo, Ext.Net.TextArea).DataIndex.ToString <> "" Then
                    pi = objeto.GetType.GetProperty(CType(hijo, Ext.Net.TextArea).DataIndex.ToString)
                    Dim val As String = pi.GetValue(objeto, Nothing)
                    CType(hijo, Ext.Net.TextArea).Text = val
                End If
            End If

            If dato = "Ext.Net.DateField" Then
                If CType(hijo, Ext.Net.DateField).DataIndex.ToString <> "" Then
                    pi = objeto.GetType.GetProperty(CType(hijo, Ext.Net.DateField).DataIndex.ToString)
                    Dim val As String = pi.GetValue(objeto, Nothing)
                    CType(hijo, Ext.Net.DateField).Text = val
                End If
            End If
            If dato = "Ext.Net.NumberField" Then
                If CType(hijo, Ext.Net.NumberField).DataIndex.ToString <> "" Then
                    pi = objeto.GetType.GetProperty(CType(hijo, Ext.Net.NumberField).DataIndex.ToString)
                    CType(hijo, Ext.Net.NumberField).Text = pi.GetValue(objeto, Nothing)
                End If
            End If
            If dato = "Ext.Net.Checkbox" Then
                If CType(hijo, Ext.Net.Checkbox).DataIndex.ToString <> "" Then
                    pi = objeto.GetType.GetProperty(CType(hijo, Ext.Net.Checkbox).DataIndex.ToString)
                    CType(hijo, Ext.Net.Checkbox).Value = Funciones.IntToBool(pi.GetValue(objeto, Nothing))
                End If
            End If
        Next
        Return True
    End Function
End Class

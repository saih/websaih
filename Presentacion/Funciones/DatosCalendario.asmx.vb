﻿Imports System.Web.Services

Imports System.Web.Services.Protocols
Imports System.ComponentModel
Imports Ext.Net
Imports System.Web.Script.Services


' Para permitir que se llame a este servicio Web desde un script, usando ASP.NET AJAX, quite la marca de comentario de la línea siguiente.
' <System.Web.Script.Services.ScriptService()> _
<System.Web.Services.WebService(Namespace:="http://tempuri.org/")> _
<System.Web.Services.WebServiceBinding(ConformsTo:=WsiProfiles.BasicProfile1_1)> _
<ToolboxItem(False)> _
<ScriptService()> _
Public Class DatosCalendario
    Inherits System.Web.Services.WebService

    <WebMethod()> _
    Public Function HelloWorld() As String
        Return "Hola a todos"
    End Function

    


    <WebMethod()> _
    Public Function getEvents(ByVal sWhere As String) As IEnumerable(Of Evento)

        Dim lista As New List(Of Evento)

        Dim negAge As New Negocio.Negociosaih_age

        sWhere &= " order by fecha_cita"


        For Each age As Datos.saih_age In negAge.Obtenersaih_agebyWhere(sWhere)
            Try
                Dim titulo As String = ""
                Dim now As DateTime = age.fecha_cita
                Dim now1 As DateTime = age.fecha_fincita

                If age.numhis <> "" Then
                    Dim negHis As New Negocio.Negociosaih_his
                    Dim his As New Datos.saih_his
                    his = negHis.Obtenersaih_hisById(age.numhis)
                    titulo = "(" & his.razonsocial & ") - " & age.estado
                Else
                    titulo = age.estado
                End If


                lista.Add(New [Evento] With { _
                .EventId = age.id, _
                .CalendarId = Funciones.estadoColorCalendar(age.estado), _
                .Title = titulo,
                .StartDate = now, _
                .EndDate = now1, _
                .IsAllDay = False, _
                .Notes = age.observaciones, _
                .Url = "", _
                .Location = "", _
                .Reminder = ""
                })
            Catch ex As Exception
                Dim negLog As New Negocio.Negociolog_error
                Dim log As New Datos.log_error
                log.tipo = "Calendario"
                log.descripcion = "Agenda : " & age.id & " - " & ex.Message
                log.fecha = Funciones.FechaActual
                log.usuario = Session("Usuario")
                negLog.Altalog_error(log)
            End Try
            
        Next







        Return lista

    End Function

    <WebMethod()> _
    Public Function Delete(ByVal e As Evento) As Evento


        

        Return e
    End Function


End Class
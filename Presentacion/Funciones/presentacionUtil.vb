﻿Public Class presentacionUtil

    Public Shared Function limpiarObjetos(ByVal Padre As Control)
        For Each hijo As Control In Padre.Controls
            Dim dato As String = hijo.GetType().ToString
            limpiarObjetos(hijo)
            If dato = "Ext.Net.ComboBox" Then
                CType(hijo, Ext.Net.ComboBox).Clear()
            End If
            If dato = "Ext.Net.TextField" Then
                CType(hijo, Ext.Net.TextField).Clear()
            End If
            If dato = "Ext.Net.TextArea" Then
                CType(hijo, Ext.Net.TextArea).Clear()
            End If
            If dato = "Ext.Net.DateField" Then
                CType(hijo, Ext.Net.DateField).Clear()
            End If
            If dato = "Ext.Net.NumberField" Then
                CType(hijo, Ext.Net.NumberField).Clear()
            End If
            If dato = "Ext.Net.TimeField" Then
                CType(hijo, Ext.Net.TimeField).Clear()
            End If

            If dato = "Ext.Net.Label" Then
                CType(hijo, Ext.Net.Label).Text = ""
            End If

        Next
        Return True
    End Function

End Class

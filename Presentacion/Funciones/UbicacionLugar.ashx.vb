﻿Imports System.Web
Imports System.Web.Services
Imports Ext.Net

Public Class UbicacionLugar
    Implements System.Web.IHttpHandler

    Sub ProcessRequest(ByVal context As HttpContext) Implements IHttpHandler.ProcessRequest

        context.Response.ContentType = "text/json"

        Dim query = String.Empty

        If Not String.IsNullOrEmpty(context.Request("query")) Then
            query = context.Request("query")
        End If

        Dim UbicacionLugar As Ext.Net.Paging(Of Lugares)
        UbicacionLugar = storeLugar(query)
        context.Response.Write(String.Format("{{total:{1},'BarrioZona':{0}}}", JSON.Serialize(UbicacionLugar.Data), UbicacionLugar.TotalRecords))
    End Sub

    ReadOnly Property IsReusable() As Boolean Implements IHttpHandler.IsReusable
        Get
            Return False
        End Get
    End Property

    Public Function storeLugar(ByVal Cond As String) As Paging(Of Lugares)
        Dim listLugares As New List(Of Lugares)
        Dim sql As String = "select a.Id, a.Descripcion,  '('+ b.descripcion +')' as dpto from saih_lugar as a " & _
                            "inner join saih_lugar as b on a.id_lugar = b.id " & _
                           " where a.descripcion like '%" & Cond & "%' and a.tipo = 3" & _
                           " order by a.descripcion"

        Dim reader As SqlClient.SqlDataReader = Datos.deTablas.ejecutaSelectDr(sql)
        While (reader.Read)
            listLugares.Add(New Lugares(reader(0), reader(1), reader(2)))
        End While
        Return New Paging(Of Lugares)(listLugares, listLugares.Count)
    End Function

    Class Lugares
        Private _Id_ciudad As Integer
        Private _Descripcion As String
        Private _DescTotal As String
        Private _Desc_Dpto As String
        Public Property Id_Ciudad As Integer
            Get
                Return _Id_Ciudad
            End Get
            Set(ByVal value As Integer)
                _Id_ciudad = value
            End Set
        End Property
        Public Property Descripcion As String
            Get
                Return _Descripcion
            End Get
            Set(ByVal value As String)
                _Descripcion = value
            End Set
        End Property
        Public Property Desc_Dpto As String
            Get
                Return _Desc_Dpto
            End Get
            Set(ByVal value As String)
                _Desc_Dpto = value
            End Set
        End Property
        Public Property DescTotal As String
            Get
                Return _DescTotal
            End Get
            Set(ByVal value As String)
                _DescTotal = value
            End Set
        End Property

        Public Sub New(ByVal id As Integer, ByVal Descripcion As String, ByVal Desc_Dpto As String)
            Me.Id_ciudad = id
            Me.desc_Dpto = Desc_Dpto
            Me.Descripcion = Descripcion
            Me.DescTotal = Me.Descripcion & " " & Me.desc_Dpto
        End Sub

    End Class

End Class
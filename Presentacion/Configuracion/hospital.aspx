﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="hospital.aspx.vb" Inherits="Presentacion.hospital" %>

<%@ Register Assembly="Ext.Net" Namespace="Ext.Net" TagPrefix="ext" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <script type="text/javascript" src="./../public/js/jquery-1.9.0.js"></script>
    <script type="text/javascript" src="./../public/js/jquery.timer.js"></script>
    <script type="text/javascript" src="./../public/js/app.js"></script>
    <title></title>

</head>
<body>    
    <form id="form1" runat="server">
    <ext:ResourceManager ID="ResourceManager1" runat="server" />
    <ext:Panel ID="Panel3" runat="server" Frame="true" PaddingSummary="5px 5px 0" AutoWidth="true" Height="500" ButtonAlign="Center" Style="text-align: left" Title="Información Institución">
        <Items>
            <ext:FormPanel 
                    ID="StatusForm" 
                    runat="server"
                    LabelWidth="75"
                    ButtonAlign="Right"
                    Border="false"
                    PaddingSummary="10px 10px 10px">
                    <Defaults>                        
                        <ext:Parameter Name="AllowBlank" Value="false" Mode="Raw" />
                        <ext:Parameter Name="SelectOnFocus" Value="true" Mode="Raw" />
                        <ext:Parameter Name="MsgTarget" Value="side" />
                        <ext:Parameter Name="Anchor" Value="99%" />
                    </Defaults>
                    <Items>
                        <ext:FieldSet ID="FieldSet3" runat="server" Title="Campos Generales" Padding="10" Height="150" >
                            <Items>
                                <ext:Container ID="Container10" runat="server" Layout="ColumnLayout" Height="30" >
                                    <Items>
                                        <ext:Container ID="Container1" runat="server" LabelAlign="Right" Layout="FormLayout" ColumnWidth=".33" >
                                            <Items>
                                                <ext:TextField runat="server" ID="txtCodEnt" FieldLabel="Cod.Entidad" EmptyText="Codigo" AllowBlank="false" />
                                            </Items>
                                        </ext:Container>
                                        <ext:Container ID="Container8" runat="server" LabelAlign="Right" Layout="FormLayout" ColumnWidth=".33" >
                                            <Items>
                                                <ext:ComboBox     
                                                    ID="cbTipDoc"                                                                                   
                                                    runat="server" 
                                                    Shadow="Drop" 
                                                    Mode="Local" 
                                                    TriggerAction="All" 
                                                    ForceSelection="true"
                                                    DisplayField="nombre"
                                                    ValueField="codigo"
                                                    FieldLabel="Tipo Doc."
                                                    EmptyText="Seleccione Tipo de Documento..."
                                                    AnchorHorizontal="99%"
                                                    AllowBlank="false"
                                                    >
                                                    <Store>
                                                        <ext:Store ID="stTipoDocumento" runat="server" AutoLoad="true">
                                                            <Reader>
                                                                <ext:JsonReader IDProperty="codigo">
                                                                    <Fields>
                                                                        <ext:RecordField Name="codigo" />
                                                                        <ext:RecordField Name="nombre" />
                                                                    </Fields>
                                                                </ext:JsonReader>
                                                            </Reader>            
                                                        </ext:Store>    
                                                    </Store>    
                                                </ext:ComboBox> 
                                            </Items>
                                        </ext:Container>
                                        <ext:Container ID="Container2" runat="server" LabelAlign="Right" Layout="FormLayout" ColumnWidth=".33" >
                                            <Items>
                                                <ext:TextField runat="server" ID="txtNroDoc" FieldLabel="Documento" EmptyText="Numero de Documento" AllowBlank="false"  />
                                            </Items>
                                        </ext:Container>
                                    </Items>
                                </ext:Container>
                                <ext:Container ID="Container3" runat="server" Layout="ColumnLayout" Height="30">
                                    <Items>
                                        <ext:Container ID="Container4" runat="server" LabelAlign="Right" Layout="FormLayout" ColumnWidth=".33" >
                                            <Items>
                                                <ext:TextField runat="server" ID="txtNombre" FieldLabel="Nombre" EmptyText="Nombre" AnchorHorizontal="99%" AllowBlank="false"/>
                                            </Items>
                                        </ext:Container>
                                        <ext:Container ID="Container5" runat="server" LabelAlign="Right" Layout="FormLayout" ColumnWidth=".33" >
                                            <Items>
                                                <ext:TextField runat="server" ID="txtDireccion" FieldLabel="Dirección" EmptyText="Dirección" AnchorHorizontal="99%" />
                                            </Items>
                                        </ext:Container>
                                        <ext:Container ID="Container6" runat="server" LabelAlign="Right" Layout="FormLayout" ColumnWidth=".33" >
                                            <Items>
                                                <ext:TextField runat="server" ID="txtTelefono" FieldLabel="Telefono" EmptyText="Telefono" AllowBlank="false" AnchorHorizontal="80%" MinLength="3" />
                                            </Items>
                                        </ext:Container>
                                    </Items>
                                </ext:Container>
                                <ext:Container ID="Container24" runat="server" Layout="ColumnLayout" Height="30" >
                                    <Items>
                                        <ext:Container ID="Container25" runat="server" LabelAlign="Right" Layout="FormLayout" ColumnWidth=".33" >
                                            <Items>
                                                <ext:Checkbox runat="server" ID="ckindtriage" BoxLabel="Maneja Triege" />
                                            </Items>
                                        </ext:Container>
                                        <ext:Container ID="Container26" runat="server" LabelAlign="Right" Layout="FormLayout" ColumnWidth=".33" >
                                            <Items>
                                                <ext:Checkbox runat="server" ID="ckindimpformula" BoxLabel="Man. Impresión Formula" />
                                            </Items>
                                        </ext:Container>
                                        <ext:Container ID="Container27" runat="server" LabelAlign="Right" Layout="FormLayout" ColumnWidth=".33" >
                                            <Items>
                                                <ext:Checkbox runat="server" ID="ckindfechaglosa" BoxLabel="Man. Fecha Glosa" />
                                            </Items>
                                        </ext:Container>
                                    </Items>
                                </ext:Container>
                                <ext:Container ID="Container21" runat="server" Layout="ColumnLayout" Height="30" >
                                    <Items>
                                        <ext:Container ID="Container22" runat="server" LabelAlign="Right" Layout="FormLayout" ColumnWidth=".5" >
                                            <Items>
                                                <ext:TextField runat="server" ID="txtGerente" FieldLabel="Gerente" EmptyText="Gerente" AnchorHorizontal="99%" AllowBlank="false" />
                                            </Items>
                                        </ext:Container>
                                        <ext:Container ID="Container23" runat="server" LabelAlign="Right" Layout="FormLayout" ColumnWidth=".5" >
                                            <Items>
                                                <ext:TextField runat="server" ID="txtSubGerente" FieldLabel="Sub Gerente" EmptyText="Sub Gerente" AnchorHorizontal="99%" />
                                            </Items>
                                        </ext:Container>
                                    </Items>
                                </ext:Container>
                            </Items>
                        </ext:FieldSet>
                        <ext:FieldSet ID="FieldSet2" runat="server" Title="Logo Hospital" Padding="10" Height="60">
                            <Items>
                                <ext:Container ID="Container13" runat="server" Layout="ColumnLayout" Height="30" >
                                    <Items>
                                        <ext:Container ID="Container14" runat="server" LabelAlign="Right" Layout="FormLayout" ColumnWidth=".5" >
                                            <Items>
                                                <ext:FileUploadField ID="Archivo" runat="server" Width="400" Icon="Attach" Hidden="false" FieldLabel="Logo" />
                                            </Items>
                                        </ext:Container>
                                        <ext:Container ID="Container28" runat="server" LabelAlign="Right" Layout="FormLayout" ColumnWidth=".5" >
                                            <Items>
                                                <ext:Button runat="server" ID="btnCarga" Text="Carga Archivo" Icon="CdGo" >
                                                    <DirectEvents>
                                                        <Click OnEvent="btnCarga_DirectClick">
                                                            <EventMask ShowMask="true" />
                                                        </Click>
                                                    </DirectEvents>
                                                </ext:Button>
                                            </Items>
                                        </ext:Container>
                                    </Items>
                                </ext:Container>
                            </Items>
                        </ext:FieldSet>
                        <ext:FieldSet ID="FieldSet1" runat="server" Title="Campos Configuración Dian" Padding="10" Height="120">
                            <Items>
                                <ext:Container ID="Container7" runat="server" Layout="ColumnLayout" Height="30" >
                                    <Items>                                        
                                        <ext:Container ID="Container9" runat="server" LabelAlign="Right" Layout="FormLayout" ColumnWidth=".6" >
                                            <Items>
                                                <ext:TextField runat="server" ID="txtResDian" FieldLabel="Resolución" EmptyText="Resolución Dian" AnchorHorizontal="99%" />
                                            </Items>
                                        </ext:Container>
                                        <ext:Container ID="Container11" runat="server" LabelAlign="Right" Layout="FormLayout" ColumnWidth=".4" >
                                            <Items>
                                                <ext:DateField runat="server" ID="txtFechaResolucion" FieldLabel="Fecha Resolución" />
                                            </Items>
                                        </ext:Container>
                                    </Items>                        
                                </ext:Container>
                                <ext:Container ID="Container12" runat="server" Layout="ColumnLayout" Height="30" >
                                    <Items>                                        
                                        <ext:Container ID="Container15" runat="server" LabelAlign="Right" Layout="FormLayout" ColumnWidth=".3" >
                                            <Items>
                                                <ext:NumberField runat="server" ID="txtNumInicial" FieldLabel="N.Inicial" />
                                            </Items>
                                        </ext:Container>
                                        <ext:Container ID="Container16" runat="server" LabelAlign="Right" Layout="FormLayout" ColumnWidth=".3" >
                                            <Items>
                                                <ext:NumberField runat="server" ID="txtNumFinal" FieldLabel="N.Final" />
                                            </Items>
                                        </ext:Container>
                                        <ext:Container ID="Container17" runat="server" LabelAlign="Right" Layout="FormLayout" ColumnWidth=".4" >
                                            <Items>
                                                <ext:NumberField runat="server" ID="txtNumFactura" FieldLabel="N.Factura" />
                                            </Items>
                                        </ext:Container>
                                    </Items>
                                </ext:Container>
                                <ext:Container ID="Container18" runat="server" Layout="ColumnLayout" Height="30" >
                                    <Items>                                        
                                        <ext:Container ID="Container19" runat="server" LabelAlign="Right" Layout="FormLayout" ColumnWidth=".3" >
                                            <Items>
                                                <ext:ComboBox     
                                                    ID="cbRegimen"                                                                                   
                                                    runat="server" 
                                                    Shadow="Drop" 
                                                    Mode="Local" 
                                                    TriggerAction="All" 
                                                    ForceSelection="true"
                                                    DisplayField="nombre"
                                                    ValueField="codigo"
                                                    FieldLabel="Regimen."
                                                    EmptyText="Seleccione Regimen..."
                                                    AnchorHorizontal="99%"
                                                    >
                                                    <Store>
                                                        <ext:Store ID="stRegimen" runat="server" AutoLoad="true">
                                                            <Reader>
                                                                <ext:JsonReader IDProperty="codigo">
                                                                    <Fields>
                                                                        <ext:RecordField Name="codigo" />
                                                                        <ext:RecordField Name="nombre" />
                                                                    </Fields>
                                                                </ext:JsonReader>
                                                            </Reader>            
                                                        </ext:Store>    
                                                    </Store>    
                                                </ext:ComboBox>
                                            </Items>
                                        </ext:Container>
                                        <ext:Container ID="Container20" runat="server" LabelAlign="Right" Layout="FormLayout" ColumnWidth=".3" >
                                            <Items>
                                                <ext:TextField runat="server" ID="txtPrefijo" FieldLabel="Prefijo" EmptyText="Prefijo" />
                                            </Items>
                                        </ext:Container>
                                    </Items>
                                </ext:Container>                                
                            </Items>
                        </ext:FieldSet>
                 </Items>
                 <Buttons>
                    <ext:Button ID="btnGuardar" runat="server" Text="Guardar" Icon="Disk">
                        <DirectEvents>
                            <Click 
                                OnEvent="btnGuardar_DirectClick" 
                                Before="return obligatorios('StatusForm');">
                                <EventMask 
                                    ShowMask="true" 
                                    MinDelay="1000" 
                                    Target="CustomTarget" 
                                    CustomTarget="={#{StatusForm}.getEl()}" 
                                    />
                            </Click>
                        </DirectEvents>
                    </ext:Button>
                    <ext:Button runat="server" ID="btnCerrarTab" Text="Cerrar" />
                </Buttons>
            </ext:FormPanel>            
        </Items>        
    </ext:Panel>
                        
    </form>
</body>
</html>

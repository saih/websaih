﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="Empresa.aspx.vb" Inherits="Presentacion.Empresa" %>

<%@ Register Assembly="Ext.Net" Namespace="Ext.Net" TagPrefix="ext" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        <ext:ResourceManager ID="ResourceManager1" runat="server" />
        <ext:Store 
            ID="stEmp" 
            runat="server" 
            AutoLoad="true">
            <Reader>
                <ext:JsonReader IDProperty="codigo">
                    <Fields>
                        <ext:RecordField Name="codigo" />
                        <ext:RecordField Name="nit" />                        
                        <ext:RecordField Name="razonsocial" />
                    </Fields>
                </ext:JsonReader>
            </Reader>
        </ext:Store>


        <ext:Panel ID="Panel3" runat="server" Frame="true" PaddingSummary="5px 5px 0" AutoWidth="true" Height="650" ButtonAlign="Center" Style="text-align: left" Title="Empresas">
            <Items>
                <ext:FieldSet ID="FieldSet3" runat="server" Title="Campos de Busqueda" Padding="10" Height="60">
                    <Items>
                        <ext:Container ID="Container10" runat="server" Layout="ColumnLayout" Height="30">
                            <Items>
                                <ext:Container ID="Container8" runat="server" LabelAlign="Right" Layout="FormLayout" ColumnWidth=".25">
                                    <Items>
                                        <ext:TextField runat="server" id="txtCodigoBsq" FieldLabel="Codigo" />
                                    </Items>    
                                </ext:Container>
                                <ext:Container ID="Container2" runat="server" LabelAlign="Right" Layout="FormLayout" ColumnWidth=".25">
                                    <Items>
                                         <ext:TextField runat="server" id="txtIdentificacionBsq" FieldLabel="Identificación" />                                       
                                    </Items>
                                </ext:Container>
                                <ext:Container ID="Container4" runat="server" LabelAlign="Right" Layout="FormLayout" ColumnWidth=".3">
                                    <Items>
                                         <ext:TextField runat="server" id="txtRazonSocialBsq" FieldLabel="Nombres" />
                                    </Items>
                                </ext:Container>
                                <ext:Container ID="Container5" runat="server" LabelAlign="Right" Layout="FormLayout" ColumnWidth=".2">
                                    <Items>
                                        <ext:Button runat="server" ID="btnBuscar" Text="Buscar" Icon="ApplicationGo" >
                                            <DirectEvents>
                                                <Click OnEvent="btnBuscar_DirectClick" Before="">
                                                    <EventMask ShowMask="true" />
                                                </Click>
                                            </DirectEvents>
                                        </ext:Button>
                                    </Items>
                                </ext:Container>
                            </Items>
                        </ext:Container>                        
                    </Items>
                </ext:FieldSet>
                <ext:GridPanel
                    ID="grdEmp"
                    runat="server"                                            
                    Margins="0 0 5 5"
                    Icon="TransmitGo"
                    Frame="true"
                    Height="400"
                    StoreId="stEmp"
                    Title="Empresas"
                    >
                    <ColumnModel ID="ColumnModel3" runat="server">
                        <Columns>                        
                            <ext:Column Header="Código" DataIndex="codigo" />                        
                            <ext:Column Header="Nit" DataIndex="nit" />                        
                            <ext:Column Header="Razon Social" DataIndex="razonsocial" Width="380"/>
                        </Columns>                                
                    </ColumnModel>
                    <SelectionModel>
                        <ext:RowSelectionModel ID="RowSelectionModel1" runat="server" SingleSelect="true" >
                            <DirectEvents>
                                <RowSelect OnEvent="rowSelect" Buffer="100">
                                    <ExtraParams>                                    
                                        <ext:Parameter Name="ID" Value="this.getSelected().id" Mode="Raw" />
                                    </ExtraParams>
                                </RowSelect>
                            </DirectEvents>
                        </ext:RowSelectionModel>
                    </SelectionModel>
                    <LoadMask ShowMask="true" Msg="Loading...." />
                    <BottomBar>
                        <ext:StatusBar runat="server" ID="StatusBar2">
                            <Items>                                                        
                                <ext:Button runat="server" Text="Nuevo Empresa" ID="btnNuevaEmpresa" Icon="Add" />
                                <ext:Button runat="server" Text="Editar Empresa" ID="btnEditaEmpresa" Icon="ApplicationEdit" />
                                <ext:Button runat="server" Text="Eliminar Empresa" ID="btnEliminaEmpresa" Icon="ApplicationDelete" />
                            </Items>
                        </ext:StatusBar>
                    </BottomBar>
                </ext:GridPanel>
            </Items>
        </ext:Panel>


        <ext:Store 
            ID="stEmc" 
            runat="server" 
            AutoLoad="true">
            <Reader>
                <ext:JsonReader>
                    <Fields>
                        <ext:RecordField Name="codemp" />
                        <ext:RecordField Name="num_contrato" />                        
                        <ext:RecordField Name="nombre" />
						<ext:RecordField Name="estado" />
						<ext:RecordField Name="fecha" />
						<ext:RecordField Name="fecha_inicial" />
						<ext:RecordField Name="fecha_final" />
						<ext:RecordField Name="monto" />						
                    </Fields>
                </ext:JsonReader>
            </Reader>
        </ext:Store>

        <ext:Window 
            ID="vntEmp"
            runat="server"
            Icon="ApplicationFormEdit"
            Width="750"
            Height="580"
            Hidden="true" 
            Modal="true"            
            Title="Contratos Con Empresas"
            Constrain="true">
            <Items>
                <ext:Panel ID="Panel1" 
                runat="server" 
                Title=""
                AnchorHorizontal="100%"
                Height="550"
                Layout="Fit">
                <Items>
                    <ext:FormPanel 
                        ID="StatusForm" 
                        runat="server"
                        LabelWidth="75"
                        ButtonAlign="Right"
                        Border="false"
                        PaddingSummary="10px 10px 10px">
                        <Defaults>                        
                            <ext:Parameter Name="AllowBlank" Value="false" Mode="Raw" />
                            <ext:Parameter Name="SelectOnFocus" Value="true" Mode="Raw" />
                            <ext:Parameter Name="MsgTarget" Value="side" />
                        </Defaults>
                        <Items>                        
                            <ext:Container ID="Container1" runat="server" Layout="ColumnLayout" Height="30" >
                                <Items>
                                    <ext:Container ID="Container6" runat="server" LabelAlign="Right" Layout="FormLayout" ColumnWidth=".4" >
                                        <Items>
                                            <ext:Label runat="server" ID="lblEmpresas" Text="Empresas y Convenio" />
                                        </Items>
                                    </ext:Container>
                                    <ext:Container ID="Container3" runat="server" LabelAlign="Right" Layout="FormLayout" ColumnWidth=".6" >
                                        <Items>
                                            <ext:ComboBox     
	                                            ID="cbEntidadDeSalud"                                                                              
	                                            runat="server" 
	                                            Shadow="Drop" 
	                                            Mode="Local" 
	                                            TriggerAction="All" 
	                                            ForceSelection="true"
	                                            DisplayField="nombre"
	                                            ValueField="codigo"	
	                                            EmptyText="Seleccione Entidad de Salud"
	                                            AnchorHorizontal="99%"
	                                            FieldLabel="Entidad"
	                                            >
	                                            <Store>
		                                            <ext:Store ID="stEntidadDeSalud" runat="server" AutoLoad="true">
			                                            <Reader>
				                                            <ext:JsonReader IDProperty="codigo">
					                                            <Fields>
						                                            <ext:RecordField Name="codigo" />
						                                            <ext:RecordField Name="nombre" />
					                                            </Fields>
				                                            </ext:JsonReader>
			                                            </Reader>            
		                                            </ext:Store>    
	                                            </Store>    
                                            </ext:ComboBox>
                                        </Items>
                                    </ext:Container>
                                </Items>
                            </ext:Container>
                            <ext:Container ID="Container7" runat="server" Layout="ColumnLayout" Height="30" >
                                <Items>
                                    <ext:Container ID="Container9" runat="server" LabelAlign="Right" Layout="FormLayout" ColumnWidth=".4" >
                                        <Items>
                                            <ext:TextField runat="server" ID="txtCodigo" FieldLabel="Código" AllowBlank="false" MinLength="3" MaxLength="10" AnchorHorizontal="90%" DataIndex="codigo" />
                                        </Items>
                                    </ext:Container>
                                    <ext:Container ID="Container11" runat="server" LabelAlign="Right" Layout="FormLayout" ColumnWidth=".4" >
                                        <Items>
                                            <ext:TextField runat="server" ID="txtNit" FieldLabel="Nit" AllowBlank="false" MinLength="5" MaxLength="15" AnchorHorizontal="60%" DataIndex="nit" />
                                        </Items>
                                    </ext:Container>
                                </Items>
                            </ext:Container>                                    
                            <ext:Container ID="Container12" runat="server" Layout="ColumnLayout" Height="30" >
                                <Items>
                                    <ext:Container ID="Container13" runat="server" LabelAlign="Right" Layout="FormLayout" ColumnWidth=".6" >
                                        <Items>
                                            <ext:TextField runat="server" ID="txtRazonSocial" FieldLabel="RazonSocial" AllowBlank="false" MaxLength="200" AnchorHorizontal="80%" DataIndex="razonsocial" />
                                        </Items>
                                    </ext:Container>
                                </Items>
                            </ext:Container>
                            <ext:Container ID="Container14" runat="server" Layout="ColumnLayout" Height="30" >
                                <Items>
                                    <ext:Container ID="Container15" runat="server" LabelAlign="Right" Layout="FormLayout" ColumnWidth=".6" >
                                        <Items>
                                            <ext:TextField runat="server" ID="txtDireccion" FieldLabel="Dirección" AllowBlank="false" MaxLength="50" AnchorHorizontal="80%" DataIndex="direccion" />
                                        </Items>
                                    </ext:Container>
                                    <ext:Container ID="Container16" runat="server" LabelAlign="Right" Layout="FormLayout" ColumnWidth=".4" >
                                        <Items>
                                            <ext:TriggerField 
                                                ID="txtCodContable" 
                                                runat="server"                                                 
                                                EmptyText="Codigo Contable"
                                                FieldLabel="Cod.Contable"
                                                AnchorHorizontal="80%"   
                                                DataIndex="cod_contable" 
                                                >
                                                <Triggers>
                                                    <ext:FieldTrigger Icon="Search" />
                                                </Triggers>
                                                <Listeners>
                                                    <TriggerClick Handler="Ext.Msg.alert('Message', 'You Clicked the Trigger!');" />
                                                </Listeners>
                                            </ext:TriggerField>
                                        </Items>
                                    </ext:Container>
                                </Items>
                            </ext:Container>
                            <ext:Container ID="Container17" runat="server" Layout="ColumnLayout" Height="30" >
                                <Items>
                                    <ext:Container ID="Container18" runat="server" LabelAlign="Right" Layout="FormLayout" ColumnWidth=".4" >
                                        <Items>
                                            <ext:TextField runat="server" ID="txtTelefono" FieldLabel="Telefono" AnchorHorizontal="99%" AllowBlank="false" MinLength="5" MaxLength="20" DataIndex="telefono" />
                                        </Items>
                                    </ext:Container>
                                    <ext:Container ID="Container19" runat="server" LabelAlign="Right" Layout="FormLayout" ColumnWidth=".4" >
                                        <Items>
                                            <ext:TextField runat="server" ID="txtFax" FieldLabel="Fax" AnchorHorizontal="99%" DataIndex="fax" />
                                        </Items>
                                    </ext:Container>
                                </Items>
                            </ext:Container>
                            <ext:Container ID="Container20" runat="server" Layout="ColumnLayout" Height="30" >
                                <Items>
                                    <ext:Container ID="Container21" runat="server" LabelAlign="Right" Layout="FormLayout" ColumnWidth=".6" >
                                        <Items>
                                            <ext:TextField runat="server" ID="txtContacto" FieldLabel="Contacto" AnchorHorizontal="99%" DataIndex="contacto" />
                                        </Items>
                                    </ext:Container>
                                    <ext:Container ID="Container22" runat="server" LabelAlign="Right" Layout="FormLayout" ColumnWidth=".4" >
                                        <Items>
                                            <ext:ComboBox ID="cbTipoFact" runat="server"  FieldLabel="Tipo Facturación" AllowBlank="false" AnchorHorizontal="95%" DataIndex="tipo_fact">
                                                <Items>
                                                    <ext:ListItem Text="Evento" Value="e" />
                                                    <ext:ListItem Text="Capitado" Value="c" />
                                                    <ext:ListItem Text="Por Orden" Value="o" />
                                                </Items>
                                            </ext:ComboBox>
                                        </Items>
                                    </ext:Container>
                                </Items>
                            </ext:Container>
                            <ext:Container ID="Container29" runat="server" Layout="ColumnLayout" Height="20" >
                                <Items>
                                    <ext:Container ID="Container30" runat="server" LabelAlign="Right" Layout="FormLayout" ColumnWidth=".99" >
                                        <Items>
                                            <ext:Label runat="server" ID="Label1" Text="Empresas a Facturar Procedimientos no Cubiertos" />
                                        </Items>
                                    </ext:Container>
                                </Items>
                            </ext:Container>
                            <ext:Container ID="Container23" runat="server" Layout="ColumnLayout" Height="30" >
                                <Items>
                                    <ext:Container ID="Container24" runat="server" LabelAlign="Right" Layout="FormLayout" ColumnWidth=".99" >
                                        <Items>
                                            <ext:ComboBox     
	                                            ID="cbEmpFctProc"                                                                              
	                                            runat="server" 
	                                            Shadow="Drop" 
	                                            Mode="Local" 
	                                            TriggerAction="All" 
	                                            ForceSelection="true"
	                                            DisplayField="razonsocial"
	                                            ValueField="codigo"	
	                                            EmptyText="Empresa a Facturar"
	                                            AnchorHorizontal="99%"
	                                            FieldLabel=""
                                                DataIndex="cod_empFctProc"
	                                            >
	                                            <Store>
		                                            <ext:Store ID="stEmpFctProc" runat="server" AutoLoad="true">
			                                            <Reader>
				                                            <ext:JsonReader IDProperty="codigo">
					                                            <Fields>
						                                            <ext:RecordField Name="codigo" />
						                                            <ext:RecordField Name="razonsocial" />
					                                            </Fields>
				                                            </ext:JsonReader>
			                                            </Reader>            
		                                            </ext:Store>    
	                                            </Store>    
                                            </ext:ComboBox>
                                        </Items>
                                    </ext:Container>
                                </Items>
                            </ext:Container>
                            <ext:Container ID="Container27" runat="server" Layout="ColumnLayout" Height="20" >
                                <Items>
                                    <ext:Container ID="Container28" runat="server" LabelAlign="Right" Layout="FormLayout" ColumnWidth=".99" >
                                        <Items>
                                            <ext:Label runat="server" ID="lblEmpSol" Text="Empresas Solidaria con las Cuentas" />
                                        </Items>
                                    </ext:Container>
                                </Items>
                            </ext:Container>
                            <ext:Container ID="Container25" runat="server" Layout="ColumnLayout" Height="30" >
                                <Items>
                                    <ext:Container ID="Container26" runat="server" LabelAlign="Right" Layout="FormLayout" ColumnWidth=".99" >
                                        <Items>
                                            <ext:ComboBox     
	                                            ID="cbEmpSolCuentas"                                                                              
	                                            runat="server" 
	                                            Shadow="Drop" 
	                                            Mode="Local" 
	                                            TriggerAction="All" 
	                                            ForceSelection="true"
	                                            DisplayField="razonsocial"
	                                            ValueField="codigo"	
	                                            EmptyText="Empresa Solidaria"
	                                            AnchorHorizontal="99%"
	                                            FieldLabel=""
                                                DataIndex="cod_empSolCuentas"
	                                            >
	                                            <Store>
		                                            <ext:Store ID="stEmpSolCuentas" runat="server" AutoLoad="true">
			                                            <Reader>
				                                            <ext:JsonReader IDProperty="codigo">
					                                            <Fields>
						                                            <ext:RecordField Name="codigo" />
						                                            <ext:RecordField Name="razonsocial" />
					                                            </Fields>
				                                            </ext:JsonReader>
			                                            </Reader>            
		                                            </ext:Store>    
	                                            </Store>    
                                            </ext:ComboBox>
                                        </Items>
                                    </ext:Container>
                                </Items>
                            </ext:Container>
                            <ext:TabPanel ID="TabEmpresa" runat="server" ActiveTabIndex="0" Border="false" Title="Center" ResizeTabs="true">
                                <Items>
                                    <ext:Panel ID="PContratos" runat="server" Closable="False" Title="Contratos" Hidden="False" >
                                        <Items>                                            
                                            <ext:GridPanel
                                                ID="grdEmc"
                                                runat="server"                                            
                                                Margins="0 0 5 5"                                                
                                                Frame="true"
                                                Height="170"
                                                StoreId="stEmc"
                                                Title="Contratos"
                                                >
                                                <ColumnModel ID="ColumnModel1" runat="server">
                                                    <Columns>                        
                                                        <ext:Column Header="codemp" DataIndex="codemp" Hidden="true" />                        
                                                        <ext:Column Header="Contrato" DataIndex="num_contrato" />
                                                        <ext:Column Header="Nombre" DataIndex="nombre" Width="280"/>
                                                        <ext:Column Header="Estado" DataIndex="estado" />
                                                        <ext:Column Header="Fecha" DataIndex="fecha" />
                                                        <ext:Column Header="Inicio" DataIndex="fecha_inicial" />
                                                        <ext:Column Header="Fin" DataIndex="fecha_final" />
                                                        <ext:Column Header="Monto" DataIndex="monto" />
                                                    </Columns>                                
                                                </ColumnModel>
                                                <SelectionModel>
                                                    <ext:RowSelectionModel ID="RowSelectionModel2" runat="server" SingleSelect="true" >
                                                        <DirectEvents>
                                                            <RowSelect OnEvent="rowSelectEmc" Buffer="100">
                                                                <ExtraParams>                                    
                                                                    <ext:Parameter Name="num_contrato" Value="this.getSelected().get('num_contrato')" Mode="Raw" />
                                                                </ExtraParams>
                                                            </RowSelect>
                                                        </DirectEvents>
                                                    </ext:RowSelectionModel>
                                                </SelectionModel>
                                                <LoadMask ShowMask="true" Msg="Loading...." />                                                
                                                <BottomBar>
                                                    <ext:StatusBar runat="server" ID="StatusBar1">
                                                        <Items>                                                        
                                                            <ext:Button runat="server" Text="Nuevo Contrato" ID="btnNuevoContrato" Icon="Add" />
                                                            <ext:Button runat="server" Text="Editar Contrato" ID="btnEditaContrato" Icon="ApplicationEdit" />
                                                            <ext:Button runat="server" Text="Eliminar Contrato" ID="btnEliminaContrato" Icon="ApplicationDelete" />
                                                        </Items>
                                                    </ext:StatusBar>
                                                </BottomBar>
                                            </ext:GridPanel>
                                        </Items>
                                    </ext:Panel>
                                </Items>
                            </ext:TabPanel>
                        </Items>
                        <Buttons>
                            <ext:Button ID="btnGuardaEmp" runat="server" Text="Guardar" Icon="Disk">
                                <DirectEvents>
                                    <Click 
                                        OnEvent="btnGuardaEmp_DirectClick" 
                                        Before="var valid= #{StatusForm}.getForm().isValid(); if (valid) {#{FormStatusBar}.showBusy('Guardando Formulario...');} return valid;">
                                        <EventMask 
                                            ShowMask="true" 
                                            MinDelay="1000" 
                                            Target="CustomTarget" 
                                            CustomTarget="={#{StatusForm}.getEl()}" 
                                            />
                                    </Click>
                                </DirectEvents>
                            </ext:Button>
                            <ext:Button runat="server" ID="btnCerrarEmp" Text="Cerrar" />
                        </Buttons>
                    </ext:FormPanel>
                </Items>
                <BottomBar>
                    <ext:StatusBar ID="FormStatusBar" runat="server" DefaultText="Ready">
                        <Plugins>
                            <ext:ValidationStatus ID="ValidationStatus1" 
                                runat="server" 
                                FormPanelID="StatusForm" 
                                ValidIcon="Accept" 
                                ErrorIcon="Exclamation" 
                                ShowText="El Formulario tiene Errores (Click para ver Detalles)"
                                HideText="Click para Ocultar Errores"
                                />
                        </Plugins>
                    </ext:StatusBar>
                </BottomBar>
            </ext:Panel>
            </Items>
        </ext:Window>

        <ext:Window 
            ID="vntEmc"
            runat="server"
            Icon="ApplicationFormEdit"
            Width="580"
            Height="380"
            Hidden="true" 
            Modal="true"            
            Title="Contratos"
            Constrain="true">
            <Items>
                <ext:Panel ID="Panel2" 
                    runat="server" 
                    Title=""
                    AnchorHorizontal="100%"
                    Height="350"
                    Layout="Fit">
                    <Items>
                        <ext:FormPanel 
                            ID="StatusFormEmc" 
                            runat="server"
                            LabelWidth="75"
                            ButtonAlign="Right"
                            Border="false"
                            PaddingSummary="10px 10px 10px">
                            <Defaults>                        
                                <ext:Parameter Name="AllowBlank" Value="false" Mode="Raw" />
                                <ext:Parameter Name="SelectOnFocus" Value="true" Mode="Raw" />
                                <ext:Parameter Name="MsgTarget" Value="side" />
                            </Defaults>
                            <Items>                        
                                <ext:Container ID="Container31" runat="server" Layout="ColumnLayout" Height="30" >
                                    <Items>
                                        <ext:Container ID="Container32" runat="server" LabelAlign="Right" Layout="FormLayout" ColumnWidth=".5" >
                                            <Items>
                                                <ext:TextField runat="server" ID="txtNumContrato" FieldLabel="Num.Contrato" DataIndex="num_contrato" AllowBlank="false" MinLength="3" MaxLength="20" />
                                            </Items>
                                        </ext:Container>
                                        <ext:Container ID="Container34" runat="server" LabelAlign="Right" Layout="FormLayout" ColumnWidth=".5" >
                                            <Items>
                                                <ext:ComboBox ID="cbEstadoEmc" runat="server"  FieldLabel="Estado" AllowBlank="false" AnchorHorizontal="95%" DataIndex="estado">
                                                    <Items>
                                                        <ext:ListItem Text="Activo" Value="1" />
                                                        <ext:ListItem Text="Inactivo" Value="0" />                                                                        
                                                    </Items>
                                                </ext:ComboBox>
                                            </Items>
                                        </ext:Container>
                                    </Items>
                                </ext:Container>
                                <ext:Container ID="Container35" runat="server" Layout="ColumnLayout" Height="50" >
                                    <Items>
                                        <ext:Container ID="Container36" runat="server" LabelAlign="Right" Layout="FormLayout" ColumnWidth=".25" >
                                            <Items>
                                                <ext:TextArea runat="server" ID="txtNombre" FieldLabel="Nombre" DataIndex="nombre" AnchorHorizontal="100%" AnchorVertical="90%" AllowBlank="false" MaxLength="200" MinLength="10" />
                                            </Items>
                                        </ext:Container>
                                    </Items>                            
                                </ext:Container>
                                <ext:Container ID="Container37" runat="server" Layout="ColumnLayout" Height="30" >
                                    <Items>
                                        <ext:Container ID="Container38" runat="server" LabelAlign="Right" Layout="FormLayout" ColumnWidth=".33" >
                                            <Items>
                                                <ext:DateField runat="server" ID="txtFecha" FieldLabel="Fecha" AllowBlank="false" DataIndex="fecha" AnchorHorizontal="95%" />
                                            </Items>
                                        </ext:Container>
                                        <ext:Container ID="Container39" runat="server" LabelAlign="Right" Layout="FormLayout" ColumnWidth=".33" >
                                            <Items>
                                                <ext:DateField runat="server" ID="txtFechaInicial" FieldLabel="Inicio" AllowBlank="false" DataIndex="fecha_inicial" AnchorHorizontal="95%" />
                                            </Items>
                                        </ext:Container>
                                        <ext:Container ID="Container40" runat="server" LabelAlign="Right" Layout="FormLayout" ColumnWidth=".33" >
                                            <Items>
                                                <ext:DateField runat="server" ID="txtFechaFinal" FieldLabel="Fin" AllowBlank="false" DataIndex="fecha_final" AnchorHorizontal="95%" />
                                            </Items>
                                        </ext:Container>
                                    </Items>
                                </ext:Container>
                                <ext:Container ID="Container41" runat="server" Layout="ColumnLayout" Height="30" >
                                    <Items>
                                        <ext:Container ID="Container42" runat="server" LabelAlign="Right" Layout="FormLayout" ColumnWidth=".5" >
                                            <Items>
                                                <ext:NumberField ID="txtNumAfiliados" runat="server" FieldLabel="Num.Afiliados" DataIndex="num_afiliados" />
                                            </Items>
                                        </ext:Container>
                                        <ext:Container ID="Container43" runat="server" LabelAlign="Right" Layout="FormLayout" ColumnWidth=".5" >
                                            <Items>
                                                <ext:ComboBox ID="cbFormaPago" runat="server"  FieldLabel="Forma Pago" AllowBlank="false" AnchorHorizontal="95%" DataIndex="forma_pago">
                                                    <Items>
                                                        <ext:ListItem Text="Evento" Value="e" />
                                                        <ext:ListItem Text="Capitado" Value="c" />
                                                        <ext:ListItem Text="Por Orden" Value="o" />                                                                        
                                                    </Items>
                                                </ext:ComboBox>
                                            </Items>
                                        </ext:Container>
                                    </Items>
                                </ext:Container>
                                <ext:Container ID="Container44" runat="server" Layout="ColumnLayout" Height="30" >
                                    <Items>
                                        <ext:Container ID="Container45" runat="server" LabelAlign="Right" Layout="FormLayout" ColumnWidth=".5" >
                                            <Items>
                                                <ext:NumberField ID="txtMonto" runat="server" FieldLabel="Monto" DataIndex="monto" AllowBlank="false" />
                                            </Items>
                                        </ext:Container>
                                        <ext:Container ID="Container46" runat="server" LabelAlign="Right" Layout="FormLayout" ColumnWidth=".5" >
                                            <Items>
                                                <ext:NumberField ID="txtValEjecutado" runat="server" FieldLabel="Val.Ejecutado" DataIndex="val_ejecutado" AllowBlank="false" />
                                            </Items>
                                        </ext:Container>
                                    </Items>
                                </ext:Container>
                            </Items>
                            <Buttons>
                            <ext:Button ID="btnGuardaEmc" runat="server" Text="Guardar" Icon="Disk">
                                <DirectEvents>
                                    <Click 
                                        OnEvent="btnGuardaEmc_DirectClick" 
                                        Before="var valid= #{StatusFormEmc}.getForm().isValid(); if (valid) {#{FormStatusBarEmc}.showBusy('Guardando Formulario...');} return valid;">
                                        <EventMask 
                                            ShowMask="true" 
                                            MinDelay="1000" 
                                            Target="CustomTarget" 
                                            CustomTarget="={#{StatusFormEmc}.getEl()}" 
                                            />
                                    </Click>
                                </DirectEvents>
                            </ext:Button>
                            <ext:Button runat="server" ID="btnCerrarEmc" Text="Cerrar" />
                        </Buttons>
                        </ext:FormPanel>
                    </Items>
                    <BottomBar>
                        <ext:StatusBar ID="FormStatusBarEmc" runat="server" DefaultText="Ready">
                            <Plugins>
                                <ext:ValidationStatus ID="ValidationStatus2" 
                                    runat="server" 
                                    FormPanelID="StatusFormEmc" 
                                    ValidIcon="Accept" 
                                    ErrorIcon="Exclamation" 
                                    ShowText="El Formulario tiene Errores (Click para ver Detalles)"
                                    HideText="Click para Ocultar Errores"
                                    />
                            </Plugins>
                        </ext:StatusBar>
                    </BottomBar>
                </ext:Panel>
            </Items>
        </ext:Window>
    </form>
</body>
</html>

﻿Imports Ext.Net

Public Class personalasistencial
    Inherits System.Web.UI.Page

    Protected Property Accion As String
        Get
            Return ViewState("Accion")
        End Get
        Set(value As String)
            ViewState.Add("Accion", value)
        End Set
    End Property

    Protected Property codigo As String
        Get
            Return ViewState("codigo")
        End Get
        Set(value As String)
            ViewState.Add("codigo", value)
        End Set
    End Property

    Protected Property codser As String
        Get
            Return ViewState("codser")
        End Get
        Set(value As String)
            ViewState.Add("codser", value)
        End Set
    End Property

    Protected Property idLugarExp As Integer
        Get
            Return ViewState("idLugarExp")
        End Get
        Set(value As Integer)
            ViewState.Add("idLugarExp", value)
        End Set
    End Property

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If IsPostBack = False Then
            Dim seg As New Seguridad(Me, Session("PERFIL"))
            codigo = "x"
            cargarPas()
        End If
    End Sub

    Protected Sub rowSelect(ByVal sender As Object, ByVal e As DirectEventArgs)
        codigo = e.ExtraParams("ID")
    End Sub

    Private Function cargarPas()
        Dim negPAsistencias As New Negocio.Negociosaih_pas
        Dim sWhere As String = " where codigo <> ''"
        If txtCodigoBsq.Text <> "" Then
            sWhere &= " and codigo = '" & txtCodigoBsq.Text & "'"
        End If

        If txtIdentificacionBsq.Text <> "" Then
            sWhere &= " and identificacion = '" & txtIdentificacionBsq.Text & "'"
        End If

        If txtNombresBsq.Text <> "" Then
            sWhere &= " and nombres like '%" & txtNombresBsq.Text & "%'"
        End If

        stPas.DataSource = negPAsistencias.Obtenersaih_pasbyWhere(sWhere)
        stPas.DataBind()

        Return True
    End Function

    Protected Sub btnBuscar_DirectClick(sender As Object, e As Ext.Net.DirectEventArgs)
        cargarPas()
    End Sub

    Private Sub cbIdLugarExp_DirectSelect(sender As Object, e As Ext.Net.DirectEventArgs) Handles cbIdLugarExp.DirectSelect
        idLugarExp = cbIdLugarExp.Value
    End Sub

    Private Sub btnNuevaPAsistencial_DirectClick(sender As Object, e As Ext.Net.DirectEventArgs) Handles btnNuevaPAsistencial.DirectClick
        Accion = "Insert"
        codigo = "X"
        cargaCb()
        presentacionUtil.limpiarObjetos(vntPas)
        'txtCodigo.Clear()
        'txtIdentificacion.Clear()
        'txtNombres.Clear()
        'txtDireccion.Clear()
        'txtDireccion_off.Clear()
        'txtTelefono.Clear()
        'txtTelefono_off.Clear()
        'cbCodClase.Clear()
        'cbEstado.Clear()
        'cbCodEspecialidad.Clear()
        'cbIdLugarExp.Clear()
        'txtNum_registro_med.Clear()
        'txtNum_citas_dia.Clear()
        'txtDuracion.Clear()
        cargarPAsistencial()
        vntPas.Show()
    End Sub

    Private Sub btnEditaPAsistencial_DirectClick(sender As Object, e As Ext.Net.DirectEventArgs) Handles btnEditaPAsistencial.DirectClick

        If codigo = "x" Then
            Ext.Net.X.Msg.Alert("Información", "Debe seleccionar un Registro").Show()
            Return
        End If

        Accion = "Edit"
        Dim negPAsistencias As New Negocio.Negociosaih_pas
        Dim PAsistencial As New Datos.saih_pas
        PAsistencial = negPAsistencias.Obtenersaih_pasById(codigo)
        cargaCb()
        txtCodigo.Text = PAsistencial.codigo
        txtIdentificacion.Text = PAsistencial.identificacion
        txtNombres.Text = PAsistencial.nombres
        txtDireccion.Text = PAsistencial.direccion
        txtDireccion_off.Text = PAsistencial.direccion_off
        txtTelefono.Text = PAsistencial.telefono
        txtTelefono_off.Text = PAsistencial.telefono_off
        cbCodClase.Value = PAsistencial.cod_clase
        cbEstado.Value = PAsistencial.estado
        cbCodEspecialidad.Value = PAsistencial.cod_especialidad
        cbIdLugarExp.Value = Datos.deTablas.DescripcionLugarCiudad(PAsistencial.id_lugarexp)
        idLugarExp = PAsistencial.id_lugarexp
        txtNum_registro_med.Text = PAsistencial.num_registro_med
        txtNum_citas_dia.Value = PAsistencial.num_citas_dia
        txtDuracion.Value = PAsistencial.duracion
        cargarPAsistencial()
        vntPas.Show()
    End Sub

    Private Function cargaCb()
        Funciones.cargaStoreDominio(stCodClase, Datos.ConstantesUtil.DOMINIO_MEDICO)
        Funciones.cargaStoreDominio(stCodEspecialidad, Datos.ConstantesUtil.DOMINIO_ESPECIALIDADES)
        Dim negSer As New Negocio.Negociosaih_ser
        stTipServicio.DataSource = negSer.Obtenersaih_ser
        stTipServicio.DataBind()
        Return True
    End Function

    <DirectMethod()>
    Public Function eliminarPas()
        Dim negPAsistencias As New Negocio.Negociosaih_pas
        negPAsistencias.Eliminasaih_pas(codigo)
        cargarPas()
        Return True
    End Function

    Private Sub btnEliminaPAsistencial_DirectClick(sender As Object, e As Ext.Net.DirectEventArgs) Handles btnEliminaPAsistencial.DirectClick
        Dim js As String = "Ext.MessageBox.confirm('Confirmación', '¿Esta seguro de eliminar?', " & _
           "function (btn) { " & _
                " if (btn == 'yes') " & _
                "   Ext.net.DirectMethods.eliminarPas(); " & _
                " });"
        Ext.Net.X.Js.AddScript(js)
    End Sub

    Protected Sub btnGuardaPas_DirectClick(sender As Object, e As Ext.Net.DirectEventArgs)
        Dim negPAsistencias As New Negocio.Negociosaih_pas
        If Accion = "Insert" Then
            If negPAsistencias.Altasaih_pas(txtCodigo.Text, txtIdentificacion.Text, idLugarExp, txtNum_registro_med.Text, txtNombres.Text, txtDireccion.Text,
                                            txtTelefono.Text, cbCodClase.Value, cbCodEspecialidad.Value, txtDireccion_off.Text, txtTelefono_off.Text,
                                            txtNum_citas_dia.Value, cbEstado.Value, txtDuracion.Value, "", "", "") Then
                Ext.Net.X.Msg.Alert("Información", "Registro Guardado Correctamente").Show()
                cargarPas()
                vntPas.Hide()
            End If
        Else
            If negPAsistencias.Editasaih_pas(codigo, txtIdentificacion.Text, idLugarExp, txtNum_registro_med.Text, txtNombres.Text, txtDireccion.Text,
                                            txtTelefono.Text, cbCodClase.Value, cbCodEspecialidad.Value, txtDireccion_off.Text, txtTelefono_off.Text,
                                            txtNum_citas_dia.Value, cbEstado.Value, txtDuracion.Value, "", "", "") Then
                Ext.Net.X.Msg.Alert("Información", "Registro Guardado Correctamente").Show()
                cargarPas()
                vntPas.Hide()
            End If
        End If
    End Sub

    Protected Sub rowSelectPSer(ByVal sender As Object, ByVal e As DirectEventArgs)
        codser = e.ExtraParams("codser")
    End Sub


    Private Sub btnSerAsociado_DirectClick(sender As Object, e As Ext.Net.DirectEventArgs) Handles btnSerAsociado.DirectClick
        Dim negPSer As New Negocio.Negociosaih_paser
        If negPSer.Altasaih_paser(codigo, cbTipServicio.Value) Then
            Ext.Net.X.Msg.Alert("Información", "Registro Guardado Correctamente").Show()
            cargarPAsistencial()
        End If
    End Sub

    Private Function cargarPAsistencial()
        Dim spFunc As New Datos.spFunciones
        stPaser.DataSource = spFunc.executaSelect("select a.codpas,a.codser,b.nombre from saih_paser a inner " & _
                             "join saih_ser b on a.codser = b.codser where codpas = '" & codigo & "'")
        stPaser.DataBind()

        Return True
    End Function

    Private Sub btnCerrarPas_DirectClick(sender As Object, e As Ext.Net.DirectEventArgs) Handles btnCerrarPas.DirectClick
        vntPas.Hide()
    End Sub
End Class
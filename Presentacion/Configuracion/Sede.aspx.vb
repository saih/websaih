﻿Imports Ext.Net

Public Class Sede
    Inherits System.Web.UI.Page

    Protected Property Accion As String
        Get
            Return ViewState("Accion")
        End Get
        Set(value As String)
            ViewState.Add("Accion", value)
        End Set
    End Property

    Protected Property codpre As String
        Get
            Return ViewState("codpre")
        End Get
        Set(value As String)
            ViewState.Add("codpre", value)
        End Set
    End Property

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If IsPostBack = False Then
            Dim seg As New Seguridad(Me, Session("PERFIL"))
        End If
    End Sub

    Private Function cargarSede()
        Dim negPrm As New Negocio.Negociosaih_prm
        Dim sWhere As String = " where codpre <> ''"

        If txtCodPreBsq.Text <> "" Then
            sWhere &= " and codpre = '" & txtCodPreBsq.Text & "'"
        End If

        If txtLugarBsq.Text <> "" Then
            sWhere &= " and codpre = '" & txtLugarBsq.Text & "'"
        End If

        stPrm.DataSource = negPrm.Obtenersaih_prmbyWhere(sWhere)
        stPrm.DataBind()

        Return True
    End Function

    Protected Sub rowSelect(ByVal sender As Object, ByVal e As DirectEventArgs)
        codpre = e.ExtraParams("ID")
    End Sub

    Protected Sub btnBuscar_DirectClick(sender As Object, e As Ext.Net.DirectEventArgs)
        cargarSede()
    End Sub

    Protected Sub btnGuardaPrm_DirectClick(sender As Object, e As Ext.Net.DirectEventArgs)
        Dim negPrm As New Negocio.Negociosaih_prm
        Dim prm As New Datos.saih_prm
        Reflexion.valoresForma(prm, vntPrm)
        If Accion = "Insert" Then
            If negPrm.Altasaih_prm(prm) Then
                Ext.Net.X.Msg.Alert("Información", "Registro Guardado Correctamente").Show()
                vntPrm.Hide()
                cargarSede()
            End If
        Else
            If negPrm.Editasaih_prm(prm) Then
                Ext.Net.X.Msg.Alert("Información", "Registro Guardado Correctamente").Show()
                vntPrm.Hide()
                cargarSede()
            End If
        End If
    End Sub

    Private Sub btnNuevaSede_DirectClick(sender As Object, e As Ext.Net.DirectEventArgs) Handles btnNuevaSede.DirectClick
        Accion = "Insert"
        presentacionUtil.limpiarObjetos(vntPrm)
        vntPrm.Show()
    End Sub

    Private Sub btnEditaSede_DirectClick(sender As Object, e As Ext.Net.DirectEventArgs) Handles btnEditaSede.DirectClick
        Accion = "Edit"
        Dim negPrm As New Negocio.Negociosaih_prm
        Dim prm As New Datos.saih_prm
        prm = negPrm.Obtenersaih_prmById(codpre)
        Reflexion.formaValores(prm, vntPrm)
        vntPrm.Show()
    End Sub

    Private Sub btnCerrarPrm_DirectClick(sender As Object, e As Ext.Net.DirectEventArgs) Handles btnCerrarPrm.DirectClick
        vntPrm.Hide()
    End Sub

    <DirectMethod()>
    Public Function eliminarPrm()
        Dim negPrm As New Negocio.Negociosaih_prm
        negPrm.Eliminasaih_prm(codpre)
        cargarSede()
        Return True
    End Function

    Private Sub btnEliminaSede_DirectClick(sender As Object, e As Ext.Net.DirectEventArgs) Handles btnEliminaSede.DirectClick
        Dim js As String = "Ext.MessageBox.confirm('Confirmación', '¿Esta seguro de eliminar?', " & _
           "function (btn) { " & _
                " if (btn == 'yes') " & _
                "   Ext.net.DirectMethods.eliminarPrm(); " & _
                " });"
        Ext.Net.X.Js.AddScript(js)
    End Sub
End Class
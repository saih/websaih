﻿Imports Ext.Net

Public Class Empresa
    Inherits System.Web.UI.Page

#Region "ViewState"
    Protected Property Accion As String
        Get
            Return ViewState("Accion")
        End Get
        Set(value As String)
            ViewState.Add("Accion", value)
        End Set
    End Property

    Protected Property codigo As String
        Get
            Return ViewState("codigo")
        End Get
        Set(value As String)
            ViewState.Add("codigo", value)
        End Set
    End Property

    Protected Property num_contrato As String
        Get
            Return ViewState("num_contrato")
        End Get
        Set(value As String)
            ViewState.Add("num_contrato", value)
        End Set
    End Property


    Protected Property AccionEmc As String
        Get
            Return ViewState("AccionEmc")
        End Get
        Set(value As String)
            ViewState.Add("AccionEmc", value)
        End Set
    End Property

#End Region
    

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If IsPostBack = False Then
            Dim seg As New Seguridad(Me, Session("PERFIL"))
        End If
    End Sub

#Region "Empresas"
    Protected Sub rowSelect(ByVal sender As Object, ByVal e As DirectEventArgs)
        codigo = e.ExtraParams("ID")
    End Sub

    Private Function cargaEmpresa()
        Dim sWhere As String = " where codigo <> ''"
        Dim negEmp As New Negocio.Negociosaih_emp

        If txtCodigoBsq.Text <> "" Then
            sWhere &= " and codigo = '" & txtCodigo.Text & "'"
        End If

        If txtIdentificacionBsq.Text <> "" Then
            sWhere &= " and nit = '" & txtIdentificacionBsq.Text & "'"
        End If

        If txtRazonSocialBsq.Text <> "" Then
            sWhere &= " and razonsocial like '%" & txtRazonSocialBsq.Text & "%'"
        End If

        stEmp.DataSource = negEmp.Obtenersaih_empbyWhere(sWhere)
        stEmp.DataBind()

        Return True
    End Function

    Protected Sub btnBuscar_DirectClick(sender As Object, e As Ext.Net.DirectEventArgs)
        cargaEmpresa()
    End Sub

    Protected Sub btnGuardaEmp_DirectClick(sender As Object, e As Ext.Net.DirectEventArgs)
        Dim negEmp As New Negocio.Negociosaih_emp
        Dim emp As New Datos.saih_emp
        Reflexion.valoresForma(emp, vntEmp)
        If Accion = "Insert" Then
            If negEmp.Altasaih_emp(emp) Then
                Ext.Net.X.Msg.Alert("Información", "Registro Guardado Correctamente").Show()
                vntEmp.Hide()
                cargaEmpresa()
            End If
        Else
            If negEmp.Editasaih_emp(emp) Then
                Ext.Net.X.Msg.Alert("Información", "Registro Guardado Correctamente").Show()
                vntEmp.Hide()
                cargaEmpresa()
            End If
        End If
    End Sub

    Private Sub btnCerrarEmp_DirectClick(sender As Object, e As Ext.Net.DirectEventArgs) Handles btnCerrarEmp.DirectClick
        vntEmp.Hide()
    End Sub

    <DirectMethod()>
    Public Function eliminarEmp()
        Dim negEmp As New Negocio.Negociosaih_emp
        negEmp.Eliminasaih_emp(codigo)
        cargaEmpresa()
        Return True
    End Function

    Private Sub btnbtnEliminaEmpresa_DirectClick(sender As Object, e As Ext.Net.DirectEventArgs) Handles btnEliminaEmpresa.DirectClick
        Dim js As String = "Ext.MessageBox.confirm('Confirmación', '¿Esta seguro de eliminar?', " & _
           "function (btn) { " & _
                " if (btn == 'yes') " & _
                "   Ext.net.DirectMethods.eliminarEmp(); " & _
                " });"
        Ext.Net.X.Js.AddScript(js)
    End Sub

    Private Sub btnNuevaEmpresa_DirectClick(sender As Object, e As Ext.Net.DirectEventArgs) Handles btnNuevaEmpresa.DirectClick
        Accion = "Insert"
        codigo = ""
        cargarContratos()
        cargarCombos()
        presentacionUtil.limpiarObjetos(vntEmp)
        vntEmp.Show()
    End Sub

    Private Sub btnEditaEmpresa_DirectClick(sender As Object, e As Ext.Net.DirectEventArgs) Handles btnEditaEmpresa.DirectClick
        Accion = "Edit"
        Dim negEmp As New Negocio.Negociosaih_emp
        Dim emp As New Datos.saih_emp
        emp = negEmp.Obtenersaih_empById(codigo)
        cargarCombos()
        cargarContratos()
        Reflexion.formaValores(emp, vntEmp)
        cbEntidadDeSalud.Value = emp.codigo
        vntEmp.Show()
    End Sub

    Private Function cargarCombos()
        Dim negEmp As New Negocio.Negociosaih_emp

        stEmpFctProc.DataSource = negEmp.Obtenersaih_empbyWhere(" where codigo <> '" & codigo & "'")
        stEmpFctProc.DataBind()

        stEmpSolCuentas.DataSource = negEmp.Obtenersaih_empbyWhere(" where codigo <> '" & codigo & "'")
        stEmpSolCuentas.DataBind()

        Funciones.cargaStoreDominio(stEntidadDeSalud, Datos.ConstantesUtil.DOMINIO_ENTIDADES_SALUD)
        Return True
    End Function

    Private Sub cbEntidadDeSalud_DirectSelect(sender As Object, e As Ext.Net.DirectEventArgs) Handles cbEntidadDeSalud.DirectSelect
        Dim negTab As New Negocio.Negociosaih_tab
        Dim tab As New Datos.saih_tab
        tab = negTab.Obtenersaih_tabById(Datos.ConstantesUtil.DOMINIO_ENTIDADES_SALUD, cbEntidadDeSalud.Value)
        txtCodigo.Text = tab.codigo
        txtRazonSocial.Text = tab.nombre
    End Sub
#End Region

#Region "Contratos"

    Protected Sub rowSelectEmc(ByVal sender As Object, ByVal e As DirectEventArgs)
        num_contrato = e.ExtraParams("num_contrato")
    End Sub

    Private Function cargarContratos()
        Dim spFunc As New Datos.spFunciones
        Dim sql As String
        sql = "select codemp,num_contrato,nombre,case when estado = 1 then 'Activo' " & _
              "else 'Inactivo' end estado,dbo.FN_FECHA_SOLA(fecha) fecha, " & _
              "dbo.FN_FECHA_SOLA(fecha_inicial) fecha_inicial, dbo.FN_FECHA_SOLA(fecha_final) " & _
              "fecha_final,monto from saih_emc where codemp = '" & codigo & "'"
        stEmc.DataSource = spFunc.executaSelect(sql)
        stEmc.DataBind()
        Return True
    End Function

    Private Sub btnNuevoContrato_DirectClick(sender As Object, e As Ext.Net.DirectEventArgs) Handles btnNuevoContrato.DirectClick
        AccionEmc = "Insert"
        presentacionUtil.limpiarObjetos(vntEmc)
        vntEmc.Show()
    End Sub

    Private Sub btnEditaContrato_DirectClick(sender As Object, e As Ext.Net.DirectEventArgs) Handles btnEditaContrato.DirectClick
        AccionEmc = "Edit"
        Dim negEmc As New Negocio.Negociosaih_emc
        Dim emc As New Datos.saih_emc
        emc = negEmc.Obtenersaih_emcById(codigo, num_contrato)
        Reflexion.formaValores(emc, vntEmc)
        vntEmc.Show()
    End Sub

    <DirectMethod()>
    Public Function eliminarEmc()
        Dim negEmc As New Negocio.Negociosaih_emc
        negEmc.Eliminasaih_emc(codigo, num_contrato)
        cargarContratos()
        Return True
    End Function

    Private Sub btnEliminaContrato_DirectClick(sender As Object, e As Ext.Net.DirectEventArgs) Handles btnEliminaContrato.DirectClick
        Dim js As String = "Ext.MessageBox.confirm('Confirmación', '¿Esta seguro de eliminar?', " & _
           "function (btn) { " & _
                " if (btn == 'yes') " & _
                "   Ext.net.DirectMethods.eliminarEmc(); " & _
                " });"
        Ext.Net.X.Js.AddScript(js)
    End Sub

    Protected Sub btnGuardaEmc_DirectClick(sender As Object, e As Ext.Net.DirectEventArgs)
        Dim negEmc As New Negocio.Negociosaih_emc
        Dim emc As New Datos.saih_emc
        Reflexion.valoresForma(emc, vntEmc)
        emc.codemp = codigo
        If Datos.deTablas.difFechas("dd", emc.fecha_inicial, emc.fecha_final) < 0 Then
            Ext.Net.X.Msg.Alert("Información", "La fecha Final es Menor a la inicial").Show()
            Return
        End If
        If AccionEmc = "Insert" Then
            If negEmc.Altasaih_emc(emc) Then
                Ext.Net.X.Msg.Alert("Información", "Registro Guardado Correctamente").Show()
                vntEmc.Hide()
                cargarContratos()
            End If
        Else
            If negEmc.Editasaih_emc(emc) Then
                Ext.Net.X.Msg.Alert("Información", "Registro Guardado Correctamente").Show()
                vntEmc.Hide()
                cargarContratos()
            End If
        End If
    End Sub

    Private Sub btnCerrarEmc_DirectClick(sender As Object, e As Ext.Net.DirectEventArgs) Handles btnCerrarEmc.DirectClick
        vntEmc.Hide()
    End Sub

#End Region

    
    
End Class
﻿Imports Ext.Net

Public Class paciente
    Inherits System.Web.UI.Page

    Protected Property Accion As String
        Get
            Return ViewState("Accion")
        End Get
        Set(value As String)
            ViewState.Add("Accion", value)
        End Set
    End Property

    Protected Property numhis As String
        Get
            Return ViewState("numhis")
        End Get
        Set(value As String)
            ViewState.Add("numhis", value)
        End Set
    End Property

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If IsPostBack = False Then
            Dim seg As New Seguridad(Me, Session("PERFIL"))
        End If
    End Sub

    Protected Sub rowSelect(ByVal sender As Object, ByVal e As DirectEventArgs)
        numhis = e.ExtraParams("ID")
    End Sub

    Protected Sub btnBuscar_DirectClick(sender As Object, e As Ext.Net.DirectEventArgs)
        cargaPacientes()
    End Sub

    Protected Sub btnAsignar_DirectClick(sender As Object, e As Ext.Net.DirectEventArgs)
        generarNumHis()
    End Sub

    Protected Sub btnGuardaHis_DirectClick(sender As Object, e As Ext.Net.DirectEventArgs)
        If validarIdentificacion() Then
            Dim negHis As New Negocio.Negociosaih_his
            Dim his As New Datos.saih_his
            Dim edad As Integer
            'Dim dt = DateTime.ParseExact(txtFechaNacimiento.Value, "dd/MM/yyyy", Nothing)

            'edad = CInt(Now.Year - dt.Year)

            edad = Datos.deTablas.DifAnios(Funciones.Fecha(txtFechaNacimiento.Value))

            If cbTipoDoc.Value <> "MS" Then
                If edad < 7 And cbTipoDoc.Value <> "RC" Then
                    Ext.Net.X.Msg.Alert("Información", "El documento de identidad asociado debe ser RC").Show()
                    Return
                End If

                If edad >= 7 And edad < 18 And cbTipoDoc.Value <> "TI" Then
                    Ext.Net.X.Msg.Alert("Información", "El documento de identidad asociado debe ser TI").Show()
                    Return
                End If

                If edad >= 18 And cbTipoDoc.Value <> "CC" Then
                    Ext.Net.X.Msg.Alert("Información", "El documento de identidad asociado debe ser CC").Show()
                    Return
                End If
            Else
                If edad >= 1 Then
                    Ext.Net.X.Msg.Alert("Información", "El menor debe ser asociado a otro tipo de Documento").Show()
                    Return
                End If
            End If


            If Accion = "Insert" Then
                Reflexion.valoresForma(his, vntHis)
                If his.identificacion <> his.numhis Then
                    Ext.Net.X.Msg.Alert("Información", "El numero de Identificación y Numero de Historia Clinica no coinciden").Show()
                    Return
                End If

                his.fecha_creacion = Funciones.FechaActual
                his.usuario_creo = Session("Usuario")
                his.usuario_modifico = Session("Usuario")
                his.fecha_modificacion = Funciones.FechaActual
                his.razonsocial = his.primer_apellido + " " + his.segundo_apellido + " " + his.primer_nombre + " " + his.segundo_nombre
                If negHis.Altasaih_his(his) Then
                    Ext.Net.X.Msg.Alert("Información", "Registro Guardado Correctamente").Show()
                    vntHis.Hide()
                    cargaPacientes()
                End If
            Else
                his = negHis.Obtenersaih_hisById(numhis)
                Reflexion.valoresForma(his, vntHis)
                his.fecha_creacion = Funciones.fechaHoraDB(his.fecha_creacion)
                his.usuario_modifico = Session("Usuario")
                his.fecha_modificacion = Funciones.FechaActual
                his.razonsocial = his.primer_apellido + " " + his.segundo_apellido + " " + his.primer_nombre + " " + his.segundo_nombre
                If negHis.Editasaih_his(his) Then
                    Ext.Net.X.Msg.Alert("Información", "Registro Guardado Correctamente").Show()
                    vntHis.Hide()
                    cargaPacientes()
                End If
            End If
        End If

    End Sub

    <DirectMethod()>
    Public Function eliminarHis()
        Dim negHis As New Negocio.Negociosaih_his
        negHis.Eliminasaih_his(numhis)
        cargaPacientes()
        Return True
    End Function

    Private Sub btnEliminaPaciente_DirectClick(sender As Object, e As Ext.Net.DirectEventArgs) Handles btnEliminaPaciente.DirectClick
        Dim js As String = "Ext.MessageBox.confirm('Confirmación', '¿Esta seguro de eliminar?', " & _
           "function (btn) { " & _
                " if (btn == 'yes') " & _
                "   Ext.net.DirectMethods.eliminarHis(); " & _
                " });"
        Ext.Net.X.Js.AddScript(js)
    End Sub

    Private Sub btnNuevoPaciente_DirectClick(sender As Object, e As Ext.Net.DirectEventArgs) Handles btnNuevoPaciente.DirectClick
        Accion = "Insert"
        cargaCombos()
        presentacionUtil.limpiarObjetos(vntHis)
        vntHis.Show()
    End Sub

    Private Sub btnEditaPaciente_DirectClick(sender As Object, e As Ext.Net.DirectEventArgs) Handles btnEditaPaciente.DirectClick
        Accion = "Edit"
        Dim negHis As New Negocio.Negociosaih_his
        Dim his As New Datos.saih_his
        his = negHis.Obtenersaih_hisById(numhis)
        cargaCombos()
        Reflexion.formaValores(his, vntHis)
        vntHis.Show()
    End Sub

    Private Function cargaCombos()
        Dim negEmpresa As New Negocio.Negociosaih_emp
        Funciones.cargaStoreDominio(stTipoDoc, Datos.ConstantesUtil.DOMINIO_TIPO_IDENTIFICACION)
        Funciones.cargaStoreDominio(stEstrato, Datos.ConstantesUtil.DOMINIO_ESTRATO)
        Funciones.cargaStoreDominio(stOcupacion, Datos.ConstantesUtil.DOMINIO_OCUPACIONES)
        Funciones.cargaStoreDominio(stReligion, Datos.ConstantesUtil.DOMINIO_RELIGION)
        Funciones.cargaStoreDominio(stEtnia, Datos.ConstantesUtil.DOMINIO_ETNIA)
        Funciones.cargaStoreDominio(stEntidadSalud, Datos.ConstantesUtil.DOMINIO_ENTIDADES_SALUD)
        Funciones.cargaStoreDominio(stTipoResidencia, Datos.ConstantesUtil.DOMINIO_TIPO_RESIDENCIA)
        Funciones.cargaStoreDominio(stNivelEscolar, Datos.ConstantesUtil.DOMINIO_ESCOLARIDAD)
        Funciones.cargaStoreDominio(stDiscapacidad, Datos.ConstantesUtil.DOMINIO_DISCAPACIDAD)
        stEmpArp.DataSource = negEmpresa.Obtenersaih_emp
        stEmpArp.DataBind()
        stEmpCcf.DataSource = negEmpresa.Obtenersaih_emp
        stEmpCcf.DataBind()
        Return True
    End Function

    Private Sub cbIdLugarRes_DirectSelect(sender As Object, e As Ext.Net.DirectEventArgs) Handles cbIdLugarRes.DirectSelect
        Dim negLugar As New Negocio.Negociosaih_lugar
        cbBarrioRes.Clear()
        stBarrioRes.DataSource = negLugar.Obtenersaih_lugarbyWhere(" where id_lugar = " & cbIdLugarRes.Value)
        stBarrioRes.DataBind()
    End Sub
    Private Function generarNumHis()
        Dim spFunc As New Datos.spFunciones
        Dim sede As String
        Dim valor As String
        Dim negPrm As New Negocio.Negociosaih_prm
        Dim prm As New Datos.saih_prm
        Dim negLugar As New Negocio.Negociosaih_lugar
        Dim codDpto As New Datos.saih_lugar
        Dim codMuni As New Datos.saih_lugar

        sede = Session("Sede")
        prm = negPrm.Obtenersaih_prmById(sede)
        codDpto = negLugar.Obtenersaih_lugarById(prm.id_lugar)
        codMuni = negLugar.Obtenersaih_lugarById(codDpto.Id_Lugar)


        Dim msg As New Datos.mensaje
        msg = spFunc.consecutivoPRM(sede, Datos.ConstantesUtil.HISTORIA_CLINICA)
        valor = codDpto.Codigo & "-" & codMuni.Codigo & "-" & msg.Valor
        txtIdentificacion.Text = valor
        txtNumHis.Text = valor
        Return True
    End Function


    Private Function cargaPacientes()
        Dim negHis As New Negocio.Negociosaih_his
        Dim sWhere As String = " where numhis <> '' "
        Dim count As Integer

        count = 0

        If txtNumHisBsq.Text <> "" Then
            sWhere &= " and numhis = '" & txtNumHisBsq.Text & "'"
            count = count + 1
        End If

        If txtIdentificacionBsq.Text <> "" Then
            sWhere &= " and identificacion = '" & txtIdentificacionBsq.Text & "'"
            count = count + 1
        End If

        If txtRazonSocialBsq.Text <> "" Then
            sWhere &= " and razonsocial like '%" & txtRazonSocialBsq.Text & "%'"
            count = count + 1
        End If

        If count = 0 Then
            Ext.Net.X.Msg.Alert("Información", "Por favor ingresar por lo menos un filtro de búsqueda").Show()
            Return True
        End If

        stHis.DataSource = negHis.Obtenersaih_hisbyWhere(sWhere)
        stHis.DataBind()

        Return True
    End Function

    Private Sub btnCerrarHis_DirectClick(sender As Object, e As Ext.Net.DirectEventArgs) Handles btnCerrarHis.DirectClick
        vntHis.Hide()
    End Sub


    Private Sub cbTipoDoc_DirectSelect(sender As Object, e As Ext.Net.DirectEventArgs) Handles cbTipoDoc.DirectSelect
        validarIdentificacion()
    End Sub

    Private Sub txtIdentificacion_DirectChange(sender As Object, e As Ext.Net.DirectEventArgs) Handles txtIdentificacion.DirectChange
        validarIdentificacion()
    End Sub

    Private Sub txtIdentificacion_TextChanged(sender As Object, e As System.EventArgs) Handles txtIdentificacion.TextChanged

    End Sub

    Private Function validarIdentificacion()

        If txtIdentificacion.Text <> "" Then
            If cbTipoDoc.Value = "MS" Then
                If txtIdentificacion.Text.Length < 10 Or txtIdentificacion.Text.Length < 10 Then
                    Ext.Net.X.Msg.Alert("Información", "El número de documento debe tener una longitud de 10 a 12 caracteres").Show()
                    txtIdentificacion.Focus(True)
                    Return False
                End If
            End If

            If cbTipoDoc.Value = "RC" Or cbTipoDoc.Value = "TI" Then
                If txtIdentificacion.Text.Length < 10 Or txtIdentificacion.Text.Length > 11 Then
                    Ext.Net.X.Msg.Alert("Información", "El número de documento debe tener una longitud de 10 a 11 caracteres").Show()
                    txtIdentificacion.Focus()
                    Return False
                End If
            End If

            If cbTipoDoc.Value = "AS" Then
                If txtIdentificacion.Text.Length <> 10 Then
                    Ext.Net.X.Msg.Alert("Información", "El número de documento debe tener una longitud de 10 caracteres").Show()
                    txtIdentificacion.Focus(True)
                    Return False
                End If
            End If

            If cbTipoDoc.Value = "CC" Then
                If txtIdentificacion.Text.Length > 10 Then
                    Ext.Net.X.Msg.Alert("Información", "El número de documento debe tener una longitud menor o igual a 10 caracteres").Show()
                    txtIdentificacion.Focus(True)
                    Return False
                End If
            End If

            If cbTipoDoc.Value = "CE" Then
                If txtIdentificacion.Text.Length > 10 Then
                    Ext.Net.X.Msg.Alert("Información", "El número de documento debe tener una longitud menor o igual a 10 caracteres").Show()
                    txtIdentificacion.Focus(True)
                    Return False
                End If
            End If

            If cbTipoDoc.Value = "PA" Then
                If txtIdentificacion.Text.Length <> 16 Then
                    Ext.Net.X.Msg.Alert("Información", "El número de documento debe tener una longitud de 16 caracteres").Show()
                    txtIdentificacion.Focus(True)
                    Return False
                End If
            End If
        End If

        Return True
    End Function
End Class
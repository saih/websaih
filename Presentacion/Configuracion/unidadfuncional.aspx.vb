﻿Imports Ext.Net

Public Class unidadfuncional
    Inherits System.Web.UI.Page

    Protected Property Accion As String
        Get
            Return ViewState("Accion")
        End Get
        Set(value As String)
            ViewState.Add("Accion", value)
        End Set
    End Property

    Protected Property codunf As String
        Get
            Return ViewState("codunf")
        End Get
        Set(value As String)
            ViewState.Add("codunf", value)
        End Set
    End Property

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If IsPostBack = False Then
            Dim seg As New Seguridad(Me, Session("PERFIL"))
            cargarUnf()
        End If
    End Sub

    Protected Sub rowSelect(ByVal sender As Object, ByVal e As DirectEventArgs)
        codunf = e.ExtraParams("ID")
    End Sub

    Private Function cargarUnf()
        Dim sWhere As String = " where codunf <> ''"
        Dim negUnf As New Negocio.Negociosaih_unf
        If txtCodUnfBsq.Text <> "" Then
            sWhere &= " and codunf = '" & txtCodUnfBsq.Text & "'"
        End If
        If txtNombreBsq.Text <> "" Then
            sWhere &= " and nombre like '%" & txtNombreBsq.Text & "%'"
        End If
        stUnf.DataSource = negUnf.Obtenersaih_unfbyWhere(sWhere)
        stUnf.DataBind()
        Return True
    End Function

    Protected Sub btnBuscar_DirectClick(sender As Object, e As Ext.Net.DirectEventArgs)
        cargarUnf()
    End Sub


    Private Sub btnNuevaUnidad_DirectClick(sender As Object, e As Ext.Net.DirectEventArgs) Handles btnNuevaUnidad.DirectClick
        Accion = "Insert"
        txtCodUnf.Clear()
        txtNombre.Clear()
        vntUnf.Show()
    End Sub

    Private Sub btnEditaUnidad_DirectClick(sender As Object, e As Ext.Net.DirectEventArgs) Handles btnEditaUnidad.DirectClick
        Accion = "Edit"
        Dim negUnf As New Negocio.Negociosaih_unf
        Dim Unf As New Datos.saih_unf
        Unf = negUnf.Obtenersaih_unfById(codunf)
        txtCodUnf.Text = Unf.codunf
        txtNombre.Text = Unf.nombre
        vntUnf.Show()
    End Sub


    <DirectMethod()>
    Public Function eliminarUnf()
        Dim negUnf As New Negocio.Negociosaih_unf
        negUnf.Eliminasaih_unf(codunf)
        cargarUnf()
        Return True
    End Function

    Private Sub btnEliminaUnidad_DirectClick(sender As Object, e As Ext.Net.DirectEventArgs) Handles btnEliminaUnidad.DirectClick
        Dim js As String = "Ext.MessageBox.confirm('Confirmación', '¿Esta seguro de eliminar?', " & _
           "function (btn) { " & _
                " if (btn == 'yes') " & _
                "   Ext.net.DirectMethods.eliminarUnf(); " & _
                " });"
        Ext.Net.X.Js.AddScript(js)
    End Sub

    Private Sub btnCerrarUnf_DirectClick(sender As Object, e As Ext.Net.DirectEventArgs) Handles btnCerrarUnf.DirectClick
        vntUnf.Hide()
    End Sub

    Protected Sub btnGuardaUnf_DirectClick(sender As Object, e As Ext.Net.DirectEventArgs)
        Dim negUnf As New Negocio.Negociosaih_unf
        If Accion = "Insert" Then
            If negUnf.Altasaih_unf(txtCodUnf.Text, txtNombre.Text) Then
                Ext.Net.X.Msg.Alert("Información", "Registro Guardado Correctamente").Show()
                cargarUnf()
                vntUnf.Hide()
            End If
        Else
            If negUnf.Editasaih_unf(codunf, txtNombre.Text) Then
                Ext.Net.X.Msg.Alert("Información", "Registro Guardado Correctamente").Show()
                cargarUnf()
                vntUnf.Hide()
            End If
        End If
    End Sub
End Class
﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="Zonas.aspx.vb" Inherits="Presentacion.Zonas" %>

<%@ Register Assembly="Ext.Net" Namespace="Ext.Net" TagPrefix="ext" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title></title>
    <script type="text/javascript" src="./../public/js/jquery-1.9.0.js"></script>
    <script type="text/javascript" src="./../public/js/jquery.timer.js"></script>
    <script type="text/javascript" src="./../public/js/app.js"></script>
    <ext:XScript ID="XScript1" runat="server">
        <script type="text/javascript">
            var addBar = function () {
                var grid = #{grdLugarBarrio};
                grid.getRowEditor().stopEditing();
                                
                grid.insertRecord(0, {
                    id     : 0, 
                    codigo   : "",
                    descripcion  : ""
                });
                                
                grid.getView().refresh();
                grid.getSelectionModel().selectRow(0);
                grid.getRowEditor().startEditing(0);
            }

            var cancelBar = function(){
                Ext.net.DirectMethods.cancelarBar();
            }

            var removeBar = function () {                
                var grid = #{grdLugarBarrio};
                grid.getRowEditor().stopEditing();
                            
                var s = grid.getSelectionModel().getSelections();
                            
                for (var i = 0, r; r = s[i]; i++) {                    
                    Ext.net.DirectMethods.btnEliminaBar(r.data.id);
                }
            }

            var editaBar = function(val, params, record){
                Ext.net.DirectMethods.insertaBar(record.get('id'),record.get('codigo'),record.get('descripcion'));                
            }
        </script>
    </ext:XScript>
    
</head>
<body>
    <form id="form1" runat="server">
    
    <ext:ResourceManager ID="ResourceManager1" runat="server" />

    <ext:Store ID="stPais" runat="server" AutoLoad="true" >
        <Reader>
            <ext:JsonReader IDProperty="Id">
                <Fields>
                    <ext:RecordField Name="Id" />
                    <ext:RecordField Name="Tipo" />
                    <ext:RecordField Name="Descripcion" />
                    <ext:RecordField Name="Id_Lugar" />                    
                </Fields>
            </ext:JsonReader>
        </Reader>
    </ext:Store>

    

    <ext:Store ID="stDpto" runat="server" AutoLoad="false" >
        <Reader>
            <ext:JsonReader IDProperty="Id">
                <Fields>
                    <ext:RecordField Name="Id" />
                    <ext:RecordField Name="Tipo" />
                    <ext:RecordField Name="Descripcion" />
                    <ext:RecordField Name="Id_Lugar" />                                    
                </Fields>
            </ext:JsonReader>
        </Reader>
    </ext:Store>

    <ext:Store ID="stCiudad" runat="server" AutoLoad="false" >
        <Reader>
            <ext:JsonReader IDProperty="Id">
                <Fields>
                    <ext:RecordField Name="Id" />
                    <ext:RecordField Name="Tipo" />
                    <ext:RecordField Name="Descripcion" />
                    <ext:RecordField Name="Id_Lugar" />    
                    <ext:RecordField Name="Departamento" />                            
                </Fields>
            </ext:JsonReader>
        </Reader>
    </ext:Store>

    <div align="center"  style="font-family:Arial; font-size:13" >
        <ext:Panel ID="PanelPrin" runat="server" AutoWidth="true" Title="Configuración Zonas" Frame="true"
        PaddingSummary="5px 5px 0" Height="640" ButtonAlign="Center" Style="text-align: left;"  >                         
            <Items>
                <ext:FormPanel ID="FormPanel1" runat="server" BodyPadding="5" MonitorResize="true" AutoWidth="true">
                    <Items>
                        <ext:GridPanel
                                ID="grdPais"
                                runat="server"
                                StoreId="stPais"
                                Margins="0 0 5 5"
                                Frame="true"
                                Height="160"
                                width ="600"
                                Title="Paises"
                                icon="World"
                                Padding="5"
                                >
                                <ColumnModel ID="ColumnModel1" runat="server" >
                                    <Columns>
                                        <ext:Column Header="Id." DataIndex="Id" Width="60" />                                       
                                        <ext:Column Header="Pais" DataIndex="Descripcion" Width="310"  />
                                        <ext:Column Header="Tipo" DataIndex="Tipo" hidden="true"  />                                                                                                                                    
                                    </Columns>                                
                                </ColumnModel>
                                <SelectionModel>
                                    <ext:RowSelectionModel ID="RowSelectionModel1" runat="server" SingleSelect="true" >
                                        <DirectEvents>
                                            <RowSelect OnEvent="rowSelectPais" Buffer="100">
                                                <ExtraParams>
                                                <%-- or can use params[2].id as value --%>
                                                    <ext:Parameter Name="IDPais" Value="this.getSelected().id" Mode="Raw" />
                                                </ExtraParams>
                                            </RowSelect>
                                        </DirectEvents>
                                    </ext:RowSelectionModel>
                                </SelectionModel>
                                                    
                                <%--<Listeners>
                                    <RowClick Handler="cargaBdg(this.getStore().getAt(rowIndex).data.ID)" />
                                    <Command Fn="commandHandler" />
                                </Listeners>--%>
                                <LoadMask ShowMask="true" />                                                                         
                                <BottomBar>
                                    <ext:StatusBar ID="StatusBar1" runat="server" >
                                        <Items>
                                            <ext:Button runat="server" Text="Nuevo Pais" ID="btnNuevoPais" Icon="Add" />
                                            <ext:Button runat="server" Text="Editar Pais" ID="btnEditaPais" Icon="ApplicationEdit" />
                                            <ext:Button runat="server" Text="Borrar Pais" ID="btnBorraPais" Icon="ApplicationDelete" />
                                      
                                      
                                        </Items>
                                    </ext:StatusBar>
                                </BottomBar>
                        </ext:GridPanel>

                        <ext:GridPanel
                                ID="grdDpto"
                                runat="server"
                                StoreId="stDpto"
                                Margins="5 5 5 5"                                
                                Frame="true"
                                Height="220"
                                Width="600"
                                icon="Map"
                                Padding="5"
                                Title="Departamentos por Pais"
                                >
                                <ColumnModel ID="ColumnModel2" runat="server" >
                                    <Columns>
                                        <ext:Column Header="Id." DataIndex="Id" Width="60" />
                                        <ext:Column Header="Pais" DataIndex="Id_Lugar"  Hidden ="true" />  
                                        <ext:Column Header="Tipo" DataIndex="Tipo" Hidden ="true"  />
                                        <ext:Column Header="Departamento" DataIndex="Descripcion" Width="305"  />
                                                                                                                                             
                                    </Columns>
                                </ColumnModel>
                                <SelectionModel>
                                    <ext:RowSelectionModel ID="RowSelectionModel2" runat="server" SingleSelect="true" >
                                        <DirectEvents>
                                            <RowSelect OnEvent="rowSelectDpto" Buffer="100">
                                                <ExtraParams>
                                                <%-- or can use params[2].id as value --%>
                                                    <ext:Parameter Name="IDDpto" Value="this.getSelected().id" Mode="Raw" />
                                                </ExtraParams>
                                            </RowSelect>
                                        </DirectEvents>
                                    </ext:RowSelectionModel>
                                </SelectionModel>
                                <LoadMask ShowMask="true" />                            
                                        
                                                                                                                
                                <BottomBar>
                                    <ext:StatusBar runat="server" ID="StatusBar2">
                                        <Items>                                                        
                                            <ext:Button runat="server" Text="Nuevo Dpto" ID="btnNuevaDpto" Icon="Add" />
                                            <ext:Button runat="server" Text="Editar Dpto" ID="btnEditaDpto" Icon="ApplicationEdit" />
                                            <ext:Button runat="server" Text="Borrar Dpto" ID="btnBorraDpto" Icon="ApplicationDelete" />
                                        </Items>
                                    </ext:StatusBar>
                                </BottomBar>
                        </ext:GridPanel>


                        <ext:GridPanel
                                ID="grdCiudad"
                                runat="server"
                                StoreId="stCiudad"
                                Margins="0 0 5 5"                                
                                Frame="true"
                                Height="220"
                                width="600"
                                icon="House"
                                Padding="5"
                                Title="Ciudades por Departamento"
                                >
                                <ColumnModel ID="ColumnModel3" runat="server" >
                                    <Columns>
                                        <ext:Column Header="Id." DataIndex="Id" Width="60" />
                                        <ext:Column Header="Tipo" DataIndex="Tipo" hidden="true"/>                                        
                                        <ext:Column Header="Ciudad" DataIndex="Descripcion" Width="180"  />
                                        <ext:Column Header="Departamento" DataIndex="Departamento" Width="125"  />                                                                                                       
                                    </Columns>
                                </ColumnModel>
                                <SelectionModel>
                                    <ext:RowSelectionModel ID="RowSelectionModel3" runat="server" SingleSelect="true" >
                                        <DirectEvents>
                                            <RowSelect OnEvent="rowSelectCiudad" Buffer="100">
                                                <ExtraParams>
                                                        
                                                    <ext:Parameter Name="IDCiudad" Value="this.getSelected().id" Mode="Raw" />
                                                </ExtraParams>
                                            </RowSelect>
                                        </DirectEvents>
                                    </ext:RowSelectionModel>
                                </SelectionModel>
                                <LoadMask ShowMask="true" />                            
                                        
                                <LoadMask ShowMask="true" />                                                                         
                                <BottomBar>
                                    <ext:StatusBar runat="server" ID="StatusBar3">
                                        <Items>                                                        
                                            <ext:Button runat="server" Text="Nueva Ciudad" ID="btnNuevaCiudad" Icon="Add" />
                                            <ext:Button runat="server" Text="Editar Ciudad" ID="btnEditaCiudad" Icon="ApplicationEdit" />
                                             <ext:Button runat="server" Text="Borrar Ciudad" ID="btnBorraCiudad" Icon="ApplicationDelete" />
                                        </Items>
                                    </ext:StatusBar>
                                </BottomBar>
                        </ext:GridPanel>

                    </Items>
                </ext:FormPanel>
            </Items>                                
        </ext:Panel>
    </div>





    <ext:Window 
            ID="VtnPais"
            runat="server" 
            Icon="World" 
            Width="400" 
            Height="180" 
            Hidden="true" 
            Modal="true"            
            Title="paises"
            Constrain="true">
            <Items>
            <ext:Panel ID="PanelV1" runat="server" Frame="true" PaddingSummary="5px 2px 0" AutoWidth="true"
            Height="200" ButtonAlign="Center" Style="text-align: left">
                <Items>
                    <ext:FieldSet ID="FieldSet1" runat="server" Title="" Padding="10" Height="80">
                        <Items>
                            <ext:Container ID="Container1" runat="server" Layout="ColumnLayout" Height="100">
                                <Items>
                                    <ext:Container ID="ContainerV1" runat="server" LabelAlign="Right" LabelWidth="60" Layout="FormLayout"
                                        ColumnWidth=".99">
                                        <Items>   
                                            <ext:Label runat="server" ID="lblIDPais"  FieldLabel="Id" />
                                            <ext:Hidden runat="server" ID="hIDPais" />
                                            <ext:Hidden runat="server" ID="txtTipoPais" />
                                            <ext:Hidden runat="server" ID="TxtLugarPais" />
                                            <ext:TextField runat="server" ID="txtPais" FieldLabel="Pais" Width ="260" AllowBlank="false" MinLength="3" MaxLength="50" />
                                            <ext:TextField runat="server" ID="txtCodPais" FieldLabel="Codigo" MinLength="4" MaxLength="20" />
                                         </Items>
                                    </ext:Container>                                    
                                </Items>
                            </ext:Container>
                        </Items>
                    </ext:FieldSet>
                </Items>
            </ext:Panel>
            </Items>
            <Buttons>
                <ext:Button runat="server" ID="btnGuardapais" Text="Guardar"  Icon="Disk" >
                    <DirectEvents>
                        <Click 
                            OnEvent="btnGuardapais_DirectClick" 
                            Before="return obligatorios('VtnPais');">
                            <EventMask 
                                ShowMask="true" 
                                MinDelay="1000"                                 
                                />
                        </Click>
                    </DirectEvents>
                </ext:Button>
                <ext:Button runat="server" ID="btnCerrarPais" Text="Cerrar" Icon="Cancel" />
            </Buttons>
        </ext:Window>


        <ext:Window 
            ID="vntDpto"
            runat="server" 
            Icon="MapEdit"
            Width="400" 
            Height="230" 
            Hidden="true" 
            Modal="true"            
            Title="Departamentos"
            Constrain="true">
            <Items>
            <ext:Panel ID="Panel2" runat="server" Frame="true" PaddingSummary="5px 2px 0" AutoWidth="true"
            Height="380" ButtonAlign="Center" Style="text-align: left">
                <Items>
                    <ext:FieldSet ID="FieldSet2" runat="server" Title="" Padding="10" Height="130">
                        <Items>
                            <ext:Container ID="Container2" runat="server" Layout="ColumnLayout" Height="370">
                                <Items>
                                    <ext:Container ID="Container3" runat="server" LabelAlign="Right" labelwidth="90" Layout="FormLayout"
                                        ColumnWidth=".99">
                                        <Items>   
                                            <ext:Label runat="server" ID="lblIDDpto"  FieldLabel="Id" />
                                            <ext:Label runat="server" ID="lblPais"  FieldLabel="Pais" widht="220"/>
                                            <ext:Hidden runat="server" ID="HdIDDpto" />  
                                            <ext:Hidden runat="server" ID="txtTipoDpto" />                              
                                            <ext:TextField runat="server" ID="txtDepartamento" FieldLabel="Departamento" width="230" AllowBlank="false" MinLength="3" MaxLength="50" />
                                            <ext:TextField runat="server" ID="txtCodDepartamento" FieldLabel="Codigo" MinLength="4" MaxLength="20" />
                                         </Items>
                                    </ext:Container>                                    
                                </Items>
                            </ext:Container>
                        </Items>
                    </ext:FieldSet>
                </Items>
            </ext:Panel>
            </Items>
            <Buttons>
                <ext:Button runat="server" ID="btnGuardarDpto" Text="Guardar" Icon ="Disk"  >
                    <DirectEvents>
                        <Click 
                            OnEvent="btnGuardarDpto_DirectClick" 
                            Before="return obligatorios('vntDpto');">
                            <EventMask 
                                ShowMask="true" 
                                MinDelay="1000"                                 
                                />
                        </Click>
                    </DirectEvents>
                </ext:Button>
                <ext:Button runat="server" ID="btnCerrarDpto" Text="Cerrar" icon="Cancel"    />
            </Buttons>
        </ext:Window>


        <ext:Store ID="stLugarBarrio" runat="server" AutoLoad="true">
			<Reader>
				<ext:JsonReader IDProperty="codigo">
					<Fields>
						<ext:RecordField Name="codigo" />
						<ext:RecordField Name="descripcion" />
					</Fields>
				</ext:JsonReader>
			</Reader>            
		</ext:Store>

        <ext:Window 
            ID="vntCiudad"
            runat="server" 
            Icon="HouseStar"
            Width="450"
            Height="400"
            Hidden="true" 
            Modal="true"            
            Title="Ciudades"
            Constrain="true">
            <Items>
            <ext:TabPanel ID="TabUsuario" runat="server" ActiveTabIndex="0" Border="false" Title="Center" ResizeTabs="true">
                <Items>
                    <ext:Panel ID="PGeneral" runat="server" Closable="False" Title="General" Hidden="False">
                        <Items>
                            <ext:Container ID="Container5" runat="server" LabelAlign="Right" LabelWidth="80" Layout="FormLayout" ColumnWidth=".99">
                                <Items>   
                                    <ext:Label runat="server" ID="lblIDCiudad"  FieldLabel="Id" />
                                    <ext:Label runat="server" ID="lblIDPaisUbq"  FieldLabel="Pais" Width="220"/>
                                    <ext:Label runat="server" ID="lblIDDptoUbq"  FieldLabel="Departamento" Width="220"/>
                                    <ext:Hidden runat="server" ID="HIDCiudad" />   
                                    <ext:Hidden runat="server" ID="txtTipoCiudad" />  
                                    <ext:Hidden runat="server" ID="txtiddpto" />  
                                    <ext:TextField runat="server" ID="txtCiudad" FieldLabel="Ciudad"  Width="220" AllowBlank="false" MinLength="3" MaxLength="50" />
                                    <ext:TextField runat="server" ID="txtCodCiudad" FieldLabel="Codigo" AllowBlank="false" MinLength="4" MaxLength="20" />
                                </Items>
                            </ext:Container>                                    
                        </Items>
                    </ext:Panel>
                    <ext:Panel ID="PBarrios" runat="server" Closable="False" Title="Barrios" Hidden="False">
                        <Items>
                            <ext:GridPanel 
                                ID="grdLugarBarrio" 
                                runat="server"
                                Width="380"
                                Height="380"
                                AutoExpandColumn="id"
                                Title="" 
                                StoreID="stLugarBarrio" 
                                >
                                <Plugins>
                                    <ext:RowEditor ID="RowEditor1" runat="server" SaveText="Guardar" CancelText="Cancelar"  >                            
                                        <Listeners>
                                            <CancelEdit Fn="cancelBar" />
                                            <AfterEdit Fn="editaBar" />
                                        </Listeners>
                                    </ext:RowEditor>
                                </Plugins>
                                <View>
                                    <ext:GridView ID="GridView1" runat="server" MarkDirty="false" />
                                </View>
                                <TopBar>
                                    <ext:Toolbar ID="Toolbar1" runat="server">
                                        <Items>
                                            <ext:Button ID="Button1" runat="server" Text="Nuevo Barrio" Icon="UserAdd">
                                                <Listeners>
                                                    <Click Fn="addBar" />
                                                </Listeners>
                                            </ext:Button>
                                            <ext:Button ID="btnEliminaTar1" runat="server" Text="Eliminar Barrio" Icon="UserDelete" >                                    
                                                <Listeners>
                                                    <Click Fn="removeBar" />
                                                </Listeners>
                                            </ext:Button>
                                        </Items>
                                    </ext:Toolbar>
                                </TopBar>
                                <SelectionModel>
                                    <ext:RowSelectionModel ID="RowSelectionModel4" runat="server" />
                                </SelectionModel>
                                <ColumnModel>
                                    <Columns>
                                        <ext:RowNumbererColumn />
                                        <ext:Column DataIndex="id" Hidden="true" />                                        
                                        <ext:Column Header="Codigo" DataIndex="codigo" Width="100">
                                            <Editor>
                                                <ext:TextField runat="server" ID="txtCodigoBarrio" AllowBlank="false" />
                                            </Editor>
                                        </ext:Column>
                                        <ext:Column Header="Descripción" DataIndex="descripcion" Width="200">
                                            <Editor>
                                                <ext:TextField runat="server" ID="txtDescripcionBarrio" AllowBlank="false" />
                                            </Editor>
                                        </ext:Column>                                        
                                    </Columns>
                                </ColumnModel>
                            </ext:GridPanel>
                        </Items>
                    </ext:Panel>
                </Items>
            </ext:TabPanel>            
            </Items>
            <Buttons>
                <ext:Button runat="server" ID="btnGuardaCiudad" Text="Guardar" icon="disk" >
                    <DirectEvents>
                        <Click 
                            OnEvent="btnGuardaCiudad_DirectClick" 
                            Before="return obligatorios('PGeneral');">
                            <EventMask 
                                ShowMask="true" 
                                MinDelay="1000"                                 
                                />
                        </Click>
                    </DirectEvents>                    
                </ext:Button>
                <ext:Button runat="server" ID="btnCierraCiudad" Text="Cerrar" Icon="Cancel" />
            </Buttons>
        </ext:Window>
    </form>
</body>
</html>

﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="paciente.aspx.vb" Inherits="Presentacion.paciente" %>

<%@ Register Assembly="Ext.Net" Namespace="Ext.Net" TagPrefix="ext" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title></title>
    <script src="../public/js/jquery-1.9.0.js" type="text/javascript"></script>
    <script type="text/javascript" src="./../public/js/app.js"></script>


    <script type="text/javascript">
        var enterKeyPressHandler = function (f, e) {

            var val = parseInt(e.getKey());
            console.log(val);
            if ((val >= 65 && val <= 90) || (val === 32) || (val === 16)) {

            } else {
                var objExt = Ext.getCmp(f.id);
                var palabra = f.getValue();
                f.setValue(palabra.substring(0, palabra.length - 1))
            }
        }    </script>

</head>
<body>
    <form id="form1" runat="server">    
        <ext:ResourceManager ID="ResourceManager1" runat="server" />

        <ext:Store 
            ID="stHis" 
            runat="server" 
            AutoLoad="true">
            <Reader>
                <ext:JsonReader IDProperty="numhis">
                    <Fields>
                        <ext:RecordField Name="numhis" />
                        <ext:RecordField Name="tipo_identificacion" />
                        <ext:RecordField Name="identificacion" />
                        <ext:RecordField Name="razonsocial" />
                    </Fields>
                </ext:JsonReader>
            </Reader>
        </ext:Store>

        <ext:Panel ID="Panel3" runat="server" Frame="true" PaddingSummary="5px 5px 0" AutoWidth="true" Height="650" ButtonAlign="Center" Style="text-align: left" Title="Pacientes">
            <Items>
                <ext:FieldSet ID="FieldSet3" runat="server" Title="Campos de Busqueda" Padding="10" Height="60">
                    <Items>
                        <ext:Container ID="Container10" runat="server" Layout="ColumnLayout" Height="30">
                            <Items>
                                <ext:Container ID="Container8" runat="server" LabelAlign="Right" Layout="FormLayout" ColumnWidth=".25">
                                    <Items>
                                        <ext:TextField runat="server" id="txtNumHisBsq" FieldLabel="Num.Historia" />
                                    </Items>    
                                </ext:Container>
                                <ext:Container ID="Container2" runat="server" LabelAlign="Right" Layout="FormLayout" ColumnWidth=".25">
                                    <Items>
                                         <ext:TextField runat="server" id="txtIdentificacionBsq" FieldLabel="Identificación" />                                       
                                    </Items>
                                </ext:Container>
                                <ext:Container ID="Container4" runat="server" LabelAlign="Right" Layout="FormLayout" ColumnWidth=".3">
                                    <Items>
                                         <ext:TextField runat="server" id="txtRazonSocialBsq" FieldLabel="Nombres" />
                                    </Items>
                                </ext:Container>
                                <ext:Container ID="Container5" runat="server" LabelAlign="Right" Layout="FormLayout" ColumnWidth=".2">
                                    <Items>
                                        <ext:Button runat="server" ID="btnBuscar" Text="Buscar" Icon="ApplicationGo" >
                                            <DirectEvents>
                                                <Click OnEvent="btnBuscar_DirectClick" Before="">
                                                    <EventMask ShowMask="true" />
                                                </Click>
                                            </DirectEvents>
                                        </ext:Button>
                                    </Items>
                                </ext:Container>
                            </Items>
                        </ext:Container>                        
                    </Items>
                </ext:FieldSet>
                <ext:GridPanel
                    ID="grdHis"
                    runat="server"                                            
                    Margins="0 0 5 5"
                    Icon="TransmitGo"
                    Frame="true"
                    Height="400"
                    StoreId="stHis"
                    Title="Pacientes"
                >
                    <ColumnModel ID="ColumnModel3" runat="server">
                        <Columns>                        
                            <ext:Column Header="Código" DataIndex="numhis" />
                            <ext:Column Header="Tipo" DataIndex="tipo_identificacion" />
                            <ext:Column Header="Identificación" DataIndex="identificacion" />                        
                            <ext:Column Header="Nombre" DataIndex="razonsocial" Width="380"/>
                        </Columns>                                
                    </ColumnModel>
                    <SelectionModel>
                        <ext:RowSelectionModel ID="RowSelectionModel1" runat="server" SingleSelect="true" >
                            <DirectEvents>
                                <RowSelect OnEvent="rowSelect" Buffer="100">
                                    <ExtraParams>                                    
                                        <ext:Parameter Name="ID" Value="this.getSelected().id" Mode="Raw" />
                                    </ExtraParams>
                                </RowSelect>
                            </DirectEvents>
                        </ext:RowSelectionModel>
                    </SelectionModel>
                    <LoadMask ShowMask="true" Msg="Loading...." />
                    <BottomBar>
                        <ext:StatusBar runat="server" ID="StatusBar2">
                            <Items>                                                        
                                <ext:Button runat="server" Text="Nuevo Paciente" ID="btnNuevoPaciente" Icon="Add" />
                                <ext:Button runat="server" Text="Editar Paciente" ID="btnEditaPaciente" Icon="ApplicationEdit" />
                                <ext:Button runat="server" Text="Eliminar Paciente" ID="btnEliminaPaciente" Icon="ApplicationDelete" />
                            </Items>
                        </ext:StatusBar>
                    </BottomBar>
                </ext:GridPanel>
            </Items>
        </ext:Panel>

        <ext:Window 
            ID="vntHis"
            runat="server"
            Icon="ApplicationFormEdit"
            Width="850"
            Height="510"
            Hidden="true" 
            Modal="true"            
            Title="Registro Pacientes"
            Constrain="true">
            <Items>
                <ext:Panel ID="Panel1" 
                    runat="server" 
                    Title=""
                    AnchorHorizontal="100%"
                    Height="480"
                    Layout="Fit">
                    <Items>
                        <ext:FormPanel 
                            ID="StatusForm" 
                            runat="server"
                            LabelWidth="75"
                            ButtonAlign="Right"
                            Border="false"
                            PaddingSummary="10px 10px 10px">
                            <Defaults>                        
                                <ext:Parameter Name="AllowBlank" Value="false" Mode="Raw" />
                                <ext:Parameter Name="SelectOnFocus" Value="true" Mode="Raw" />
                                <ext:Parameter Name="MsgTarget" Value="side" />
                            </Defaults>
                            <Items>
                                <ext:FieldSet ID="FieldSet1" runat="server" Title="Identificación:" Padding="10" Height="120">
                                    <Items>
                                        <ext:Container ID="Container3" runat="server" Layout="ColumnLayout" Height="30">
                                            <Items>
                                                <ext:Container ID="Container7" runat="server" LabelAlign="Right" Layout="FormLayout" ColumnWidth=".15">
                                                    <Items>
                                                        <ext:Button runat="server" ID="btnAsignar" Text="Asignar" Icon="ArrowRefreshSmall">
                                                            <DirectEvents>
                                                                <Click OnEvent="btnAsignar_DirectClick">
                                                                    <EventMask ShowMask="true" />
                                                                </Click>
                                                            </DirectEvents>
                                                        </ext:Button>
                                                    </Items>
                                                </ext:Container>
                                                <ext:Container ID="Container11" runat="server" LabelAlign="Right" Layout="FormLayout" ColumnWidth=".25">
                                                    <Items>
                                                        <ext:ComboBox     
	                                                        ID="cbTipoDoc"                                                                              
	                                                        runat="server" 
	                                                        Shadow="Drop" 
	                                                        Mode="Local" 
	                                                        TriggerAction="All" 
	                                                        ForceSelection="true"
	                                                        DisplayField="codigo"
	                                                        ValueField="codigo"	
	                                                        EmptyText="Seleccione Tipo"
	                                                        AnchorHorizontal="99%"
	                                                        AllowBlank="false"
                                                            FieldLabel="Tipo"
                                                            DataIndex="tipo_identificacion"
	                                                        >
	                                                        <Store>
		                                                        <ext:Store ID="stTipoDoc" runat="server" AutoLoad="true">
			                                                        <Reader>
				                                                        <ext:JsonReader IDProperty="codigo">
					                                                        <Fields>
						                                                        <ext:RecordField Name="codigo" />
						                                                        <ext:RecordField Name="nombre" />
					                                                        </Fields>
				                                                        </ext:JsonReader>
			                                                        </Reader>            
		                                                        </ext:Store>    
	                                                        </Store>    
                                                        </ext:ComboBox>
                                                    </Items>
                                                </ext:Container>
                                                <ext:Container ID="Container9" runat="server" LabelAlign="Right" Layout="FormLayout" ColumnWidth=".3">
                                                    <Items>
                                                        <ext:TextField runat="server" FieldLabel="Doc.Identidad" ID="txtIdentificacion" AllowBlank="false" MinLength="3" MaxLength="15" AnchorHorizontal="90%" DataIndex="identificacion" SelectOnFocus="true" />
                                                    </Items>
                                                </ext:Container>
                                                <ext:Container ID="Container12" runat="server" LabelAlign="Right" Layout="FormLayout" ColumnWidth=".3">
                                                    <Items>
                                                        <ext:TextField runat="server" FieldLabel="Nro.Historia" ID="txtNumHis" AllowBlank="false" MinLength="3" MaxLength="15" AnchorHorizontal="90%" DataIndex="numhis" />
                                                    </Items>
                                                </ext:Container>
                                            </Items>
                                        </ext:Container>
                                        <ext:Container ID="Container13" runat="server" Layout="ColumnLayout" Height="30">
                                            <Items>
                                                <ext:Container ID="Container14" runat="server" LabelAlign="Right" Layout="FormLayout" ColumnWidth=".5">
                                                    <Items>
                                                        <ext:TextField runat="server" ID="txtPrimerNombre" FieldLabel="P.Nombre" AnchorHorizontal="99%" AllowBlank="false" MinLength="2" MaxLength="15" DataIndex="primer_nombre"  EnableKeyEvents="true">
                                                        <Listeners>
                                                                <KeyUp Fn="enterKeyPressHandler" />                                                                
                                                        </Listeners>
                                                        </ext:TextField>
                                                    </Items>
                                                </ext:Container>
                                                <ext:Container ID="Container15" runat="server" LabelAlign="Right" Layout="FormLayout" ColumnWidth=".5">
                                                    <Items>
                                                        <ext:TextField runat="server" ID="txtSegundoNombre" FieldLabel="S.Nombre" AnchorHorizontal="99%" MaxLength="15" DataIndex="segundo_nombre" EnableKeyEvents="true">
                                                        <Listeners>
                                                                <KeyUp Fn="enterKeyPressHandler" />                                                                
                                                        </Listeners>
                                                        </ext:TextField>
                                                    </Items>
                                                </ext:Container>
                                            </Items>
                                        </ext:Container>
                                        <ext:Container ID="Container16" runat="server" Layout="ColumnLayout" Height="30">
                                            <Items>
                                                <ext:Container ID="Container17" runat="server" LabelAlign="Right" Layout="FormLayout" ColumnWidth=".5">
                                                    <Items>
                                                        <ext:TextField runat="server" ID="txtPrimerApellido" FieldLabel="P.Apellido" AnchorHorizontal="99%" AllowBlank="false" MinLength="2" MaxLength="15" DataIndex="primer_apellido" EnableKeyEvents="true">
                                                        <Listeners>
                                                                <KeyUp Fn="enterKeyPressHandler" />                                                                
                                                        </Listeners>
                                                        </ext:TextField>
                                                    </Items>
                                                </ext:Container>
                                                <ext:Container ID="Container18" runat="server" LabelAlign="Right" Layout="FormLayout" ColumnWidth=".5">
                                                    <Items>
                                                        <ext:TextField runat="server" ID="txtSegundoApellido" FieldLabel="S.Apellido" AnchorHorizontal="99%" MaxLength="15" DataIndex="segundo_apellido" EnableKeyEvents="true">
                                                        <Listeners>
                                                                <KeyUp Fn="enterKeyPressHandler" />                                                                
                                                        </Listeners>
                                                        </ext:TextField>
                                                    </Items>
                                                </ext:Container>
                                            </Items>
                                        </ext:Container>
                                    </Items>
                                </ext:FieldSet>
                                <ext:TabPanel ID="TabPaciente" runat="server" ActiveTabIndex="0" Border="false" Title="Center" ResizeTabs="true">
                                    <Items>
                                        <ext:Panel ID="PGeneral" runat="server" Closable="False" Title="Datos Basicos" Hidden="False" >
                                            <Items>
                                                <ext:Container ID="Container33" runat="server" Layout="ColumnLayout" Height="10" >
                                                </ext:Container>
                                                <ext:Container ID="Container21" runat="server" Layout="ColumnLayout" Height="30" >
                                                    <Items>
                                                        <ext:Container ID="Container22" runat="server" LabelAlign="Right" Layout="FormLayout" ColumnWidth=".35" >
                                                            <Items>
                                                                <ext:ComboBox ID="cbSexo" runat="server"  FieldLabel="Sexo" AllowBlank="false" AnchorHorizontal="95%" DataIndex="sexo" >
                                                                    <Items>
                                                                        <ext:ListItem Text="Femenino" Value="f" />
                                                                        <ext:ListItem Text="Masculino" Value="m" />
                                                                    </Items>
                                                                </ext:ComboBox>
                                                            </Items>
                                                        </ext:Container>
                                                        <ext:Container ID="Container23" runat="server" LabelAlign="Right" Layout="FormLayout" ColumnWidth=".35" >
                                                            <Items>
                                                                <ext:DateField runat="server" ID="txtFechaNacimiento" FieldLabel="F.Nacimiento" AnchorHorizontal="95%" DataIndex="fecha_nacimiento" />
                                                            </Items>
                                                        </ext:Container>
                                                        
                                                    </Items>
                                                </ext:Container>
                                                <ext:Container ID="Container32" runat="server" Layout="ColumnLayout" Height="30" >
                                                    <Items>
                                                        <ext:Container ID="Container24" runat="server" LabelAlign="Right" Layout="FormLayout" ColumnWidth=".5" >
                                                            <Items>
                                                                <ext:ComboBox
                                                                    ID="cbIdLugar"
                                                                    FieldLabel="L.Nacimiento"
                                                                    runat="server"
                                                                    DisplayField="DescTotal"
                                                                    ValueField="Id_Ciudad"
                                                                    TypeAhead="false"
                                                                    LoadingText="Buscando...."
                                                                    AnchorHorizontal="90%"
                                                                    PageSize="10"
                                                                    HideTrigger="true"
                                                                    ItemSelector="div.search-item"
                                                                    MinChars="3"
                                                                    AllowBlank="false"
                                                                    BlankText="El Lugar de Nacimiento es requerido"
                                                                    EmptyText="Seleccione Lugar de Nacimiento..."
                                                                    DataIndex="id_lugar"
                                                                    >
                                                                    <Store>
                                                                        <ext:Store runat="server" AutoLoad="false" ID="Store1">
                                                                            <Proxy>
                                                                                <ext:HttpProxy Method="POST" Url=".././Funciones/UbicacionLugar.ashx" />
                                                                            </Proxy>
                                                                            <Reader>
                                                                                <ext:JsonReader Root="BarrioZona" TotalProperty="total">
                                                                                    <Fields>
                                                                                        <ext:RecordField Name="Id_Ciudad" />
                                                                                        <ext:RecordField Name="Descripcion" />    
                                                                                        <ext:RecordField Name="Desc_Dpto" />                                                                                                                         
                                                                                        <ext:RecordField Name="DescTotal" />
                                                                                    </Fields>                                                        
                                                                                </ext:JsonReader>
                                                                            </Reader>                                                
                                                                        </ext:Store>
                                                                    </Store>
                                                                    <Template runat="server" ID="Template2">
                                                                        <Html>
                                                                            <tpl for=".">
                                                                                <div class="search-item">
							                                                        <h3>{Descripcion}</h3>
                                                                                    {Desc_Dpto}
						                                                            </div>                                                        
                                                                            </tpl>                                                    
                                                                        </Html>                                            
                                                                    </Template>                                            
                                                                </ext:ComboBox>
                                                            </Items>
                                                        </ext:Container>
                                                        <ext:Container ID="Container31" runat="server" LabelAlign="Right" Layout="FormLayout" ColumnWidth=".3" >
                                                            <Items>
                                                                <ext:ComboBox ID="cbEstadoCivil" runat="server"  FieldLabel="Est.Civil" AllowBlank="false" AnchorHorizontal="70%" DataIndex="estado_civil">
                                                                    <Items>
                                                                        <ext:ListItem Text="Soltero" Value="s" />
                                                                        <ext:ListItem Text="Casado" Value="c" />
                                                                        <ext:ListItem Text="Unión Libre" Value="u" />
                                                                    </Items>
                                                                </ext:ComboBox>
                                                            </Items>
                                                        </ext:Container>
                                                    </Items>
                                                </ext:Container>
                                                <ext:Container ID="Container1" runat="server" Layout="ColumnLayout" Height="30" >
                                                    <Items>
                                                        <ext:Container ID="Container6" runat="server" LabelAlign="Right" Layout="FormLayout" ColumnWidth=".4" >
                                                            <Items>
                                                                <ext:ComboBox
                                                                    ID="cbIdLugarRes"
                                                                    FieldLabel="Residencia"
                                                                    runat="server"
                                                                    DisplayField="DescTotal"
                                                                    ValueField="Id_Ciudad"
                                                                    TypeAhead="false"
                                                                    LoadingText="Buscando...."
                                                                    AnchorHorizontal="99%"
                                                                    PageSize="10"
                                                                    HideTrigger="true"
                                                                    ItemSelector="div.search-item"       
                                                                    MinChars="3"
                                                                    AllowBlank="false"
                                                                    BlankText="Municipio donde Reside"
                                                                    EmptyText="Seleccione Municipio..."
                                                                    DataIndex="id_lugar_municipio"
                                                                    >
                                                                    <Store>
                                                                        <ext:Store runat="server" AutoLoad="false" ID="Store2">
                                                                            <Proxy>
                                                                                <ext:HttpProxy Method="POST" Url=".././Funciones/UbicacionLugar.ashx" />
                                                                            </Proxy>
                                                                            <Reader>
                                                                                <ext:JsonReader Root="BarrioZona" TotalProperty="total">
                                                                                    <Fields>
                                                                                        <ext:RecordField Name="Id_Ciudad" />
                                                                                        <ext:RecordField Name="Descripcion" />    
                                                                                        <ext:RecordField Name="Desc_Dpto" />                                                                                                                         
                                                                                        <ext:RecordField Name="DescTotal" />
                                                                                    </Fields>                                                        
                                                                                </ext:JsonReader>
                                                                            </Reader>                                                
                                                                        </ext:Store>
                                                                    </Store>
                                                                    <Template runat="server" ID="Template1">
                                                                        <Html>
                                                                            <tpl for=".">
                                                                                <div class="search-item">
							                                                        <h3>{Descripcion}</h3>
                                                                                    {Desc_Dpto}
						                                                            </div>                                                        
                                                                            </tpl>                                                    
                                                                        </Html>                                            
                                                                    </Template>                                            
                                                                </ext:ComboBox>
                                                            </Items>
                                                        </ext:Container>
                                                        <ext:Container ID="Container25" runat="server" LabelAlign="Right" Layout="FormLayout" ColumnWidth=".3" >
                                                            <Items>
                                                                <ext:ComboBox     
	                                                                ID="cbBarrioRes"                                                                              
	                                                                runat="server"
	                                                                Shadow="Drop" 
	                                                                Mode="Local" 
	                                                                TriggerAction="All" 
	                                                                ForceSelection="true"
	                                                                DisplayField="Descripcion"
	                                                                ValueField="Id"	
	                                                                EmptyText="Seleccione Barrio"
	                                                                AnchorHorizontal="99%"
	                                                                AllowBlank="true"
                                                                    FieldLabel="Barrio"
                                                                    DataIndex="id_lugar_barrio"
	                                                                >
	                                                                <Store>
		                                                                <ext:Store ID="stBarrioRes" runat="server" AutoLoad="true">
			                                                                <Reader>
				                                                                <ext:JsonReader IDProperty="Id">
					                                                                <Fields>
						                                                                <ext:RecordField Name="Id" />
						                                                                <ext:RecordField Name="Descripcion" />
					                                                                </Fields>
				                                                                </ext:JsonReader>
			                                                                </Reader>            
		                                                                </ext:Store>    
	                                                                </Store>    
                                                                </ext:ComboBox>
                                                            </Items>
                                                        </ext:Container>
                                                        <ext:Container ID="Container51" runat="server" LabelAlign="Right" Layout="FormLayout" ColumnWidth=".3" >
                                                            <Items>
                                                                <ext:ComboBox     
	                                                                ID="cbTipoResidencia"                                                                              
	                                                                runat="server" 
	                                                                Shadow="Drop" 
	                                                                Mode="Local" 
	                                                                TriggerAction="All" 
	                                                                ForceSelection="true"
	                                                                DisplayField="nombre"
	                                                                ValueField="codigo"	
	                                                                EmptyText="Seleccione Tipo Residencia"
	                                                                AnchorHorizontal="99%"
	                                                                AllowBlank="false"
                                                                    FieldLabel="Tipo"
                                                                    DataIndex="tipo_residencia"                                                                    
	                                                                >
	                                                                <Store>
		                                                                <ext:Store ID="stTipoResidencia" runat="server" AutoLoad="true">
			                                                                <Reader>
				                                                                <ext:JsonReader IDProperty="codigo">
					                                                                <Fields>
						                                                                <ext:RecordField Name="codigo" />
						                                                                <ext:RecordField Name="nombre" />
					                                                                </Fields>
				                                                                </ext:JsonReader>
			                                                                </Reader>            
		                                                                </ext:Store>    
	                                                                </Store>    
                                                                </ext:ComboBox>
                                                            </Items>
                                                        </ext:Container>
                                                        
                                                        
                                                    </Items>
                                                </ext:Container>
                                                <ext:Container ID="Container26" runat="server" Layout="ColumnLayout" Height="30" >
                                                    <Items>
                                                        <ext:Container ID="Container27" runat="server" LabelAlign="Right" Layout="FormLayout" ColumnWidth=".2" >
                                                            <Items>
                                                                <ext:ComboBox ID="cbZonaRes" runat="server"  FieldLabel="Zona" AllowBlank="false" AnchorHorizontal="95%" DataIndex="zona_residencia">
                                                                    <Items>
                                                                        <ext:ListItem Text="Urbana" Value="u" />
                                                                        <ext:ListItem Text="Rural" Value="r" />                                                                        
                                                                    </Items>
                                                                </ext:ComboBox>
                                                            </Items>
                                                        </ext:Container>
                                                        <ext:Container ID="Container28" runat="server" LabelAlign="Right" Layout="FormLayout" ColumnWidth=".25" >
                                                            <Items>
                                                                <ext:ComboBox     
	                                                                ID="cbEstrato"                                                                              
	                                                                runat="server" 
	                                                                Shadow="Drop" 
	                                                                Mode="Local" 
	                                                                TriggerAction="All" 
	                                                                ForceSelection="true"
	                                                                DisplayField="nombre"
	                                                                ValueField="codigo"	
	                                                                EmptyText="Seleccione Nivel SISBEN"
	                                                                AnchorHorizontal="95%"
	                                                                AllowBlank="false"
                                                                    FieldLabel="Nivel"
                                                                    DataIndex="estrato"
	                                                                >
	                                                                <Store>
		                                                                <ext:Store ID="stEstrato" runat="server" AutoLoad="true">
			                                                                <Reader>
				                                                                <ext:JsonReader IDProperty="codigo">
					                                                                <Fields>
						                                                                <ext:RecordField Name="codigo" />
						                                                                <ext:RecordField Name="nombre" />
					                                                                </Fields>
				                                                                </ext:JsonReader>
			                                                                </Reader>            
		                                                                </ext:Store>    
	                                                                </Store>    
                                                                </ext:ComboBox>
                                                            </Items>
                                                        </ext:Container>
                                                        <ext:Container ID="Container29" runat="server" LabelAlign="Right" Layout="FormLayout" ColumnWidth=".35" >
                                                            <Items>
                                                                <ext:TextField runat="server" ID="txtDireccion" FieldLabel="Dirección" AnchorHorizontal="99%" AllowBlank="false" MinLength="5" MaxLength="50" DataIndex="direccion" />
                                                            </Items>
                                                        </ext:Container>
                                                    </Items>
                                                </ext:Container>
                                                <ext:Container ID="Container80" runat="server" Layout="ColumnLayout" Height="30" >
                                                    <Items>
                                                        <ext:Container ID="Container30" runat="server" LabelAlign="Right" Layout="FormLayout" ColumnWidth=".35" >
                                                            <Items>
                                                                <ext:TextField runat="server" ID="txtTelefono" FieldLabel="Telefono" AnchorHorizontal="99%" AllowBlank="false" MinLength="5" MaxLength="20" DataIndex="telefono" />
                                                            </Items>
                                                        </ext:Container>
                                                        <ext:Container ID="Container52" runat="server" LabelAlign="Right" Layout="FormLayout" ColumnWidth=".2" >
                                                            <Items>
                                                                <ext:TextField runat="server" ID="txtTelefono2" FieldLabel="Telefono 2" AnchorHorizontal="50%" DataIndex="telefonoaux" />
                                                            </Items>
                                                        </ext:Container>
                                                    </Items>
                                                </ext:Container>
                                                <ext:Container ID="Container34" runat="server" Layout="ColumnLayout" Height="30" >
                                                    <Items>
                                                        <ext:Container ID="Container35" runat="server" LabelAlign="Right" Layout="FormLayout" ColumnWidth=".5" >
                                                            <Items>
                                                                <ext:ComboBox     
	                                                                ID="cbOcupacion"                                                                              
	                                                                runat="server" 
	                                                                Shadow="Drop" 
	                                                                Mode="Local" 
	                                                                TriggerAction="All" 
	                                                                ForceSelection="true"
	                                                                DisplayField="nombre"
	                                                                ValueField="codigo"	
	                                                                EmptyText="Seleccione Ocupación"
	                                                                AnchorHorizontal="95%"
	                                                                AllowBlank="false"
                                                                    FieldLabel="Ocupación"
                                                                    DataIndex="codocu"
	                                                                >
	                                                                <Store>
		                                                                <ext:Store ID="stOcupacion" runat="server" AutoLoad="true">
			                                                                <Reader>
				                                                                <ext:JsonReader IDProperty="codigo">
					                                                                <Fields>
						                                                                <ext:RecordField Name="codigo" />
						                                                                <ext:RecordField Name="nombre" />
					                                                                </Fields>
				                                                                </ext:JsonReader>
			                                                                </Reader>            
		                                                                </ext:Store>    
	                                                                </Store>    
                                                                </ext:ComboBox>
                                                            </Items>
                                                        </ext:Container>
                                                        <ext:Container ID="Container36" runat="server" LabelAlign="Right" Layout="FormLayout" ColumnWidth=".5" >
                                                            <Items>
                                                                <ext:ComboBox     
	                                                                ID="cbNivelEscolar"                                                                              
	                                                                runat="server" 
	                                                                Shadow="Drop" 
	                                                                Mode="Local" 
	                                                                TriggerAction="All" 
	                                                                ForceSelection="true"
	                                                                DisplayField="nombre"
	                                                                ValueField="codigo"	
	                                                                EmptyText="Seleccione Ocupación"
	                                                                AnchorHorizontal="95%"
	                                                                AllowBlank="false"
                                                                    FieldLabel="Escolaridad"
                                                                    DataIndex="escolar"
	                                                                >
	                                                                <Store>
		                                                                <ext:Store ID="stNivelEscolar" runat="server" AutoLoad="true">
			                                                                <Reader>
				                                                                <ext:JsonReader IDProperty="codigo">
					                                                                <Fields>
						                                                                <ext:RecordField Name="codigo" />
						                                                                <ext:RecordField Name="nombre" />
					                                                                </Fields>
				                                                                </ext:JsonReader>
			                                                                </Reader>            
		                                                                </ext:Store>    
	                                                                </Store>    
                                                                </ext:ComboBox>
                                                            </Items>
                                                        </ext:Container>
                                                    </Items>
                                                </ext:Container>
                                                <ext:Container ID="Container37" runat="server" Layout="ColumnLayout" Height="30" >
                                                    <Items>
                                                        <ext:Container ID="Container38" runat="server" LabelAlign="Right" Layout="FormLayout" ColumnWidth=".5" >
                                                            <Items>
                                                                <ext:ComboBox     
	                                                                ID="cbEtnia"                                                                              
	                                                                runat="server" 
	                                                                Shadow="Drop" 
	                                                                Mode="Local" 
	                                                                TriggerAction="All" 
	                                                                ForceSelection="true"
	                                                                DisplayField="nombre"
	                                                                ValueField="codigo"	
	                                                                EmptyText="Seleccione Etnia"
	                                                                AnchorHorizontal="95%"
	                                                                AllowBlank="false"
                                                                    FieldLabel="Etnia"
                                                                    DataIndex="etnia"
	                                                                >
	                                                                <Store>
		                                                                <ext:Store ID="stEtnia" runat="server" AutoLoad="true">
			                                                                <Reader>
				                                                                <ext:JsonReader IDProperty="codigo">
					                                                                <Fields>
						                                                                <ext:RecordField Name="codigo" />
						                                                                <ext:RecordField Name="nombre" />
					                                                                </Fields>
				                                                                </ext:JsonReader>
			                                                                </Reader>            
		                                                                </ext:Store>    
	                                                                </Store>    
                                                                </ext:ComboBox>
                                                            </Items>
                                                        </ext:Container>
                                                        <ext:Container ID="Container39" runat="server" LabelAlign="Right" Layout="FormLayout" ColumnWidth=".5" >
                                                            <Items>
                                                                <ext:ComboBox     
	                                                                ID="cbReligion"                                                                              
	                                                                runat="server" 
	                                                                Shadow="Drop" 
	                                                                Mode="Local" 
	                                                                TriggerAction="All" 
	                                                                ForceSelection="true"
	                                                                DisplayField="nombre"
	                                                                ValueField="codigo"	
	                                                                EmptyText="Seleccione Religión"
	                                                                AnchorHorizontal="95%"
	                                                                AllowBlank="false"
                                                                    FieldLabel="Religión"
                                                                    DataIndex="religion"
	                                                                >
	                                                                <Store>
		                                                                <ext:Store ID="stReligion" runat="server" AutoLoad="true">
			                                                                <Reader>
				                                                                <ext:JsonReader IDProperty="codigo">
					                                                                <Fields>
						                                                                <ext:RecordField Name="codigo" />
						                                                                <ext:RecordField Name="nombre" />
					                                                                </Fields>
				                                                                </ext:JsonReader>
			                                                                </Reader>            
		                                                                </ext:Store>    
	                                                                </Store>    
                                                                </ext:ComboBox>
                                                            </Items>
                                                        </ext:Container>
                                                    </Items>
                                                </ext:Container>
                                                <ext:Container ID="Container40" runat="server" Layout="ColumnLayout" Height="30" >
                                                    <Items>
                                                        <ext:Container ID="Container41" runat="server" LabelAlign="Right" Layout="FormLayout" ColumnWidth=".7" >
                                                            <Items>
                                                                <ext:ComboBox     
	                                                                ID="cbEntidadSalud"                                                                              
	                                                                runat="server" 
	                                                                Shadow="Drop" 
	                                                                Mode="Local" 
	                                                                TriggerAction="All" 
	                                                                ForceSelection="true"
	                                                                DisplayField="nombre"
	                                                                ValueField="codigo"	
	                                                                EmptyText="Seleccione Entidad"
	                                                                AnchorHorizontal="95%"
	                                                                AllowBlank="false"
                                                                    FieldLabel="Eps/Ars"
                                                                    DataIndex="codemp"
	                                                                >
	                                                                <Store>
		                                                                <ext:Store ID="stEntidadSalud" runat="server" AutoLoad="true">
			                                                                <Reader>
				                                                                <ext:JsonReader IDProperty="codigo">
					                                                                <Fields>
						                                                                <ext:RecordField Name="codigo" />
						                                                                <ext:RecordField Name="nombre" />
					                                                                </Fields>
				                                                                </ext:JsonReader>
			                                                                </Reader>            
		                                                                </ext:Store>    
	                                                                </Store>    
                                                                </ext:ComboBox>
                                                            </Items>
                                                        </ext:Container>
                                                        <ext:Container ID="Container42" runat="server" LabelAlign="Right" Layout="FormLayout" ColumnWidth=".3" >
                                                            <Items>
                                                                <ext:TextField runat="server" ID="txtNumCarne" FieldLabel="Nro.Carné" AnchorHorizontal="99%" DataIndex="nrocon" />
                                                            </Items>
                                                        </ext:Container>
                                                    </Items>
                                                </ext:Container>
                                            </Items>
                                        </ext:Panel>
                                        <ext:Panel ID="PGeneral2" runat="server" Closable="False" Title="Otros Datos Personales" Hidden="False">
                                            <Items>
                                                <ext:Container ID="Container46" runat="server" Layout="ColumnLayout" Height="10" >
                                                </ext:Container>
                                                <ext:Container ID="Container47" runat="server" Layout="ColumnLayout" Height="20" >
                                                    <Items>
                                                        <ext:Container ID="Container48" runat="server" LabelAlign="Right" Layout="FormLayout" ColumnWidth=".5" >
                                                            <Items>
                                                                <ext:Label runat="server" ID="lblArp" Text="Aseg. Riesgos Profesionales" />
                                                            </Items>
                                                        </ext:Container>
                                                        <ext:Container ID="Container49" runat="server" LabelAlign="Right" Layout="FormLayout" ColumnWidth=".5" >
                                                            <Items>
                                                                <ext:Label runat="server" ID="Label1" Text="Caja de Compensación Familiar" />
                                                            </Items>
                                                        </ext:Container>
                                                    </Items>
                                                </ext:Container>
                                                <ext:Container ID="Container19" runat="server" Layout="ColumnLayout" Height="30" >
                                                    <Items>
                                                        <ext:Container ID="Container20" runat="server" LabelAlign="Right" Layout="FormLayout" ColumnWidth=".5" >
                                                            <Items>
                                                                <ext:ComboBox     
	                                                                ID="cbEmpArp"                                                                              
	                                                                runat="server" 
	                                                                Shadow="Drop" 
	                                                                Mode="Local" 
	                                                                TriggerAction="All" 
	                                                                ForceSelection="true"
	                                                                DisplayField="razonsocial"
	                                                                ValueField="codigo"	
	                                                                EmptyText="Seleccione Aseg. Riesgos"
	                                                                AnchorHorizontal="95%"	                                                 
                                                                    DataIndex="codarp"
	                                                                >
	                                                                <Store>
		                                                                <ext:Store ID="stEmpArp" runat="server" AutoLoad="true">
			                                                                <Reader>
				                                                                <ext:JsonReader IDProperty="codigo">
					                                                                <Fields>
						                                                                <ext:RecordField Name="codigo" />
						                                                                <ext:RecordField Name="razonsocial" />
					                                                                </Fields>
				                                                                </ext:JsonReader>
			                                                                </Reader>            
		                                                                </ext:Store>    
	                                                                </Store>    
                                                                </ext:ComboBox>
                                                            </Items>
                                                        </ext:Container>
                                                        <ext:Container ID="Container50" runat="server" LabelAlign="Right" Layout="FormLayout" ColumnWidth=".5" >
                                                            <Items>
                                                                <ext:ComboBox     
	                                                                ID="cmEmpCcf"                                                                              
	                                                                runat="server" 
	                                                                Shadow="Drop" 
	                                                                Mode="Local" 
	                                                                TriggerAction="All" 
	                                                                ForceSelection="true"
	                                                                DisplayField="razonsocial"
	                                                                ValueField="codigo"	
	                                                                EmptyText="Seleccione Caja de Compensación"
	                                                                AnchorHorizontal="95%"
                                                                    DataIndex="codarp"                                                                    
	                                                                >
	                                                                <Store>
		                                                                <ext:Store ID="stEmpCcf" runat="server" AutoLoad="true">
			                                                                <Reader>
				                                                                <ext:JsonReader IDProperty="codigo">
					                                                                <Fields>
						                                                                <ext:RecordField Name="codigo" />
						                                                                <ext:RecordField Name="razonsocial" />
					                                                                </Fields>
				                                                                </ext:JsonReader>
			                                                                </Reader>            
		                                                                </ext:Store>    
	                                                                </Store>    
                                                                </ext:ComboBox>
                                                            </Items>
                                                        </ext:Container>
                                                    </Items>
                                                </ext:Container>
                                                <ext:Container ID="Container53" runat="server" Layout="ColumnLayout" Height="50" >
                                                    <Items>
                                                        <ext:Container ID="Container54" runat="server" LabelAlign="Right" Layout="FormLayout" ColumnWidth=".25" >
                                                            <Items>
                                                                <ext:TextArea runat="server" ID="txtObservaciones" FieldLabel="Observaciones" AnchorHorizontal="100%" AnchorVertical="90%" DataIndex="observaciones" />
                                                            </Items>
                                                        </ext:Container>
                                                    </Items>                            
                                                </ext:Container>
                                                <ext:Container ID="Container56" runat="server" Layout="ColumnLayout" Height="50" >
                                                    <Items>
                                                        <ext:Container ID="Container57" runat="server" LabelAlign="Right" Layout="FormLayout" ColumnWidth=".25" >
                                                            <Items>
                                                                <ext:ComboBox     
	                                                                ID="ComboBox1"                                                                              
	                                                                runat="server" 
	                                                                Shadow="Drop" 
	                                                                Mode="Local" 
	                                                                TriggerAction="All" 
	                                                                ForceSelection="true"
	                                                                DisplayField="nombre"
	                                                                ValueField="codigo"	
	                                                                EmptyText="Seleccione Discapacidad"
	                                                                AnchorHorizontal="95%"
	                                                                AllowBlank="false"
                                                                    FieldLabel="Discapacacidad"
                                                                    DataIndex="discapacidad"
	                                                                >
	                                                                <Store>
		                                                                <ext:Store ID="stDiscapacidad" runat="server" AutoLoad="true">
			                                                                <Reader>
				                                                                <ext:JsonReader IDProperty="codigo">
					                                                                <Fields>
						                                                                <ext:RecordField Name="codigo" />
						                                                                <ext:RecordField Name="nombre" />
					                                                                </Fields>
				                                                                </ext:JsonReader>
			                                                                </Reader>            
		                                                                </ext:Store>    
	                                                                </Store>    
                                                                </ext:ComboBox>

                                                            </Items>
                                                        </ext:Container>
                                                    </Items>                            
                                                </ext:Container>

                                            </Items>
                                        </ext:Panel>
                                        <ext:Panel ID="PParientes" runat="server" Closable="False" Title="Parientes" Hidden="true">
                                            <Items>
                                                <ext:Container ID="Container43" runat="server" Layout="ColumnLayout" Height="10" >
                                                </ext:Container>
                                                <ext:Container ID="Container44" runat="server" Layout="ColumnLayout" Height="30" >
                                                    <Items>
                                                        <ext:Container ID="Container45" runat="server" LabelAlign="Right" Layout="FormLayout" ColumnWidth=".35" >
                                                            <Items>
                                                            </Items>
                                                        </ext:Container>
                                                    </Items>
                                                </ext:Container>                                                        
                                            </Items>
                                        </ext:Panel>
                                    </Items>
                                </ext:TabPanel>
                            </Items>
                            <Buttons>
                            <ext:Button ID="btnGuardaHis" runat="server" Text="Guardar" Icon="Disk">
                                <DirectEvents>
                                    <Click 
                                        OnEvent="btnGuardaHis_DirectClick" 
                                        Before="var valid= #{StatusForm}.getForm().isValid(); if (valid) {#{FormStatusBar}.showBusy('Guardando Formulario...');} return valid;">
                                        <EventMask 
                                            ShowMask="true" 
                                            MinDelay="1000" 
                                            Target="CustomTarget" 
                                            CustomTarget="={#{StatusForm}.getEl()}" 
                                            />
                                    </Click>
                                </DirectEvents>
                            </ext:Button>
                            <ext:Button runat="server" ID="btnCerrarHis" Text="Cerrar" />
                        </Buttons>
                    </ext:FormPanel>
                </Items>
                    <BottomBar>
                        <ext:StatusBar ID="FormStatusBar" runat="server" DefaultText="Ready">
                            <Plugins>
                                <ext:ValidationStatus ID="ValidationStatus1" 
                                    runat="server" 
                                    FormPanelID="StatusForm" 
                                    ValidIcon="Accept" 
                                    ErrorIcon="Exclamation" 
                                    ShowText="El Formulario tiene Errores (Click para ver Detalles)"
                                    HideText="Click para Ocultar Errores"
                                    />
                            </Plugins>
                        </ext:StatusBar>
                    </BottomBar>
                </ext:Panel>
            </Items>
        </ext:Window>
    </form>
</body>
</html>

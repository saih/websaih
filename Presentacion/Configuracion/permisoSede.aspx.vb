﻿Imports Ext.Net
Public Class permisoSede
    Inherits System.Web.UI.Page


    Protected Property Accion As String
        Get
            Return ViewState("Accion")
        End Get
        Set(ByVal value As String)
            ViewState.Add("Accion", value)
        End Set
    End Property

    Protected Property UsuarioSede As String
        Get
            Return ViewState("UsuarioSede")
        End Get
        Set(ByVal value As String)
            ViewState.Add("UsuarioSede", value)
        End Set
    End Property

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        cargaCombos()
    End Sub

    Protected Sub rowSelect(ByVal sender As Object, ByVal e As DirectEventArgs)
        UsuarioSede = e.ExtraParams("ID")
    End Sub

    Protected Sub btnBuscar_DirectClick(ByVal sender As Object, ByVal e As Ext.Net.DirectEventArgs)
        cargaUsuariosSede()
    End Sub
    Protected Sub btnLimpiar_DirectClick(ByVal sender As Object, ByVal e As Ext.Net.DirectEventArgs)
        limpiarCampos()
    End Sub
    Protected Sub btnAsignar_DirectClick(ByVal sender As Object, ByVal e As Ext.Net.DirectEventArgs)

    End Sub

    Protected Sub btnGuardaUsuSede_DirectClick(ByVal sender As Object, ByVal e As Ext.Net.DirectEventArgs)
        Dim negUsuSede As New Negocio.NegocioUsuarioSede
        Dim usuSede As New Datos.UsuarioSede

        If Accion = "Insert" Then
            Reflexion.valoresForma(usuSede, vntUsuSede)            

            If negUsuSede.AltaUsuarioSede(usuSede) Then
                Ext.Net.X.Msg.Alert("Información", "Registro Guardado Correctamente").Show()
                vntUsuSede.Hide()
                cargaUsuariosSede()
            End If
        Else
            usuSede = negUsuSede.ObtenerUsuarioSedeById(UsuarioSede)
            Reflexion.valoresForma(usuSede, vntUsuSede)
            If negUsuSede.EditaUsuarioSede(usuSede) Then
                Ext.Net.X.Msg.Alert("Información", "Registro Guardado Correctamente").Show()
                vntUsuSede.Hide()
                cargaUsuariosSede()
            End If
        End If
    End Sub

    <DirectMethod()>
    Public Function eliminarUsuSede()
        Dim negUsuSede As New Negocio.NegocioUsuarioSede
        negUsuSede.EliminaUsuarioSede(UsuarioSede)
        cargaUsuariosSede()
        Return True
    End Function

    Private Sub btnEliminaUsuSede_DirectClick(ByVal sender As Object, ByVal e As Ext.Net.DirectEventArgs) Handles btnEliminaUsuSede.DirectClick
        Dim js As String = "Ext.MessageBox.confirm('Confirmación', '¿Esta seguro de eliminar?', " & _
           "function (btn) { " & _
                " if (btn == 'yes') " & _
                "   Ext.net.DirectMethods.eliminarUsuSede(); " & _
                " });"
        Ext.Net.X.Js.AddScript(js)
    End Sub

    Private Sub btnNuevoUsuSede_DirectClick(ByVal sender As Object, ByVal e As Ext.Net.DirectEventArgs) Handles btnNuevoUsuSede.DirectClick
        Accion = "Insert"
        cargaCombos()
        presentacionUtil.limpiarObjetos(vntUsuSede)
        vntUsuSede.Show()
    End Sub

    Private Sub btnEditaUsuSede_DirectClick(ByVal sender As Object, ByVal e As Ext.Net.DirectEventArgs) Handles btnEditaUsuSede.DirectClick
        Accion = "Edit"
        Dim negUsuSede As New Negocio.NegocioUsuarioSede
        Dim usuSede As New Datos.UsuarioSede
        usuSede = negUsuSede.ObtenerUsuarioSedeById(UsuarioSede)
        cargaCombos()
        Reflexion.formaValores(usuSede, vntUsuSede)
        vntUsuSede.Show()
    End Sub

    Private Function cargaCombos()
        Dim negUsu As New Negocio.NegocioUsuario
        Dim sWhere As String = " where Usuario <> '' "
        Dim negSede As New Negocio.Negociosaih_prm
        Dim sWhere1 As String = " where codpre <> '' "

        stUsuario.DataSource = negUsu.ObtenerUsuariobyWhere(sWhere)
        stUsuario.DataBind()

        stUsuarioSede.DataSource = negUsu.ObtenerUsuariobyWhere(sWhere)
        stUsuarioSede.DataBind()

        stSede.DataSource = negSede.Obtenersaih_prmbyWhere(sWhere1)
        stSede.DataBind()

        stSedeB.DataSource = negSede.Obtenersaih_prmbyWhere(sWhere1)
        stSedeB.DataBind()

        Return True
    End Function
    Private Function limpiarCampos()
        cbxUsuario.Clear()
        cbSedeB.Clear()
        Return True
    End Function


    Private Function cargaUsuariosSede()
        Dim negUsuSede As New Negocio.NegocioUsuarioSede
        Dim sWhere As String = "select a.UsuarioSede,b.razonsocial,c.lugar from UsuarioSede a inner " & _
                                "join  Usuario b on a.idUsuario = b.Usuario left join saih_prm c on a.idSede=c.codpre " & _
                                "where a.UsuarioSede <> '' "
        If cbxUsuario.Value <> "" Then
            sWhere &= " and a.idUsuario = '" & cbxUsuario.Value & "'"
        End If

        If cbSedeB.Value <> "" Then
            sWhere &= " and a.idSede = '" & cbSedeB.Value & "'"
        End If
 
        Dim spFunc As New Datos.spFunciones
        stUsuSede.DataSource = spFunc.executaSelect(sWhere)
        stUsuSede.DataBind()

        Return True
    End Function

    Private Sub btnCerrarUsuSede_DirectClick(ByVal sender As Object, ByVal e As Ext.Net.DirectEventArgs) Handles btnCerrarUsuSede.DirectClick
        vntUsuSede.Hide()
    End Sub
End Class
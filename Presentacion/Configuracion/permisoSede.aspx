﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="permisoSede.aspx.vb" Inherits="Presentacion.permisoSede" %>

<%@ Register Assembly="Ext.Net" Namespace="Ext.Net" TagPrefix="ext" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
    <ext:ResourceManager ID="ResourceManager1" runat="server" />
            <ext:Store 
            ID="stUsuSede" 
            runat="server" 
            AutoLoad="true">
            <Reader>
                <ext:JsonReader IDProperty="UsuarioSede">
                    <Fields>
                        <ext:RecordField Name="UsuarioSede" />
                        <ext:RecordField Name="razonsocial" />
                        <ext:RecordField Name="lugar" />
                    </Fields>
                </ext:JsonReader>
            </Reader>
        </ext:Store>
        <ext:Panel ID="Panel03" runat="server" Frame="true" PaddingSummary="5px 5px 0" AutoWidth="true" Height="650" ButtonAlign="Center" Style="text-align: left" Title="Permiso Sede">
            <Items>
                <ext:FieldSet ID="FieldSet03" runat="server" Title="Campos de Busqueda" Padding="10" Height="60">
                    <Items>
                        <ext:Container ID="Container10" runat="server" Layout="ColumnLayout" Height="30">
                            <Items>
                                <ext:Container ID="Container08" runat="server" LabelAlign="Right" Layout="FormLayout" ColumnWidth=".25">
                                    <Items>
                                           <ext:ComboBox     
	                                                                ID="cbxUsuario"                                                                              
	                                                                runat="server" 
	                                                                Shadow="Drop" 
	                                                                Mode="Local" 
	                                                                TriggerAction="All" 
	                                                                ForceSelection="true"
	                                                                DisplayField="razonsocial"
	                                                                ValueField="Usuario"	
	                                                                EmptyText="Seleccione Usuario"
	                                                                AnchorHorizontal="95%"
	                                                                AllowBlank="false"
                                                                    FieldLabel="Usuario"
                                                                    DataIndex="idUsuario"
	                                                                >
	                                                                <Store>
		                                                                <ext:Store ID="stUsuarioSede" runat="server" AutoLoad="true">
			                                                                <Reader>
				                                                                <ext:JsonReader IDProperty="razonsocial">
					                                                                <Fields>
						                                                                <ext:RecordField Name="razonsocial" />	
                                                                                        <ext:RecordField Name="Usuario" />							                                                                
					                                                                </Fields>
				                                                                </ext:JsonReader>
			                                                                </Reader>            
		                                                                </ext:Store>    
	                                                                </Store>    
                                                                </ext:ComboBox>
                                    </Items>    
                                </ext:Container>
                                <ext:Container ID="Container02" runat="server" LabelAlign="Right" Layout="FormLayout" ColumnWidth=".25">
                                    <Items>
                                                                <ext:ComboBox     
	                                                                ID="cbSedeB"                                                                              
	                                                                runat="server" 
	                                                                Shadow="Drop" 
	                                                                Mode="Local" 
	                                                                TriggerAction="All" 
	                                                                ForceSelection="true"
	                                                                DisplayField="lugar"
	                                                                ValueField="codpre"	
	                                                                EmptyText="Seleccione Sede"
	                                                                AnchorHorizontal="95%"
	                                                                AllowBlank="false"
                                                                    FieldLabel="Sede"
                                                                    DataIndex="idSede"
	                                                                >
	                                                                <Store>
		                                                                <ext:Store ID="stSedeB" runat="server" AutoLoad="true">
			                                                                <Reader>
				                                                                <ext:JsonReader IDProperty="sede">
					                                                                <Fields>
						                                                                <ext:RecordField Name="lugar" />
						                                                                <ext:RecordField Name="codpre" />
					                                                                </Fields>
				                                                                </ext:JsonReader>
			                                                                </Reader>            
		                                                                </ext:Store>    
	                                                                </Store>    
                                                                </ext:ComboBox>

                                    </Items>
                                </ext:Container>
                                <ext:Container ID="Container5" runat="server" LabelAlign="Right" Layout="FormLayout" ColumnWidth=".2">
                                    <Items>
                                        <ext:Button runat="server" ID="btnBuscar" Text="Buscar" Icon="ApplicationGo" >
                                            <DirectEvents>
                                                <Click OnEvent="btnBuscar_DirectClick" Before="">
                                                    <EventMask ShowMask="true" />
                                                </Click>
                                            </DirectEvents>
                                        </ext:Button>
                                    </Items>
                                </ext:Container>
                                <ext:Container ID="Container1" runat="server" LabelAlign="Right" Layout="FormLayout" ColumnWidth=".2">
                                    <Items>
                                        <ext:Button runat="server" ID="btnLimpiar" Text="Limpiar Campos" Icon="ApplicationGo" >
                                            <DirectEvents>
                                                <Click OnEvent="btnLimpiar_DirectClick" Before="">
                                                    <EventMask ShowMask="true" />
                                                </Click>
                                            </DirectEvents>
                                        </ext:Button>
                                    </Items>
                                </ext:Container>

                            </Items>
                        </ext:Container>                        
                    </Items>
                </ext:FieldSet>
                <ext:GridPanel
                ID="grdUsuSede"
                runat="server"                                            
                Margins="0 0 5 5"
                Icon="TransmitGo"
                Frame="true"
                Height="400"
                StoreId="stUsuSede"
                Title="Usuario Sede"
                >
                <ColumnModel ID="ColumnModel03" runat="server">
                    <Columns>                        
                        <ext:Column Header="Código" DataIndex="UsuarioSede" />
                        <ext:Column Header="Usuario" DataIndex="razonsocial" />
                        <ext:Column Header="Sede" DataIndex="lugar" />
                    </Columns>                                
                </ColumnModel>
                <SelectionModel>
                    <ext:RowSelectionModel ID="RowSelectionModel1" runat="server" SingleSelect="true" >
                        <DirectEvents>
                            <RowSelect OnEvent="rowSelect" Buffer="100">
                                <ExtraParams>                                    
                                    <ext:Parameter Name="ID" Value="this.getSelected().id" Mode="Raw" />
                                </ExtraParams>
                            </RowSelect>
                        </DirectEvents>
                    </ext:RowSelectionModel>
                </SelectionModel>
                <LoadMask ShowMask="true" Msg="Loading...." />
                <BottomBar>
                    <ext:StatusBar runat="server" ID="StatusBar2">
                        <Items>                                                        
                            <ext:Button runat="server" Text="Nuevo Permiso" ID="btnNuevoUsuSede" Icon="Add" />
                            <ext:Button runat="server" Text="Editar Permiso" ID="btnEditaUsuSede" Icon="ApplicationEdit" />
                            <ext:Button runat="server" Text="Eliminar Permiso" ID="btnEliminaUsuSede" Icon="ApplicationDelete" />
                        </Items>
                    </ext:StatusBar>
                </BottomBar>
            </ext:GridPanel>
            </Items>
        </ext:Panel>


            <ext:Window 
            ID="vntUsuSede"
            runat="server"
            Icon="ApplicationFormEdit"
            Width="850"
            Height="480"
            Hidden="true" 
            Modal="true"            
            Title="Registro UsuarioSede"
            Constrain="true">
            <Items>
                <ext:Panel ID="Panel1" 
                    runat="server" 
                    Title=""
                    AnchorHorizontal="100%"
                    Height="450"
                    Layout="Fit">
                    <Items>
                        <ext:FormPanel 
                            ID="StatusForm" 
                            runat="server"
                            LabelWidth="75"
                            ButtonAlign="Right"
                            Border="false"
                            PaddingSummary="10px 10px 10px">
                            <Defaults>                        
                                <ext:Parameter Name="AllowBlank" Value="false" Mode="Raw" />
                                <ext:Parameter Name="SelectOnFocus" Value="true" Mode="Raw" />
                                <ext:Parameter Name="MsgTarget" Value="side" />
                            </Defaults>
                            <Items>
            
                                <ext:TabPanel ID="TabUsuarioSede" runat="server" ActiveTabIndex="0" Border="false" Title="Center" ResizeTabs="true">
                                    <Items>
                                        <ext:Panel ID="PGeneral" runat="server" Closable="False" Title="Datos Basicos" Hidden="False" >
                                            <Items>
                                                <ext:Container ID="Container33" runat="server" Layout="ColumnLayout" Height="10" >
                                                </ext:Container>                                                
                                                <ext:Container ID="Container34" runat="server" Layout="ColumnLayout" Height="30" >
                                                    <Items>
                                                        <ext:Container ID="Container35" runat="server" LabelAlign="Right" Layout="FormLayout" ColumnWidth=".5" >
                                                            <Items>
                                                                <ext:ComboBox     
	                                                                ID="cbUsuario"                                                                              
	                                                                runat="server" 
	                                                                Shadow="Drop" 
	                                                                Mode="Local" 
	                                                                TriggerAction="All" 
	                                                                ForceSelection="true"
	                                                                DisplayField="razonsocial"
	                                                                ValueField="Usuario"	
	                                                                EmptyText="Seleccione Usuario"
	                                                                AnchorHorizontal="95%"
	                                                                AllowBlank="false"
                                                                    FieldLabel="Usuario"
                                                                    DataIndex="idUsuario"
	                                                                >
	                                                                <Store>
		                                                                <ext:Store ID="stUsuario" runat="server" AutoLoad="true">
			                                                                <Reader>
				                                                                <ext:JsonReader IDProperty="razonsocial">
					                                                                <Fields>
						                                                                <ext:RecordField Name="razonsocial" />	
                                                                                        <ext:RecordField Name="Usuario" />							                                                                
					                                                                </Fields>
				                                                                </ext:JsonReader>
			                                                                </Reader>            
		                                                                </ext:Store>    
	                                                                </Store>    
                                                                </ext:ComboBox>
                                                            </Items>
                                                        </ext:Container>
                                                        <ext:Container ID="Container36" runat="server" LabelAlign="Right" Layout="FormLayout" ColumnWidth=".5" >
                                                            <Items>
                                                                <ext:ComboBox     
	                                                                ID="cbSede"                                                                              
	                                                                runat="server" 
	                                                                Shadow="Drop" 
	                                                                Mode="Local" 
	                                                                TriggerAction="All" 
	                                                                ForceSelection="true"
	                                                                DisplayField="lugar"
	                                                                ValueField="codpre"	
	                                                                EmptyText="Seleccione Sede"
	                                                                AnchorHorizontal="95%"
	                                                                AllowBlank="false"
                                                                    FieldLabel="sede"
                                                                    DataIndex="idSede"
	                                                                >
	                                                                <Store>
		                                                                <ext:Store ID="stSede" runat="server" AutoLoad="true">
			                                                                <Reader>
				                                                                <ext:JsonReader IDProperty="sede">
					                                                                <Fields>
						                                                                <ext:RecordField Name="lugar" />
						                                                                <ext:RecordField Name="codpre" />
					                                                                </Fields>
				                                                                </ext:JsonReader>
			                                                                </Reader>            
		                                                                </ext:Store>    
	                                                                </Store>    
                                                                </ext:ComboBox>
                                                            </Items>
                                                        </ext:Container>
                                                    </Items>
                                                </ext:Container>
                                            </Items>
                                        </ext:Panel>
                                        
                                    </Items>
                                </ext:TabPanel>
                            </Items>
                            <Buttons>
                            <ext:Button ID="btnGuardaUsuSede" runat="server" Text="Guardar" Icon="Disk">
                                <DirectEvents>
                                    <Click 
                                        OnEvent="btnGuardaUsuSede_DirectClick" 
                                        Before="var valid= #{StatusForm}.getForm().isValid(); if (valid) {#{FormStatusBar}.showBusy('Guardando Formulario...');} return valid;">
                                        <EventMask 
                                            ShowMask="true" 
                                            MinDelay="1000" 
                                            Target="CustomTarget" 
                                            CustomTarget="={#{StatusForm}.getEl()}" 
                                            />
                                    </Click>
                                </DirectEvents>
                            </ext:Button>
                            <ext:Button runat="server" ID="btnCerrarUsuSede" Text="Cerrar" />
                        </Buttons>
                    </ext:FormPanel>
                </Items>
                    <BottomBar>
                        <ext:StatusBar ID="FormStatusBar" runat="server" DefaultText="Ready">
                            <Plugins>
                                <ext:ValidationStatus ID="ValidationStatus1" 
                                    runat="server" 
                                    FormPanelID="StatusForm" 
                                    ValidIcon="Accept" 
                                    ErrorIcon="Exclamation" 
                                    ShowText="El Formulario tiene Errores (Click para ver Detalles)"
                                    HideText="Click para Ocultar Errores"
                                    />
                            </Plugins>
                        </ext:StatusBar>
                    </BottomBar>
                </ext:Panel>
            </Items>
        </ext:Window>
    <div>
        
    </div>
    </form>
</body>
</html>

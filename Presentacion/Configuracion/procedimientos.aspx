﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="procedimientos.aspx.vb" Inherits="Presentacion.procedimientos" %>

<%@ Register Assembly="Ext.Net" Namespace="Ext.Net" TagPrefix="ext" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <ext:XScript ID="XScript1" runat="server">
        <script type="text/javascript">
            var addTar = function () {
                var grid = #{grdTar};
                grid.getRowEditor().stopEditing();
                
                grid.insertRecord(0, {
                    id     : 0, 
                    manual   : "",
                    id_plan  : "",
                    valor  : 0,
                    valsmv : 0,
                    porincremento : 0
                });
                
                grid.getView().refresh();
                grid.getSelectionModel().selectRow(0);
                grid.getRowEditor().startEditing(0);
            }

            var fmtPlan = function(val, params, record){		        
                val = record.get('id_plan')
		        var opts = stPlan.data.items;
		        for(var i = 0, len = opts.length;i < len; i++){                    
			        if (opts[i].data.codigo === record.get('id_plan')){				        
				        return opts[i].data.nombre;
			        }
		        }
		        return val;
	        };

            var fmtManual = function(val, params, record){		        
                val = record.get('manual')
		        var opts = stManual.data.items;
		        for(var i = 0, len = opts.length;i < len; i++){                    
			        if (opts[i].data.codigo === record.get('manual')){				        
				        return opts[i].data.nombre;
			        }
		        }
		        return val;
	        };

            var cancelTar = function(){
                Ext.net.DirectMethods.cancelarTar();
            }
            
            var removeTar = function () {
                
                var grid = #{grdTar};
                grid.getRowEditor().stopEditing();
                
                var s = grid.getSelectionModel().getSelections();
                
                for (var i = 0, r; r = s[i]; i++) {                    
                    Ext.net.DirectMethods.btnEliminaTar_DirectClick(r.data.id);
                }
            }

            var editaTar = function(val, params, record){
                Ext.net.DirectMethods.insertaTar(record.get('id'),record.get('manual'),record.get('id_plan'),record.get('valor'),
                                                 record.get('valsmv'),record.get('porincremento'));                
            }
        </script>
    </ext:XScript>
</head>
<body>
    <form id="form1" runat="server">
        <ext:ResourceManager ID="ResourceManager1" runat="server" />

        <ext:Store 
            ID="stProcedimientos" 
            runat="server" 
            AutoLoad="true">
            <Reader>
                <ext:JsonReader>
                    <Fields>                        
                        <ext:RecordField Name="manual" />
                        <ext:RecordField Name="codigo" />
                        <ext:RecordField Name="nombre" />                        
                    </Fields>
                </ext:JsonReader>
            </Reader>
        </ext:Store>

        <ext:Panel ID="Panel3" runat="server" Frame="true" PaddingSummary="5px 5px 0" AutoWidth="true" Height="650" ButtonAlign="Center" Style="text-align: left" Title="Procedimientos">
            <Items>
                <ext:FieldSet ID="FieldSet3" runat="server" Title="Campos de Busqueda" Padding="10" Height="60">
                    <Items>
                        <ext:Container ID="Container10" runat="server" Layout="ColumnLayout" Height="30">
                            <Items>
                                <ext:Container ID="Container8" runat="server" LabelAlign="Right" Layout="FormLayout" ColumnWidth=".25">
                                    <Items>
                                        <ext:TextField runat="server" ID="txtCodigoBsq" FieldLabel="Codigo" />
                                    </Items>
                                </ext:Container>
                                <ext:Container ID="Container1" runat="server" LabelAlign="Right" Layout="FormLayout" ColumnWidth=".3">
                                    <Items>
                                        <ext:TextField runat="server" ID="txtNombreBsq" FieldLabel="Nombre" AnchorHorizontal="99%" />
                                    </Items>
                                </ext:Container>
                                <ext:Container ID="Container2" runat="server" LabelAlign="Right" Layout="FormLayout" ColumnWidth=".2">
                                    <Items>
                                        <ext:Button runat="server" ID="btnBuscar" Text="Buscar" Icon="ApplicationGo" >
                                        <DirectEvents>
                                            <Click OnEvent="btnBuscar_DirectClick">
                                                <EventMask ShowMask="true" />
                                            </Click>
                                        </DirectEvents>
                                    </ext:Button>
                                    </Items>
                                </ext:Container>
                            </Items>
                        </ext:Container>
                    </Items>
                </ext:FieldSet>
                <ext:GridPanel
                    ID="grdProcedimientos"
                    runat="server"                                            
                    Margins="0 0 5 5"
                    Icon="VectorAdd"
                    Frame="true"
                    Height="400"
                    StoreId="stProcedimientos"
                    Title="Procedimientos"
                    >
                    <ColumnModel ID="ColumnModel3" runat="server">
                        <Columns>                            
                            <ext:Column Header="Manual" DataIndex="manual" />
                            <ext:Column Header="Codigo" DataIndex="codigo" />                            
                            <ext:Column Header="Nombre" DataIndex="nombre" Width="380"/>
                        </Columns>                                
                    </ColumnModel>
                    <SelectionModel>
                        <ext:RowSelectionModel ID="RowSelectionModel1" runat="server" SingleSelect="true" >
                            <DirectEvents>
                                <RowSelect OnEvent="rowSelect" Buffer="100">
                                    <ExtraParams>                                                                            
                                        <ext:Parameter Name="manual" Value="this.getSelected().get('manual')" Mode="Raw" />
                                        <ext:Parameter Name="codigo" Value="this.getSelected().get('codigo')" Mode="Raw" />
                                    </ExtraParams>
                                </RowSelect>
                            </DirectEvents>
                        </ext:RowSelectionModel>
                    </SelectionModel>
                    <LoadMask ShowMask="true" Msg="Loading...." />
                    <BottomBar>
                        <ext:StatusBar runat="server" ID="StatusBar2">
                            <Items>                                                        
                                <ext:Button runat="server" Text="Nueva Procedimiento" ID="btnNuevoProcedimiento" Icon="Add" />
                                <ext:Button runat="server" Text="Editar Procedimiento" ID="btnEditaProcedimiento" Icon="ApplicationEdit" />
                                <ext:Button runat="server" Text="Eliminar Procedimiento" ID="btnEliminaProcedimiento" Icon="ApplicationDelete" />
                            </Items>
                        </ext:StatusBar>
                    </BottomBar>
                </ext:GridPanel>
            </Items>
        </ext:Panel>

        <ext:Window 
        ID="vntProcedimiento"
        runat="server"
        Icon="ApplicationFormEdit"
        Width="900"
        Height="600"
        Hidden="true" 
        Modal="true"            
        Title="Procedimiento"
        Constrain="true">
        <Items>
            <ext:Panel ID="Panel1" 
            runat="server" 
            Title=""
            AnchorHorizontal="100%"
            Height="570"
            >
            <Items>
                <ext:FormPanel 
                    ID="StatusForm" 
                    runat="server"
                    LabelWidth="75"
                    ButtonAlign="Right"
                    Border="false"
                    PaddingSummary="10px 10px 10px">
                    <Defaults>                        
                        <ext:Parameter Name="AllowBlank" Value="false" Mode="Raw" />
                        <ext:Parameter Name="SelectOnFocus" Value="true" Mode="Raw" />
                        <ext:Parameter Name="MsgTarget" Value="side" />
                    </Defaults>
                    <Items>
                        <ext:Container ID="Container3" runat="server" Layout="ColumnLayout" Height="30" >
                            <Items>
                                <ext:Container ID="Container4" runat="server" LabelAlign="Right" Layout="FormLayout" ColumnWidth=".5" >
                                    <Items>
                                        <ext:Label runat="server" ID="lblProcedimiento" Text="Procedimiento" />
                                    </Items>
                                </ext:Container>
                                <ext:Container ID="Container5" runat="server" LabelAlign="Right" Layout="FormLayout" ColumnWidth=".5" >
                                    <Items>
                                        <ext:ComboBox     
                                            ID="cbManTar"                                                                                   
                                            runat="server" 
                                            Shadow="Drop" 
                                            Mode="Local" 
                                            TriggerAction="All" 
                                            ForceSelection="true"
                                            DisplayField="nombre"
                                            ValueField="codigo"
                                            FieldLabel="Manual de Tarifas"
                                            EmptyText="Seleccione Manual..."
                                            AnchorHorizontal="99%"
                                            AllowBlank="false"
                                            >
                                            <Store>
                                                <ext:Store ID="stManTar" runat="server" AutoLoad="true">
                                                    <Reader>
                                                        <ext:JsonReader IDProperty="codigo">
                                                            <Fields>
                                                                <ext:RecordField Name="codigo" />
                                                                <ext:RecordField Name="nombre" />
                                                            </Fields>
                                                        </ext:JsonReader>
                                                    </Reader>            
                                                </ext:Store>    
                                            </Store>    
                                        </ext:ComboBox> 
                                    </Items>
                                </ext:Container>
                            </Items>
                        </ext:Container>
                        <ext:Container ID="Container6" runat="server" Layout="ColumnLayout" Height="30" >
                            <Items>
                                <ext:Container ID="Container7" runat="server" LabelAlign="Right" Layout="FormLayout" ColumnWidth=".3" >
                                    <Items>
                                        <ext:TextField runat="server" id="txtCodigo" FieldLabel="Código" AnchorHorizontal="95%" AllowBlank="false" MinLength="4" MaxLength="10" />
                                    </Items>
                                </ext:Container>
                                <ext:Container ID="Container9" runat="server" LabelAlign="Right" Layout="FormLayout" ColumnWidth=".4" >
                                    <Items> 
                                        <ext:LinkButton ID="lkTarifas" runat="server" Text="Tarifas por Procedimiento" />
                                    </Items>
                                </ext:Container>                                
                            </Items>
                        </ext:Container>
                        <ext:Container ID="Container13" runat="server" Layout="ColumnLayout" Height="50" >
                            <Items>
                                <ext:Container ID="Container14" runat="server" LabelAlign="Right" Layout="FormLayout" ColumnWidth=".25" >
                                    <Items>
                                        <ext:TextArea runat="server" ID="txtNombre" FieldLabel="Nombre" AnchorHorizontal="100%" AnchorVertical="90%" AllowBlank="false" MaxLength="200" MinLength="10" />
                                    </Items>
                                </ext:Container>
                            </Items>                            
                        </ext:Container>
                        <ext:Container ID="Container15" runat="server" Layout="ColumnLayout" Height="20" >
                            <Items>
                                <ext:Container ID="Container16" runat="server" LabelAlign="Right" Layout="FormLayout" ColumnWidth=".5" >
                                    <Items>
                                        <ext:Label runat="server" ID="lblCCosto" Text="Centro de Costo Relacionado" />
                                    </Items>
                                </ext:Container>
                                <ext:Container ID="Container17" runat="server" LabelAlign="Right" Layout="FormLayout" ColumnWidth=".5" >
                                    <Items>
                                        <ext:Label runat="server" ID="Label1" Text="Tipo de Servicio" />
                                    </Items>
                                </ext:Container>
                            </Items>
                        </ext:Container>
                        <ext:Container ID="Container18" runat="server" Layout="ColumnLayout" Height="30" >
                            <Items>
                                <ext:Container ID="Container19" runat="server" LabelAlign="Right" Layout="FormLayout" ColumnWidth=".5" >
                                    <Items>
                                        <ext:ComboBox     
	                                        ID="cbCCosRel"
	                                        runat="server" 
	                                        Shadow="Drop" 
	                                        Mode="Local" 
	                                        TriggerAction="All" 
	                                        ForceSelection="true"
	                                        DisplayField="nombre"
	                                        ValueField="codigo"	
	                                        EmptyText="Seleccione Centro de Costo Relacionado"
	                                        AnchorHorizontal="99%"
	                                        AllowBlank="false"
	                                        >
	                                        <Store>
		                                        <ext:Store ID="stCCostoRel" runat="server" AutoLoad="true">
			                                        <Reader>
				                                        <ext:JsonReader IDProperty="codigo">
					                                        <Fields>
						                                        <ext:RecordField Name="codigo" />
						                                        <ext:RecordField Name="nombre" />
					                                        </Fields>
				                                        </ext:JsonReader>
			                                        </Reader>            
		                                        </ext:Store>    
	                                        </Store>    
                                        </ext:ComboBox>
                                    </Items>
                                </ext:Container>
                                <ext:Container ID="Container20" runat="server" LabelAlign="Right" Layout="FormLayout" ColumnWidth=".5" >
                                    <Items>
                                        <ext:ComboBox     
	                                        ID="cbTipServicio"                                                                              
	                                        runat="server" 
	                                        Shadow="Drop" 
	                                        Mode="Local" 
	                                        TriggerAction="All" 
	                                        ForceSelection="true"
	                                        DisplayField="nombre"
	                                        ValueField="codser"	
	                                        EmptyText="Seleccione Tipo de Servicio"
	                                        AnchorHorizontal="99%"
	                                        AllowBlank="false"
	                                        >
	                                        <Store>
		                                        <ext:Store ID="stTipServicio" runat="server" AutoLoad="true">
			                                        <Reader>
				                                        <ext:JsonReader IDProperty="codser">
					                                        <Fields>
						                                        <ext:RecordField Name="codser" />
						                                        <ext:RecordField Name="nombre" />
					                                        </Fields>
				                                        </ext:JsonReader>
			                                        </Reader>            
		                                        </ext:Store>    
	                                        </Store>    
                                        </ext:ComboBox>
                                    </Items>
                                </ext:Container>
                            </Items>
                        </ext:Container>
                        <ext:Container ID="Container21" runat="server" Layout="ColumnLayout" Height="30" >
                            <Items>
                                <ext:Container ID="Container22" runat="server" LabelAlign="Right" Layout="FormLayout" ColumnWidth=".4" >
                                    <Items>
                                        <ext:TriggerField 
                                            ID="txtcodgrupoq" 
                                            runat="server" 
                                            Width="200" 
                                            EmptyText="Grupo Quirurgico"
                                            FieldLabel="Grupo Quir."
                                            AnchorHorizontal="80%"    
                                            >
                                            <Triggers>
                                                <ext:FieldTrigger Icon="Search" />
                                            </Triggers>
                                            <Listeners>
                                                <TriggerClick Handler="Ext.Msg.alert('Message', 'You Clicked the Trigger!');" />
                                            </Listeners>
                                        </ext:TriggerField>
                                    </Items>
                                </ext:Container>
                                <ext:Container ID="Container23" runat="server" LabelAlign="Right" Layout="FormLayout" ColumnWidth=".6" >
                                    <Items>
                                        <ext:TextField runat="server" ID="txtNomGrupoq" FieldLabel="" AnchorHorizontal="99%" />
                                    </Items>
                                </ext:Container>
                            </Items>
                        </ext:Container>
                        <ext:FieldSet ID="FieldSet1" runat="server" Title="Tipos de Cobros" Padding="5" Height="80">
                            <Items>
                                <ext:Container ID="Container24" runat="server" Layout="ColumnLayout" Height="30">
                                    <Items>
                                        <ext:Container ID="Container25" runat="server" LabelAlign="Right" Layout="FormLayout" ColumnWidth=".33">
                                            <Items>
                                                <ext:Checkbox runat="server" ID="ckcobhonmed" BoxLabel="Cobra Hon. Medico" />
                                            </Items>
                                        </ext:Container>
                                        <ext:Container ID="Container27" runat="server" LabelAlign="Right" Layout="FormLayout" ColumnWidth=".33">
                                            <Items>
                                                <ext:Checkbox runat="server" ID="ckcobdersal" BoxLabel="Cobra Der. Sala" />
                                            </Items>
                                        </ext:Container>
                                        <ext:Container ID="Container28" runat="server" LabelAlign="Right" Layout="FormLayout" ColumnWidth=".33">
                                            <Items>
                                                <ext:Checkbox runat="server" ID="ckcobmatins" BoxLabel="Cobra Materiales e Insumos" />
                                            </Items>
                                        </ext:Container>                                        
                                    </Items>
                                </ext:Container>
                                <ext:Container ID="Container29" runat="server" Layout="ColumnLayout" Height="30">
                                    <Items>
                                        <ext:Container ID="Container30" runat="server" LabelAlign="Right" Layout="FormLayout" ColumnWidth=".33">
                                            <Items>
                                                <ext:Checkbox runat="server" ID="ckcobhonayu" BoxLabel="Cobra Hon. Ayudante" />
                                            </Items>
                                        </ext:Container>
                                        <ext:Container ID="Container31" runat="server" LabelAlign="Right" Layout="FormLayout" ColumnWidth=".33">
                                            <Items>
                                                <ext:Checkbox runat="server" ID="ckcobhonanes" BoxLabel="Cobra Hon. Anestesiologo" />
                                            </Items>
                                        </ext:Container>
                                        <ext:Container ID="Container32" runat="server" LabelAlign="Right" Layout="FormLayout" ColumnWidth=".33">
                                            <Items>
                                                <ext:Checkbox runat="server" ID="ckconhonper" BoxLabel="Cobra Hon. Perfusionista" />
                                            </Items>
                                        </ext:Container>
                                    </Items>
                                </ext:Container>
                            </Items>
                        </ext:FieldSet>
                        <ext:Container ID="Container11" runat="server" Layout="ColumnLayout" Height="20" >
                            <Items>
                                <ext:Container ID="Container12" runat="server" LabelAlign="Right" Layout="FormLayout" ColumnWidth=".5" >
                                    <Items>
                                        <ext:Label ID="label9" runat="server" Text="Unidad Funcional" />
                                    </Items>
                                </ext:Container>
                                <ext:Container ID="Container55" runat="server" LabelAlign="Right" Layout="FormLayout" ColumnWidth=".5" >
                                    <Items>
                                        <ext:Label ID="label10" runat="server" Text="Finalidad" />
                                    </Items>
                                </ext:Container>
                            </Items>
                        </ext:Container>
                        <ext:Container ID="Container56" runat="server" Layout="ColumnLayout" Height="30" >
                            <Items>
                                <ext:Container ID="Container57" runat="server" LabelAlign="Right" Layout="FormLayout" ColumnWidth=".5" >
                                    <Items>
                                        <ext:ComboBox     
	                                        ID="cbUnidadFuncional"                                                                              
	                                        runat="server" 
	                                        Shadow="Drop" 
	                                        Mode="Local" 
	                                        TriggerAction="All" 
	                                        ForceSelection="true"
	                                        DisplayField="nombre"
	                                        ValueField="codunf"	
	                                        EmptyText="Seleccione Unidad Funcional"
	                                        AnchorHorizontal="99%"
	                                        AllowBlank="false"                                            
	                                        >
	                                        <Store>
		                                        <ext:Store ID="stUnidadFuncional" runat="server" AutoLoad="true">
			                                        <Reader>
				                                        <ext:JsonReader IDProperty="codunf">
					                                        <Fields>
						                                        <ext:RecordField Name="codunf" />
						                                        <ext:RecordField Name="nombre" />
					                                        </Fields>
				                                        </ext:JsonReader>
			                                        </Reader>            
		                                        </ext:Store>    
	                                        </Store>    
                                        </ext:ComboBox>
                                    </Items>
                                </ext:Container>
                                <ext:Container ID="Container58" runat="server" LabelAlign="Right" Layout="FormLayout" ColumnWidth=".5" >
                                    <Items>
                                        <ext:ComboBox     
	                                        ID="cbFinalidad"                                                                              
	                                        runat="server" 
	                                        Shadow="Drop" 
	                                        Mode="Local" 
	                                        TriggerAction="All" 
	                                        ForceSelection="true"
	                                        DisplayField="nombre"
	                                        ValueField="codigo"	
	                                        EmptyText="Seleccione Finalidad"
	                                        AnchorHorizontal="99%"
	                                        AllowBlank="false"
	                                        >
	                                        <Store>
		                                        <ext:Store ID="stFinalidad" runat="server" AutoLoad="true">
			                                        <Reader>
				                                        <ext:JsonReader IDProperty="codigo">
					                                        <Fields>
						                                        <ext:RecordField Name="codigo" />
						                                        <ext:RecordField Name="nombre" />
					                                        </Fields>
				                                        </ext:JsonReader>
			                                        </Reader>            
		                                        </ext:Store>    
	                                        </Store>    
                                        </ext:ComboBox>
                                    </Items>
                                </ext:Container>
                            </Items>
                        </ext:Container>
                        <ext:Container ID="Container26" runat="server" Layout="ColumnLayout" Height="20" >
                            <Items>                                
                                <ext:Container ID="Container34" runat="server" LabelAlign="Right" Layout="FormLayout" ColumnWidth=".33" >
                                    <Items>
                                        <ext:Label ID="Label2" runat="server" Text="Nivel de Complejidad" />
                                    </Items>
                                </ext:Container>
                                <ext:Container ID="Container35" runat="server" LabelAlign="Right" Layout="FormLayout" ColumnWidth=".33" >
                                    <Items>
                                        <ext:Label ID="Label3" runat="server" Text="Aplica a Sexos" />
                                    </Items>
                                </ext:Container>
                                <ext:Container ID="Container36" runat="server" LabelAlign="Right" Layout="FormLayout" ColumnWidth=".33" >
                                    <Items>
                                        <ext:Label ID="Label4" runat="server" Text="A Edades Entre" />
                                    </Items>
                                </ext:Container>                                
                            </Items>
                        </ext:Container>                        
                        <ext:Container ID="Container37" runat="server" Layout="ColumnLayout" Height="30" >
                            <Items>                                
                                <ext:Container ID="Container44" runat="server" LabelAlign="Right" Layout="FormLayout" ColumnWidth=".33" >
                                    <Items>
                                        <ext:ComboBox     
	                                        ID="cbNivelComplejidad"                                                                              
	                                        runat="server" 
	                                        Shadow="Drop" 
	                                        Mode="Local" 
	                                        TriggerAction="All" 
	                                        ForceSelection="true"
	                                        DisplayField="nombre"
	                                        ValueField="codigo"	
	                                        EmptyText="Seleccione Nivel Complejidad"
	                                        AnchorHorizontal="99%"
	                                        AllowBlank="false"
	                                        >
	                                        <Store>
		                                        <ext:Store ID="stNivelComplejidad" runat="server" AutoLoad="true">
			                                        <Reader>
				                                        <ext:JsonReader IDProperty="codigo">
					                                        <Fields>
						                                        <ext:RecordField Name="codigo" />
						                                        <ext:RecordField Name="nombre" />
					                                        </Fields>
				                                        </ext:JsonReader>
			                                        </Reader>            
		                                        </ext:Store>    
	                                        </Store>    
                                        </ext:ComboBox>
                                    </Items>
                                </ext:Container>
                                <ext:Container ID="Container45" runat="server" LabelAlign="Right" Layout="FormLayout" ColumnWidth=".33" >
                                    <Items>
                                        <ext:ComboBox     
	                                        ID="cbAplicaSexos"                                                                              
	                                        runat="server" 
	                                        Shadow="Drop" 
	                                        Mode="Local" 
	                                        TriggerAction="All" 
	                                        ForceSelection="true"
	                                        DisplayField="nombre"
	                                        ValueField="codigo"	
	                                        EmptyText="Seleccione Sexo"
	                                        AnchorHorizontal="99%"
	                                        AllowBlank="false"
	                                        >
	                                        <Store>
		                                        <ext:Store ID="stAplicaSexos" runat="server" AutoLoad="true">
			                                        <Reader>
				                                        <ext:JsonReader IDProperty="codigo">
					                                        <Fields>
						                                        <ext:RecordField Name="codigo" />
						                                        <ext:RecordField Name="nombre" />
					                                        </Fields>
				                                        </ext:JsonReader>
			                                        </Reader>            
		                                        </ext:Store>    
	                                        </Store>    
                                        </ext:ComboBox>
                                    </Items>
                                </ext:Container>
                                <ext:Container ID="Container46" runat="server" LabelAlign="Right" Layout="FormLayout" ColumnWidth=".16" >
                                    <Items>
                                        <ext:NumberField runat="server" ID="txtapledadinicio" AnchorHorizontal="95%" />
                                    </Items>
                                </ext:Container>
                                <ext:Container ID="Container47" runat="server" LabelAlign="Right" Layout="FormLayout" ColumnWidth=".17" >
                                    <Items>
                                        <ext:NumberField runat="server" ID="txtapledadfin" FieldLabel="y" AnchorHorizontal="95%" />
                                    </Items>
                                </ext:Container>
                            </Items>
                        </ext:Container>
                        <ext:Container ID="Container39" runat="server" Layout="ColumnLayout" Height="20" >
                            <Items>                                
                                <ext:Container ID="Container41" runat="server" LabelAlign="Right" Layout="FormLayout" ColumnWidth=".33" >
                                    <Items>
                                        <ext:Label ID="Label6" runat="server" Text="Valor en SMDV" />
                                    </Items>
                                </ext:Container>
                                <ext:Container ID="Container42" runat="server" LabelAlign="Right" Layout="FormLayout" ColumnWidth=".33" >
                                    <Items>
                                        <ext:Label ID="Label7" runat="server" Text="Factor de Incremento" />
                                    </Items>
                                </ext:Container>
                                <ext:Container ID="Container43" runat="server" LabelAlign="Right" Layout="FormLayout" ColumnWidth=".33" >
                                    <Items>
                                        <ext:Label ID="Label8" runat="server" Text="Valor Tarifa Actual" />
                                    </Items>
                                </ext:Container>                                
                            </Items>
                        </ext:Container>
                        <ext:Container ID="Container48" runat="server" Layout="ColumnLayout" Height="30" >
                            <Items>                                
                                <ext:Container ID="Container50" runat="server" LabelAlign="Right" Layout="FormLayout" ColumnWidth=".33" >
                                    <Items>
                                        <ext:NumberField runat="server" ID="txtvalsmv" AnchorHorizontal="95%" />
                                    </Items>
                                </ext:Container>
                                <ext:Container ID="Container51" runat="server" LabelAlign="Right" Layout="FormLayout" ColumnWidth=".33" >
                                    <Items>
                                        <ext:NumberField runat="server" ID="txtporincremento" AnchorHorizontal="95%" />
                                    </Items>
                                </ext:Container>
                                <ext:Container ID="Container52" runat="server" LabelAlign="Right" Layout="FormLayout" ColumnWidth=".33" >
                                    <Items>
                                        <ext:NumberField runat="server" ID="txtvalor" AnchorHorizontal="95%" />
                                    </Items>
                                </ext:Container>                                
                            </Items>
                        </ext:Container>
                        <ext:Container ID="Container53" runat="server" Layout="ColumnLayout" Height="50" >
                            <Items>
                                <ext:Container ID="Container54" runat="server" LabelAlign="Right" Layout="FormLayout" ColumnWidth=".25" >
                                    <Items>
                                        <ext:TextArea runat="server" ID="txtObservaciones" FieldLabel="Observaciones" AnchorHorizontal="100%" AnchorVertical="90%" />
                                    </Items>
                                </ext:Container>
                            </Items>                            
                        </ext:Container>
                    </Items>
                        <Buttons>
                            <ext:Button ID="btnGuardaProc" runat="server" Text="Guardar" Icon="Disk">
                                <DirectEvents>
                                    <Click 
                                        OnEvent="btnGuardaProc_DirectClick" 
                                        Before="var valid= #{StatusForm}.getForm().isValid(); if (valid) {#{FormStatusBar}.showBusy('Guardando Formulario...');} return valid;">
                                        <EventMask 
                                            ShowMask="true" 
                                            MinDelay="1000" 
                                            Target="CustomTarget" 
                                            CustomTarget="={#{StatusForm}.getEl()}" 
                                            />
                                    </Click>
                                </DirectEvents>
                            </ext:Button>
                            <ext:Button runat="server" ID="btnCerrarProc" Text="Cerrar" />
                        </Buttons>
                    </ext:FormPanel>
                </Items>
                <BottomBar>
                    <ext:StatusBar ID="FormStatusBar" runat="server" DefaultText="Ready">
                        <Plugins>
                            <ext:ValidationStatus ID="ValidationStatus1" 
                                runat="server" 
                                FormPanelID="StatusForm" 
                                ValidIcon="Accept" 
                                ErrorIcon="Exclamation" 
                                ShowText="El Formulario tiene Errores (Click para ver Detalles)"
                                HideText="Click para Ocultar Errores"
                                />
                        </Plugins>
                    </ext:StatusBar>
                </BottomBar>
        </ext:Panel>
        </Items>
    </ext:Window>

        <ext:Store 
            ID="stTar" 
            runat="server" 
            AutoLoad="true">
            <Reader>
                <ext:JsonReader IDProperty="id">
                    <Fields>                        
                        <ext:RecordField Name="codpro" />
                        <ext:RecordField Name="manual" />
                        <ext:RecordField Name="id" />
                        <ext:RecordField Name="id_plan" />
                        <ext:RecordField Name="codgruq" />
                        <ext:RecordField Name="valor" />
                        <ext:RecordField Name="valsmv" />
                        <ext:RecordField Name="porincremento" />
                        
                    </Fields>
                </ext:JsonReader>
            </Reader>
        </ext:Store>

        <ext:Store ID="stManual" runat="server" AutoLoad="true">
			<Reader>
				<ext:JsonReader IDProperty="codigo">
					<Fields>
						<ext:RecordField Name="codigo" />
						<ext:RecordField Name="nombre" />
					</Fields>
				</ext:JsonReader>
			</Reader>            
		</ext:Store>

        <ext:Store ID="stPlan" runat="server" AutoLoad="true">
			<Reader>
				<ext:JsonReader IDProperty="codigo">
					<Fields>
						<ext:RecordField Name="codigo" />
						<ext:RecordField Name="nombre" />
					</Fields>
				</ext:JsonReader>
			</Reader>            
		</ext:Store>

    <ext:Window 
        ID="vntTarifas"
        runat="server"
        Icon="ApplicationFormEdit"
        Width="750"
        Height="400"
        Hidden="true" 
        Modal="true"            
        Title="Tarifas"
        Constrain="true">
        <Items>
        <ext:Panel ID="Panel2" runat="server" Frame="true" PaddingSummary="5px 2px 0" AutoWidth="true" Height="380" ButtonAlign="Center" Style="text-align: left">
            <Items>
                <ext:GridPanel 
                    ID="grdTar" 
                    runat="server"
                    Width="700"
                    Height="380"
                    AutoExpandColumn="manual"
                    Title="" 
                    StoreID="stTar" 
                    >
                    <Plugins>
                        <ext:RowEditor ID="RowEditor1" runat="server" SaveText="Guardar" CancelText="Cancelar"  >                            
                            <Listeners>
                                <CancelEdit Fn="cancelTar" />
                                <AfterEdit Fn="editaTar" />                                
                            </Listeners>
                        </ext:RowEditor>
                    </Plugins>
                    <View>
                        <ext:GridView ID="GridView1" runat="server" MarkDirty="false" />
                    </View>
                    <TopBar>
                        <ext:Toolbar ID="Toolbar1" runat="server">
                            <Items>
                                <ext:Button ID="Button1" runat="server" Text="Nueva Tarifa" Icon="UserAdd">
                                    <Listeners>
                                        <Click Fn="addTar" />
                                    </Listeners>
                                </ext:Button>
                                <ext:Button ID="btnEliminaTar1" runat="server" Text="Eliminar Tarifa" Icon="UserDelete" >                                    
                                    <Listeners>
                                        <Click Fn="removeTar" />
                                    </Listeners>
                                </ext:Button>
                            </Items>
                        </ext:Toolbar>
                    </TopBar>
                    <SelectionModel>
                        <ext:RowSelectionModel ID="RowSelectionModel2" runat="server" />
                    </SelectionModel>
                    <ColumnModel>
                        <Columns>
                            <ext:RowNumbererColumn />
                            <ext:Column DataIndex="id" Hidden="true" />
                            <ext:Column 
                                ColumnID="id_plan" 
                                Header="Plan" 
                                DataIndex="id_plan" 
                                Width="130"
                                >
                                <Editor>
                                    <ext:ComboBox    
                                        ID="cbPlan"  	                                                                                  
	                                    runat="server" 
	                                    Shadow="Drop" 
	                                    Mode="Local" 
	                                    TriggerAction="All" 
	                                    ForceSelection="true"
	                                    DisplayField="nombre"
	                                    ValueField="codigo"	                                    
	                                    EmptyText="Seleccione Plan..."
	                                    AnchorHorizontal="99%"
	                                    AllowBlank="false"
                                        StoreID="stPlan" />	                                    
                                </Editor>
                                <Renderer Fn="fmtPlan" />
                            </ext:Column>
                            <ext:Column Header="Valor" DataIndex="valor" Width="100">
                                <Editor>
                                    <ext:NumberField ID="txtTarValor" runat="server" AllowBlank="false" />
                                </Editor>
                            </ext:Column>
                            <ext:Column Header="Valor SMDV" DataIndex="valsmv" Width="100">
                                <Editor>
                                    <ext:NumberField ID="txtTarValsmv" runat="server" AllowBlank="false" />
                                </Editor>
                            </ext:Column>
                            <ext:Column Header="Porcentaje Inc." DataIndex="porincremento" Width="100">
                                <Editor>
                                    <ext:NumberField ID="txtTarPorcentaje" runat="server" AllowBlank="false" />
                                </Editor>
                            </ext:Column>
                            <ext:Column 
                                ColumnID="manual" 
                                Header="Manual" 
                                DataIndex="manual" 
                                Width="130"                                
                                Sortable="true">
                                <Editor>
                                    <ext:ComboBox    
                                        ID="cbTarManual"  	                                                                                  
	                                    runat="server" 
	                                    Shadow="Drop" 
	                                    Mode="Local" 
	                                    TriggerAction="All" 
	                                    ForceSelection="true"
	                                    DisplayField="nombre"
	                                    ValueField="codigo"	                                    
	                                    EmptyText="Seleccione Manual..."
	                                    AnchorHorizontal="99%"
	                                    AllowBlank="false"
                                        StoreID="stManual" />	                                    
                                </Editor>
                                <Renderer Fn="fmtManual" />
                            </ext:Column>
                        </Columns>
                    </ColumnModel>
                </ext:GridPanel>
            </Items>
        </ext:Panel>
        </Items>
        <Buttons>            
            <ext:Button runat="server" ID="btnCerrarTarifa" Text="Cerrar" />
        </Buttons>
    </ext:Window>

    </form>
</body>
</html>


﻿Imports Ext.Net

Public Class Familia
    Inherits System.Web.UI.Page

    Protected Property Accion As String
        Get
            Return ViewState("Accion")
        End Get
        Set(value As String)
            ViewState.Add("Accion", value)
        End Set
    End Property


    Protected Property id_familia As String
        Get
            Return ViewState("id_familia")
        End Get
        Set(value As String)
            ViewState.Add("id_familia", value)
        End Set
    End Property

    Protected Property numHis As String
        Get
            Return ViewState("numHis")
        End Get
        Set(value As String)
            ViewState.Add("numHis", value)
        End Set
    End Property

    Protected Property codEspecialidad As String
        Get
            Return ViewState("codEspecialidad")
        End Get
        Set(value As String)
            ViewState.Add("codEspecialidad", value)
        End Set
    End Property


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If IsPostBack = False Then
            Dim seg As New Seguridad(Me, Session("PERFIL"))
            id_familia = "0"
        End If
    End Sub

    Protected Sub rowSelect(ByVal sender As Object, ByVal e As DirectEventArgs)
        id_familia = e.ExtraParams("ID")
        cargaPacientes()
        cargaEspecialidades()
    End Sub

    Protected Sub rowSelectHis(ByVal sender As Object, ByVal e As DirectEventArgs)
        numHis = e.ExtraParams("ID")
    End Sub

    Protected Sub rowSelectEsp(ByVal sender As Object, ByVal e As DirectEventArgs)
        codEspecialidad = e.ExtraParams("ID")
    End Sub

    Private Function cargarFamilas()
        Dim spFunc As New Datos.spFunciones
        Dim Query As String
        Dim sWhere As String = " where a.id <> 0"
        Query = "select a.id,a.descripcion,a.fecha_adicion from familia a"

        If txtCodigoBsq.Text <> "" Then
            sWhere &= " and a.id = " & txtCodigoBsq.Text
        End If

        If txtDescripcionBsq.Text <> "" Then
            sWhere &= " and a.descripcion like '%" & txtDescripcionBsq.Text & "%'"
        End If

        If txtNumHisBsq.Text <> "" Then
            Query &= " inner join familia_his b on a.id = b.id_familia "
            sWhere &= " and b.numhis = '" & txtNumHisBsq.Text & "'"
        End If

        stFamilia.DataSource = spFunc.executaSelect(Query & sWhere)
        stFamilia.DataBind()
        If Accion <> "Edit" Then
            id_familia = "0"
        End If

        cargaPacientes()
        cargaEspecialidades()
        Return True
    End Function

    Private Function cargaPaciente(id As String)

        Dim negPaciente As New Negocio.Negociosaih_his
        Dim paciente As New Datos.saih_his

        paciente = negPaciente.Obtenersaih_hisById(id)
        If Accion = "Insert" Then
            numHis = paciente.numhis
        End If

        lblIdentificacion.Text = paciente.identificacion
        lblNumHistoria.Text = paciente.numhis
        CbPaciente.Text = paciente.razonsocial

        Return True
    End Function

    Private Function cargaPacientes()
        Dim spFunc As New Datos.spFunciones
        Dim Query As String
        Dim sWhere As String = " where a.id_familia = " & id_familia
        Query = "select a.id_familia,a.numhis,b.tipo_identificacion,b.identificacion,b.razonsocial,a.rol from familia_his a " & _
                "inner join saih_his b on a.numhis = b.numhis"
        stHis.DataSource = spFunc.executaSelect(Query & sWhere)
        stHis.DataBind()

        If Accion <> "Edit" Then
            numHis = "0"
        End If

        Return True
    End Function

    Private Function cargaEspecialidades()
        Dim spFunc As New Datos.spFunciones
        Dim Query As String
        Dim sWhere As String = " where a.id_familia = " & id_familia
        Query = "select a.id_familia,c.codigo cod_especialidad,c.nombre especialidad," & _
                "b.identificacion,b.nombres " & _
                " from familia_esp a inner join saih_pas b on a.codpas = b.codigo" & _
                " inner join saih_tab c on a.cod_especialidad = c.codigo and c.dominio = '" &
                    Datos.ConstantesUtil.DOMINIO_ESPECIALIDADES & "' "
        stEsp.DataSource = spFunc.executaSelect(Query & sWhere)
        stEsp.DataBind()

        If Accion <> "Edit" Then
            codEspecialidad = "0"
        End If

        Return True
    End Function

    Private Function cargaMedicos(cod_especialidad As String)
        Dim negMedicos As New Negocio.Negociosaih_pas
        stPAsistencial.DataSource = negMedicos.Obtenersaih_pasbyWhere(" where cod_especialidad = '" & cod_especialidad & "' and estado = 'A'")
        stPAsistencial.DataBind()
        Return True
    End Function

    Protected Sub btnBuscar_DirectClick(sender As Object, e As Ext.Net.DirectEventArgs)
        cargarFamilas()
    End Sub

    Private Sub btnNuevoGruFamiliar_DirectClick(sender As Object, e As Ext.Net.DirectEventArgs) Handles btnNuevoGruFamiliar.DirectClick
        Accion = "Insert"
        presentacionUtil.limpiarObjetos(vntFamilia)
        vntFamilia.Show()
    End Sub

    Private Sub btnEditaGruFamiliar_DirectClick(sender As Object, e As Ext.Net.DirectEventArgs) Handles btnEditaGruFamiliar.DirectClick

        If id_familia = "0" Then
            Ext.Net.X.Msg.Alert("Información", "Debe Seleccionar Un Grupo Familiar").Show()
            Return
        End If

        Accion = "Edit"
        Dim negFamilia As New Negocio.Negociofamilia
        Dim familia As New Datos.familia

        familia = negFamilia.ObtenerfamiliaById(id_familia)
        Reflexion.formaValores(familia, vntFamilia)
        txtId.Text = familia.id

        vntFamilia.Show()
    End Sub

    <DirectMethod()>
    Public Function eliminarGru()
        Dim negFamilia As New Negocio.Negociofamilia
        If negFamilia.Eliminafamilia(id_familia) Then
            cargarFamilas()
        End If

        Return True
    End Function

    Private Sub btnEliminaGruFamiliar_DirectClick(sender As Object, e As Ext.Net.DirectEventArgs) Handles btnEliminaGruFamiliar.DirectClick
        If id_familia <> "0" Then
            Accion = "Delete"
            Dim js As String = "Ext.MessageBox.confirm('Confirmación', '¿Esta seguro de eliminar?', " & _
           "function (btn) { " & _
                " if (btn == 'yes') " & _
                "   Ext.net.DirectMethods.eliminarGru(); " & _
                " });"
            Ext.Net.X.Js.AddScript(js)
        Else
            Ext.Net.X.Msg.Alert("Información", "Debe Seleccionar Un Grupo Familiar").Show()
        End If
    End Sub

    Protected Sub btnGuardaFamilia_DirectClick(sender As Object, e As Ext.Net.DirectEventArgs)
        Dim negFamilia As New Negocio.Negociofamilia
        Dim familia As New Datos.familia
        If Accion = "Insert" Then
            Reflexion.valoresForma(familia, vntFamilia)
            familia.usuario_creo = Session("Usuario")
            familia.fecha_adicion = Funciones.FechaActual
            If negFamilia.Altafamilia(familia) Then
                Ext.Net.X.Msg.Alert("Información", "Registro Guardado Correctamente").Show()
                cargarFamilas()
                vntFamilia.Hide()
            End If
        Else
            familia = negFamilia.ObtenerfamiliaById(id_familia)
            familia.descripcion = txtDescripcion.Text
            familia.fecha_adicion = Funciones.fechaHoraDB(familia.fecha_adicion)
            If negFamilia.Editafamilia(familia) Then
                Ext.Net.X.Msg.Alert("Información", "Registro Guardado Correctamente").Show()
                cargarFamilas()
                vntFamilia.Hide()
            End If
        End If
    End Sub

    Private Sub btnCerrarVntFamilia_DirectClick(sender As Object, e As Ext.Net.DirectEventArgs) Handles btnCerrarVntFamilia.DirectClick
        vntFamilia.Hide()
    End Sub


    Private Sub btnNuevoPaciente_DirectClick(sender As Object, e As Ext.Net.DirectEventArgs) Handles btnNuevoPaciente.DirectClick
        Accion = "Insert"
        CbPaciente.Disabled = False
        presentacionUtil.limpiarObjetos(vntHis)
        vntHis.Show()
    End Sub

    Private Sub btnEditaPaciente_DirectClick(sender As Object, e As Ext.Net.DirectEventArgs) Handles btnEditaPaciente.DirectClick
        If numHis = "0" Then
            Ext.Net.X.Msg.Alert("Información", "Debe Seleccionar Un Paciente").Show()
            Return
        End If

        Accion = "Edit"
        Dim negFamilia_His As New Negocio.Negociofamilia_his
        Dim familia_His As New Datos.familia_his

        CbPaciente.Disabled = True

        familia_His = negFamilia_His.Obtenerfamilia_hisById(id_familia, numHis)

        Reflexion.formaValores(familia_His, vntHis)

        cargaPaciente(familia_His.numhis)

        vntHis.Show()
    End Sub


    <DirectMethod()>
    Public Function eliminarHis()
        Dim negFamilia_His As New Negocio.Negociofamilia_his
        If negFamilia_His.Eliminafamilia_his(id_familia, numHis) Then
            cargaPacientes()
        End If

        Return True
    End Function

    Private Sub btnEliminaPaciente_DirectClick(sender As Object, e As Ext.Net.DirectEventArgs) Handles btnEliminaPaciente.DirectClick
        If numHis <> "0" Then
            Accion = "Delete"
            Dim js As String = "Ext.MessageBox.confirm('Confirmación', '¿Esta seguro de eliminar?', " & _
           "function (btn) { " & _
                " if (btn == 'yes') " & _
                "   Ext.net.DirectMethods.eliminarHis(); " & _
                " });"
            Ext.Net.X.Js.AddScript(js)
        Else
            Ext.Net.X.Msg.Alert("Información", "Debe Seleccionar Un Paciente").Show()
        End If
    End Sub

    Private Sub btnCerrarVntPaciente_DirectClick(sender As Object, e As Ext.Net.DirectEventArgs) Handles btnCerrarVntPaciente.DirectClick
        vntHis.Hide()
    End Sub

    Private Sub CbPaciente_DirectSelect(sender As Object, e As Ext.Net.DirectEventArgs) Handles CbPaciente.DirectSelect
        cargaPaciente(CbPaciente.Value)
    End Sub

    Protected Sub btnGuardarPaciente_DirectClick(sender As Object, e As Ext.Net.DirectEventArgs)
        Dim negFamilia_His As New Negocio.Negociofamilia_his
        Dim familia_His As New Datos.familia_his

        If Accion = "Insert" Then
            Reflexion.valoresForma(familia_His, vntHis)
            familia_His.numhis = numHis
            familia_His.id_familia = id_familia
            familia_His.usuario_creo = Session("Usuario")
            familia_His.usuario_modifico = Session("Usuario")
            familia_His.fecha_adicion = Funciones.FechaActual
            familia_His.fecha_modificacion = Funciones.FechaActual
            If negFamilia_His.Altafamilia_his(familia_His) Then
                vntHis.Hide()
                Ext.Net.X.Msg.Alert("Información", "Registro Guardado Correctamente").Show()
                cargaPacientes()
            End If

        Else
            familia_His = negFamilia_His.Obtenerfamilia_hisById(id_familia, numHis)
            Reflexion.valoresForma(familia_His, vntHis)
            familia_His.numhis = numHis
            familia_His.id_familia = id_familia
            familia_His.usuario_modifico = Session("Usuario")
            familia_His.fecha_adicion = Funciones.fechaHoraDB(familia_His.fecha_adicion)
            familia_His.fecha_modificacion = Funciones.FechaActual
            If negFamilia_His.Editafamilia_his(familia_His) Then
                vntHis.Hide()
                Ext.Net.X.Msg.Alert("Información", "Registro Guardado Correctamente").Show()
                cargaPacientes()
            End If
        End If
    End Sub

    Private Sub btnNuevaEspecialidad_DirectClick(sender As Object, e As Ext.Net.DirectEventArgs) Handles btnNuevaEspecialidad.DirectClick
        Accion = "Insert"
        cbCodEspecialidad.Disabled = False
        presentacionUtil.limpiarObjetos(vntEsp)
        Funciones.cargaStoreDominio(stCodEspecialidad,
                Datos.ConstantesUtil.DOMINIO_ESPECIALIDADES, Datos.ConstantesUtil.VALOR_ESPECIALID_GRUPO_FAMILIAR)
        vntEsp.Show()
    End Sub

    Private Sub btnEditaEspecialidad_DirectClick(sender As Object, e As Ext.Net.DirectEventArgs) Handles btnEditaEspecialidad.DirectClick

        If codEspecialidad = "0" Then
            Ext.Net.X.Msg.Alert("Información", "Debe Seleccionar Una Especialidad").Show()
            Return
        End If

        Accion = "Edit"
        Dim negFamiliaEsp As New Negocio.Negociofamilia_esp
        Dim familiaEsp As New Datos.familia_esp
        cbCodEspecialidad.Disabled = True

        Funciones.cargaStoreDominio(stCodEspecialidad,
                Datos.ConstantesUtil.DOMINIO_ESPECIALIDADES, Datos.ConstantesUtil.VALOR_ESPECIALID_GRUPO_FAMILIAR)

        familiaEsp = negFamiliaEsp.Obtenerfamilia_espById(id_familia, codEspecialidad)

        cargaMedicos(familiaEsp.cod_especialidad)

        Reflexion.formaValores(familiaEsp, vntEsp)

        vntEsp.Show()
    End Sub

    <DirectMethod()>
    Public Function eliminarEsp()
        Dim negFamiliaEsp As New Negocio.Negociofamilia_esp
        If negFamiliaEsp.Eliminafamilia_esp(id_familia, codEspecialidad) Then
            cargaEspecialidades()
        End If
        Return True
    End Function

    Private Sub btnEliminaEspecialidad_DirectClick(sender As Object, e As Ext.Net.DirectEventArgs) Handles btnEliminaEspecialidad.DirectClick
        If codEspecialidad <> "0" Then
            Accion = "Delete"
            Dim js As String = "Ext.MessageBox.confirm('Confirmación', '¿Esta seguro de eliminar?', " & _
           "function (btn) { " & _
                " if (btn == 'yes') " & _
                "   Ext.net.DirectMethods.eliminarEsp(); " & _
                " });"
            Ext.Net.X.Js.AddScript(js)
        Else
            Ext.Net.X.Msg.Alert("Información", "Debe Seleccionar Una Especialidad").Show()
        End If
    End Sub

    Protected Sub btnGuardarEspecialidad_DirectClick(sender As Object, e As Ext.Net.DirectEventArgs)
        Dim negFamiliaEsp As New Negocio.Negociofamilia_esp
        Dim familiaEsp As New Datos.familia_esp
        If Accion = "Insert" Then
            Reflexion.valoresForma(familiaEsp, vntEsp)
            familiaEsp.id_familia = id_familia
            familiaEsp.usuario_creo = Session("Usuario")
            familiaEsp.usuario_modifico = Session("Usuario")
            familiaEsp.fecha_adicion = Funciones.FechaActual
            familiaEsp.fecha_modificacion = Funciones.FechaActual
            If negFamiliaEsp.Altafamilia_esp(familiaEsp) Then
                vntEsp.Hide()
                Ext.Net.X.Msg.Alert("Información", "Registro Guardado Correctamente").Show()
                cargaEspecialidades()
            End If
        Else
            familiaEsp = negFamiliaEsp.Obtenerfamilia_espById(id_familia, codEspecialidad)
            Reflexion.valoresForma(familiaEsp, vntEsp)
            familiaEsp.usuario_modifico = Session("Usuario")
            familiaEsp.fecha_adicion = Funciones.fechaHoraDB(familiaEsp.fecha_adicion)
            familiaEsp.fecha_modificacion = Funciones.FechaActual
            If negFamiliaEsp.Editafamilia_esp(familiaEsp) Then
                vntEsp.Hide()
                Ext.Net.X.Msg.Alert("Información", "Registro Guardado Correctamente").Show()
                cargaEspecialidades()
            End If
        End If

    End Sub

    Private Sub btnCerrarVntEspecialidad_DirectClick(sender As Object, e As Ext.Net.DirectEventArgs) Handles btnCerrarVntEspecialidad.DirectClick
        vntEsp.Hide()
    End Sub

    Private Sub cbCodEspecialidad_DirectSelect(sender As Object, e As Ext.Net.DirectEventArgs) Handles cbCodEspecialidad.DirectSelect
        cargaMedicos(cbCodEspecialidad.Value)
    End Sub
End Class
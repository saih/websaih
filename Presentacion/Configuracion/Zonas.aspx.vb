﻿Imports System.Web.Services
Imports Ext.Net
Public Class Zonas
    Inherits System.Web.UI.Page

    Protected Property IDPais As Integer
        Get
            Return Me.ViewState("IDPais")
        End Get
        Set(ByVal value As Integer)
            Me.ViewState.Add("IDPais", value)
        End Set
    End Property
    Protected Property IDDpto As Integer
        Get
            Return Me.ViewState("IDDpto")
        End Get
        Set(ByVal value As Integer)
            Me.ViewState.Add("IDDpto", value)
        End Set
    End Property
    Protected Property IDCiudad As Integer
        Get
            Return Me.ViewState("IDCiudad")
        End Get
        Set(ByVal value As Integer)
            Me.ViewState.Add("IDCiudad", value)
        End Set
    End Property
    Protected Property Accion As String
        Get
            Return Me.ViewState("Accion")
        End Get
        Set(ByVal value As String)
            Me.ViewState.Add("Accion", value)
        End Set
    End Property

    Protected Property idBar As String
        Get
            Return Me.ViewState("idBar")
        End Get
        Set(ByVal value As String)
            Me.ViewState.Add("idBar", value)
        End Set
    End Property

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If IsPostBack = False Then
            Dim seg As New Seguridad(Me, Session("PERFIL"))
            Dim negPais As New Negocio.Negociosaih_lugar
            stPais.DataSource = negPais.Obtenersaih_lugarbyWhere("where tipo = 1")
            stPais.DataBind()
        End If
    End Sub

    Protected Sub rowSelectPais(ByVal sender As Object, ByVal e As DirectEventArgs)
        Dim id_Pais As Integer = e.ExtraParams("IDPais")
        IDPais = id_Pais
        carga_Dpto(id_Pais)
    End Sub
    Private Sub btnNuevoPais_DirectClick(ByVal sender As Object, ByVal e As Ext.Net.DirectEventArgs) Handles btnNuevoPais.DirectClick
        Accion = "Insert"
        hIDPais.Text = "_"
        lblIDPais.Text = "_"
        txtPais.Text = ""
        txtTipoPais.Text = 1
        TxtLugarPais.Text = 0
        txtCodPais.Clear()
        VtnPais.Show()
    End Sub


    Protected Sub btnGuardaPais_DirectClick(ByVal sender As Object, ByVal e As Ext.Net.DirectEventArgs)
        Dim negPais As New Negocio.Negociosaih_lugar
        If Accion = "Insert" Then
            negPais.Altasaih_lugar(0, txtTipoPais.Text, txtPais.Text, TxtLugarPais.Text, txtCodPais.Text)
            Ext.Net.X.Msg.Alert("Información", "Registro Guardado Correctamente").Show()
        Else
            negPais.Editasaih_lugar(IDPais, txtTipoPais.Text, txtPais.Text, TxtLugarPais.Text, txtCodPais.Text)
            Ext.Net.X.Msg.Alert("Información", "Registro Editado Correctamente").Show()
        End If
        VtnPais.Hide()

        stPais.DataSource = negPais.Obtenersaih_lugarbyWhere("where tipo = 1")
        stPais.DataBind()
    End Sub
    Private Sub btnEditaPais_DirectClick(ByVal sender As Object, ByVal e As Ext.Net.DirectEventArgs) Handles btnEditaPais.DirectClick
        Accion = "Edit"
        Dim negPais As New Negocio.Negociosaih_lugar
        Dim Pais As Datos.saih_lugar
        If IDPais = 0 Then
            Ext.Net.X.Msg.Alert("Información", "Debe seleccionar un registro").Show()
            Return
        End If
        lblIDPais.Text = IDPais
        hIDPais.Text = IDPais
        Pais = negPais.Obtenersaih_lugarById(IDPais)
        txtPais.Text = Pais.Descripcion
        txtTipoPais.Text = Pais.Tipo
        TxtLugarPais.Text = Pais.Id_Lugar
        txtCodPais.Text = Pais.Codigo
        VtnPais.Show()
    End Sub
    Private Sub btnBorraPais_DirectClick(ByVal sender As Object, ByVal e As Ext.Net.DirectEventArgs) Handles btnBorraPais.DirectClick
        Accion = "Borrar"
        Dim negPais As New Negocio.Negociosaih_lugar
        Dim count As Integer

        If IDPais = 0 Then
            Ext.Net.X.Msg.Alert("Información", "Debe seleccionar un registro").Show()
            Return
        End If
        count = Datos.deTablas.ValidaLugar(IDPais)
        If count > 0 Then
            Ext.Net.X.Msg.Alert("Información", "El registro no se puede eliminar, es posible que esté relacionado a información del sistema.").Show()
            Return
        End If
        If IDPais = 0 Then
            Ext.Net.X.Msg.Alert("Información", "Debe seleccionar un registro").Show()
            Return
        End If
        If negPais.Eliminasaih_lugar(IDPais) Then
            Ext.Net.X.Msg.Alert("Información", "Registro Eliminado Correctamente").Show()
            IDPais = 0
        Else
            Ext.Net.X.Msg.Alert("Información", "El registro no se puede eliminar, es posible que esté relacionado a información del sistema.").Show()
        End If


        stPais.DataSource = negPais.Obtenersaih_lugarbyWhere(" where tipo = '1'")
        stPais.DataBind()

    End Sub
    Public Function carga_Pais(ByVal IdPais As Integer)
        Dim negDpto As New Negocio.Negociosaih_lugar
        stPais.DataSource = negDpto.Obtenersaih_lugarbyWhere(" where ID = " & IdPais)
        stPais.DataBind()
        carga_Pais(0)
        Return True
    End Function
    Private Sub btnCerrarPais_DirectClick(ByVal sender As Object, ByVal e As Ext.Net.DirectEventArgs) Handles btnCerrarPais.DirectClick
        VtnPais.Hide()
    End Sub
    <DirectMethod()>
    Public Function editaPais(ByVal Id_Pais As Integer)
        MsgBox(Id_Pais)
        Return True
    End Function

    Protected Sub rowSelectDpto(ByVal sender As Object, ByVal e As DirectEventArgs)
        Dim id_Dpto As Integer = e.ExtraParams("IDDpto")
        IDDpto = id_Dpto
        carga_Ciudad(id_Dpto)
    End Sub
    Private Sub btnNuevaDpto_DirectClick(ByVal sender As Object, ByVal e As Ext.Net.DirectEventArgs) Handles btnNuevaDpto.DirectClick
        Accion = "Insert"

        If IDPais = 0 Then
            Ext.Net.X.Msg.Alert("Información", "Debe seleccionar una Pais").Show()
            Return
        End If
        lblIDDpto.Text = "_"
        lblPais.Text = Datos.deTablas.DescripcionLugar(IDPais)
        txtDepartamento.Text = ""
        txtTipoDpto.Text = 2
        txtCodDepartamento.Clear()
        vntDpto.Show()
    End Sub
    Private Sub btnEditaDpto_DirectClick(ByVal sender As Object, ByVal e As Ext.Net.DirectEventArgs) Handles btnEditaDpto.DirectClick
        Accion = "Edit"
        lblIDDpto.Text = IDDpto
        lblPais.Text = Datos.deTablas.DescripcionLugar(IDPais)
        Dim negDpto As New Negocio.Negociosaih_lugar
        Dim Dpto As Datos.saih_lugar
        If IDDpto = 0 Then
            Ext.Net.X.Msg.Alert("Información", "Debe seleccionar un registro").Show()
            Return
        End If
        Dpto = negDpto.Obtenersaih_lugarById(IDDpto)
        txtDepartamento.Text = Dpto.Descripcion
        txtTipoDpto.Text = Dpto.Tipo
        lblIDDpto.Text = Dpto.Id
        HdIDDpto.Text = Dpto.Id
        txtCodDepartamento.Text = Dpto.Codigo
        vntDpto.Show()
    End Sub
    Protected Sub btnGuardarDpto_DirectClick(ByVal sender As Object, ByVal e As Ext.Net.DirectEventArgs)
        Dim negDpto As New Negocio.Negociosaih_lugar
        If Accion = "Insert" Then
            negDpto.Altasaih_lugar(0, txtTipoDpto.Text, txtDepartamento.Text, IDPais, txtCodDepartamento.Text)
            Ext.Net.X.Msg.Alert("Información", "Registro Guardado Correctamente").Show()
        Else
            negDpto.Editasaih_lugar(IDDpto, txtTipoDpto.Text, txtDepartamento.Text, IDPais, txtCodDepartamento.Text)
            Ext.Net.X.Msg.Alert("Información", "Registro Editado Correctamente").Show()
        End If
        vntDpto.Hide()
        carga_Dpto(IDPais)
    End Sub
    Private Sub btnBorraDpto_DirectClick(ByVal sender As Object, ByVal e As Ext.Net.DirectEventArgs) Handles btnBorraDpto.DirectClick
        Accion = "Borrar"
        Dim negDpto As New Negocio.Negociosaih_lugar
        Dim count As Integer
        If IDDpto = 0 Then
            Ext.Net.X.Msg.Alert("Información", "Debe seleccionar un registro").Show()
            Return
        End If
        count = Datos.deTablas.ValidaLugar(IDDpto)
        If count > 0 Then
            Ext.Net.X.Msg.Alert("Información", "El registro no se puede eliminar, es posible que esté relacionado a información del sistema.").Show()
            Return
        End If

        If negDpto.Eliminasaih_lugar(IDDpto) Then
            Ext.Net.X.Msg.Alert("Información", "Registro Eliminado Correctamente").Show()
            IDDpto = 0
        Else
            Ext.Net.X.Msg.Alert("Información", "El registro no se puede eliminar, es posible que esté relacionado a información del sistema.").Show()
        End If

        stDpto.DataSource = negDpto.Obtenersaih_lugarbyWhere(" where Id_lugar = '" & IDPais & "' ")
        stDpto.DataBind()
    End Sub
    Public Function carga_Dpto(ByVal Id_Pais As Integer)
        Dim negDpto As New Negocio.Negociosaih_lugar
        stDpto.DataSource = negDpto.Obtenersaih_lugarbyWhere(" where Id_lugar = " & Id_Pais)
        stDpto.DataBind()
        'carga_Ciudad(0)
        Return True
    End Function
    Private Sub btnCerrarDpto_DirectClick(ByVal sender As Object, ByVal e As Ext.Net.DirectEventArgs) Handles btnCerrarDpto.DirectClick
        vntDpto.Hide()
    End Sub

    Protected Sub rowSelectCiudad(ByVal sender As Object, ByVal e As DirectEventArgs)
        Dim id_Ciudad As Integer = e.ExtraParams("IDCiudad")
        IDCiudad = id_Ciudad
    End Sub
    Private Sub btnNuevaCiudad_DirectClick(ByVal sender As Object, ByVal e As Ext.Net.DirectEventArgs) Handles btnNuevaCiudad.DirectClick
        Accion = "Insert"
        If IDDpto = 0 Then
            Ext.Net.X.Msg.Alert("Información", "Debe seleccionar una bodega").Show()
            Return
        End If
        lblIDCiudad.Text = "_"
        lblIDPaisUbq.Text = Datos.deTablas.DescripcionLugar(IDPais)
        lblIDDptoUbq.Text = Datos.deTablas.DescripcionLugar(IDDpto)
        txtCiudad.Text = ""
        txtTipoCiudad.Text = 3
        txtCodCiudad.Clear()
        vntCiudad.Show()
    End Sub
    Private Sub btnEditaCiudad_DirectClick(ByVal sender As Object, ByVal e As Ext.Net.DirectEventArgs) Handles btnEditaCiudad.DirectClick
        Accion = "Edit"
        lblIDCiudad.Text = IDCiudad
        lblIDDptoUbq.Text = Datos.deTablas.DescripcionLugar(IDDpto)

        Dim negciudad As New Negocio.Negociosaih_lugar
        Dim ciudad As New Datos.saih_lugar

        If IDCiudad = 0 Then
            Ext.Net.X.Msg.Alert("Información", "Debe seleccionar un registro").Show()
            Return
        End If
        ciudad = negciudad.Obtenersaih_lugarById(IDCiudad)
        txtCiudad.Text = ciudad.Descripcion
        lblIDCiudad.Text = ciudad.Id
        lblIDPaisUbq.Text = Datos.deTablas.DescripcionLugar(IDPais)
        lblIDDptoUbq.Text = Datos.deTablas.DescripcionLugar(IDDpto)
        HIDCiudad.Text = ciudad.Id
        txtTipoCiudad.Text = ciudad.Tipo
        txtCodCiudad.Text = ciudad.Codigo
        cargarBar()
        vntCiudad.Show()
    End Sub
    Protected Sub btnGuardaCiudad_DirectClick(ByVal sender As Object, ByVal e As Ext.Net.DirectEventArgs)
        Dim negCiudad As New Negocio.Negociosaih_lugar
        If Accion = "Insert" Then
            negCiudad.Altasaih_lugar(0, txtTipoCiudad.Text, txtCiudad.Text, IDDpto, txtCodCiudad.Text)
            Ext.Net.X.Msg.Alert("Información", "Registro Guardado Correctamente").Show()
        Else
            negCiudad.Editasaih_lugar(IDCiudad, txtTipoCiudad.Text, txtCiudad.Text, IDDpto, txtCodCiudad.Text)
            Ext.Net.X.Msg.Alert("Información", "Registro Editado Correctamente").Show()
        End If
        vntCiudad.Hide()
        carga_Ciudad(IDDpto)
    End Sub
    Private Sub btnBorraCiudad_DirectClick(ByVal sender As Object, ByVal e As Ext.Net.DirectEventArgs) Handles btnBorraCiudad.DirectClick
        Accion = "Borrar"
        Dim negCiudad As New Negocio.Negociosaih_lugar
        If IDCiudad = 0 Then
            Ext.Net.X.Msg.Alert("Información", "Debe seleccionar un registro").Show()
            Return
        End If
        If negCiudad.Eliminasaih_lugar(IDCiudad) Then
            Ext.Net.X.Msg.Alert("Información", "Registro Eliminado Correctamente").Show()
            IDCiudad = 0
        Else
            Ext.Net.X.Msg.Alert("Información", "El registro no se puede eliminar, es posible que esté relacionado a información del sistema.").Show()
        End If

        stCiudad.DataSource = negCiudad.Obtenersaih_lugarbyWhere(" where id_lugar = '" & IDDpto & "' ")
        stCiudad.DataBind()
    End Sub
    Public Function carga_Ciudad(ByVal Id_Dpto As Integer)

        Dim spFunc As New Datos.spFunciones
        Dim Sql As String
        Sql = "select a.Id,a.Tipo,a.Descripcion, " & _
             " b.Descripcion Departamento from saih_lugar as a " & _
             " inner join saih_lugar as b on a.Id_lugar = b.Id where a.tipo= 3"

        Dim query As String = ""

        If Id_Dpto <> 0 Then
            query &= " and a.id_lugar = '" & Id_Dpto & "' "
        End If

        Dim negCiudad As New Negocio.Negociosaih_lugar
        stCiudad.DataSource = spFunc.executaSelect(Sql & query)
        stCiudad.DataBind()
        Return True
    End Function
    Private Sub btnCierraCiudad_DirectClick(ByVal sender As Object, ByVal e As Ext.Net.DirectEventArgs) Handles btnCierraCiudad.DirectClick
        vntCiudad.Hide()
    End Sub
  
    <DirectMethod()>
    Public Function ID_Ciudad(ByVal ID_Ubq As Integer)
        IDCiudad = ID_Ubq
        Return True
    End Function

    <DirectMethod()>
    Public Function btnEliminaBar(id As String)
        idBar = id
        Dim js As String = "Ext.MessageBox.confirm('Confirmación', '¿Esta seguro de eliminar?', " & _
           "function (btn) { " & _
                " if (btn == 'yes') " & _
                "   Ext.net.DirectMethods.eliminarBar(); " & _
                " });"
        Ext.Net.X.Js.AddScript(js)

        Return True
    End Function

    <DirectMethod()>
    Public Function eliminarBar()
        Dim negLugar As New Negocio.Negociosaih_lugar
        negLugar.Eliminasaih_lugar(idBar)
        Return True
    End Function

    <DirectMethod()>
    Public Function insertaBar(id As Integer, codigo As String, descripcion As String)
        Dim negLugar As New Negocio.Negociosaih_lugar
        If id = 0 Then
            negLugar.Altasaih_lugar(0, 4, descripcion, IDCiudad, codigo)
        Else
            negLugar.Editasaih_lugar(id, 4, descripcion, IDCiudad, codigo)
        End If
        Return True
    End Function

    <DirectMethod()>
    Public Function cancelarBar()
        cargarBar()
        Return True
    End Function

    Public Function cargarBar()
        Dim negLugar As New Negocio.Negociosaih_lugar
        stLugarBarrio.DataSource = negLugar.Obtenersaih_lugarbyWhere(" where id_lugar = " & IDCiudad)
        stLugarBarrio.DataBind()
        Return True
    End Function

End Class
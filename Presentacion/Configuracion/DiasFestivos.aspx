﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="DiasFestivos.aspx.vb" Inherits="Presentacion.DiasFestivos" %>

<%@ Register Assembly="Ext.Net" Namespace="Ext.Net" TagPrefix="ext" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
    <ext:ResourceManager ID="ResourceManager1" runat="server" />
    <ext:Store ID="stDiasFestivos" runat="server" AutoLoad="false" >
        <Reader>
            <ext:JsonReader IDProperty="id">
                <Fields>
					<ext:RecordField Name="id" />
                    <ext:RecordField Name="dia" />
                    <ext:RecordField Name="descripcion" />
                </Fields>
            </ext:JsonReader>
        </Reader>
    </ext:Store>
    <div>
        <ext:Panel ID="Panel3" runat="server" Frame="true" PaddingSummary="5px 5px 0" AutoWidth="true" Height="650" ButtonAlign="Center" Style="text-align: left" Title="DiasFestivos">
            <Items>
                <ext:FieldSet ID="FieldSet3" runat="server" Title="Campos de Busqueda" Padding="10" Height="90">
                    <Items>
                        <ext:Container ID="Container7" runat="server" Layout="ColumnLayout" Height="80">
                            <Items>
                                <ext:Container ID="Container8" runat="server" LabelAlign="Right" Layout="FormLayout"
                                    ColumnWidth=".5">
                                    <Items>
                                        <ext:DateField runat="server" ID="txtDiaBsq" FieldLabel="Día Festivo" AnchorHorizontal="60%" />
                                        <ext:TextField ID="txtDescripcionBsq" runat="server" FieldLabel="Descripción" />
                                    </Items>
                                </ext:Container>
                                <ext:Container ID="Container9" runat="server" LabelAlign="Right" Layout="FormLayout"
                                    ColumnWidth=".5">
                                    <Items>
                                        <ext:Button runat="server" ID="btnBuscar" Text="Buscar" Icon="ApplicationGo" />                                        
                                    </Items>
                                </ext:Container>
                            </Items>
                        </ext:Container>
                    </Items>
                </ext:FieldSet>
                <ext:GridPanel
                    ID="grdDiasFestivos"
                    runat="server"                                            
                    Margins="0 0 5 5"
                    Icon="UserSuit"
                    Frame="true"
                    Height="400"
                    StoreId="stDiasFestivos"
                    Title="DiasFestivos"
                    >
                    <ColumnModel ID="ColumnModel3" runat="server">
                        <Columns>
                            <ext:Column Header="Id" DataIndex="id"  />                            
                            <ext:Column Header="Día" DataIndex="dia" Width="150"/>
                            <ext:Column Header="Descripción" DataIndex="descripcion" Width="380"/>
                        </Columns>                                
                    </ColumnModel>
                    <SelectionModel>
                        <ext:RowSelectionModel ID="RowSelectionModel1" runat="server" SingleSelect="true" >
                            <DirectEvents>
                                <RowSelect OnEvent="rowSelect" Buffer="100">
                                    <ExtraParams>                                    
                                        <ext:Parameter Name="ID" Value="this.getSelected().id" Mode="Raw" />
                                    </ExtraParams>
                                </RowSelect>
                            </DirectEvents>
                        </ext:RowSelectionModel>
                    </SelectionModel>
                    <LoadMask ShowMask="true" Msg="Loading...." />
                    <BottomBar>
                        <ext:StatusBar runat="server" ID="StatusBar2">
                            <Items>                                                        
                                <ext:Button runat="server" Text="Nuevo Día Festivo" ID="btnNuevaDiaFestivo" Icon="Add" />
                                <ext:Button runat="server" Text="Editar Día Festivo" ID="btnEditaDiaFestivo" Icon="ApplicationEdit" />
                                <ext:Button runat="server" Text="Eliminar Día Festivo" ID="btnEliminaDiaFestivo" Icon="ApplicationDelete" />
                            </Items>
                        </ext:StatusBar>
                    </BottomBar>
                </ext:GridPanel>
            </Items>
            
        </ext:Panel>
    </div>

    <ext:Window 
        ID="VntDiasFestivos"
        runat="server"
        Icon="ApplicationFormEdit"
        Width="450"
        Height="180"
        Hidden="true" 
        Modal="true"            
        Title="DiasFestivos"
        Constrain="true">
        <Items>
        <ext:Panel ID="Panel1" runat="server" Frame="true" PaddingSummary="5px 2px 0" AutoWidth="true" Height="599" ButtonAlign="Center" Style="text-align: left">
            <Items>
                <ext:FieldSet ID="FieldSet2" runat="server" Title="" Padding="10" Height="599">
                    <Items>
                        <ext:Container ID="Container1" runat="server" Layout="ColumnLayout" Height="599">
                            <Items>
                                <ext:Container ID="Container2" runat="server" LabelAlign="Right" Layout="FormLayout"
                                    ColumnWidth=".99">
                                    <Items>
                                        <ext:DateField runat="server" ID="txtDia" FieldLabel="Día Festivo" AnchorHorizontal="60%" />
                                        <ext:TextField runat="server" ID="txtDescripcion" FieldLabel="Descripción" AnchorHorizontal="99%" />                                        
                                    </Items>
                                </ext:Container>
                            </Items>
                        </ext:Container>
                    </Items>
                </ext:FieldSet>
            </Items>
        </ext:Panel>
        </Items>
        <Buttons>
            <ext:Button runat="server" ID="btnGuardaDiaFestivo" Text="Guardar" Icon="ApplicationAdd" />
            <ext:Button runat="server" ID="btnCerrarDiaFestivo" Text="Cerrar" Icon="ApplicationDelete" />
        </Buttons>
    </ext:Window>
    
    </form>
</body>
</html>

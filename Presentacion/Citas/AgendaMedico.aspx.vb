﻿Imports Ext.Net

Public Class AgendaMedico
    Inherits System.Web.UI.Page

    Protected Property Accion As String
        Get
            Return ViewState("Accion")
        End Get
        Set(value As String)
            ViewState.Add("Accion", value)
        End Set
    End Property

    Protected Property nropme As String
        Get
            Return ViewState("nropme")
        End Get
        Set(value As String)
            ViewState.Add("nropme", value)
        End Set
    End Property

    Protected Property codpre As String
        Get
            Return ViewState("codpre")
        End Get
        Set(value As String)
            ViewState.Add("codpre", value)
        End Set
    End Property



    Protected Property lstFechas As List(Of fecha)
        Get
            Try
                Dim MatrizFechas() As fecha = CType(Me.ViewState("lstFechas"), fecha())
                Dim lstFechas1 As List(Of fecha) = New List(Of fecha)(MatrizFechas)
                Return lstFechas1
            Catch ex As Exception
                Return New List(Of fecha)
            End Try
        End Get
        Set(ByVal value As List(Of fecha))
            ViewState.Add("lstFechas", value.ToArray)
        End Set
    End Property

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If IsPostBack = False Then
            Dim seg As New Seguridad(Me, Session("PERFIL"))
            Dim fecActual As Date = Now
            txtFecha.MinDate = fecActual
            txtFecha.MaxDate = fecActual.AddDays(60)
        End If
    End Sub

    Protected Sub rowSelect(ByVal sender As Object, ByVal e As DirectEventArgs)
        nropme = e.ExtraParams("nropme")
        codpre = e.ExtraParams("codpre")
    End Sub

    Private Function cargarAgendaMedico()

        If txtNombreBsq.Text = "" And txtCodPasBsq.Text = "" Then
            Ext.Net.X.Msg.Alert("Información", "Debe ingresar Codigo o Nombres para realizar la busqueda").Show()
            Return True
        End If

        Dim spFunc As New Datos.spFunciones
        Dim Query As String
        Dim sWhere As String = " where a.nropme <> '0' and a.estado <> 'Cancelado' "
        Query = "select a.codpre,a.nropme,a.codpas,b.nombres,a.fecha_inicial,a.fecha_final,a.estado,a.intervalo, " & _
                "'(' + a.consultorio + ') - ' + c.nombre consultorio from saih_apas a inner join saih_pas b on a.codpas = b.codigo left join saih_tab c " & _
                " on a.consultorio = c.codigo and c.dominio = '" & Datos.ConstantesUtil.DOMINIO_CONSULTORIO & "'"

        If txtNombreBsq.Text <> "" Then
            sWhere &= " and b.nombres like '%" & txtNombreBsq.Text & "%'"
        End If

        If txtCodPasBsq.Text <> "" Then
            sWhere &= " and a.codpas = '" & txtCodPasBsq.Text & "'"
        End If

        stAPas.DataSource = spFunc.executaSelect(Query & sWhere & " order by a.fecha_inicial desc")
        stAPas.DataBind()

        Return True
    End Function

    Private Function cargaCombos()
        Dim negPrm As New Negocio.Negociosaih_prm
        Dim negPas As New Negocio.Negociosaih_pas
        Funciones.cargaStoreDominioCodNom(stConsultorio, Datos.ConstantesUtil.DOMINIO_CONSULTORIO)
        stSede.DataSource = negPrm.Obtenersaih_prm
        stSede.DataBind()
        stPAsistencial.DataSource = negPas.Obtenersaih_pasbyWhere(" where estado = 'A'")
        stPAsistencial.DataBind()
        Return True
    End Function


    Protected Sub btnBuscar_DirectClick(sender As Object, e As Ext.Net.DirectEventArgs)
        cargarAgendaMedico()
    End Sub


    Private Sub btnNuevoAgendaMedico_DirectClick(sender As Object, e As Ext.Net.DirectEventArgs) Handles btnNuevoAgendaMedico.DirectClick
        Accion = "Insert"
        contFechas.Hidden = False
        conCancelacion.Hidden = True
        txtMotivoCancelacion.AllowBlank = True
        Session.Add("diasAgenda", "")
        lstFechas = New List(Of fecha)
        stFechas.DataSource = lstFechas
        stFechas.DataBind()
        cbPAsistencial.Disabled = False
        cargaCombos()
        presentacionUtil.limpiarObjetos(vntAPas)
        cbSede.Value = Session("Sede")
        vntAPas.Show()
    End Sub

    Private Sub btnEditaAgendaMedico_DirectClick(sender As Object, e As Ext.Net.DirectEventArgs) Handles btnEditaAgendaMedico.DirectClick
        Accion = "Edit"
        Dim negAPas As New Negocio.Negociosaih_apas
        Dim APas As New Datos.saih_apas
        APas = negAPas.Obtenersaih_apasById(codpre, nropme)
        conCancelacion.Hidden = True
        txtMotivoCancelacion.AllowBlank = True
        cbPAsistencial.Disabled = True
        cargaCombos()
        Reflexion.formaValores(APas, vntAPas)
        txtFecha.Text = APas.fecha_inicial
        txtHoraInicio.Text = Funciones.HoraWindow(APas.fecha_inicial)
        txtHoraFinal.Text = Funciones.HoraWindow(APas.fecha_final)
        contFechas.Hidden = True
        vntAPas.Show()
    End Sub

    Private Sub btnEliminaAgendaMedico_DirectClick(sender As Object, e As Ext.Net.DirectEventArgs) Handles btnEliminaAgendaMedico.DirectClick
        Accion = "Cancel"
        Dim negAPas As New Negocio.Negociosaih_apas
        Dim APas As New Datos.saih_apas
        APas = negAPas.Obtenersaih_apasById(codpre, nropme)
        conCancelacion.Hidden = False
        txtMotivoCancelacion.AllowBlank = False
        presentacionUtil.limpiarObjetos(vntAPas)
        cargaCombos()
        Reflexion.formaValores(APas, vntAPas)
        txtFecha.Text = APas.fecha_inicial
        txtHoraInicio.Text = Funciones.HoraWindow(APas.fecha_inicial)
        txtHoraFinal.Text = Funciones.HoraWindow(APas.fecha_final)
        contFechas.Hidden = True
        vntAPas.Show()
        Dim contAge As Integer = Datos.deTablas.datoCont("saih_age", " where estado = 'Programada' and codpre = '" & _
                                    APas.codpre & "' and nropme = '" & APas.nropme & "'")
        If contAge > 0 Then
            Dim texto As String = ""
            Dim reader As SqlClient.SqlDataReader = Datos.deTablas.ejecutaSelectDr("select a.numhis,a.nroage,b.razonsocial,a.fecha_cita,a.fecha_fincita" & _
                                                    " from saih_age a inner join saih_his b on a.numhis = b.numhis where a.codpre = '" & APas.codpre & _
                                                    "' and nropme = '" & APas.nropme & "'")
            While reader.Read
                texto &= reader("nroage") & " - " & reader("numhis") & " - " & reader("razonsocial") & " - " & reader("fecha_cita") & " - " & reader("fecha_fincita") & vbCrLf
            End While

            txtAgendas.Text = texto

            Ext.Net.X.Msg.Alert("Información", "La Agenda ya tiene Pacientes Inscritos").Show()
        End If

    End Sub


    Private Sub btnCerrarAPas_DirectClick(sender As Object, e As Ext.Net.DirectEventArgs) Handles btnCerrarAPas.DirectClick
        vntAPas.Hide()

    End Sub

    Private Sub btnRepetir_DirectClick(sender As Object, e As Ext.Net.DirectEventArgs) Handles btnRepetir.DirectClick
        If txtFecha.Text = "01/01/0001 0:00:00" Then
            Ext.Net.X.Msg.Alert("Información", "Debe Seleccionar el Día Principal").Show()
            Return
        End If
        Session.Add("diasAgenda", "")
        vntDias.Show()
    End Sub

    Private Function ValidaAgenda(APas As Datos.saih_apas) As Boolean
        Dim valida As Boolean = True
        Dim texto As String = ""
        Dim msn As New Datos.mensaje
        Dim spFunc As New Datos.spFunciones
        Dim listaFechas As List(Of fecha) = lstFechas

        msn = spFunc.ValidarAgendaMedico(APas)

        If msn.Codigo <> "0" Then
            valida = False
            texto &= msn.Texto & Chr(13)
        End If
        If listaFechas.Count > 0 Then
            For Each itm In listaFechas
                APas.fecha_inicial = Funciones.FechaAgendaMedico(itm.Fecha) & " " & txtHoraInicio.Text
                APas.fecha_final = Funciones.FechaAgendaMedico(itm.Fecha) & " " & txtHoraFinal.Text
                msn = spFunc.ValidarAgendaMedico(APas)

                If msn.Codigo <> "0" Then
                    valida = False
                    texto &= msn.Texto & vbCrLf
                End If
            Next
        End If
        If valida = False Then
            Ext.Net.X.Msg.Alert("Información", texto).Show()
        End If
        Return valida
    End Function

    Protected Sub btnGuardaAPas_DirectClick(sender As Object, e As Ext.Net.DirectEventArgs)
        Dim negAPas As New Negocio.Negociosaih_apas
        Dim APas As New Datos.saih_apas
        Dim APasVal As New Datos.saih_apas
        Dim spFunc As New Datos.spFunciones
        Dim msg As New Datos.mensaje
        If Accion = "Insert" Then
            Dim listaFechas As List(Of fecha) = lstFechas
            Dim fechaAgenda As String = "" 'Session("diasAgenda")

            If listaFechas.Count > 0 Then
                For Each itm In listaFechas
                    fechaAgenda &= itm.Fecha & ","
                Next
                fechaAgenda = fechaAgenda.Substring(0, fechaAgenda.Length - 1)
            End If
            APas.fecha_inicial = Funciones.Fecha(txtFecha.Text) & " " & txtHoraInicio.Text
            APas.fecha_final = Funciones.Fecha(txtFecha.Text) & " " & txtHoraFinal.Text
            Reflexion.valoresForma(APas, vntAPas)
            APasVal = APas
            If ValidaAgenda(APasVal) = False Then
                Return
            End If
            APas.fecha_inicial = Funciones.Fecha(txtFecha.Text) & " " & txtHoraInicio.Text
            APas.fecha_final = Funciones.Fecha(txtFecha.Text) & " " & txtHoraFinal.Text
            Dim nropme As Integer = 0
            msg = spFunc.consecutivoPRM(cbSede.Value, Datos.ConstantesUtil.AGENDA_MEDICO)
            If msg.Codigo = "" Or msg.Codigo = "0" Then
                nropme = msg.Valor
            End If
            If nropme <> 0 Then
                APas.nropme = nropme
                APas.fecha_adicion = Funciones.FechaActual
                APas.fecha_modificacion = Funciones.FechaActual
                APas.usuario_creo = Session("Usuario")
                APas.usuario_modifico = Session("Usuario")
                APas.estado = "Activo"
                If negAPas.Altasaih_apas(APas) Then
                    spFunc.creaAgenda(APas.codpre, APas.nropme)
                    If fechaAgenda <> "" Then
                        Dim fechas As String()
                        fechas = fechaAgenda.Split(",")
                        For Each fecha In fechas
                            fecha = Funciones.FechaAgendaMedico(fecha)
                            msg = spFunc.consecutivoPRM(cbSede.Value, Datos.ConstantesUtil.AGENDA_MEDICO)
                            If msg.Codigo = "" Or msg.Codigo = "0" Then
                                nropme = msg.Valor
                            End If
                            If nropme <> 0 Then
                                APas.nropme = nropme
                                APas.fecha_inicial = fecha & " " & txtHoraInicio.Text
                                APas.fecha_final = fecha & " " & txtHoraFinal.Text
                                If negAPas.Altasaih_apas(APas) Then
                                    spFunc.creaAgenda(APas.codpre, APas.nropme)
                                End If
                            End If
                        Next
                        Ext.Net.X.Msg.Alert("Información", "Registros Guardado Correctamente").Show()
                    Else
                        Ext.Net.X.Msg.Alert("Información", "Registro Guardado Correctamente").Show()
                    End If
                    vntAPas.Hide()
                    cargarAgendaMedico()
                End If
            End If
        End If
        If Accion = "Edit" Then
            APas = negAPas.Obtenersaih_apasById(codpre, nropme)
            Reflexion.valoresForma(APas, vntAPas)
            APas.fecha_modificacion = Funciones.FechaActual
            APas.usuario_modifico = Session("Usuario")
            APas.fecha_adicion = Funciones.fechaHoraDB(APas.fecha_adicion)
            APas.fecha_inicial = Funciones.Fecha(txtFecha.Text) & " " & txtHoraInicio.Text
            APas.fecha_final = Funciones.Fecha(txtFecha.Text) & " " & txtHoraFinal.Text
            Dim contAge As Integer = Datos.deTablas.datoCont("saih_age", " where estado = 'Programada' and codpre = '" & _
                                    APas.codpre & "' and nropme = '" & APas.nropme & "'")
            If contAge > 0 Then
                Ext.Net.X.Msg.Alert("Información", "La Agenda ya tiene Pacientes Inscritos").Show()
                Return
            End If

            msg = spFunc.ValidarAgendaMedicoChange(APas)

            If msg.Codigo <> 0 Then
                Ext.Net.X.Msg.Alert("Información", msg.Texto).Show()
                Return
            End If

            If negAPas.Editasaih_apas(APas) Then
                spFunc.executUpdate("delete from saih_age where codpre = '" & _
                                    APas.codpre & "' and nropme = '" & APas.nropme & "'")
                spFunc.creaAgenda(APas.codpre, APas.nropme)
                Ext.Net.X.Msg.Alert("Información", "Registro Guardado Correctamente").Show()
                vntAPas.Hide()
                cargarAgendaMedico()
            End If
        End If
        If Accion = "Cancel" Then
            APas = negAPas.Obtenersaih_apasById(codpre, nropme)
            'Reflexion.valoresForma(APas, vntAPas)
            APas.fecha_modificacion = Funciones.FechaActual
            APas.usuario_modifico = Session("Usuario")
            APas.fecha_adicion = Funciones.fechaHoraDB(APas.fecha_adicion)
            APas.fecha_inicial = Funciones.Fecha(txtFecha.Text) & " " & txtHoraInicio.Text
            APas.fecha_final = Funciones.Fecha(txtFecha.Text) & " " & txtHoraFinal.Text
            Dim contAge As Integer = Datos.deTablas.datoCont("saih_age", " where estado = 'Programada' and codpre = '" & _
                                    APas.codpre & "' and nropme = '" & APas.nropme & "'")
            If contAge > 0 Then
                APas.estado = "Cancelando"
            Else
                APas.estado = "Cancelado"
            End If

            APas.motivo_cancelacion = txtMotivoCancelacion.Text
            If negAPas.Editasaih_apas(APas) Then
                spFunc.executUpdate("update saih_age set estado = 'Cancelado' where estado = 'Pendiente' and codpre = '" & _
                                    APas.codpre & "' and nropme = '" & APas.nropme & "'")
                If contAge > 0 Then
                    Ext.Net.X.Msg.Alert("Información", "Agenda Medico en proceso de Cancelación, Debe Cancelar Manualmene las Agendas ya Otorgadas").Show()
                Else
                    Ext.Net.X.Msg.Alert("Información", "Registro Guardado Correctamente").Show()
                End If

                vntAPas.Hide()
                cargarAgendaMedico()
            End If
        End If
    End Sub

    Private Sub btnCerrarDias_DirectClick(sender As Object, e As Ext.Net.DirectEventArgs) Handles btnCerrarDias.DirectClick
        Dim listaFechas As New List(Of fecha)
        Dim fechaAgenda As String = Session("diasAgenda")
        Dim fechas As String()
        fechas = fechaAgenda.Split(",")
        For Each fecha In fechas
            If Funciones.FechaAgendaMedico(fecha) <> Funciones.Fecha(txtFecha.Text) Then
                listaFechas.Add(New fecha(fecha))
            End If
        Next
        lstFechas = listaFechas
        stFechas.DataSource = listaFechas
        stFechas.DataBind()
        vntDias.Hide()
    End Sub

    <DirectMethod()>
    Public Function eliminaFecha(fecha As String)

        Dim listaFechas As List(Of fecha) = lstFechas
        Dim listaFechas1 As New List(Of fecha)

        For Each fechas In listaFechas
            If fechas.Fecha <> fecha Then
                listaFechas1.Add(New fecha(fechas.Fecha))
            End If
        Next

        lstFechas = listaFechas1
        stFechas.DataSource = listaFechas1
        stFechas.DataBind()

        Return True
    End Function


    Private Sub cbPAsistencial_DirectSelect(sender As Object, e As Ext.Net.DirectEventArgs) Handles cbPAsistencial.DirectSelect
        Dim spFunc As New Datos.spFunciones
        cbTipServicio.Clear()
        stTipServicio.DataSource = spFunc.executaSelect("select b.codser,b.nombre from saih_paser a inner " & _
                                    "join saih_ser b on a.codser = b.codser where a.codpas = '" & cbPAsistencial.Value & "'")
        stTipServicio.DataBind()
    End Sub

    <Serializable()>
    Class fecha
        Private _fecha As String

        Public Property Fecha As String
            Get
                Return _fecha
            End Get
            Set(value As String)
                _fecha = value
            End Set
        End Property

        Public Sub New(fecha As String)
            Me.Fecha = fecha
        End Sub

    End Class
End Class
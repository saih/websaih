﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="AgendaMedico.aspx.vb" Inherits="Presentacion.AgendaMedico" %>

<%@ Register Assembly="Ext.Net" Namespace="Ext.Net" TagPrefix="ext" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <link href="./../public/css/redmond/jquery-ui-1.9.2.custom.css" rel="stylesheet" />
    <script type="text/javascript" src="./../public/js/jquery-1.9.0.js"></script>
    <script type="text/javascript" src="./../public/js/jquery-ui-1.9.2.custom.js"></script>
	<script type="text/javascript" src="./../public/js/jquery-ui.multidatespicker.js"></script>
    <script type="text/javascript">
        var commandHandler = function (cmd, record) {
            switch (cmd) {
                case "DeleteMe":

                    Ext.MessageBox.confirm('Confirmación', '¿Esta seguro de eliminar?',
                    function (btn) {
                        if (btn == "yes") Ext.net.DirectMethods.eliminaFecha(record.data.Fecha);
                    })
                break;
                

            }
        };

        $(function () {
            $('#simpliest-usage').multiDatesPicker({
                altField: '#altField'
            });
            $('#btnPrueba').click(function () {
                alert($('#altField').val());
            });
        });
	</script>
</head>
<body>
    <form id="form1" runat="server">
        <ext:ResourceManager ID="ResourceManager1" runat="server" />
        <ext:Store 
        ID="stAPas" 
        runat="server" 
        AutoLoad="true">
        <Reader>
            <ext:JsonReader IDProperty="nropme">
                <Fields>
                    <ext:RecordField Name="codpre" />
                    <ext:RecordField Name="nropme" />
                    <ext:RecordField Name="codpas" />
                    <ext:RecordField Name="nombres" />
                    <ext:RecordField Name="fecha_inicial" />
                    <ext:RecordField Name="fecha_final" />                    
                    <ext:RecordField Name="intervalo" />
                    <ext:RecordField Name="consultorio" />
                    <ext:RecordField Name="estado" />
                </Fields>
            </ext:JsonReader>
        </Reader>
    </ext:Store>
    <ext:Panel ID="Panel3" runat="server" Frame="true" PaddingSummary="5px 5px 0" AutoWidth="true" Height="650" ButtonAlign="Center" Style="text-align: left" Title="AgendaMedicos">
        <Items>
            <ext:FieldSet ID="FieldSet3" runat="server" Title="Campos de Busqueda" Padding="10" Height="60">
                <Items>
                    <ext:Container ID="Container10" runat="server" Layout="ColumnLayout" Height="30">
                        <Items>
                            <ext:Container ID="Container1" runat="server" LabelAlign="Right" Layout="FormLayout" ColumnWidth=".25">
                                <Items>
                                    <ext:TextField runat="server" ID="txtCodPasBsq" FieldLabel="Codigo" />
                                </Items>
                            </ext:Container>
                            <ext:Container ID="Container3" runat="server" LabelAlign="Right" Layout="FormLayout" ColumnWidth=".25">
                                <Items>
                                    <ext:TextField runat="server" ID="txtNombreBsq" FieldLabel="Nombre" AnchorHorizontal="99%" />
                                </Items>
                            </ext:Container>
                            <ext:Container ID="Container2" runat="server" LabelAlign="Right" Layout="FormLayout" ColumnWidth=".2">
                                <Items>
                                    <ext:Button runat="server" ID="btnBuscar" Text="Buscar" Icon="ApplicationGo" >
                                        <DirectEvents>
                                            <Click OnEvent="btnBuscar_DirectClick" Before="">
                                                <EventMask ShowMask="true" />
                                            </Click>
                                        </DirectEvents>
                                    </ext:Button>
                                </Items>
                            </ext:Container>
                        </Items>
                    </ext:Container>
                </Items>
            </ext:FieldSet>
            <ext:GridPanel
                ID="grdAPas"
                runat="server"                                            
                Margins="0 0 5 5"
                Icon="TransmitGo"
                Frame="true"
                Height="400"
                StoreId="stAPas"
                Title="AgendaMedicos"
                >
                <ColumnModel ID="ColumnModel3" runat="server">
                    <Columns>
                        <ext:Column Header="Sede" DataIndex="codpre" Width="40"/>
                        <ext:Column Header="Numero" DataIndex="nropme" Width="50" />
                        <ext:Column Header="Codigo" DataIndex="codpas" Width="70"/>
                        <ext:Column Header="Nombres" DataIndex="nombres" Width="200"/>
                        <ext:Column Header="Fecha Inicial" DataIndex="fecha_inicial" Width="120"/>
                        <ext:Column Header="Fecha Final" DataIndex="fecha_final" Width="120"/>
                        <ext:Column Header="Int." DataIndex="intervalo" Width="40"/>
                        <ext:Column Header="Consultorio" DataIndex="consultorio" Width="180"/>
                        <ext:Column Header="Estado" DataIndex="estado" Width="60"/>
                    </Columns>                                
                </ColumnModel>
                <SelectionModel>
                    <ext:RowSelectionModel ID="RowSelectionModel1" runat="server" SingleSelect="true" >
                        <DirectEvents>
                            <RowSelect OnEvent="rowSelect" Buffer="100">
                                <ExtraParams>             
                                    <ext:Parameter Name="codpre" Value="this.getSelected().get('codpre')" Mode="Raw" />
                                    <ext:Parameter Name="nropme" Value="this.getSelected().get('nropme')" Mode="Raw" />
                                </ExtraParams>
                            </RowSelect>
                        </DirectEvents>
                    </ext:RowSelectionModel>
                </SelectionModel>
                <LoadMask ShowMask="true" Msg="Loading...." />
                <BottomBar>
                    <ext:StatusBar runat="server" ID="StatusBar2">
                        <Items>                                                        
                            <ext:Button runat="server" Text="Nuevo Agenda Medico" ID="btnNuevoAgendaMedico" Icon="Add" />
                            <ext:Button runat="server" Text="Editar Agenda Medico" ID="btnEditaAgendaMedico" Icon="ApplicationEdit" />
                            <ext:Button runat="server" Text="Cancelar Agenda Medico" ID="btnEliminaAgendaMedico" Icon="ApplicationDelete" />
                                
                        </Items>
                    </ext:StatusBar>
                </BottomBar>
            </ext:GridPanel>
        </Items>
    </ext:Panel>


    <ext:Store ID="stFechas" runat="server" AutoLoad="false" >
        <Reader>
            <ext:JsonReader IDProperty="Fecha">
                <Fields>
                    <ext:RecordField Name="Fecha" />
                </Fields>
            </ext:JsonReader>
        </Reader>
    </ext:Store>

    <ext:Window 
        ID="vntAPas"
        runat="server"
        Icon="ApplicationFormEdit"
        Width="800"
        Height="350"
        Hidden="true" 
        Modal="true"            
        Title="Agenda Medico"
        Constrain="true">
        <Items>
            <ext:Panel ID="Panel1" 
            runat="server" 
            Title=""
            AnchorHorizontal="100%"
            Height="320"
            Layout="Fit">
            <Items>
                <ext:FormPanel 
                    ID="StatusForm" 
                    runat="server"
                    LabelWidth="90"
                    ButtonAlign="Right"
                    Border="false"
                    PaddingSummary="10px 10px 10px">
                    <Defaults>                        
                        <ext:Parameter Name="AllowBlank" Value="false" Mode="Raw" />
                        <ext:Parameter Name="SelectOnFocus" Value="true" Mode="Raw" />
                        <ext:Parameter Name="MsgTarget" Value="side" />
                    </Defaults>
                    <Items>                        
                        <ext:Container ID="Container4" runat="server" Layout="ColumnLayout" Height="30" >
                            <Items>
                                <ext:Container ID="Container6" runat="server" LabelAlign="Right" Layout="FormLayout" ColumnWidth=".5" >
                                    <Items>
                                        <ext:ComboBox     
	                                        ID="cbPAsistencial"                                                                              
	                                        runat="server" 
	                                        Shadow="Drop" 
	                                        Mode="Local" 
	                                        TriggerAction="All" 
	                                        ForceSelection="true"
	                                        DisplayField="nombres"
	                                        ValueField="codigo"	
	                                        EmptyText="Seleccione Profesional"
	                                        AnchorHorizontal="99%"
	                                        FieldLabel="Medico"
                                            AllowBlank="false"
                                            DataIndex="codpas"
	                                        >
	                                        <Store>
		                                        <ext:Store ID="stPAsistencial" runat="server" AutoLoad="true">
			                                        <Reader>
				                                        <ext:JsonReader IDProperty="codigo">
					                                        <Fields>
						                                        <ext:RecordField Name="codigo" />
						                                        <ext:RecordField Name="nombres" />
					                                        </Fields>
				                                        </ext:JsonReader>
			                                        </Reader>            
		                                        </ext:Store>    
	                                        </Store>    
                                        </ext:ComboBox>
                                    </Items>
                                </ext:Container>
                                <ext:Container ID="Container5" runat="server" LabelAlign="Right" Layout="FormLayout" ColumnWidth=".5" >
                                    <Items>
                                        <ext:ComboBox     
	                                        ID="cbSede"
	                                        runat="server" 
	                                        Shadow="Drop" 
	                                        Mode="Local" 
	                                        TriggerAction="All" 
	                                        ForceSelection="true"
	                                        DisplayField="lugar"
	                                        ValueField="codpre"	
	                                        EmptyText="Seleccione Sede"
	                                        AnchorHorizontal="99%"
	                                        AllowBlank="false"
                                            DataIndex="codpre"
                                            FieldLabel="Sede">
	                                        <Store>
		                                        <ext:Store ID="stSede" runat="server" AutoLoad="true">
			                                        <Reader>
				                                        <ext:JsonReader IDProperty="codpre">
					                                        <Fields>
						                                        <ext:RecordField Name="codpre" />
						                                        <ext:RecordField Name="lugar" />
					                                        </Fields>
				                                        </ext:JsonReader>
			                                        </Reader>            
		                                        </ext:Store>    
	                                        </Store>    
                                        </ext:ComboBox>
                                    </Items>
                                </ext:Container>
                            </Items>
                        </ext:Container>
                        <ext:Container ID="Container7" runat="server" Layout="ColumnLayout" Height="30" >
                            <Items>
                                <ext:Container ID="Container8" runat="server" LabelAlign="Right" Layout="FormLayout" ColumnWidth=".4" >
                                    <Items>
                                        <ext:DateField runat="server" ID="txtFecha" FieldLabel="Fecha Agenda" AllowBlank="false" />
                                    </Items>
                                </ext:Container>
                                <ext:Container ID="Container9" runat="server" LabelAlign="Right" Layout="FormLayout" ColumnWidth=".2" >
                                    <Items>
                                        <ext:TimeField runat="server" ID="txtHoraInicio" FieldLabel="H.Inicial" AllowBlank="false" AnchorHorizontal="99%" />
                                    </Items>
                                </ext:Container>
                                <ext:Container ID="Container11" runat="server" LabelAlign="Right" Layout="FormLayout" ColumnWidth=".2" >
                                    <Items>
                                        <ext:TimeField runat="server" ID="txtHoraFinal" FieldLabel="H.Final" AllowBlank="false" AnchorHorizontal="99%"/>
                                    </Items>
                                </ext:Container>
                                <ext:Container ID="Container13" runat="server" LabelAlign="Right" Layout="FormLayout" ColumnWidth=".2" >
                                    <Items>
                                        <ext:NumberField runat="server" ID="txtIntervalo" FieldLabel="Intervalo" DataIndex="intervalo" AllowBlank="false" AnchorHorizontal="99%" />
                                    </Items>
                                </ext:Container>
                            </Items>
                        </ext:Container>
                        <ext:Container ID="Container12" runat="server" Layout="ColumnLayout" Height="30" >
                            <Items>                                
                                <ext:Container ID="Container14" runat="server" LabelAlign="Right" Layout="FormLayout" ColumnWidth=".5" >
                                    <Items>
                                        <ext:ComboBox     
	                                        ID="cbConsultorio"                                                                              
	                                        runat="server" 
	                                        Shadow="Drop" 
	                                        Mode="Local" 
	                                        TriggerAction="All" 
	                                        ForceSelection="true"
	                                        DisplayField="nombre"
	                                        ValueField="codigo"	
	                                        EmptyText="Seleccione Consultorio"
	                                        AnchorHorizontal="99%"
                                            AllowBlank="false"
	                                        FieldLabel="Consultorio"
                                            DataIndex="consultorio"
	                                        >
	                                        <Store>
		                                        <ext:Store ID="stConsultorio" runat="server" AutoLoad="true">
			                                        <Reader>
				                                        <ext:JsonReader IDProperty="codigo">
					                                        <Fields>
						                                        <ext:RecordField Name="codigo" />
						                                        <ext:RecordField Name="nombre" />
					                                        </Fields>
				                                        </ext:JsonReader>
			                                        </Reader>            
		                                        </ext:Store>    
	                                        </Store>    
                                        </ext:ComboBox>
                                    </Items>
                                </ext:Container>
                                <ext:Container ID="Container17" runat="server" LabelAlign="Right" Layout="FormLayout" ColumnWidth=".5" >
                                    <Items>
                                        <ext:ComboBox     
	                                        ID="cbTipServicio"                                                                              
	                                        runat="server" 
	                                        Shadow="Drop" 
	                                        Mode="Local" 
	                                        TriggerAction="All" 
	                                        ForceSelection="true"
	                                        DisplayField="nombre"
	                                        ValueField="codser"	
	                                        EmptyText="Seleccione Tipo de Servicio"
	                                        AnchorHorizontal="99%"
	                                        AllowBlank="false"
                                            FieldLabel="Servicio"
                                            DataIndex="codser"
	                                        >
	                                        <Store>
		                                        <ext:Store ID="stTipServicio" runat="server" AutoLoad="true">
			                                        <Reader>
				                                        <ext:JsonReader IDProperty="codser">
					                                        <Fields>
						                                        <ext:RecordField Name="codser" />
						                                        <ext:RecordField Name="nombre" />
					                                        </Fields>
				                                        </ext:JsonReader>
			                                        </Reader>            
		                                        </ext:Store>    
	                                        </Store>    
                                        </ext:ComboBox>
                                    </Items>
                                </ext:Container>
                                
                            </Items>
                        </ext:Container>
                        <ext:Container ID="contFechas" runat="server" Layout="ColumnLayout" Height="180" >
                            <Items>
                                <ext:Container ID="Container16" runat="server" LabelAlign="Right" Layout="FormLayout" ColumnWidth=".4" >
                                    <Items>
                                        <ext:Button runat="server" ID="btnRepetir" Text="Repetir Días" Icon="Calendar" />
                                    </Items>
                                </ext:Container>
                                <ext:Container ID="Container21" runat="server" LabelAlign="Right" Layout="FormLayout" ColumnWidth=".6" >
                                    <Items>
                                        <ext:GridPanel
                                            ID="grdFechas"
                                            runat="server"                                            
                                            Margins="0 0 5 5"
                                            Icon="TransmitGo"
                                            Frame="true"
                                            Height="150"
                                            StoreId="stFechas"
                                            Title="Fechas"
                                            >
                                            <ColumnModel ID="ColumnModel1" runat="server">
                                                <Columns>
                                                    <ext:Column Header="Fecha" DataIndex="Fecha" >
                                                        <Commands>
                                                            <ext:ImageCommand CommandName="DeleteMe" Icon="NoteDelete">
                                                                <ToolTip Text="Eliminar" />
                                                            </ext:ImageCommand>                                                            
                                                        </Commands>
                                                    </ext:Column>                        
                                                </Columns>                                
                                            </ColumnModel>
                                            <SelectionModel>
                                                <ext:RowSelectionModel ID="RowSelectionModel2" runat="server" SingleSelect="true" >                        
                                                </ext:RowSelectionModel>
                                            </SelectionModel>
                                            <Listeners>
                                                <Command Fn="commandHandler" />
                                            </Listeners>
                                            <LoadMask ShowMask="true" Msg="Loading...." />
                                            <BottomBar>
                                                <ext:StatusBar runat="server" ID="StatusBar1">
                                                    <Items>                                                                                    
                                                    </Items>
                                                </ext:StatusBar>
                                            </BottomBar>
                                        </ext:GridPanel>
                                    </Items>
                                </ext:Container>
                            </Items>
                        </ext:Container>                        
                        <ext:Container ID="conCancelacion" runat="server" Layout="ColumnLayout" Height="180" >
                            <Items>
                                <ext:Container ID="Container19" runat="server" LabelAlign="Right" Layout="FormLayout" ColumnWidth=".7" >
                                    <Items>
                                        <ext:TextField runat="server" ID="txtMotivoCancelacion" FieldLabel="M.Cancelación" AnchorHorizontal="100%" />
                                        <ext:TextArea runat="server" ID="txtAgendas" FieldLabel="Agendas" AnchorHorizontal="100%" Height="130" />
                                    </Items>
                                </ext:Container>
                            </Items>
                        </ext:Container>
                    </Items>
                    <Buttons>
                        <ext:Button ID="btnGuardaAPas" runat="server" Text="Guardar" Icon="Disk">
                            <DirectEvents>
                                <Click 
                                    OnEvent="btnGuardaAPas_DirectClick" 
                                    Before="var valid= #{StatusForm}.getForm().isValid(); if (valid) {#{FormStatusBar}.showBusy('Guardando Formulario...');} return valid;">
                                    <EventMask 
                                        ShowMask="true" 
                                        MinDelay="1000" 
                                        Target="CustomTarget" 
                                        CustomTarget="={#{StatusForm}.getEl()}" 
                                        />
                                </Click>
                            </DirectEvents>
                        </ext:Button>
                        <ext:Button runat="server" ID="btnCerrarAPas" Text="Cerrar" />
                    </Buttons>
                </ext:FormPanel>
            </Items>
            <BottomBar>
                <ext:StatusBar ID="FormStatusBar" runat="server" DefaultText="Ready">
                    <Plugins>
                        <ext:ValidationStatus ID="ValidationStatus1" 
                            runat="server" 
                            FormPanelID="StatusForm" 
                            ValidIcon="Accept" 
                            ErrorIcon="Exclamation" 
                            ShowText="El Formulario tiene Errores (Click para ver Detalles)"
                            HideText="Click para Ocultar Errores"
                            />
                    </Plugins>
                </ext:StatusBar>
            </BottomBar>
        </ext:Panel>
        </Items>
    </ext:Window> 
    
    
    <ext:Window
        ID="vntDias"
        runat="server"
        Icon="ApplicationFormEdit"
        Width="320" 
        Height="350" 
        Hidden="true" 
        Modal="true"
        Constrain="true"   
        Closable="false" 
        >
        <AutoLoad 
            Url="diasAgenda.aspx" 
            Mode="IFrame" 
            TriggerEvent="show" 
            ReloadOnEvent="true" 
            ShowMask="true" 
            MaskMsg="Agenda...">
            <Params>
                <ext:Parameter Name="Csc" Value="" Mode="Value" />
            </Params>
        </AutoLoad>
        <Buttons>
            <ext:Button ID="btnCerrarDias" runat="server" Text="Cerrar"  Icon="CogStop" />                
        </Buttons>    
    </ext:Window>
       
    </form>
</body>
</html>

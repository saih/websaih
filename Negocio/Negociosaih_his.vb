Public Class Negociosaih_his

    Public Function Altasaih_his(
        ByVal numhis As String,
        ByVal identificacion As String,
        ByVal tipo_identificacion As String,
        ByVal primer_nombre As String,
        ByVal segundo_nombre As String,
        ByVal primer_apellido As String,
        ByVal segundo_apellido As String,
        ByVal fecha_nacimiento As String,
        ByVal id_lugar As Integer,
        ByVal unidad_medida As String,
        ByVal sexo As String,
        ByVal estado_civil As String,
        ByVal direccion As String,
        ByVal telefono As String,
        ByVal telefonoaux As String,
        ByVal id_lugar_municipio As Integer,
        ByVal id_lugar_barrio As Integer,
        ByVal zona_residencia As String,
        ByVal estrato As String,
        ByVal codemp As String,
        ByVal codarp As String,
        ByVal codccf As String,
        ByVal apellido_conyuge As String,
        ByVal nombre_conyuge As String,
        ByVal nombre_padre As String,
        ByVal nombre_madre As String,
        ByVal usuario_creo As String,
        ByVal fecha_creacion As String,
        ByVal usuario_modifico As String,
        ByVal fecha_modificacion As String,
        ByVal observacion As String,
        ByVal nrocon As String,
        ByVal ubica As String,
        ByVal factor_hr As String,
        ByVal fecha_muerte As String,
        ByVal url_foto As String,
        ByVal etnia As String,
        ByVal huella As String,
        ByVal nivel_escolar As String,
        ByVal responsable_economico As String,
        ByVal religion As String,
        ByVal tipo_residencia As String,
        ByVal escolar As String,
        ByVal razonsocial As String,
        ByVal estatura As Decimal,
        ByVal observaciones As String,
        ByVal codocu As String,
         ByVal discapacidad As String
      )
        Dim saih_his As New Datos.saih_his(numhis, identificacion, tipo_identificacion, primer_nombre, segundo_nombre, primer_apellido, segundo_apellido, fecha_nacimiento, id_lugar, unidad_medida, sexo, estado_civil, direccion, telefono, telefonoaux, id_lugar_municipio, id_lugar_barrio, zona_residencia, estrato, codemp, codarp, codccf, apellido_conyuge, nombre_conyuge, nombre_padre, nombre_madre, usuario_creo, fecha_creacion, usuario_modifico, fecha_modificacion, observacion, nrocon, ubica, factor_hr, fecha_muerte, url_foto, etnia, huella, nivel_escolar, responsable_economico, religion, tipo_residencia, escolar, razonsocial, estatura, observaciones, codocu, discapacidad)
        Dim dtsaih_his As New Datos.Datosaih_his
        Return dtsaih_his.Insertar(saih_his)
    End Function

    Public Function Altasaih_his(
        ByVal saih_his As Datos.saih_his
  )
        Dim dtsaih_his As New Datos.Datosaih_his
        Return dtsaih_his.Insertar(saih_his)
    End Function

    Public Function AltaRetsaih_his(
        ByVal numhis As String,
        ByVal identificacion As String,
        ByVal tipo_identificacion As String,
        ByVal primer_nombre As String,
        ByVal segundo_nombre As String,
        ByVal primer_apellido As String,
        ByVal segundo_apellido As String,
        ByVal fecha_nacimiento As String,
        ByVal id_lugar As Integer,
        ByVal unidad_medida As String,
        ByVal sexo As String,
        ByVal estado_civil As String,
        ByVal direccion As String,
        ByVal telefono As String,
        ByVal telefonoaux As String,
        ByVal id_lugar_municipio As Integer,
        ByVal id_lugar_barrio As Integer,
        ByVal zona_residencia As String,
        ByVal estrato As String,
        ByVal codemp As String,
        ByVal codarp As String,
        ByVal codccf As String,
        ByVal apellido_conyuge As String,
        ByVal nombre_conyuge As String,
        ByVal nombre_padre As String,
        ByVal nombre_madre As String,
        ByVal usuario_creo As String,
        ByVal fecha_creacion As String,
        ByVal usuario_modifico As String,
        ByVal fecha_modificacion As String,
        ByVal observacion As String,
        ByVal nrocon As String,
        ByVal ubica As String,
        ByVal factor_hr As String,
        ByVal fecha_muerte As String,
        ByVal url_foto As String,
        ByVal etnia As String,
        ByVal huella As String,
        ByVal nivel_escolar As String,
        ByVal responsable_economico As String,
        ByVal religion As String,
        ByVal tipo_residencia As String,
        ByVal escolar As String,
        ByVal razonsocial As String,
        ByVal estatura As Decimal,
        ByVal observaciones As String,
        ByVal codocu As String,
        ByVal discapacidad As String
      ) As Integer
        Dim saih_his As New Datos.saih_his(numhis, identificacion, tipo_identificacion, primer_nombre, segundo_nombre, primer_apellido, segundo_apellido, fecha_nacimiento, id_lugar, unidad_medida, sexo, estado_civil, direccion, telefono, telefonoaux, id_lugar_municipio, id_lugar_barrio, zona_residencia, estrato, codemp, codarp, codccf, apellido_conyuge, nombre_conyuge, nombre_padre, nombre_madre, usuario_creo, fecha_creacion, usuario_modifico, fecha_modificacion, observacion, nrocon, ubica, factor_hr, fecha_muerte, url_foto, etnia, huella, nivel_escolar, responsable_economico, religion, tipo_residencia, escolar, razonsocial, estatura, observaciones, codocu, discapacidad)
        Dim dtsaih_his As New Datos.Datosaih_his
        Return dtsaih_his.InsertarRet(saih_his)
    End Function

    Public Function AltaRetsaih_his(
        ByVal saih_his As Datos.saih_his
  )
        Dim dtsaih_his As New Datos.Datosaih_his
        Return dtsaih_his.InsertarRet(saih_his)
    End Function

    Public Function Editasaih_his(
        ByVal numhis As String,
        ByVal identificacion As String,
        ByVal tipo_identificacion As String,
        ByVal primer_nombre As String,
        ByVal segundo_nombre As String,
        ByVal primer_apellido As String,
        ByVal segundo_apellido As String,
        ByVal fecha_nacimiento As String,
        ByVal id_lugar As Integer,
        ByVal unidad_medida As String,
        ByVal sexo As String,
        ByVal estado_civil As String,
        ByVal direccion As String,
        ByVal telefono As String,
        ByVal telefonoaux As String,
        ByVal id_lugar_municipio As Integer,
        ByVal id_lugar_barrio As Integer,
        ByVal zona_residencia As String,
        ByVal estrato As String,
        ByVal codemp As String,
        ByVal codarp As String,
        ByVal codccf As String,
        ByVal apellido_conyuge As String,
        ByVal nombre_conyuge As String,
        ByVal nombre_padre As String,
        ByVal nombre_madre As String,
        ByVal usuario_creo As String,
        ByVal fecha_creacion As String,
        ByVal usuario_modifico As String,
        ByVal fecha_modificacion As String,
        ByVal observacion As String,
        ByVal nrocon As String,
        ByVal ubica As String,
        ByVal factor_hr As String,
        ByVal fecha_muerte As String,
        ByVal url_foto As String,
        ByVal etnia As String,
        ByVal huella As String,
        ByVal nivel_escolar As String,
        ByVal responsable_economico As String,
        ByVal religion As String,
        ByVal tipo_residencia As String,
        ByVal escolar As String,
        ByVal razonsocial As String,
        ByVal estatura As Decimal,
        ByVal observaciones As String,
        ByVal codocu As String,
         ByVal discapacidad As String
      )
        Dim saih_his As New Datos.saih_his(numhis, identificacion, tipo_identificacion, primer_nombre, segundo_nombre, primer_apellido, segundo_apellido, fecha_nacimiento, id_lugar, unidad_medida, sexo, estado_civil, direccion, telefono, telefonoaux, id_lugar_municipio, id_lugar_barrio, zona_residencia, estrato, codemp, codarp, codccf, apellido_conyuge, nombre_conyuge, nombre_padre, nombre_madre, usuario_creo, fecha_creacion, usuario_modifico, fecha_modificacion, observacion, nrocon, ubica, factor_hr, fecha_muerte, url_foto, etnia, huella, nivel_escolar, responsable_economico, religion, tipo_residencia, escolar, razonsocial, estatura, observaciones, codocu, discapacidad)
        Dim dtsaih_his As New Datos.Datosaih_his
        Return dtsaih_his.Editar(saih_his)
    End Function

    Public Function Editasaih_his(
        ByVal saih_his As Datos.saih_his
  )
        Dim dtsaih_his As New Datos.Datosaih_his
        Return dtsaih_his.Editar(saih_his)
    End Function

    Public Function Eliminasaih_his(
         ByVal numhis As String)
        Dim dtsaih_his As New Datos.Datosaih_his
        Return dtsaih_his.Borrar(numhis)
    End Function

    Public Function Obtenersaih_hisById(
        ByVal numhis As String) As Datos.saih_his
        Dim saih_his As Datos.saih_his = Nothing
        Dim dtsaih_his As New Datos.Datosaih_his
        For Each saih_his In dtsaih_his.Select_ById_saih_his(numhis)
        Next
        Return saih_his
    End Function

    Public Function Obtenersaih_his() As List(Of Datos.saih_his)
        Dim dtsaih_his As New Datos.Datosaih_his
        Return dtsaih_his.Select_saih_his()
    End Function

    Public Function Obtenersaih_hisbyWhere(ByVal sWhere As String) As List(Of Datos.saih_his)
        Dim dtsaih_his As New Datos.Datosaih_his
        Return dtsaih_his.Select_Where_saih_his(sWhere)
    End Function
End Class
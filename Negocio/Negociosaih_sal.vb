Public Class Negociosaih_sal

  Public Function Altasaih_sal(
      ByVal id As integer,
      ByVal id_prm As integer,
      ByVal fecha_inicio As string,
      ByVal fecha_fin As string,
      ByVal estado As string,
      ByVal salario As decimal,
      ByVal observaciones As string
    )
      Dim saih_sal As New Datos.saih_sal(id,id_prm,fecha_inicio,fecha_fin,estado,salario,observaciones)
        Dim dtsaih_sal As New Datos.Datosaih_sal
        Return dtsaih_sal.Insertar(saih_sal)
    End Function

  Public Function AltaRetsaih_sal(
      ByVal id As integer,
      ByVal id_prm As integer,
      ByVal fecha_inicio As string,
      ByVal fecha_fin As string,
      ByVal estado As string,
      ByVal salario As decimal,
      ByVal observaciones As string
    ) As Integer
      Dim saih_sal As New Datos.saih_sal(id,id_prm,fecha_inicio,fecha_fin,estado,salario,observaciones)
        Dim dtsaih_sal As New Datos.Datosaih_sal
        Return dtsaih_sal.InsertarRet(saih_sal)
    End Function

  Public Function Editasaih_sal(
      ByVal id As integer,
      ByVal id_prm As integer,
      ByVal fecha_inicio As string,
      ByVal fecha_fin As string,
      ByVal estado As string,
      ByVal salario As decimal,
      ByVal observaciones As string
    )
      Dim saih_sal As New Datos.saih_sal(id,id_prm,fecha_inicio,fecha_fin,estado,salario,observaciones)
        Dim dtsaih_sal As New Datos.Datosaih_sal
        Return dtsaih_sal.Editar(saih_sal)
    End Function

 Public Function Eliminasaih_sal(
      ByVal id As integer)
        Dim dtsaih_sal As New Datos.Datosaih_sal
        Return dtsaih_sal.Borrar(id)
    End Function

  Public Function Obtenersaih_salById(
      ByVal id As integer) As Datos.saih_sal
        Dim saih_sal As Datos.saih_sal = Nothing
        Dim dtsaih_sal As New Datos.Datosaih_sal
        For Each saih_sal In dtsaih_sal.Select_ById_saih_sal(id)
        Next
        Return saih_sal
    End Function

  Public Function Obtenersaih_sal() As List(Of Datos.saih_sal)
        Dim dtsaih_sal As New Datos.Datosaih_sal
        Return dtsaih_sal.Select_saih_sal()
    End Function

  Public Function Obtenersaih_salbyWhere(ByVal sWhere as String) As List(Of Datos.saih_sal)
        Dim dtsaih_sal As New Datos.Datosaih_sal
        Return dtsaih_sal.Select_Where_saih_sal(sWhere)
    End Function
End Class
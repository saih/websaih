Public Class NegocioUsuarioClave

  Public Function AltaUsuarioClave(
      ByVal id As integer,
      ByVal Usuario As string,
      ByVal fecha As string,
      ByVal clave As string
    )
      Dim UsuarioClave As New Datos.UsuarioClave(id,Usuario,fecha,clave)
        Dim dtUsuarioClave As New Datos.DatoUsuarioClave
        Return dtUsuarioClave.Insertar(UsuarioClave)
    End Function

  Public Function AltaRetUsuarioClave(
      ByVal id As integer,
      ByVal Usuario As string,
      ByVal fecha As string,
      ByVal clave As string
    ) As Integer
      Dim UsuarioClave As New Datos.UsuarioClave(id,Usuario,fecha,clave)
        Dim dtUsuarioClave As New Datos.DatoUsuarioClave
        Return dtUsuarioClave.InsertarRet(UsuarioClave)
    End Function

  Public Function EditaUsuarioClave(
      ByVal id As integer,
      ByVal Usuario As string,
      ByVal fecha As string,
      ByVal clave As string
    )
      Dim UsuarioClave As New Datos.UsuarioClave(id,Usuario,fecha,clave)
        Dim dtUsuarioClave As New Datos.DatoUsuarioClave
        Return dtUsuarioClave.Editar(UsuarioClave)
    End Function

 Public Function EliminaUsuarioClave(
      ByVal id As integer)
        Dim dtUsuarioClave As New Datos.DatoUsuarioClave
        Return dtUsuarioClave.Borrar(id)
    End Function

  Public Function ObtenerUsuarioClaveById(
      ByVal id As integer) As Datos.UsuarioClave
        Dim UsuarioClave As Datos.UsuarioClave = Nothing
        Dim dtUsuarioClave As New Datos.DatoUsuarioClave
        For Each UsuarioClave In dtUsuarioClave.Select_ById_UsuarioClave(id)
        Next
        Return UsuarioClave
    End Function

  Public Function ObtenerUsuarioClave() As List(Of Datos.UsuarioClave)
        Dim dtUsuarioClave As New Datos.DatoUsuarioClave
        Return dtUsuarioClave.Select_UsuarioClave()
    End Function

  Public Function ObtenerUsuarioClavebyWhere(ByVal sWhere as String) As List(Of Datos.UsuarioClave)
        Dim dtUsuarioClave As New Datos.DatoUsuarioClave
        Return dtUsuarioClave.Select_Where_UsuarioClave(sWhere)
    End Function
End Class
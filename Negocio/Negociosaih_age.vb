Public Class Negociosaih_age

  Public Function Altasaih_age(
      ByVal id As integer,
      ByVal codpre As string,
      ByVal nroage As string,
      ByVal fecha_adicion As string,
      ByVal fecha_modificacion As string,
      ByVal usuario_creo As string,
      ByVal usuario_modifico As string,
      ByVal fecha_cita As string,
      ByVal numhis As string,
      ByVal codemp As string,
      ByVal codpas As string,
      ByVal codpcd As string,
      ByVal codser As string,
      ByVal consultorio As string,
      ByVal estado As string,
      ByVal cod_cancelacion As string,
      ByVal es_nueva As integer,
      ByVal observaciones As string,
      ByVal fecha_fincita As string,
      ByVal nropme As string,
      ByVal telefono As string,
      ByVal manual As string
    )
      Dim saih_age As New Datos.saih_age(id,codpre,nroage,fecha_adicion,fecha_modificacion,usuario_creo,usuario_modifico,fecha_cita,numhis,codemp,codpas,codpcd,codser,consultorio,estado,cod_cancelacion,es_nueva,observaciones,fecha_fincita,nropme,telefono,manual)
        Dim dtsaih_age As New Datos.Datosaih_age
        Return dtsaih_age.Insertar(saih_age)
    End Function

  Public Function Altasaih_age(
      ByVal saih_age As Datos.saih_age    
)
        Dim dtsaih_age As New Datos.Datosaih_age
        Return dtsaih_age.Insertar(saih_age)
    End Function

  Public Function AltaRetsaih_age(
      ByVal id As integer,
      ByVal codpre As string,
      ByVal nroage As string,
      ByVal fecha_adicion As string,
      ByVal fecha_modificacion As string,
      ByVal usuario_creo As string,
      ByVal usuario_modifico As string,
      ByVal fecha_cita As string,
      ByVal numhis As string,
      ByVal codemp As string,
      ByVal codpas As string,
      ByVal codpcd As string,
      ByVal codser As string,
      ByVal consultorio As string,
      ByVal estado As string,
      ByVal cod_cancelacion As string,
      ByVal es_nueva As integer,
      ByVal observaciones As string,
      ByVal fecha_fincita As string,
      ByVal nropme As string,
      ByVal telefono As string,
      ByVal manual As string
    ) As Integer
      Dim saih_age As New Datos.saih_age(id,codpre,nroage,fecha_adicion,fecha_modificacion,usuario_creo,usuario_modifico,fecha_cita,numhis,codemp,codpas,codpcd,codser,consultorio,estado,cod_cancelacion,es_nueva,observaciones,fecha_fincita,nropme,telefono,manual)
        Dim dtsaih_age As New Datos.Datosaih_age
        Return dtsaih_age.InsertarRet(saih_age)
    End Function

  Public Function AltaRetsaih_age(
      ByVal saih_age As Datos.saih_age    
)
        Dim dtsaih_age As New Datos.Datosaih_age
        Return dtsaih_age.InsertarRet(saih_age)
    End Function

  Public Function Editasaih_age(
      ByVal id As integer,
      ByVal codpre As string,
      ByVal nroage As string,
      ByVal fecha_adicion As string,
      ByVal fecha_modificacion As string,
      ByVal usuario_creo As string,
      ByVal usuario_modifico As string,
      ByVal fecha_cita As string,
      ByVal numhis As string,
      ByVal codemp As string,
      ByVal codpas As string,
      ByVal codpcd As string,
      ByVal codser As string,
      ByVal consultorio As string,
      ByVal estado As string,
      ByVal cod_cancelacion As string,
      ByVal es_nueva As integer,
      ByVal observaciones As string,
      ByVal fecha_fincita As string,
      ByVal nropme As string,
      ByVal telefono As string,
      ByVal manual As string
    )
      Dim saih_age As New Datos.saih_age(id,codpre,nroage,fecha_adicion,fecha_modificacion,usuario_creo,usuario_modifico,fecha_cita,numhis,codemp,codpas,codpcd,codser,consultorio,estado,cod_cancelacion,es_nueva,observaciones,fecha_fincita,nropme,telefono,manual)
        Dim dtsaih_age As New Datos.Datosaih_age
        Return dtsaih_age.Editar(saih_age)
    End Function

  Public Function Editasaih_age(
      ByVal saih_age As Datos.saih_age    
)
        Dim dtsaih_age As New Datos.Datosaih_age
        Return dtsaih_age.Editar(saih_age)
    End Function

 Public Function Eliminasaih_age(
      ByVal id As integer)
        Dim dtsaih_age As New Datos.Datosaih_age
        Return dtsaih_age.Borrar(id)
    End Function

  Public Function Obtenersaih_ageById(
      ByVal id As integer) As Datos.saih_age
        Dim saih_age As Datos.saih_age = Nothing
        Dim dtsaih_age As New Datos.Datosaih_age
        For Each saih_age In dtsaih_age.Select_ById_saih_age(id)
        Next
        Return saih_age
    End Function

  Public Function Obtenersaih_age() As List(Of Datos.saih_age)
        Dim dtsaih_age As New Datos.Datosaih_age
        Return dtsaih_age.Select_saih_age()
    End Function

  Public Function Obtenersaih_agebyWhere(ByVal sWhere as String) As List(Of Datos.saih_age)
        Dim dtsaih_age As New Datos.Datosaih_age
        Return dtsaih_age.Select_Where_saih_age(sWhere)
    End Function
End Class
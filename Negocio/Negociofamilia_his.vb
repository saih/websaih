Public Class Negociofamilia_his

  Public Function Altafamilia_his(
      ByVal id_familia As integer,
      ByVal numhis As string,
      ByVal rol As string,
      ByVal fecha_adicion As string,
      ByVal usuario_creo As string,
      ByVal fecha_modificacion As string,
      ByVal usuario_modifico As string
    )
      Dim familia_his As New Datos.familia_his(id_familia,numhis,rol,fecha_adicion,usuario_creo,fecha_modificacion,usuario_modifico)
        Dim dtfamilia_his As New Datos.Datofamilia_his
        Return dtfamilia_his.Insertar(familia_his)
    End Function

  Public Function Altafamilia_his(
      ByVal familia_his As Datos.familia_his    
)
        Dim dtfamilia_his As New Datos.Datofamilia_his
        Return dtfamilia_his.Insertar(familia_his)
    End Function

  Public Function AltaRetfamilia_his(
      ByVal id_familia As integer,
      ByVal numhis As string,
      ByVal rol As string,
      ByVal fecha_adicion As string,
      ByVal usuario_creo As string,
      ByVal fecha_modificacion As string,
      ByVal usuario_modifico As string
    ) As Integer
      Dim familia_his As New Datos.familia_his(id_familia,numhis,rol,fecha_adicion,usuario_creo,fecha_modificacion,usuario_modifico)
        Dim dtfamilia_his As New Datos.Datofamilia_his
        Return dtfamilia_his.InsertarRet(familia_his)
    End Function

  Public Function AltaRetfamilia_his(
      ByVal familia_his As Datos.familia_his    
)
        Dim dtfamilia_his As New Datos.Datofamilia_his
        Return dtfamilia_his.InsertarRet(familia_his)
    End Function

  Public Function Editafamilia_his(
      ByVal id_familia As integer,
      ByVal numhis As string,
      ByVal rol As string,
      ByVal fecha_adicion As string,
      ByVal usuario_creo As string,
      ByVal fecha_modificacion As string,
      ByVal usuario_modifico As string
    )
      Dim familia_his As New Datos.familia_his(id_familia,numhis,rol,fecha_adicion,usuario_creo,fecha_modificacion,usuario_modifico)
        Dim dtfamilia_his As New Datos.Datofamilia_his
        Return dtfamilia_his.Editar(familia_his)
    End Function

  Public Function Editafamilia_his(
      ByVal familia_his As Datos.familia_his    
)
        Dim dtfamilia_his As New Datos.Datofamilia_his
        Return dtfamilia_his.Editar(familia_his)
    End Function

 Public Function Eliminafamilia_his(
      ByVal id_familia As integer,
      ByVal numhis As string)
        Dim dtfamilia_his As New Datos.Datofamilia_his
        Return dtfamilia_his.Borrar(id_familia,numhis)
    End Function

  Public Function Obtenerfamilia_hisById(
      ByVal id_familia As integer,
      ByVal numhis As string) As Datos.familia_his
        Dim familia_his As Datos.familia_his = Nothing
        Dim dtfamilia_his As New Datos.Datofamilia_his
        For Each familia_his In dtfamilia_his.Select_ById_familia_his(id_familia,numhis)
        Next
        Return familia_his
    End Function

  Public Function Obtenerfamilia_his() As List(Of Datos.familia_his)
        Dim dtfamilia_his As New Datos.Datofamilia_his
        Return dtfamilia_his.Select_familia_his()
    End Function

  Public Function Obtenerfamilia_hisbyWhere(ByVal sWhere as String) As List(Of Datos.familia_his)
        Dim dtfamilia_his As New Datos.Datofamilia_his
        Return dtfamilia_his.Select_Where_familia_his(sWhere)
    End Function
End Class
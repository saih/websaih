﻿Public Class NegocioUsuarioSede
    Public Function AltaUsuarioSede(
        ByVal UsuarioSede As Integer,
        ByVal idUsuario As String,
        ByVal idSede As String
      )
        Dim UsuarioSedes As New Datos.UsuarioSede(UsuarioSede, idUsuario, idSede)
        Dim dtUsuarioSede As New Datos.DatoUsuarioSede
        Return dtUsuarioSede.Insertar(UsuarioSedes)
    End Function


    Public Function AltaUsuarioSede(
        ByVal usuarioSede As Datos.UsuarioSede
  )
        Dim dtusuarioSede As New Datos.DatoUsuarioSede
        Return dtusuarioSede.Insertar(usuarioSede)
    End Function

    Public Function AltaRetUsuarioSede(
        ByVal UsuarioSede As Integer,
        ByVal idUsuario As String,
        ByVal idSede As String
      ) As Integer
        Dim UsuarioSedes As New Datos.UsuarioSede(UsuarioSede, idUsuario, idSede)
        Dim dtUsuarioSede As New Datos.DatoUsuarioSede
        Return dtUsuarioSede.InsertarRet(UsuarioSedes)
    End Function

    Public Function EditaUsuarioSede(
        ByVal UsuarioSede As Integer,
        ByVal idUsuario As String,
        ByVal idSede As String
      )
        Dim UsuarioSedes As New Datos.UsuarioSede(UsuarioSede, idUsuario, idSede)
        Dim dtUsuarioSede As New Datos.DatoUsuarioSede
        Return dtUsuarioSede.Editar(UsuarioSedes)
    End Function
    Public Function EditaUsuarioSede(
     ByVal usuarioSede As Datos.UsuarioSede
)
        Dim dtusuariosede As New Datos.DatoUsuarioSede
        Return dtusuariosede.Editar(usuarioSede)
    End Function

    Public Function EliminaUsuarioSede(
         ByVal id As Integer)
        Dim dtUsuarioSede As New Datos.DatoUsuarioSede
        Return dtUsuarioSede.Borrar(id)
    End Function

    Public Function ObtenerUsuarioSedeById(
        ByVal id As Integer) As Datos.UsuarioSede
        Dim UsuarioSede As Datos.UsuarioSede = Nothing
        Dim dtUsuarioSede As New Datos.DatoUsuarioSede
        For Each UsuarioSede In dtUsuarioSede.Select_ById_UsuarioSede(id)
        Next
        Return UsuarioSede
    End Function

    Public Function ObtenerUsuarioSede() As List(Of Datos.UsuarioSede)
        Dim dtUsuarioSede As New Datos.DatoUsuarioSede
        Return dtUsuarioSede.Select_UsuarioSede()
    End Function

    Public Function ObtenerUsuarioSedebyWhere(ByVal sWhere As String) As List(Of Datos.UsuarioSede)
        Dim dtUsuarioSede As New Datos.DatoUsuarioSede
        Return dtUsuarioSede.Select_Where_UsuarioSede(sWhere)
    End Function
End Class


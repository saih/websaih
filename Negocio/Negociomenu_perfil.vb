Public Class Negociomenu_perfil

  Public Function Altamenu_perfil(
      ByVal idperfil As string,
      ByVal descripcion As string,
      ByVal Perfil_Csc As integer,
      ByVal manAtenciones As integer
    )
      Dim menu_perfil As New Datos.menu_perfil(idperfil,descripcion,Perfil_Csc,manAtenciones)
        Dim dtmenu_perfil As New Datos.Datomenu_perfil
        Return dtmenu_perfil.Insertar(menu_perfil)
    End Function

  Public Function AltaRetmenu_perfil(
      ByVal idperfil As string,
      ByVal descripcion As string,
      ByVal Perfil_Csc As integer,
      ByVal manAtenciones As integer
    ) As Integer
      Dim menu_perfil As New Datos.menu_perfil(idperfil,descripcion,Perfil_Csc,manAtenciones)
        Dim dtmenu_perfil As New Datos.Datomenu_perfil
        Return dtmenu_perfil.InsertarRet(menu_perfil)
    End Function

  Public Function Editamenu_perfil(
      ByVal idperfil As string,
      ByVal descripcion As string,
      ByVal Perfil_Csc As integer,
      ByVal manAtenciones As integer
    )
      Dim menu_perfil As New Datos.menu_perfil(idperfil,descripcion,Perfil_Csc,manAtenciones)
        Dim dtmenu_perfil As New Datos.Datomenu_perfil
        Return dtmenu_perfil.Editar(menu_perfil)
    End Function

  Public Function Obtenermenu_perfilById(
      ByVal idperfil As string) As Datos.menu_perfil
        Dim menu_perfil As Datos.menu_perfil = Nothing
        Dim dtmenu_perfil As New Datos.Datomenu_perfil
        For Each menu_perfil In dtmenu_perfil.Select_ById_menu_perfil(idperfil)
        Next
        Return menu_perfil
    End Function

  Public Function Obtenermenu_perfil() As List(Of Datos.menu_perfil)
        Dim dtmenu_perfil As New Datos.Datomenu_perfil
        Return dtmenu_perfil.Select_menu_perfil()
    End Function

  Public Function Obtenermenu_perfilbyWhere(ByVal sWhere as String) As List(Of Datos.menu_perfil)
        Dim dtmenu_perfil As New Datos.Datomenu_perfil
        Return dtmenu_perfil.Select_Where_menu_perfil(sWhere)
    End Function
End Class
Public Class NegocioPaginaWebControl

  Public Function AltaPaginaWebControl(
      ByVal id As integer,
      ByVal id_PaginaWeb As integer,
      ByVal nombre As string,
      ByVal tipo As string,
      ByVal descripcion As string
    )
      Dim PaginaWebControl As New Datos.PaginaWebControl(id,id_PaginaWeb,nombre,tipo,descripcion)
        Dim dtPaginaWebControl As New Datos.DatoPaginaWebControl
        Return dtPaginaWebControl.Insertar(PaginaWebControl)
    End Function

  Public Function EditaPaginaWebControl(
      ByVal id As integer,
      ByVal id_PaginaWeb As integer,
      ByVal nombre As string,
      ByVal tipo As string,
      ByVal descripcion As string
    )
      Dim PaginaWebControl As New Datos.PaginaWebControl(id,id_PaginaWeb,nombre,tipo,descripcion)
        Dim dtPaginaWebControl As New Datos.DatoPaginaWebControl
        dtPaginaWebControl.Editar(PaginaWebControl)
        Return True
    End Function

  Public Function ObtenerPaginaWebControlById(
      ByVal id As integer) As Datos.PaginaWebControl
        Dim PaginaWebControl As Datos.PaginaWebControl = Nothing
        Dim dtPaginaWebControl As New Datos.DatoPaginaWebControl
        For Each PaginaWebControl In dtPaginaWebControl.Select_ById_PaginaWebControl(id)
        Next
        Return PaginaWebControl
    End Function

  Public Function ObtenerPaginaWebControl() As List(Of Datos.PaginaWebControl)
        Dim dtPaginaWebControl As New Datos.DatoPaginaWebControl
        Return dtPaginaWebControl.Select_PaginaWebControl()
    End Function
    Public Function ObtenerPaginaWebControlbyWhere(ByVal sWhere As String) As List(Of Datos.PaginaWebControl)
        Dim dtPaginaWebControl As New Datos.DatoPaginaWebControl
        Return dtPaginaWebControl.Select_Where_PaginaWebControl(sWhere)
    End Function
End Class
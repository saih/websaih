Public Class Negociosaih_prm

  Public Function Altasaih_prm(
      ByVal codpre As string,
      ByVal lugar As string,
      ByVal numhis As string,
      ByVal nropac As string,
      ByVal numfac As string,
      ByVal nrocta As string,
      ByVal nroord As string,
      ByVal nronci As string,
      ByVal nroglo As string,
      ByVal nroges As string,
      ByVal nrocit As string,
      ByVal norage As string,
      ByVal norres As string,
      ByVal nroepi As string,
      ByVal nroiqu As string,
      ByVal indhuella As integer,
      ByVal nrorem As string,
      ByVal nroacm As string,
      ByVal nrofor As string,
      ByVal nropro As string,
      ByVal nropme As string,
      ByVal nrofur As string,
      ByVal id_lugar As integer,
      ByVal con_pago_ser_no As string,
      ByVal cod_abono_fac As string,
      ByVal cod_pago_act As string,
      ByVal cod_recaudo_copagos As string,
      ByVal cns_num_cuenta_cobro As string,
      ByVal cns_pag_cuenta_cobro As string,
      ByVal cod_recaudo_cuota_moderadora As string,
      ByVal HoraExpedicion As integer,
      ByVal HoraInicioAtencion As string,
      ByVal IntervaloCitas As integer,
      ByVal codurgencias As string,
      ByVal codambulatorias As string,
      ByVal codespecialidad As string,
      ByVal observaciones As string
    )
      Dim saih_prm As New Datos.saih_prm(codpre,lugar,numhis,nropac,numfac,nrocta,nroord,nronci,nroglo,nroges,nrocit,norage,norres,nroepi,nroiqu,indhuella,nrorem,nroacm,nrofor,nropro,nropme,nrofur,id_lugar,con_pago_ser_no,cod_abono_fac,cod_pago_act,cod_recaudo_copagos,cns_num_cuenta_cobro,cns_pag_cuenta_cobro,cod_recaudo_cuota_moderadora,HoraExpedicion,HoraInicioAtencion,IntervaloCitas,codurgencias,codambulatorias,codespecialidad,observaciones)
        Dim dtsaih_prm As New Datos.Datosaih_prm
        Return dtsaih_prm.Insertar(saih_prm)
    End Function

  Public Function Altasaih_prm(
      ByVal saih_prm As Datos.saih_prm    
)
        Dim dtsaih_prm As New Datos.Datosaih_prm
        Return dtsaih_prm.Insertar(saih_prm)
    End Function

  Public Function AltaRetsaih_prm(
      ByVal codpre As string,
      ByVal lugar As string,
      ByVal numhis As string,
      ByVal nropac As string,
      ByVal numfac As string,
      ByVal nrocta As string,
      ByVal nroord As string,
      ByVal nronci As string,
      ByVal nroglo As string,
      ByVal nroges As string,
      ByVal nrocit As string,
      ByVal norage As string,
      ByVal norres As string,
      ByVal nroepi As string,
      ByVal nroiqu As string,
      ByVal indhuella As integer,
      ByVal nrorem As string,
      ByVal nroacm As string,
      ByVal nrofor As string,
      ByVal nropro As string,
      ByVal nropme As string,
      ByVal nrofur As string,
      ByVal id_lugar As integer,
      ByVal con_pago_ser_no As string,
      ByVal cod_abono_fac As string,
      ByVal cod_pago_act As string,
      ByVal cod_recaudo_copagos As string,
      ByVal cns_num_cuenta_cobro As string,
      ByVal cns_pag_cuenta_cobro As string,
      ByVal cod_recaudo_cuota_moderadora As string,
      ByVal HoraExpedicion As integer,
      ByVal HoraInicioAtencion As string,
      ByVal IntervaloCitas As integer,
      ByVal codurgencias As string,
      ByVal codambulatorias As string,
      ByVal codespecialidad As string,
      ByVal observaciones As string
    ) As Integer
      Dim saih_prm As New Datos.saih_prm(codpre,lugar,numhis,nropac,numfac,nrocta,nroord,nronci,nroglo,nroges,nrocit,norage,norres,nroepi,nroiqu,indhuella,nrorem,nroacm,nrofor,nropro,nropme,nrofur,id_lugar,con_pago_ser_no,cod_abono_fac,cod_pago_act,cod_recaudo_copagos,cns_num_cuenta_cobro,cns_pag_cuenta_cobro,cod_recaudo_cuota_moderadora,HoraExpedicion,HoraInicioAtencion,IntervaloCitas,codurgencias,codambulatorias,codespecialidad,observaciones)
        Dim dtsaih_prm As New Datos.Datosaih_prm
        Return dtsaih_prm.InsertarRet(saih_prm)
    End Function

  Public Function AltaRetsaih_prm(
      ByVal saih_prm As Datos.saih_prm    
)
        Dim dtsaih_prm As New Datos.Datosaih_prm
        Return dtsaih_prm.InsertarRet(saih_prm)
    End Function

  Public Function Editasaih_prm(
      ByVal codpre As string,
      ByVal lugar As string,
      ByVal numhis As string,
      ByVal nropac As string,
      ByVal numfac As string,
      ByVal nrocta As string,
      ByVal nroord As string,
      ByVal nronci As string,
      ByVal nroglo As string,
      ByVal nroges As string,
      ByVal nrocit As string,
      ByVal norage As string,
      ByVal norres As string,
      ByVal nroepi As string,
      ByVal nroiqu As string,
      ByVal indhuella As integer,
      ByVal nrorem As string,
      ByVal nroacm As string,
      ByVal nrofor As string,
      ByVal nropro As string,
      ByVal nropme As string,
      ByVal nrofur As string,
      ByVal id_lugar As integer,
      ByVal con_pago_ser_no As string,
      ByVal cod_abono_fac As string,
      ByVal cod_pago_act As string,
      ByVal cod_recaudo_copagos As string,
      ByVal cns_num_cuenta_cobro As string,
      ByVal cns_pag_cuenta_cobro As string,
      ByVal cod_recaudo_cuota_moderadora As string,
      ByVal HoraExpedicion As integer,
      ByVal HoraInicioAtencion As string,
      ByVal IntervaloCitas As integer,
      ByVal codurgencias As string,
      ByVal codambulatorias As string,
      ByVal codespecialidad As string,
      ByVal observaciones As string
    )
      Dim saih_prm As New Datos.saih_prm(codpre,lugar,numhis,nropac,numfac,nrocta,nroord,nronci,nroglo,nroges,nrocit,norage,norres,nroepi,nroiqu,indhuella,nrorem,nroacm,nrofor,nropro,nropme,nrofur,id_lugar,con_pago_ser_no,cod_abono_fac,cod_pago_act,cod_recaudo_copagos,cns_num_cuenta_cobro,cns_pag_cuenta_cobro,cod_recaudo_cuota_moderadora,HoraExpedicion,HoraInicioAtencion,IntervaloCitas,codurgencias,codambulatorias,codespecialidad,observaciones)
        Dim dtsaih_prm As New Datos.Datosaih_prm
        Return dtsaih_prm.Editar(saih_prm)
    End Function

  Public Function Editasaih_prm(
      ByVal saih_prm As Datos.saih_prm    
)
        Dim dtsaih_prm As New Datos.Datosaih_prm
        Return dtsaih_prm.Editar(saih_prm)
    End Function

 Public Function Eliminasaih_prm(
      ByVal codpre As string)
        Dim dtsaih_prm As New Datos.Datosaih_prm
        Return dtsaih_prm.Borrar(codpre)
    End Function

  Public Function Obtenersaih_prmById(
      ByVal codpre As string) As Datos.saih_prm
        Dim saih_prm As Datos.saih_prm = Nothing
        Dim dtsaih_prm As New Datos.Datosaih_prm
        For Each saih_prm In dtsaih_prm.Select_ById_saih_prm(codpre)
        Next
        Return saih_prm
    End Function

  Public Function Obtenersaih_prm() As List(Of Datos.saih_prm)
        Dim dtsaih_prm As New Datos.Datosaih_prm
        Return dtsaih_prm.Select_saih_prm()
    End Function

  Public Function Obtenersaih_prmbyWhere(ByVal sWhere as String) As List(Of Datos.saih_prm)
        Dim dtsaih_prm As New Datos.Datosaih_prm
        Return dtsaih_prm.Select_Where_saih_prm(sWhere)
    End Function
End Class
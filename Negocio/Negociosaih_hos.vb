Public Class Negociosaih_hos

  Public Function Altasaih_hos(
      ByVal codent As string,
      ByVal tipdoc As string,
      ByVal nrodoc As string,
      ByVal nombre As string,
      ByVal direccion As string,
      ByVal telefono As string,
      ByVal logotipo As string,
      ByVal resdian As string,
      ByVal fecha_resolucion As string,
      ByVal num_inicial As integer,
      ByVal num_final As integer,
      ByVal num_factura As string,
      ByVal prefijo As string,
      ByVal regimen As string,
      ByVal gerente As string,
      ByVal subgerente As string,
      ByVal indtriage As integer,
      ByVal indimpformula As integer,
      ByVal indfechaglosa As integer
    )
      Dim saih_hos As New Datos.saih_hos(codent,tipdoc,nrodoc,nombre,direccion,telefono,logotipo,resdian,fecha_resolucion,num_inicial,num_final,num_factura,prefijo,regimen,gerente,subgerente,indtriage,indimpformula,indfechaglosa)
        Dim dtsaih_hos As New Datos.Datosaih_hos
        Return dtsaih_hos.Insertar(saih_hos)
    End Function

  Public Function AltaRetsaih_hos(
      ByVal codent As string,
      ByVal tipdoc As string,
      ByVal nrodoc As string,
      ByVal nombre As string,
      ByVal direccion As string,
      ByVal telefono As string,
      ByVal logotipo As string,
      ByVal resdian As string,
      ByVal fecha_resolucion As string,
      ByVal num_inicial As integer,
      ByVal num_final As integer,
      ByVal num_factura As string,
      ByVal prefijo As string,
      ByVal regimen As string,
      ByVal gerente As string,
      ByVal subgerente As string,
      ByVal indtriage As integer,
      ByVal indimpformula As integer,
      ByVal indfechaglosa As integer
    ) As Integer
      Dim saih_hos As New Datos.saih_hos(codent,tipdoc,nrodoc,nombre,direccion,telefono,logotipo,resdian,fecha_resolucion,num_inicial,num_final,num_factura,prefijo,regimen,gerente,subgerente,indtriage,indimpformula,indfechaglosa)
        Dim dtsaih_hos As New Datos.Datosaih_hos
        Return dtsaih_hos.InsertarRet(saih_hos)
    End Function

  Public Function Editasaih_hos(
      ByVal codent As string,
      ByVal tipdoc As string,
      ByVal nrodoc As string,
      ByVal nombre As string,
      ByVal direccion As string,
      ByVal telefono As string,
      ByVal logotipo As string,
      ByVal resdian As string,
      ByVal fecha_resolucion As string,
      ByVal num_inicial As integer,
      ByVal num_final As integer,
      ByVal num_factura As string,
      ByVal prefijo As string,
      ByVal regimen As string,
      ByVal gerente As string,
      ByVal subgerente As string,
      ByVal indtriage As integer,
      ByVal indimpformula As integer,
      ByVal indfechaglosa As integer
    )
      Dim saih_hos As New Datos.saih_hos(codent,tipdoc,nrodoc,nombre,direccion,telefono,logotipo,resdian,fecha_resolucion,num_inicial,num_final,num_factura,prefijo,regimen,gerente,subgerente,indtriage,indimpformula,indfechaglosa)
        Dim dtsaih_hos As New Datos.Datosaih_hos
        Return dtsaih_hos.Editar(saih_hos)
    End Function

 Public Function Eliminasaih_hos(
      ByVal codent As string)
        Dim dtsaih_hos As New Datos.Datosaih_hos
        Return dtsaih_hos.Borrar(codent)
    End Function

  Public Function Obtenersaih_hosById(
      ByVal codent As string) As Datos.saih_hos
        Dim saih_hos As Datos.saih_hos = Nothing
        Dim dtsaih_hos As New Datos.Datosaih_hos
        For Each saih_hos In dtsaih_hos.Select_ById_saih_hos(codent)
        Next
        Return saih_hos
    End Function

  Public Function Obtenersaih_hos() As List(Of Datos.saih_hos)
        Dim dtsaih_hos As New Datos.Datosaih_hos
        Return dtsaih_hos.Select_saih_hos()
    End Function

  Public Function Obtenersaih_hosbyWhere(ByVal sWhere as String) As List(Of Datos.saih_hos)
        Dim dtsaih_hos As New Datos.Datosaih_hos
        Return dtsaih_hos.Select_Where_saih_hos(sWhere)
    End Function
End Class
Public Class Negociosaih_tpr

  Public Function Altasaih_tpr(
      ByVal codpro As string,
      ByVal manual As string,
      ByVal id As integer,
      ByVal id_plan As string,
      ByVal codgruq As string,
      ByVal valor As decimal,
      ByVal valsmv As decimal,
      ByVal porincremento As decimal
    )
      Dim saih_tpr As New Datos.saih_tpr(codpro,manual,id,id_plan,codgruq,valor,valsmv,porincremento)
        Dim dtsaih_tpr As New Datos.Datosaih_tpr
        Return dtsaih_tpr.Insertar(saih_tpr)
    End Function

  Public Function AltaRetsaih_tpr(
      ByVal codpro As string,
      ByVal manual As string,
      ByVal id As integer,
      ByVal id_plan As string,
      ByVal codgruq As string,
      ByVal valor As decimal,
      ByVal valsmv As decimal,
      ByVal porincremento As decimal
    ) As Integer
      Dim saih_tpr As New Datos.saih_tpr(codpro,manual,id,id_plan,codgruq,valor,valsmv,porincremento)
        Dim dtsaih_tpr As New Datos.Datosaih_tpr
        Return dtsaih_tpr.InsertarRet(saih_tpr)
    End Function

  Public Function Editasaih_tpr(
      ByVal codpro As string,
      ByVal manual As string,
      ByVal id As integer,
      ByVal id_plan As string,
      ByVal codgruq As string,
      ByVal valor As decimal,
      ByVal valsmv As decimal,
      ByVal porincremento As decimal
    )
      Dim saih_tpr As New Datos.saih_tpr(codpro,manual,id,id_plan,codgruq,valor,valsmv,porincremento)
        Dim dtsaih_tpr As New Datos.Datosaih_tpr
        Return dtsaih_tpr.Editar(saih_tpr)
    End Function

 Public Function Eliminasaih_tpr(
      ByVal id As integer)
        Dim dtsaih_tpr As New Datos.Datosaih_tpr
        Return dtsaih_tpr.Borrar(id)
    End Function

  Public Function Obtenersaih_tprById(
      ByVal id As integer) As Datos.saih_tpr
        Dim saih_tpr As Datos.saih_tpr = Nothing
        Dim dtsaih_tpr As New Datos.Datosaih_tpr
        For Each saih_tpr In dtsaih_tpr.Select_ById_saih_tpr(id)
        Next
        Return saih_tpr
    End Function

  Public Function Obtenersaih_tpr() As List(Of Datos.saih_tpr)
        Dim dtsaih_tpr As New Datos.Datosaih_tpr
        Return dtsaih_tpr.Select_saih_tpr()
    End Function

  Public Function Obtenersaih_tprbyWhere(ByVal sWhere as String) As List(Of Datos.saih_tpr)
        Dim dtsaih_tpr As New Datos.Datosaih_tpr
        Return dtsaih_tpr.Select_Where_saih_tpr(sWhere)
    End Function
End Class
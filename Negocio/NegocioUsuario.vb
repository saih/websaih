Public Class NegocioUsuario

  Public Function AltaUsuario(
      ByVal Usuario As string,
      ByVal Password As string,
      ByVal Nombre1 As string,
      ByVal Nombre2 As string,
      ByVal Apellido1 As string,
      ByVal Apellido2 As string,
      ByVal Correo As string,
      ByVal Estado As integer,
      ByVal Identificacion As string,
      ByVal idperfil As string,
      ByVal razonsocial As string,
      ByVal cod_pas As string
    )
        Dim etUsuario As New Datos.Usuario(Usuario, Password, Nombre1, Nombre2, Apellido1, Apellido2, Correo, Estado, Identificacion, idperfil, razonsocial, cod_pas)
        Dim dtUsuario As New Datos.DatoUsuario
        Return dtUsuario.Insertar(etUsuario)
    End Function

  Public Function AltaUsuario(
      ByVal Usuario As Datos.Usuario    
)
        Dim dtUsuario As New Datos.DatoUsuario
        Return dtUsuario.Insertar(Usuario)
    End Function

  Public Function AltaRetUsuario(
      ByVal Usuario As string,
      ByVal Password As string,
      ByVal Nombre1 As string,
      ByVal Nombre2 As string,
      ByVal Apellido1 As string,
      ByVal Apellido2 As string,
      ByVal Correo As string,
      ByVal Estado As integer,
      ByVal Identificacion As string,
      ByVal idperfil As string,
      ByVal razonsocial As string,
      ByVal cod_pas As string
    ) As Integer
        Dim etUsuario As New Datos.Usuario(Usuario, Password, Nombre1, Nombre2, Apellido1, Apellido2, Correo, Estado, Identificacion, idperfil, razonsocial, cod_pas)
        Dim dtUsuario As New Datos.DatoUsuario
        Return dtUsuario.InsertarRet(etUsuario)
    End Function

  Public Function AltaRetUsuario(
      ByVal Usuario As Datos.Usuario    
)
        Dim dtUsuario As New Datos.DatoUsuario
        Return dtUsuario.InsertarRet(Usuario)
    End Function

  Public Function EditaUsuario(
      ByVal Usuario As string,
      ByVal Password As string,
      ByVal Nombre1 As string,
      ByVal Nombre2 As string,
      ByVal Apellido1 As string,
      ByVal Apellido2 As string,
      ByVal Correo As string,
      ByVal Estado As integer,
      ByVal Identificacion As string,
      ByVal idperfil As string,
      ByVal razonsocial As string,
      ByVal cod_pas As string
    )
        Dim etUsuario As New Datos.Usuario(Usuario, Password, Nombre1, Nombre2, Apellido1, Apellido2, Correo, Estado, Identificacion, idperfil, razonsocial, cod_pas)
        Dim dtUsuario As New Datos.DatoUsuario
        Return dtUsuario.Editar(etUsuario)
    End Function

  Public Function EditaUsuario(
      ByVal Usuario As Datos.Usuario    
)
        Dim dtUsuario As New Datos.DatoUsuario
        Return dtUsuario.Editar(Usuario)
    End Function

 Public Function EliminaUsuario(
      ByVal Usuario As string)
        Dim dtUsuario As New Datos.DatoUsuario
        Return dtUsuario.Borrar(Usuario)
    End Function

  Public Function ObtenerUsuarioById(
      ByVal Usuario As string) As Datos.Usuario
        Dim etUsuario As Datos.Usuario = Nothing
        Dim dtUsuario As New Datos.DatoUsuario
        For Each etUsuario In dtUsuario.Select_ById_Usuario(Usuario)
        Next
        Return etUsuario
    End Function

  Public Function ObtenerUsuario() As List(Of Datos.Usuario)
        Dim dtUsuario As New Datos.DatoUsuario
        Return dtUsuario.Select_Usuario()
    End Function

  Public Function ObtenerUsuariobyWhere(ByVal sWhere as String) As List(Of Datos.Usuario)
        Dim dtUsuario As New Datos.DatoUsuario
        Return dtUsuario.Select_Where_Usuario(sWhere)
    End Function
End Class
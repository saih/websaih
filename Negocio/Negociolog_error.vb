Public Class Negociolog_error

  Public Function Altalog_error(
      ByVal id As integer,
      ByVal tipo As string,
      ByVal descripcion As string,
      ByVal fecha As string,
      ByVal usuario As string
    )
      Dim log_error As New Datos.log_error(id,tipo,descripcion,fecha,usuario)
        Dim dtlog_error As New Datos.Datolog_error
        Return dtlog_error.Insertar(log_error)
    End Function

  Public Function Altalog_error(
      ByVal log_error As Datos.log_error    
)
        Dim dtlog_error As New Datos.Datolog_error
        Return dtlog_error.Insertar(log_error)
    End Function

  Public Function AltaRetlog_error(
      ByVal id As integer,
      ByVal tipo As string,
      ByVal descripcion As string,
      ByVal fecha As string,
      ByVal usuario As string
    ) As Integer
      Dim log_error As New Datos.log_error(id,tipo,descripcion,fecha,usuario)
        Dim dtlog_error As New Datos.Datolog_error
        Return dtlog_error.InsertarRet(log_error)
    End Function

  Public Function AltaRetlog_error(
      ByVal log_error As Datos.log_error    
)
        Dim dtlog_error As New Datos.Datolog_error
        Return dtlog_error.InsertarRet(log_error)
    End Function

  Public Function Editalog_error(
      ByVal id As integer,
      ByVal tipo As string,
      ByVal descripcion As string,
      ByVal fecha As string,
      ByVal usuario As string
    )
      Dim log_error As New Datos.log_error(id,tipo,descripcion,fecha,usuario)
        Dim dtlog_error As New Datos.Datolog_error
        Return dtlog_error.Editar(log_error)
    End Function

  Public Function Editalog_error(
      ByVal log_error As Datos.log_error    
)
        Dim dtlog_error As New Datos.Datolog_error
        Return dtlog_error.Editar(log_error)
    End Function

 Public Function Eliminalog_error(
      ByVal id As integer)
        Dim dtlog_error As New Datos.Datolog_error
        Return dtlog_error.Borrar(id)
    End Function

  Public Function Obtenerlog_errorById(
      ByVal id As integer) As Datos.log_error
        Dim log_error As Datos.log_error = Nothing
        Dim dtlog_error As New Datos.Datolog_error
        For Each log_error In dtlog_error.Select_ById_log_error(id)
        Next
        Return log_error
    End Function

  Public Function Obtenerlog_error() As List(Of Datos.log_error)
        Dim dtlog_error As New Datos.Datolog_error
        Return dtlog_error.Select_log_error()
    End Function

  Public Function Obtenerlog_errorbyWhere(ByVal sWhere as String) As List(Of Datos.log_error)
        Dim dtlog_error As New Datos.Datolog_error
        Return dtlog_error.Select_Where_log_error(sWhere)
    End Function
End Class
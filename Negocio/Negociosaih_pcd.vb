Public Class Negociosaih_pcd

  Public Function Altasaih_pcd(
      ByVal manual As string,
      ByVal codigo As string,
      ByVal nombre As string,
      ByVal ncomplejidad As string,
      ByVal cencosto As string,
      ByVal aplsex As string,
      ByVal apledadinicio As integer,
      ByVal apledadfin As integer,
      ByVal codgrupoq As string,
      ByVal valor As decimal,
      ByVal valsmv As decimal,
      ByVal porincremento As decimal,
      ByVal cobhonmed As integer,
      ByVal cobdersal As integer,
      ByVal cobmatins As integer,
      ByVal cobhonayu As integer,
      ByVal cobhonanes As integer,
      ByVal conhonper As integer,
      ByVal codunif As string,
      ByVal codfp As string,
      ByVal observaciones As string,
      ByVal codser As string
    )
      Dim saih_pcd As New Datos.saih_pcd(manual,codigo,nombre,ncomplejidad,cencosto,aplsex,apledadinicio,apledadfin,codgrupoq,valor,valsmv,porincremento,cobhonmed,cobdersal,cobmatins,cobhonayu,cobhonanes,conhonper,codunif,codfp,observaciones,codser)
        Dim dtsaih_pcd As New Datos.Datosaih_pcd
        Return dtsaih_pcd.Insertar(saih_pcd)
    End Function

  Public Function AltaRetsaih_pcd(
      ByVal manual As string,
      ByVal codigo As string,
      ByVal nombre As string,
      ByVal ncomplejidad As string,
      ByVal cencosto As string,
      ByVal aplsex As string,
      ByVal apledadinicio As integer,
      ByVal apledadfin As integer,
      ByVal codgrupoq As string,
      ByVal valor As decimal,
      ByVal valsmv As decimal,
      ByVal porincremento As decimal,
      ByVal cobhonmed As integer,
      ByVal cobdersal As integer,
      ByVal cobmatins As integer,
      ByVal cobhonayu As integer,
      ByVal cobhonanes As integer,
      ByVal conhonper As integer,
      ByVal codunif As string,
      ByVal codfp As string,
      ByVal observaciones As string,
      ByVal codser As string
    ) As Integer
      Dim saih_pcd As New Datos.saih_pcd(manual,codigo,nombre,ncomplejidad,cencosto,aplsex,apledadinicio,apledadfin,codgrupoq,valor,valsmv,porincremento,cobhonmed,cobdersal,cobmatins,cobhonayu,cobhonanes,conhonper,codunif,codfp,observaciones,codser)
        Dim dtsaih_pcd As New Datos.Datosaih_pcd
        Return dtsaih_pcd.InsertarRet(saih_pcd)
    End Function

  Public Function Editasaih_pcd(
      ByVal manual As string,
      ByVal codigo As string,
      ByVal nombre As string,
      ByVal ncomplejidad As string,
      ByVal cencosto As string,
      ByVal aplsex As string,
      ByVal apledadinicio As integer,
      ByVal apledadfin As integer,
      ByVal codgrupoq As string,
      ByVal valor As decimal,
      ByVal valsmv As decimal,
      ByVal porincremento As decimal,
      ByVal cobhonmed As integer,
      ByVal cobdersal As integer,
      ByVal cobmatins As integer,
      ByVal cobhonayu As integer,
      ByVal cobhonanes As integer,
      ByVal conhonper As integer,
      ByVal codunif As string,
      ByVal codfp As string,
      ByVal observaciones As string,
      ByVal codser As string
    )
      Dim saih_pcd As New Datos.saih_pcd(manual,codigo,nombre,ncomplejidad,cencosto,aplsex,apledadinicio,apledadfin,codgrupoq,valor,valsmv,porincremento,cobhonmed,cobdersal,cobmatins,cobhonayu,cobhonanes,conhonper,codunif,codfp,observaciones,codser)
        Dim dtsaih_pcd As New Datos.Datosaih_pcd
        Return dtsaih_pcd.Editar(saih_pcd)
    End Function

 Public Function Eliminasaih_pcd(
      ByVal manual As string,
      ByVal codigo As string)
        Dim dtsaih_pcd As New Datos.Datosaih_pcd
        Return dtsaih_pcd.Borrar(manual,codigo)
    End Function

  Public Function Obtenersaih_pcdById(
      ByVal manual As string,
      ByVal codigo As string) As Datos.saih_pcd
        Dim saih_pcd As Datos.saih_pcd = Nothing
        Dim dtsaih_pcd As New Datos.Datosaih_pcd
        For Each saih_pcd In dtsaih_pcd.Select_ById_saih_pcd(manual,codigo)
        Next
        Return saih_pcd
    End Function

  Public Function Obtenersaih_pcd() As List(Of Datos.saih_pcd)
        Dim dtsaih_pcd As New Datos.Datosaih_pcd
        Return dtsaih_pcd.Select_saih_pcd()
    End Function

  Public Function Obtenersaih_pcdbyWhere(ByVal sWhere as String) As List(Of Datos.saih_pcd)
        Dim dtsaih_pcd As New Datos.Datosaih_pcd
        Return dtsaih_pcd.Select_Where_saih_pcd(sWhere)
    End Function

    Public Function Obtenersaih_pcdByCodigo(
      ByVal codigo As String) As Datos.saih_pcd
        Dim saih_pcd As Datos.saih_pcd = Nothing
        Dim dtsaih_pcd As New Datos.Datosaih_pcd
        For Each saih_pcd In dtsaih_pcd.Select_Where_saih_pcd(" where codigo = '" & codigo & "'")
        Next
        Return saih_pcd
    End Function
End Class
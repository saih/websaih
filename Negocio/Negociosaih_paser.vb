Public Class Negociosaih_paser

  Public Function Altasaih_paser(
      ByVal codpas As string,
      ByVal codser As string
    )
      Dim saih_paser As New Datos.saih_paser(codpas,codser)
        Dim dtsaih_paser As New Datos.Datosaih_paser
        Return dtsaih_paser.Insertar(saih_paser)
    End Function

  Public Function AltaRetsaih_paser(
      ByVal codpas As string,
      ByVal codser As string
    ) As Integer
      Dim saih_paser As New Datos.saih_paser(codpas,codser)
        Dim dtsaih_paser As New Datos.Datosaih_paser
        Return dtsaih_paser.InsertarRet(saih_paser)
    End Function

  Public Function Editasaih_paser(
      ByVal codpas As string,
      ByVal codser As string
    )
      Dim saih_paser As New Datos.saih_paser(codpas,codser)
        Dim dtsaih_paser As New Datos.Datosaih_paser
        Return dtsaih_paser.Editar(saih_paser)
    End Function

 Public Function Eliminasaih_paser(
      ByVal codpas As string,
      ByVal codser As string)
        Dim dtsaih_paser As New Datos.Datosaih_paser
        Return dtsaih_paser.Borrar(codpas,codser)
    End Function

  Public Function Obtenersaih_paserById(
      ByVal codpas As string,
      ByVal codser As string) As Datos.saih_paser
        Dim saih_paser As Datos.saih_paser = Nothing
        Dim dtsaih_paser As New Datos.Datosaih_paser
        For Each saih_paser In dtsaih_paser.Select_ById_saih_paser(codpas,codser)
        Next
        Return saih_paser
    End Function

  Public Function Obtenersaih_paser() As List(Of Datos.saih_paser)
        Dim dtsaih_paser As New Datos.Datosaih_paser
        Return dtsaih_paser.Select_saih_paser()
    End Function

  Public Function Obtenersaih_paserbyWhere(ByVal sWhere as String) As List(Of Datos.saih_paser)
        Dim dtsaih_paser As New Datos.Datosaih_paser
        Return dtsaih_paser.Select_Where_saih_paser(sWhere)
    End Function
End Class